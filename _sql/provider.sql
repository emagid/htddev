-- =============================================
-- Author     :	Tulio Moraes
-- Create date: 07/24/2015
-- Update date: 08/14/2015
-- Description:	Create the basic providers table
-- =============================================

CREATE TABLE public.provider (
	id SERIAL PRIMARY KEY,
	active SMALLINT DEFAULT 1 NOT NULL,
	insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
	email VARCHAR NOT NULL,
	password VARCHAR,
	hash VARCHAR,
	first_name VARCHAR,
	last_name VARCHAR,
	zip NUMERIC,
	phone NUMERIC,
	photo VARCHAR,
	description VARCHAR,
	teaching_description VARCHAR,
	dob TIMESTAMP,
	experience INTEGER,
	key VARCHAR,
	step_2_completed BOOLEAN,
	authorized BOOLEAN,
	promise_statement VARCHAR,
	certificates VARCHAR,
	testimonial_1 VARCHAR,
	testimonial_1_reference VARCHAR,
	testimonial_2 VARCHAR,
	testimonial_2_reference VARCHAR,
	testimonial_3 VARCHAR,
	testimonial_3_reference VARCHAR,
	lifestyle VARCHAR,
	license_photo VARCHAR,
	extra_photo_1 VARCHAR,
	extra_photo_2 VARCHAR,
	agreed_manifesto TIMESTAMP WITHOUT TIME ZONE,
	agreed_ethics TIMESTAMP WITHOUT TIME ZONE,
	agreed_terms TIMESTAMP WITHOUT TIME ZONE,
	agreed_privacy TIMESTAMP WITHOUT TIME ZONE
) WITH (oids = false);