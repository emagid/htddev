-- =============================================
-- Author     :	Tulio Moraes
-- Create date: 07/23/2015
-- Update date: 07/23/2015
-- Description:	Create the basic configurations table
-- =============================================

CREATE TABLE public.config (
	id SERIAL PRIMARY KEY,
	active SMALLINT DEFAULT 1,
	insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
	name VARCHAR,
	value VARCHAR
) WITH (oids = false);

INSERT INTO config (id, name) values (1, 'Meta Title');
INSERT INTO config (id, name) values (2, 'Meta Description');
INSERT INTO config (id, name) values (3, 'Meta Keywords');