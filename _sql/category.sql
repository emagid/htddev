-- =============================================
-- Author     :	Segev {CJ} Shmueli
-- Create date: 05/04/2015
-- Update date: 07/02/2015 - Tulio Moraes
-- Description:	Create the basic categories
-- =============================================

CREATE TABLE public.category (
	id SERIAL PRIMARY KEY,
	active SMALLINT DEFAULT 1,
	insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
	name VARCHAR,
	subtitle VARCHAR,
	parent_category INTEGER,
	description VARCHAR,
	slug VARCHAR,
	meta_title VARCHAR,
	meta_keywords VARCHAR,
	meta_description VARCHAR,
	display_order INTEGER
) WITH (oids = false);