-- =============================================
-- Author     : Tulio Moraes
-- Create date: 06/11/2015
-- Update date: 06/11/2015
-- Description: Create the roles
-- =============================================

CREATE TABLE public.role (
  id SERIAL,
  active SMALLINT DEFAULT 1 NOT NULL,
  insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  name VARCHAR NOT NULL,
  CONSTRAINT role_pkey PRIMARY KEY(id)
) 
WITH (oids = false);

CREATE TABLE public.admin_roles (
  id SERIAL,
  active SMALLINT DEFAULT 1 NOT NULL,
  insert_time TIMESTAMP DEFAULT now() NOT NULL,
  admin_id INTEGER NOT NULL,
  role_id INTEGER NOT NULL,
  PRIMARY KEY(id)
);

INSERT INTO role (id, name) VALUES (1, 'admin');

CREATE TABLE public.user_roles (
  id SERIAL,
  active SMALLINT DEFAULT 1 NOT NULL,
  insert_time TIMESTAMP DEFAULT now() NOT NULL,
  user_id INTEGER NOT NULL,
  role_id INTEGER NOT NULL,
  PRIMARY KEY(id)
);

INSERT INTO role (id, name) VALUES (2, 'customer');

CREATE TABLE public.provider_roles (
  id SERIAL,
  active SMALLINT DEFAULT 1 NOT NULL,
  insert_time TIMESTAMP DEFAULT now() NOT NULL,
  provider_id INTEGER NOT NULL,
  role_id INTEGER NOT NULL,
  PRIMARY KEY(id)
);

INSERT INTO role (id, name) VALUES (3, 'provider');