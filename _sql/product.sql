-- =============================================
-- Author     :	Tulio Moraes
-- Create date: 08/03/2015
-- Update date: 08/03/2015
-- Description:	Create the basic products table
-- =============================================

CREATE TABLE public.product (
	id SERIAL PRIMARY KEY,
	active SMALLINT DEFAULT 1,
	insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
	name VARCHAR,
	sku VARCHAR,
	price NUMERIC,
	list_price NUMERIC,
	summary VARCHAR,
	description VARCHAR,
	brand_id VARCHAR,
	manufacturer VARCHAR,
	mpn VARCHAR,
	gender_id VARCHAR,
	wholesale_price NUMERIC,
	msrp NUMERIC,
	quantity VARCHAR,
	slug VARCHAR,
	tags VARCHAR,
	meta_title VARCHAR,
	meta_keywords VARCHAR,
	meta_description VARCHAR,
	featured_image VARCHAR,
	colors VARCHAR,
	featured SMALLINT,
	length VARCHAR,
	width VARCHAR,
	height VARCHAR,
	video_url VARCHAR,
	video_thumbnail VARCHAR,
  	google_product_category VARCHAR,
  	availability VARCHAR,
  	sizes VARCHAR
) WITH (oids = false);