-- =============================================
-- Author     :	Tulio Moraes
-- Create date: 07/06/2015
-- Update date: 07/06/2015
-- Description:	Create the database for the emailing list
-- =============================================

CREATE TABLE public.email_list (
  	id SERIAL PRIMARY KEY,
  	active SMALLINT DEFAULT 1,
  	insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
  	email VARCHAR
) 
WITH (oids = false);