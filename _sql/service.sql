-- =============================================
-- Author     :	John
-- Create date: 08/13/2015
-- Update date: 08/14/2015 - Tulio Moraes
-- Description:	Create the table to store services
-- =============================================

CREATE TABLE public.service (
	id SERIAL PRIMARY KEY,
	active SMALLINT DEFAULT 1 NOT NULL,
	insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
	name VARCHAR,
	description VARCHAR,
	slogan VARCHAR,
	photo VARCHAR,
	slug VARCHAR,
	meta_title VARCHAR,
	meta_keywords VARCHAR,
	meta_description VARCHAR,
	display_order NUMERIC,
	featured_image VARCHAR
) WITH (oids = false);