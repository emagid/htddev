-- =============================================
-- Author     :	Tulio Moraes
-- Create date: 05/27/2015
-- Update date: 05/27/2015
-- Description:	Create the relations table between product and categories
-- =============================================

CREATE TABLE public.product_categories (
	id SERIAL PRIMARY KEY,
	active SMALLINT DEFAULT 1,
	insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
	product_id INTEGER,
	category_id INTEGER
) WITH (oids = false);