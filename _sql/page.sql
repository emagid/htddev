-- =============================================
-- Author     :	Tulio Moraes
-- Create date: 05/28/2015
-- Update date: 05/28/2015
-- Description:	Create the pages table
-- =============================================

CREATE TABLE public.page (
  	id SERIAL,
  	active SMALLINT DEFAULT 1,
  	insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
	title VARCHAR, 
	slug VARCHAR,
	description VARCHAR, 
	featured_image VARCHAR,
	meta_title VARCHAR, 
	meta_keywords VARCHAR, 
	meta_description VARCHAR,
	date_modified VARCHAR,
  	CONSTRAINT page_pkey PRIMARY KEY(id)
) 
WITH (oids = false);