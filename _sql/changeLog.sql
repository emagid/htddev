-- Aug 17

CREATE SEQUENCE public.office_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE SEQUENCE public.practice_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.office
(
  id integer NOT NULL,
  insert_time timestamp with time zone DEFAULT now(),
  active integer DEFAULT 1,
  phone character varying,
  address character varying,
  address2 character varying,
  city character varying,
  zip character varying,
  label character varying,
  practice_id integer,
  email character varying,
  state character varying,
  CONSTRAINT office_pkey PRIMARY KEY (id)
);

CREATE TABLE public.practice
(
  id integer NOT NULL DEFAULT nextval('practice_id_seq'::regclass),
  insert_time timestamp without time zone DEFAULT now(),
  active integer DEFAULT 1,
  name character varying,
  slug character varying,
  CONSTRAINT practice_pkey PRIMARY KEY (id)
);

-- Aug 18

ALTER TABLE public.practice ADD COLUMN original_name character varying;



-- June 6, 2018

CREATE SEQUENCE public.home_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.home
(
  id integer NOT NULL DEFAULT nextval('home_id_seq'::regclass),
  insert_time timestamp with time zone DEFAULT now(),
  active integer DEFAULT 1,
  main_title character varying,
  section character varying,
  content character varying,
  CONSTRAINT home_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE public.media_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.media
(
  id integer NOT NULL DEFAULT nextval('media_id_seq'::regclass),
  insert_time timestamp with time zone DEFAULT now(),
  active integer DEFAULT 1,
  name character varying,
  logo character varying,
  url character varying,
  CONSTRAINT media_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE public.quotes_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.quotes
(
  id integer NOT NULL DEFAULT nextval('quotes_id_seq'::regclass),
  insert_time timestamp with time zone DEFAULT now(),
  active integer DEFAULT 1,
  quote character varying,
  by_name character varying,
  CONSTRAINT quotes_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE public.faq_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.faq
(
  id integer NOT NULL DEFAULT nextval('faq_id_seq'::regclass),
  insert_time timestamp with time zone DEFAULT now(),
  active integer DEFAULT 1,
  question character varying,
  answer character varying,
  display_order integer,
  CONSTRAINT faq_pkey PRIMARY KEY (id)
);

ALTER TABLE contact RENAME COLUMN name TO first_name;
ALTER TABLE contact RENAME COLUMN email TO last_name;
ALTER TABLE contact RENAME COLUMN message TO email;
ALTER TABLE contact ADD COLUMN company character varying;
ALTER TABLE contact ADD COLUMN message character varying;
ALTER TABLE contact ALTER COLUMN email TYPE character varying;
ALTER TABLE banner RENAME COLUMN link TO main_description;
ALTER TABLE banner RENAME COLUMN date_modified TO page;
ALTER TABLE banner RENAME COLUMN featured_id TO banner_type_id;

CREATE SEQUENCE public.reviews_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.reviews
(
  id integer NOT NULL DEFAULT nextval('reviews_id_seq'::regclass),
  insert_time timestamp with time zone DEFAULT now(),
  active integer DEFAULT 1,
  name character varying,
  review_comment character varying,
  CONSTRAINT reviews_pkey PRIMARY KEY (id)
);

-- June 7, 2018
ALTER TABLE home ADD COLUMN featured_image1 character varying;
ALTER TABLE home ADD COLUMN featured_image2 character varying;
ALTER TABLE home ADD COLUMN featured_image3 character varying;

--June 8, 2018

CREATE SEQUENCE public.team_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.team
(
  id integer NOT NULL DEFAULT nextval('team_id_seq'::regclass),
  insert_time timestamp with time zone DEFAULT now(),
  active integer DEFAULT 1,
  name character varying,
  position character varying,
  image character varying,
  description character varying,
  CONSTRAINT team_pkey PRIMARY KEY (id)
);

-- June 20. 2018
CREATE SEQUENCE public.privacy_policy_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.privacy_policy
(
  id integer NOT NULL DEFAULT nextval('privacy_policy_id_seq'::regclass),
  insert_time timestamp with time zone DEFAULT now(),
  active integer DEFAULT 1,
  content character varying,
  CONSTRAINT team_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE public.hipaa_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.hipaa
(
  id integer NOT NULL DEFAULT nextval('hipaa_id_seq'::regclass),
  insert_time timestamp with time zone DEFAULT now(),
  active integer DEFAULT 1,
  content character varying,
  CONSTRAINT hipaa_pkey PRIMARY KEY (id)
);


-- Jun 26

ALTER TABLE contact ADD COLUMN first_name CHARACTER VARYING;
ALTER TABLE contact ADD COLUMN last_name CHARACTER VARYING;
ALTER TABLE contact ADD COLUMN company CHARACTER VARYING;

-- Septem 25, 2018

ALTER TABLE transaction ADD COLUMN ship_first_name CHARACTER VARYING;
ALTER TABLE transaction ADD COLUMN ship_last_name CHARACTER VARYING;
ALTER TABLE transaction ADD COLUMN email CHARACTER VARYING;
ALTER TABLE transaction ADD COLUMN phone CHARACTER VARYING;
ALTER TABLE transaction ADD COLUMN amount decimal;
ALTER TABLE transaction ADD COLUMN transaction_date timestamp without time zone;
ALTER TABLE transaction ADD COLUMN payment_status CHARACTER VARYING;
ALTER TABLE transaction ADD COLUMN type CHARACTER VARYING;
ALTER TABLE transaction ADD COLUMN ship_address CHARACTER VARYING;
ALTER TABLE transaction ADD COLUMN ship_address2 CHARACTER VARYING;
ALTER TABLE transaction ADD COLUMN ship_city CHARACTER VARYING;
ALTER TABLE transaction ADD COLUMN ship_state CHARACTER VARYING;
ALTER TABLE transaction ADD COLUMN ship_country CHARACTER VARYING;
ALTER TABLE transaction ADD COLUMN ship_zip CHARACTER VARYING;
ALTER TABLE transaction ADD COLUMN slug CHARACTER VARYING;

--September 26, 2018

ALTER TABLE transaction ADD COLUMN ref_num CHARACTER VARYING;
ALTER TABLE transaction ADD COLUMN ref_tran_id CHARACTER VARYING;