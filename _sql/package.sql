-- =============================================
-- Author     :	Tulio Moraes
-- Create date: 08/19/2015
-- Update date: 08/19/2015
-- Description:	Create the basic packages table
-- =============================================

CREATE TABLE public.package (
	id SERIAL PRIMARY KEY,
	active SMALLINT DEFAULT 1,
	insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
	provider_id NUMERIC,
	title VARCHAR,
	description_1 VARCHAR,
	description_2 VARCHAR,
	description_3 VARCHAR,
	image VARCHAR
) WITH (oids = false);