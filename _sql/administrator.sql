-- =============================================
-- Author     :	Tulio Moraes
-- Create date: 05/28/2015
-- Update date: 05/28/2015
-- Description:	Create the administrators
-- =============================================

CREATE TABLE public.administrator (
  	id SERIAL,
  	active SMALLINT DEFAULT 1,
  	insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
  	first_name VARCHAR,
  	last_name VARCHAR,
  	email VARCHAR,
  	username VARCHAR,
  	password VARCHAR,
  	hash VARCHAR,
  	permissions VARCHAR,
  	signup_ip VARCHAR,
  	update_time VARCHAR,
  	CONSTRAINT administrator_pkey PRIMARY KEY(id)
) 
WITH (oids = false);