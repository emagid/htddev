-- =============================================
-- Author     :	John
-- Create date: 08/13/2015
-- Update date: 08/14/2015 - Tulio Moraes
-- Description:	Create the table to link providers and services
-- =============================================

CREATE TABLE public.provider_services (
	id SERIAL PRIMARY KEY,
	active SMALLINT DEFAULT 1 NOT NULL,
	insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
	provider_id INTEGER,
	service_id INTEGER
) WITH (oids = false);