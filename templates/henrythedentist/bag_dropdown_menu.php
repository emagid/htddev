<div class="shopping_bag_menu_wrapper">
    <li>
        <a class="header_link bag_link_active" href="/checkout" id="bag-header">
            <p id="bag-count">Shopping Bag (<?=count($model->cart->products)?>)</p>
            <icon></icon>
        </a>
    </li>
    <div class="shopping_bag_menu_box" id="shopping_bag_menu">
        <div class="hide_shopping_bag_inner_btn">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="32" height="32" viewBox="0 0 32 32">
                <path class="modal_svg_path"
                      d="M31.158 32.118c-.246 0-.492-.093-.678-.282L.163 1.52c-.375-.375-.375-.983 0-1.358s.983-.375 1.358 0l30.317 30.316c.375.375.375.983 0 1.358-.187.188-.433.282-.678.282zM.842 32.118c-.246 0-.492-.093-.678-.282-.375-.375-.375-.983 0-1.358L30.48.162c.375-.375.983-.375 1.358 0s.375.983 0 1.358L1.52 31.836c-.186.188-.432.282-.677.282z"
                      fill="#fff"></path>
            </svg>
        </div>
        <div id="shopping-lists" style="<?php if(count($model->cart->products) == 0){ echo 'display:none';}?>" >
            <h2><?=count($model->cart->products)?> Items in your Bag</h2>
            <h1>Subtotal: $<?=number_format($model->cart->total,1)?></h1>
            <span class="horiz_bag_separator"></span>
            <ul class="bag_menu_items_list">
            <?php foreach ($model->cart->products as $product) { ?>
                    <li class="bag_menu_list_item">
                        <div class=""
                             style="background-image:url(<?=$product->product->featuredImage(); ?>);background-repeat:no-repeat;">
                        </div>
                        <p><?= $product->product->name ?></p>
                        <span>$<?= number_format(\Model\Product::getPriceByProducts([$product]), 2) ?></span>
                    </li>
            <?php } ?>
            </ul>
            <p>Tax &#x26; shipping costs will be applied during check out.</p>
            <a href="/checkout" class="checkout_btn">Proceed to Check Out</a>
        </div>
        <div style="<?php if(count($model->cart->products) > 0){ echo 'display:none';}?>">
            <h1 class="cart_empty_message">Your bag is empty, <span>Continue Shopping</span><br><a
                    href="<?= SITE_URL ?>diamonds/search">Find Diamonds</a></h1>
        </div>

    </div>


</div>
