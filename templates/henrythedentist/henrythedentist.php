<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	    <meta name = "apple-mobile-web-app-capable" content = "yes" />
		<meta name="google-site-verification" content="KJ5zxDYTDtIcIxId3dCgozvL1KynRVcy7wtd8IyB0RA" />
	    <title><?= $this->configs['Meta Title']; ?></title>
	    <meta name="Description" content="<?= $this->configs['Meta Description']; ?>">
	    <meta name="Keywords" content="<?= $this->configs['Meta Keywords']; ?>">

	    <meta property="og:title" content="<?=SITE_NAME?>" />
	    <meta property="og:type" content="website" /> 
	    <meta property="og:url" content="<?=SITE_URL?>" />
        
        <meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
        <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">
<!--
        <div itemscope itemtype="http://schema.org/Dentist">
<span itemprop="name">HENRY the Dentist</span>
<span itemprop="streetAddress">50 E Mount Pleasant Ave</span>
<span itemprop="addressLocality">Livingston</span>
<span itemprop="adressRegion">NJ</span>
<span itemporp="postalCode">07039</span>
</div>
-->
 
	    <? require_once('includes.php'); ?>
	</head>

	<body class="<?=$this->emagid->route['controller']?>_<?=$this->emagid->route['action']?>">
		<?display_notification();?>

	    <? require_once('promotional_banner.php'); ?>
		<? require_once('header.php'); ?>

		<div class = "modal_overlay"></div>
		<div id="primary_page_controller_wrapper" class="blur_on_overlay">
			<?php $emagid->controller->renderBody($model);?>
            <? require_once("footer.php") ?>
	    </div>

	    <div class='scroller'>
		    <iframe class='mobile_frame' id='book2' scrolling='no' style='display: none;' src=""></iframe>
	    </div>
        <div class="mobile_hide hide_register_overlay hide_modal_overlay">
	        <svg class="modal_svg" xmlns="http://www.w3.org/2000/svg" version="1.1" width="32" height="32" viewBox="0 0 32 32">
	            <path class="modal_svg_path" d="M31.158 32.118c-.246 0-.492-.093-.678-.282L.163 1.52c-.375-.375-.375-.983 0-1.358s.983-.375 1.358 0l30.317 30.316c.375.375.375.983 0 1.358-.187.188-.433.282-.678.282zM.842 32.118c-.246 0-.492-.093-.678-.282-.375-.375-.375-.983 0-1.358L30.48.162c.375-.375.983-.375 1.358 0s.375.983 0 1.358L1.52 31.836c-.186.188-.432.282-.677.282z" fill="#fff"></path>
	        </svg>
	    </div>
	    <div class='mobile_frame_background'></div>


	    <? require_once('booking.php'); ?>
			<script type='text/javascript'>
				$('#book_btn').click(function(e){
					e.preventDefault();
					window.scrollTo(0, 0);
					$('.scroller').css('z-index', '999');

		            if ( $('.mobile_show').is(':visible') ) {
		                $('.mobile_frame_background, .mobile_hide, #book2').fadeIn();
		                $('.mobile_hide').css('display', 'flex');
		                $('.global_header').removeClass('blur_on_overlay_active');
		                $('#primary_page_controller_wrapper').fadeOut();
		            } else {
				        $('.blur_on_overlay').removeClass('blur_on_overlay_active');
				        
				        // $("body").addClass("no_scroll");
				        $('.iframe.book_frame').slideDown();
				        $('.iframe.book_frame').css('display', 'flex');
		            }
		        });




				(function() {
					$.post( "/info", function( data ) {
						if(data.status){
							$("[name=office_states]").html(data.states);
							$("[id=office_id]").html(data.office_ids);
						}

						$('[name=office_states]').change(function(){
							$('option.office_loc').hide();
							$('option.office_loc[data-state="'+this.value+'"]').show();
							$('[name=office_states]').not(this).val('');
							$('select[id=office_id]').val('').change();
						}).change();

						$('select[id=office_id]').change(function(){
							if(this.value != '') {
								$('#book').attr('src','https://www.appointnow.com/?P=3066&O='+this.value+'&PT=0');
								$('#book2').attr('src','https://www.appointnow.com/?P=3066&O='+this.value+'&PT=0');

								$('#register_frame').attr('src','https://patientregistration.denticon.com/?P=3066&O='+this.value+'&PT=0');
								$(this).parents('.signin_box').find('a.book_appointment_btn, a.reg_patient_btn').show();
							} else {
								$(this).parents('.signin_box').find('a.book_appointment_btn, a.reg_patient_btn').hide();
							}
						}).change();
					});




					// $('a.book_appointment_btn, a.reg_patient_btn').click(function(){
					// 	window.open($(this).attr('href'), '_blank');
					// });
				})();
			</script>
			<script type='text/javascript'>
				window.__lo_site_id = 69667;

				(function() {
					var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
					wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
				})();
			</script>
		<!-- begin olark code -->
<!--
		<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
				f[z]=function(){
					(a.s=a.s||[]).push(arguments)};var a=f[z]._={
				},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
					f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
					0:+new Date};a.P=function(u){
					a.p[u]=new Date-a.p[0]};function s(){
					a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
					hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
					return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
					b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
					b.contentWindow[g].open()}catch(w){
					c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
					var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
					b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
				loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
			/* custom configuration goes here (www.olark.com/documentation) */
			olark.identify('6941-894-10-9924');/*]]>*/</script><noscript><a href="https://www.olark.com/site/6941-894-10-9924/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
-->
		<!-- end olark code -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-97820172-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-97820172-1');
</script>
        
<script type="text/javascript">
_linkedin_partner_id = "451537";
window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
window._linkedin_data_partner_ids.push(_linkedin_partner_id);
</script><script type="text/javascript">
(function(){var s = document.getElementsByTagName("script")[0];
var b = document.createElement("script");
b.type = "text/javascript";b.async = true;
b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
s.parentNode.insertBefore(b, s);})();
</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=451537&fmt=gif" />
</noscript>

	</body>
</html>