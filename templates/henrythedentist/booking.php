<style type="text/css">
    .iframe {
        z-index: 201;
        position: fixed;
        background-color: #005aa9;
        top: 78px;
        left: 0;
        display: none;
        justify-content: center;
        align-items: center;
        height: 100%;
        width: 100%;
        -webkit-overflow-scrolling: touch !important;
        overflow: scroll !important;
        padding-bottom: 60px;
    }

    .iframe .hide_modal_overlay {
        background: #e9edf0;
        width: 100%;
        left: 0;
        height: 83px;
        text-align: right;
        padding: 30px;
        z-index: 210;
    }

    .register_frame .hide_modal_overlay {
        background: #3a5563;
    }

    .book_frame .hide_modal_overlay svg {
        filter: none;
    }

    .iframe .hide_modal_overlay:hover {
        opacity: 1; 
    }


    iframe {
        width: 100%;
        height: 100%;
        overflow-y: auto;
        /*margin-top: 67px;*/
    }
</style>

<div class="modal_overlay_wrapper" id="modal_overlay_booking">

    <div class = "centered_wrapper signin_box">
        <div class="hide_booking_overlay hide_modal_overlay">
            <svg class="modal_svg" xmlns="http://www.w3.org/2000/svg" version="1.1" width="32" height="32" viewBox="0 0 32 32">
                <path class="modal_svg_path" d="M31.158 32.118c-.246 0-.492-.093-.678-.282L.163 1.52c-.375-.375-.375-.983 0-1.358s.983-.375 1.358 0l30.317 30.316c.375.375.375.983 0 1.358-.187.188-.433.282-.678.282zM.842 32.118c-.246 0-.492-.093-.678-.282-.375-.375-.375-.983 0-1.358L30.48.162c.375-.375.983-.375 1.358 0s.375.983 0 1.358L1.52 31.836c-.186.188-.432.282-.677.282z" fill="#fff"></path>
            </svg>
        </div>
        <h4>Schedule An Appointment</h4>
        <div class="a_row">
            <select name="office_states">
            </select>
        </div>
        <div class="a_row">
            <select id="office_id" name="office_id">
            </select>
        </div>
        <div class = "a_row">
            <a id='book_btn' class="book_appointment_btn btn-warning" style="display: none">
                Book Now
            </a>
<!--
            <a href="#" class="reg_patient_btn btn-warning" style="display: none">
                Register as a Patient
            </a>
-->
        </div>
    </div>

</div>

<div class='iframe book_frame'>
    <div class="hide_booking_overlay hide_modal_overlay">
        <svg class="modal_svg" xmlns="http://www.w3.org/2000/svg" version="1.1" width="32" height="32" viewBox="0 0 32 32">
            <path class="modal_svg_path" d="M31.158 32.118c-.246 0-.492-.093-.678-.282L.163 1.52c-.375-.375-.375-.983 0-1.358s.983-.375 1.358 0l30.317 30.316c.375.375.375.983 0 1.358-.187.188-.433.282-.678.282zM.842 32.118c-.246 0-.492-.093-.678-.282-.375-.375-.375-.983 0-1.358L30.48.162c.375-.375.983-.375 1.358 0s.375.983 0 1.358L1.52 31.836c-.186.188-.432.282-.677.282z" fill="#fff"></path>
        </svg>
    </div>
    <iframe id='book' src=""></iframe>
</div>



<div class="modal_overlay_wrapper" id="modal_overlay_register">

    <div class = "centered_wrapper signin_box">
        <div class="hide_register_overlay hide_modal_overlay">
            <svg class="modal_svg" xmlns="http://www.w3.org/2000/svg" version="1.1" width="32" height="32" viewBox="0 0 32 32">
                <path class="modal_svg_path" d="M31.158 32.118c-.246 0-.492-.093-.678-.282L.163 1.52c-.375-.375-.375-.983 0-1.358s.983-.375 1.358 0l30.317 30.316c.375.375.375.983 0 1.358-.187.188-.433.282-.678.282zM.842 32.118c-.246 0-.492-.093-.678-.282-.375-.375-.375-.983 0-1.358L30.48.162c.375-.375.983-.375 1.358 0s.375.983 0 1.358L1.52 31.836c-.186.188-.432.282-.677.282z" fill="#fff"></path>
            </svg>
        </div>
        <h4>New Patient Registration</h4>
        <div class="a_row">
            <select name="office_states">
            </select>
        </div>

        <div class="a_row">
            <select id="office_id" class="register_sel" name="office_id">
            </select>
        </div>

        <div class = "a_row">
            <div class='error_msg' style="display:none">
                St. Joseph's Health employees will fill out their New Patient Registration forms at the HENRY practice during check-in. 
            </div>
        </div>

        <div style='padding-top: 30px;' class = "a_row">
            <a id='register_btn' href="#" class="reg_patient_btn btn-warning" >
                Register
            </a>

        </div>
    </div>
</div>

<div class='iframe register_frame'>
    <div class="hide_register_overlay hide_modal_overlay">
        <svg class="modal_svg" xmlns="http://www.w3.org/2000/svg" version="1.1" width="32" height="32" viewBox="0 0 32 32">
            <path class="modal_svg_path" d="M31.158 32.118c-.246 0-.492-.093-.678-.282L.163 1.52c-.375-.375-.375-.983 0-1.358s.983-.375 1.358 0l30.317 30.316c.375.375.375.983 0 1.358-.187.188-.433.282-.678.282zM.842 32.118c-.246 0-.492-.093-.678-.282-.375-.375-.375-.983 0-1.358L30.48.162c.375-.375.983-.375 1.358 0s.375.983 0 1.358L1.52 31.836c-.186.188-.432.282-.677.282z" fill="#fff"></path>
        </svg>
    </div>
    <iframe id='register_frame' src=""></iframe>
</div>
<style>
    #register_btn.disabled {
        pointer-events: none;
        background-color: grey; 
    }
</style>
<script type="text/javascript">
    // $('#book_btn').click(function(e){
    //     e.preventDefault();
    //     $('.blur_on_overlay').removeClass('blur_on_overlay_active');
        
    //     $("body").addClass("no_scroll");
    //     $('.iframe.book_frame').slideDown();
    //     $('.iframe.book_frame').css('display', 'flex');
    // });
    function directAppt(office_id){
        $('#book_btn').parents('.signin_box').find('#office_id').val(office_id).change();
        $('#book_btn').click();
        $('#book_btn').parents('.signin_box').find('#office_id').val('').change();
    };
    function directReg(office_id){
        $('#register_btn').parents('.signin_box').find('#office_id').val(office_id).change();
        $('#register_btn').click();
        $('#register_btn').parents('.signin_box').find('#office_id').val('').change();
    };

    $('#modal_overlay_register #office_id.register_sel').change(function(){
        if(this.value == 173){
            $('#register_btn').addClass('disabled');
            $('#modal_overlay_register .error_msg').show();
        } else {
            $('#register_btn').removeClass('disabled');
            $('#modal_overlay_register .error_msg').hide();
        }
    });

    $('#register_btn').click(function(e){
        e.preventDefault();
        $('.blur_on_overlay').removeClass('blur_on_overlay_active');
        
        $("body").addClass("no_scroll");
        $('.iframe.register_frame').slideDown();
        $('.iframe.register_frame').css('display', 'flex');
    });
</script>