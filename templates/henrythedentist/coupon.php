<div class="modal_overlay_wrapper coupon_modal_overlay" id="modal_overlay_coupon">
    <div class="hide_coupon_overlay hide_modal_overlay">
        <svg class="modal_svg" xmlns="http://www.w3.org/2000/svg" version="1.1" width="32" height="32" viewBox="0 0 32 32">
            <path class="modal_svg_path" d="M31.158 32.118c-.246 0-.492-.093-.678-.282L.163 1.52c-.375-.375-.375-.983 0-1.358s.983-.375 1.358 0l30.317 30.316c.375.375.375.983 0 1.358-.187.188-.433.282-.678.282zM.842 32.118c-.246 0-.492-.093-.678-.282-.375-.375-.375-.983 0-1.358L30.48.162c.375-.375.983-.375 1.358 0s.375.983 0 1.358L1.52 31.836c-.186.188-.432.282-.677.282z" fill="#fff"></path>
        </svg>
        <p>Close</p>
    </div>
    <div class = "centered_wrapper signin_box coupon_handout_box">
        <div class="modal_header geo_pattern"><h6 class="fashion">Here's a Free Coupon</h6></div>
        <h4>We're Saving the Earth, one Diamond at a Time, and we're Happy to Invite you Along for this Journey with us</h4>
        <h2>Please accept the coupon below as a token of our gratitude for you visiting our web site</h2>
        <div class="coupon_details">
            <? if(($model->coupon->discount_type) === '%'){?>
                <h1><?$model->coupon->discount_amount?></h1>
                <div class="right">
                    <h2>%</h2>
                    <h4>Off
                        <? if($model->coupon->min_amount){?>
                        <span>minimum total of <?=$model->coupon->min_amount?></span>
                        <?}?>
                    </h4>
                </div>
            <?}else{?>
                <h1>$<?$model->coupon->discount_amount?></h1>
                <div class="right right_dollar_format">
                    <h4>Off
                        <? if($model->coupon->min_amount){?>
                        <span>minimum total of <?=$model->coupon->min_amount?></span>
                        <?}?>
                    </h4>
                </div>
            <?}?>
        </div>
        <label>Coupon Code:</label>
        <input type="text" readonly disabled class="copy_text_on_click" value="<?=$model->coupon->code?>">
        <div class="coupon_accepted"><p>Continue Browsing Lab-Grown Diamonds</p></div>
    </div>
</div>