<div class="modal_overlay_wrapper" id="modal_overlay_signin">
	<div class="hide_signin_overlay hide_modal_overlay">
		<svg class="modal_svg" xmlns="http://www.w3.org/2000/svg" version="1.1" width="32" height="32" viewBox="0 0 32 32">
            <path class="modal_svg_path" d="M31.158 32.118c-.246 0-.492-.093-.678-.282L.163 1.52c-.375-.375-.375-.983 0-1.358s.983-.375 1.358 0l30.317 30.316c.375.375.375.983 0 1.358-.187.188-.433.282-.678.282zM.842 32.118c-.246 0-.492-.093-.678-.282-.375-.375-.375-.983 0-1.358L30.48.162c.375-.375.983-.375 1.358 0s.375.983 0 1.358L1.52 31.836c-.186.188-.432.282-.677.282z" fill="#fff"></path>
        </svg>
		<p>Close</p>
	</div>
	<div class = "centered_wrapper signin_box">
		<a href="<?=SITE_URL?>"><img class="login_logo" src="<?=FRONT_ASSETS?>img/logo_new.png"></a>
		<h4>Sign in</h4>
		<form class = "signin_form" method="post" action="/signin">
			<input style="display:none" name="redirect_url" value="<?=$_SERVER['REQUEST_URI']?>"/>
			<div class="a_row">
				<label for="username">Username</label>
				<input id="username" type="username" name="username" class="login_page_input" placeholder="Username" />
			</div>
			<div class="a_row">
				<label for="password">Password</label>
				<span><a href="#" id="forgot-password">Forgot your password?</a></span>
			</div>
			<input type="password" name="password" class="login_page_input" placeholder="Password" />
			<div class = "a_row">
				<input type="submit" class="login_page_submit" value="Secure Sign in"/>
			</div>
		</form>
		<div id="create-account">
			<div class="a_divider a_divider_break">
				<h5>New to Diamond Lab Grown?</h5>
			</div>
			<a class="register_btn show_register_overlay">Create an Account</a>
		</div>
		<div id="forget-password-email" style="display: none;">
			<div class="a_divider a_divider_break">
				<h5>Please enter your email, and we will send you a reset password email shortly.</h5>
			</div>
			<label for="email">Email address</label>
			<input id="forgot-email" placeholder="Email"/>
			<div class = "a_row">
				<button class="login_page_submit" id="forget-password-cancel">Cancel</button><button class="login_page_submit" id="forget-password-submit">Submit</button>
			</div>
			<div id="forget-password-success" style="color:green">Your reset password is on the way, please check your mailbox.</div>
		</div>
	</div>
</div>