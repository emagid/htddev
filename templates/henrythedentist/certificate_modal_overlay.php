<div class="certificate_modal modal_overlay_wrapper" id="modal_overlay_pdf">
	<div class="hide_certificate_overlay hide_modal_overlay">
		<svg class="modal_svg" xmlns="http://www.w3.org/2000/svg" version="1.1" width="32" height="32" viewBox="0 0 32 32">
            <path class="modal_svg_path" d="M31.158 32.118c-.246 0-.492-.093-.678-.282L.163 1.52c-.375-.375-.375-.983 0-1.358s.983-.375 1.358 0l30.317 30.316c.375.375.375.983 0 1.358-.187.188-.433.282-.678.282zM.842 32.118c-.246 0-.492-.093-.678-.282-.375-.375-.375-.983 0-1.358L30.48.162c.375-.375.983-.375 1.358 0s.375.983 0 1.358L1.52 31.836c-.186.188-.432.282-.677.282z" fill="#fff"></path>
        </svg>
		<p>Close</p>
	</div>
	<? if(file_exists(ROOT_DIR.'content/uploads/certificate/'.$this->viewData->product->certificate.'.pdf')){?>
		<a href="<?=SITE_DOMAIN.UPLOAD_URL?>certificate/<?=$this->viewData->product->certificate?>.pdf" class="embed" id="certificate_iframe"></a>
	<?}else{?>
		<div class="noCertificateMessage">
			<h2>No digital certificate is currently available for this product. Please call or email us for information about the diamond you are interested in. We apologize for this inconvenience.</h2>
		</div>
	<?}?>
	
	
</div>