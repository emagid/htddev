           	<div class="marketing_row home_section_two ig_section">
            <div class="graphic">
                <a href="https://www.instagram.com/henrythedentist/" target="_blank">
                <p class="blue_font">follow @HENRYthedentist</p></a>
            </div>

                <div class="ig_feed">
                    <div class="ig_pic" style="background-image:url('/content/frontend/assets/img/ig_1.jpg')"></div>
                    <div class="ig_pic" style="background-image:url('/content/frontend/assets/img/ig_2.jpg')"></div>
                    <div class="ig_pic" style="background-image:url('/content/frontend/assets/img/ig_3.jpg')"></div>
                    <div class="ig_pic" style="background-image:url('/content/frontend/assets/img/ig_4.jpg')"></div>
                    <div class="ig_pic" style="background-image:url('/content/frontend/assets/img/ig_5.jpg')"></div>
                    <div class="ig_pic" style="background-image:url('/content/frontend/assets/img/ig_6.jpg')"></div>

                </div>
	       </div>
<div class="henry_footer">
    <div class="leftLines"></div>
    <div class="rightLines"></div>
    <div class="wrapper">
    
    
        <div class="footer_links">
            <ul>
                <li>
                    <a href="/about"><p>Who We Are</p></a>
                </li>
                <li>
                    <a href="/experience"><p>Experience</p></a>
                </li>
                <li>
                    <a href="/why-henry"><p>Why Henry</p></a>
                </li>
                <li>
                    <a href="/faq"><p>FAQ</p></a>
                </li>
                <li>
                    <a href="/contact"><p>Contact</p></a>
                </li>
                <li>
                    <a href="/checkout"><p style="color:#f37021;">PAYMENT PORTAL</p></a>
                </li>
            </ul>
        </div>
        <div class="locations">
            <p><span>Now Serving all of New Jersey</span></p>
            <p>HENRY The Dentist, 890 Mountain Ave, New Providence, NJ 07974</p>
                <p><a href="mailto:hello@HENRYthedentist.com?Subject=Hello%20HENRY" target="_top">hello@HENRYthedentist.com</a></p>
                <p><a href="tel:1-551-999-2226">551.999.2226</a></p>
            <ul class="footer_social">
                <li>
                    <a href="https://www.facebook.com/HENRYthedentist/" target="_blank">
                        <img src="<?=FRONT_ASSETS?>img/fbletter.png" alt="HENRY The Dentist Facebook">
                    </a>
                </li>
                <li>
                    <a href="https://www.instagram.com/henrythedentist/" target="_blank">
                        <img src="<?=FRONT_ASSETS?>img/igletter.png" alt="HENRY The Dentist Instagram">
                    </a>
                </li>
                                <li>
                    <a href="https://www.linkedin.com/company-beta/16179424/" target="_blank">
                        <img src="<?=FRONT_ASSETS?>img/linkedinletter.png" alt="HENRY The Dentist Linkedin">
                    </a>
                </li>
            </ul>
            <p class="uppercase">© 2018 HENRY The Dentist, Inc. All rights reserved  |  <a href="privacy_policy">Privacy Policy</a></p>
        </div>
        <div class="footer_contact">
            <p></p>
        </div>
        </div>
</div>


<!--
<div class = "global_footer blur_on_overlay">
        
	<div class = "footer_content_wrapper">
		<div class = "row">
			<div class = "left_footer_section">
				<ul class = "footer_links">
					<li class = "footer_link_list_title">
						<p>About</p>
					</li>
					<li class = "footer_list_link">
						<a href="<?=SITE_URL?>about">Our Company</a>
					</li>
					<li class = "footer_list_link">
						<a href="<?=SITE_URL?>aboutlabgrowndiamonds">Lab-Grown Diamonds</a>
					</li>
					<li class = "footer_list_link">
						<a href="<?=SITE_URL?>contact">Contact Us</a>
					</li>
				</ul>
				<ul class = "footer_links">
					<li class = "footer_link_list_title">
						<p>At your Service</p>
					</li>
					<?$pages = ['shipping','returns','faq','warranty'];
					foreach($model->pages as $page){
						if(in_array($page->slug,$pages)){?>
							<li class = "footer_list_link">
								<a href="<?=SITE_URL.'page/'.$page->slug?>"><?=$page->title?></a>
							</li>
					<?}
					}?>
					<li class = "footer_list_link">
						<a href="<?=SITE_URL?>expertise/the4cs">The 4Cs</a>
					</li>
				</ul>
				<ul class = "footer_links">
					<li class = "footer_link_list_title">
						<p>Explore</p>
					</li>
					<li class = "footer_list_link">
						<a href="<?=SITE_URL?>contact">Find a Diamond</a>
					</li>
					<li class = "footer_list_link">
						<a href="<?=SITE_URL?>browse/rings">Engagement Rings</a>
					</li>
					<li class = "footer_list_link">
						<a href="<?=SITE_URL?>browse/weddingbands">Wedding Bands</a>
					</li>
					<li class = "footer_list_link">
						<a href="<?=SITE_URL?>browse/jewelry">Browse Jewelry</a>
					</li>
				</ul>
			</div>
			<div class = "right_footer_section">
	            <div class="footerLinksGrouping">
	                <h6 class="sackers">Follow Us</h6>
	                <div class="row footerSocialIcons">
	                    <div class="col">
	                        <a>
	                            <div class="media" href="mailto:info@theluggagecollection.com" style="background-image:url('<?=FRONT_ASSETS?>img/mailCircleBtn.png')"></div>
	                        </a>
	                    </div>
	                    <div class="col">
	                        <a>
	                            <div class="media" style="background-image:url('<?=FRONT_ASSETS?>img/twitterCircleBtn.png')"></div>
	                        </a>
	                    </div>
	                    <div class="col">
	                        <a>
	                            <div class="media" style="background-image:url('<?=FRONT_ASSETS?>img/facebookCircleBtn.png')"></div>
	                        </a>
	                    </div>
	                    <div class="col">
	                        <a>
	                            <div class="media" style="background-image:url('<?=FRONT_ASSETS?>img/instaCircleBtn.png')"></div>
	                        </a>
	                    </div>
	                </div>
	            </div>			
				<h1>Sign up for our Newsletter</h1>
				<div class = "newsletter_section">
					<div>
						 
							<input type = "email" name="email" class="form-control" id="email_nwsl" placeholder = "Your email">
							<input type = "submit" value = "Submit" class = "mercury" id="submit">
							<script>
						  $('body').on('click', '#submit', function() {
                                
                                var email = $("#email_nwsl").val();

                                $.ajax({
                                    type: "POST",
                                    url: "/newsletter/add",
                                    data: {
                                        email: email
                                    },
                                    success: function(data) {
                                    	alert("Thanks for signing up to receive our newsletter!");
                                    }
                                });

                            });
</script>


					</div>
				</div>
				<div class="alert alert-success" style="display:none;">
					<strong>Thank You! </strong>Your are now subscribed to our newsletter.
				</div>


 				<div class = "social_section">
					<p>Follow Us</p>
					<div class = "social_links_wrapper">
						<a>
							<icon></icon>
						</a>
						<a>
							<icon></icon>
						</a>
						<a>
							<icon></icon>
						</a>
						<a>
							<icon></icon>
						</a>
					</div>
				</div> 
			</div>
		</div>
		<div class = "row">
			<div class = "horizontal_separator"></div>
		</div>
		<div class = "row footer_bottom_row">
			<div class = "footer_bottom_row_left">
				<p>Diamond Lab Grown</p>
				<a href="<?=SITE_URL.'page/privacy-policy'?>">Privacy Policy</a>
				<a href="<?=SITE_URL.'page/terms-and-conditions'?>">Terms and Conditions</a>
			</div>
		</div>
	</div>
</div>
-->


<script>
(function(){
    var burger = document.querySelector('.burger-container'),
        header = document.querySelector('.header');
    
    burger.onclick = function() {
        header.classList.toggle('menu-opened');
    }
}());
</script>