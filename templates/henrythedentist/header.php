
<?
    if ($this->emagid->route['controller'] == 'categories'){
        $unfix=true;
    } else {
        $unfix=false;
    }
?>


<?
    if (($this->emagid->route['controller'] == 'checkout')||($this->emagid->route['controller'] == 'products')||($this->emagid->route['controller'] == 'diamonds' && $this->emagid->route['action'] == 'search')){
        $checkout_hold_header=true;
    } else {
        $checkout_hold_header=false;
    }
?>

<? if($unfix){?>
<div class="global_header unfixed blur_on_overlay">
<?}else if($checkout_hold_header){?>
<div class="global_header unfixed no_promo_above blur_on_overlay">
<?}else{?>
<div class="global_header blur_on_overlay">
<?}?>


	<div class="header_row">
		<div class="logo_wrapper">
			<a href = "<?=SITE_URL?>">
				<div class="background_image_logo"></div>
				<img src="<?=FRONT_ASSETS?>img/white_logo.png" alt="HENRY the Dentist logo">
			</a>
		</div>
		<div class="right show_690">
			<a class="mobileContactLink" href="/contact">
				<icon></icon>
			</a>
			<a class="cart_link header_link" href = "/cart">
				<p>
					<icon class="bagIcon"></icon>

					(<?=count($model->cart->products)?>)</p>
				<!-- <icon></icon> -->
			</a>		
		</div>
        

	</div>

	<div class="header_row">
		<ul class="horizontal_list">
            <li id="link_about">
                <a href="/about"><p>Who We Are</p></a>
            </li>
            <li id="link_experience">
                <a href="/experience"><p>Experience</p></a>
            </li>
            <li id="link_why-henry">
                <a href="/why-henry"><p>Why Henry</p></a>
            </li>
                                <li id="link_services">
                <a href="/home/services"><p>Services</p></a>
            </li>
            <li id="link_faq">
                <a href="/faq"><p>FAQ</p></a>
            </li>
            <li id="link_contact">
                <a href="/contact"><p>Contact</p></a>
            </li>
		</ul>
	</div>

	<div class="header_row header_nav_link_row">
		<ul class= "horizontal_list">
			<li class="show_on_mobile show_on_mobile_690">
				<div class="show_mobile_menu">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        viewBox="0 0 38.4 22.8" enable-background="new 0 0 38.4 22.8" xml:space="preserve">
                       <rect x="0.6" y="0.8" fill="#FFFFFF " width="37" height="2.9"/>
                       <rect x="0.6" y="9.8" fill="#FFFFFF " width="37" height="2.9"/>
                       <rect x="0.6" y="18.8" fill="#FFFFFF " width="37" height="2.9"/>
                   </svg>
	            </div>
	            <div class="hide_mobile_menu">
					<svg class="modal_svg" xmlns="http://www.w3.org/2000/svg" version="1.1" width="32" height="32" viewBox="0 0 32 32">
                       <path class="modal_svg_path" d="M31.158 32.118c-.246 0-.492-.093-.678-.282L.163 1.52c-.375-.375-.375-.983 0-1.358s.983-.375 1.358 0l30.317 30.316c.375.375.375.983 0 1.358-.187.188-.433.282-.678.282zM.842 32.118c-.246 0-.492-.093-.678-.282-.375-.375-.375-.983 0-1.358L30.48.162c.375-.375.983-.375 1.358 0s.375.983 0 1.358L1.52 31.836c-.186.188-.432.282-.677.282z" fill="#fff"></path>
                   </svg>
	            </div>
			</li>

<!--			<//? if($model->user){ ?>-->
<!--
				<li>
					<a class="header_link" href="/account"><icon class="accountIcon"></icon><//? echo $model->user->username; ?></a>
				</li>
				<li>
					<a class="header_link" href="/logout">Logout</a>
				</li>
-->
<!--				<//? } else { ?>-->
							<li class="">
<!--					<a class="header_link show_booking_overlay" id="booking" href="#"><p>Book</p></a>-->
                                <a class="header_link" href="/book"><p>Book</p></a>
				</li>

<!--			<//? } ?>-->
		</ul>
        
	</div>
        <div class="slick_menu">

<div class="window">
  <div class="header">
    <a href = "<?=SITE_URL?>" class="mobile_top_logo">
        <div class="background_image_logo"></div>
        <img src="<?=FRONT_ASSETS?>img/white_logo.png">
    </a>
    <div class="burger-container">
      <div id="burger">
        <div class="bar topBar"></div>
        <div class="bar btmBar"></div>
      </div>
    </div>
    <div class="icon icon-apple"></div>
    <ul class="menu">
            <li class="menu-item">
                <a href="/about"><p>Who We Are</p></a>
            </li>
            <li class="menu-item">
                <a href="/experience"><p>Experience</p></a>
            </li>
            <li class="menu-item">
                <a href="/why-henry"><p>Why Henry</p></a>
            </li>
                    <li class="menu-item">
                <a href="/home/services"><p>Services</p></a>
            </li>
            <li class="menu-item">
                <a href="/faq"><p>FAQ</p></a>
            </li>
            <li class="menu-item">
                <a href="/contact"><p>Contact</p></a>
            </li>
    </ul>
    <div class="shop icon icon-bag"></div>
  </div>

</div>
</ul>
        </div>
	<menu class="mobile_menu_wrapper show_on_mobile_690 show_on_mobile">
			<div class="accountManagementRow">	
				<a class="header_link show_bookin_overlay" id="signin" href="#">Sign In</a>
			</div>
	</menu>
	<div id = "full_page_header_dark_overlay"></div>
    
</div>
    
    
    <div class="mobile_show mobile_buttons">
    <ul class= "horizontal_list">

        <li class="left_btn">
            <a class="header_link show_booking_overlay" onclick="directAppt(175);"><p>Book Brick & Mortar</p></a>
        </li>
        <li class="right_btn">
            <a class="header_link show_booking_overlay" id="booking" href="#"><p>Book Mobile Clinic</p></a>
        </li>

    </ul>
</div>
