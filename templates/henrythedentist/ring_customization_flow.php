<div class="ring_customization_flow">
	<div>
	    <ul>
		    <li role="button" class="heading">create your Ring <a class="start_over_button" href="/checkout/cancel_combo">start over</a></li>
			<?if(isset($_SESSION['product']) && isset($_SESSION['first']) && $_SESSION['first'] == 'product'){?>
				<li class="flow_step flow_step_first">
					<div class="flow_step_inner"><span class="num">1</span>
						<div class="title">
							<h4>Diamond</h4>
						</div>
						<div class="select-product">
							<div>
								<?$product = unserialize($_SESSION['product'])?>
								<span style="background-image:url(<?=$product->featuredImage();?>" class="product-img"></span>
							</div>
							<div>
								<h6 data-product_id = <?=$product->id?>><?echo $product->name?></h6>
								<span class="price1">$<?echo $product->getPrice()?><br></span>
								<span class="view-change"><a href="/products/<?=$product->slug?>">View</a>&nbsp;<a href="/browse/diamonds" rel="nofollow">Change</a></span>
							</div>
						</div>
					</div>
					<span class="shadow"></span>
				</li>

				<li class="flow_step active flow_step_second">
					<?php if(!isset($_SESSION['ring'])){ ?>
					<div class="flow_step_inner"><span class="num">2</span>
						<div class="title">
							<h4>Setting</h4>
						</div>
						<span class="product-img"></span>
						<div class="select-product">choose a setting</div>
					</div>
					<span class="shadow"></span>
					<?php } else {?>
						<div class="flow_step_inner"><span class="num">2</span>
							<div class="title">
								<h4>Setting</h4>
							</div>
							<div class="select-product">
								<div>
									<?$ring = unserialize($_SESSION['ring'])?>
									<span style="background-image:url(<?=$ring->featuredImage()?$ring->featuredImage():FRONT_ASSETS.'img/ring5.png'?>" class="product-img"></span>
								</div>
								<div>
									<h6 data-ring_id = <?=$ring->id?>><?echo $ring->name?></h6>
									<span class="price1">$<?echo $ring->getPrice()?><br></span>
									<span class="view-change"><a href="/rings/<?=$ring->slug?>">View</a>&nbsp;<a href="/browse/rings" rel="nofollow">Change</a></span>
								</div>
							</div>
						</div>
						<span class="shadow"></span>
					<?php } ?>
				</li>
			<?} elseif(isset($_SESSION['ring']) && isset($_SESSION['first']) && $_SESSION['first'] == 'ring') {?>
				<li class="flow_step flow_step_first">
					<div class="flow_step_inner"><span class="num">1</span>
					<div class="title">
						<h4>Setting</h4>
					</div>
					<div class="select-product">
						<div>
							<?$ring = unserialize($_SESSION['ring'])?>
							<span style="background-image:url(<?=$ring->featuredImage()?$ring->featuredImage():FRONT_ASSETS.'img/ring5.png'?>" class="product-img"></span>
						</div>
						<div>
							<h6 data-ring_id = <?=$ring->id?>><?echo $ring->name?></h6>
							<span class="price1">$<?echo $ring->price?><br></span>
							<span class="view-change"><a href="/rings/<?=$ring->slug?>">View</a>&nbsp;<a href="/browse/rings" rel="nofollow">Change</a></span>
						</div>
					</div>
					</div>
					<span class="shadow"></span>
				</li>

				<li class="flow_step active flow_step_second">
					<?php if(!isset($_SESSION['product'])){ ?>
						<div class="flow_step_inner"><span class="num">2</span>
							<div class="title">
								<h4>Diamond</h4>
							</div>
							<span class="product-img"></span>
							<div class="select-product">choose a diamond</div>
						</div>
						<span class="shadow"></span>
					<?php } else {?>
						<div class="flow_step_inner"><span class="num">2</span>
							<div class="title">
								<h4>Diamond</h4>
							</div>
							<div class="select-product">
								<div>
									<?$product = unserialize($_SESSION['product'])?>
									<span style="background-image:url(<?=$product->featuredImage()?$product->featuredImage():FRONT_ASSETS.'img/ring5.png'?>" class="product-img"></span>
								</div>
								<div>
									<h6 data-ring_id = <?=$product->id?>><?echo $product->name?></h6>
									<span class="price1">$<?echo $product->price?><br></span>
									<span class="view-change"><a href="/products/<?=$product->slug?>">View</a>&nbsp;<a href="/browse/diamonds" rel="nofollow">Change</a></span>
								</div>
							</div>
						</div>
						<span class="shadow"></span>
					<?php } ?>
				</li>
			<?}?>
			<li class="flow_step flow_step_third">
				<div class="flow_step_inner"><span class="num">3</span>
				<div class="title">
					<h4>complete your ring</h4>
				</div>
				<?if(isset($_SESSION['product']) && isset($_SESSION['ring'])){?>
					<div class="select-product">
						<div>
							<span class="view-change"><a href="/browse/confirm">Click to confirm</a></span>
						</div>
					</div>
				<?}?>
					<span class="product-img"></span>
					<div class="select-product"></div>
				</div>
				<span class="shadow"></span>
			</li>
	    </ul>
	</div>
</div>
<script>
	$(document).ready(function(){
	})
</script>