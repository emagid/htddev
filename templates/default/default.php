<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->

<!--[if lte IE 9]>
<script type="text/javascript" src="<?=FRONT_JS?>js/ie8/html5shiv-printshiv.min.js"></script>
<script "text/javascript" src="<?=FRONT_JS?>js/ie8/respond.min.js"></script>
<![endif]-->

<head>
    <!-- META DATA -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?= $this->configs['Meta Title']; ?></title>
    <meta name="Description" content="<?= $this->configs['Meta Description']; ?>">
    <meta name="Keywords" content="<?= $this->configs['Meta Keywords']; ?>">

    <meta property="og:title" content="<?=SITE_NAME?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?=SITE_URL?>" />

    <!-- FAVICONS -->
    <!-- http://realfavicongenerator.net/ -->
<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">
    
    <link rel="stylesheet" href="<?=FRONT_CSS?>whitney/whitney.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/SlickNav/1.0.10/slicknav.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/SlickNav/1.0.10/jquery.slicknav.min.js"></script>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>

    <!-- SITE CSS -->

    <?php
    if ($_SERVER['REMOTE_ADDR']=='127.0.0.1' || $_SERVER['REMOTE_ADDR']=='localhost') {
        define('WP_ENV', 'development');
    } else {
        define('WP_ENV', 'production');
    }
    if (WP_ENV == 'development') { ?>
    	<link rel="stylesheet" href="<?=FRONT_CSS?>main.css">
    <?php } else { ?>
    	<link rel="stylesheet" href="<?=FRONT_CSS?>main.min.css">
    <?php } ?>

    <script src="<?=FRONT_JS?>jquery.min.js"></script>
    <?php
    if (WP_ENV == 'development') { ?>
        <script src="<?=FRONT_JS?>main.js"></script>
    <?php } else { ?>
        <script src="<?=FRONT_JS?>main.min.js"></script>
    <?php } ?>

</head>

<body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=SITE_URL?>"><svg width="100" height="33">
                  <image xlink:href="<?=FRONT_IMG?>logo.svg" src="<?=FRONT_IMG?>logo.png" width="100" height="33" />
                  </svg>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <?php foreach($model->pages as $page) { ?>
                        <li><a href='<?= SITE_URL.'page/'.$page->slug;?>'><?= $page->title;?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-lg-24">
                <?php $emagid->controller->renderBody($model); ?>
            </div>
        </div>
    </div>
    <footer>
		<div class="container">
			<div class="row">
				<div class="col-sm-18">
					<ul class="list-inline list-unstyled">
						<!-- pages -->
						<?php foreach($model->pages as $page) { ?>
							<a href='<?= SITE_URL.'page/'.$page->slug;?>'><?= $page->title;?></a>
						<?php } ?>
					</ul>
				</div>
				<div class="col-sm-6">
					<ul class="list-inline list-unstyled social">
						<li><a href=""><i class="icon-facebook"></i></a></li>
						<li><a href=""><i class="icon-twitter"></i></a></li>
						<li><a href=""><i class="icon-instagramm"></i></a></li>
						<li><a href=""><i class="icon-gplus"></i></a></li>
					</ul>
					<p class="text-center">Powered by <a href="http://emagid.com" target="_blank">eMagid</a></p>
				</div>
			</div>
			<p>Copyright 2015 INC.</p>
		</div>
	</footer>
</body>
</html>