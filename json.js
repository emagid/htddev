[
	    {
	        "id": "1",
	        "title": "Unavailable",
	        "type":"unavailable",
	        "start": "2015-9-04 10:20:00",
	        "end": "2015-9-04 11:00:00",
	        "allDay": false,
	        "className":"unavailable_block",
	        "editable": true,
	        "displayEventEnd":true,
	        "borderColor": "transparent",
	        "backgroundColor": "#E5E5E5",
	        "textColor": "#AC547D" // Optional
	    },
	    {
	        "id": "2", // Optional
	        "title": "Unavailable", // Required
	        "type":"unavailable",
	        "start": "2015-9-03 00:00:00", // Required
	        "end": "2015-9-03 23:59:00", // Optional
	        "allDay": true, // Optional
	        "className":"unavailable_block all_day_session_block",
	        "editable": true, // Optional
	        "borderColor": "transparent", // Optional
	        "backgroundColor": "#E5E5E5", // Optional
	        "textColor": "#AC547D" // Optional
	    },
	    {
	        "id": "3", // Optional
	        "title": "with First Last",
	        "type":"confirmed", // Required
	        "start": "2015-9-06 10:20:00", // Required
	        "end": "2015-9-06 16:00:00", // Optional- end time will be automatically set to 1 hour after start. 
	        "allDay": false, // Optional
	        "className":"session_block",
	        "displayEventEnd":true,
	        "editable": true, // Optional
	        "description":"Session with Charlie",
	        "location":"Times Square"
	    },
	    {
	        "id": "4", // Optional
	        "title": "with First Lastadasdsa",
	        "type":"confirmed", // Required
	        "start": "2015-9-04 10:20:00", // Required
	        "end": "2015-9-04 16:00:00", // Optional- end time will be automatically set to 1 hour after start. 
	        "allDay": false, // Optional
	        "className":"pending_session_block",
	        "displayEventEnd":true,
	        "editable": true, // Optional
	        "description":"Session with Charlie",
	        "location":"Times Square"
	    },
	    {
	        "id": "5", // Optional
	        "title": "with First Last",
	        "type":"pending", // Required
	        "start": "2015-9-05 10:20:00", // Required
	        "end": "2015-9-05 18:00:00", // Optional- end time will be automatically set to 1 hour after start. 
	        "allDay": false, // Optional
	        "className":"pending_session_block",
	        "displayEventEnd":true,
	        "editable": true // Optional
	    },
	    {
	        "id": "6", // Optional
	        "title": "Nothing Booked Yet",
	        "type":"available", // Required
	        "start": "2015-9-05 00:00:00", // Required
	        "end": "2015-9-05 24:00:00", // Optional- end time will be automatically set to 1 hour after start. 
	        "allDay": true, // Optional
	        "className":"available_session_block all_day_session_block",
	        "editable": true // Optional
	    },
	    {
	        "id": "7", // Optional
	        "title": "Nothing Booked Yet",
	        "type":"available", // Required
	        "start": "2015-9-09 00:20:00", // Required
	        "end": "2015-9-09 24:00:00", // Optional- end time will be automatically set to 1 hour after start. 
	        "allDay": true, // Optional
	        "displayEventEnd":true,
	        "className":"available_session_block all_day_session_block",
	        "editable": true

	    }
	]