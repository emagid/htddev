--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: address; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE address (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    is_default boolean DEFAULT false,
    user_id integer,
    first_name character varying,
    last_name character varying,
    phone character varying,
    address character varying,
    address2 character varying,
    city character varying,
    state character varying,
    country character varying,
    zip character varying,
    label character varying
);


ALTER TABLE address OWNER TO htdnewweb;

--
-- Name: address_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE address_id_seq OWNER TO htdnewweb;

--
-- Name: address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE address_id_seq OWNED BY address.id;


--
-- Name: admin_roles; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE admin_roles (
    id integer NOT NULL,
    active smallint DEFAULT 1 NOT NULL,
    insert_time timestamp without time zone DEFAULT now() NOT NULL,
    admin_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE admin_roles OWNER TO htdnewweb;

--
-- Name: admin_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE admin_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin_roles_id_seq OWNER TO htdnewweb;

--
-- Name: admin_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE admin_roles_id_seq OWNED BY admin_roles.id;


--
-- Name: administrator; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE administrator (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    first_name character varying,
    last_name character varying,
    email character varying,
    username character varying,
    password character varying,
    hash character varying,
    permissions character varying,
    signup_ip character varying,
    update_time character varying
);


ALTER TABLE administrator OWNER TO htdnewweb;

--
-- Name: administrator_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE administrator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE administrator_id_seq OWNER TO htdnewweb;

--
-- Name: administrator_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE administrator_id_seq OWNED BY administrator.id;


--
-- Name: banner; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE banner (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    title character varying,
    image character varying,
    main_description character varying,
    page character varying,
    is_deal boolean DEFAULT false,
    banner_type_id integer DEFAULT 0,
    url character varying,
    banner_order integer
);


ALTER TABLE banner OWNER TO htdnewweb;

--
-- Name: banner_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE banner_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE banner_id_seq OWNER TO htdnewweb;

--
-- Name: banner_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE banner_id_seq OWNED BY banner.id;


--
-- Name: category; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE category (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    subtitle character varying,
    parent_category integer,
    description character varying,
    slug character varying,
    meta_title character varying,
    meta_keywords character varying,
    meta_description character varying,
    display_order integer,
    in_menu integer,
    image character varying
);


ALTER TABLE category OWNER TO htdnewweb;

--
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE category_id_seq OWNER TO htdnewweb;

--
-- Name: category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE category_id_seq OWNED BY category.id;


--
-- Name: config; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE config (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    value character varying
);


ALTER TABLE config OWNER TO htdnewweb;

--
-- Name: config_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE config_id_seq OWNER TO htdnewweb;

--
-- Name: config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE config_id_seq OWNED BY config.id;


--
-- Name: contact; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE contact (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    first_name character varying,
    last_name character varying,
    phone character varying,
    email character varying,
    company character varying,
    message character varying
);


ALTER TABLE contact OWNER TO htdnewweb;

--
-- Name: contact_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE contact_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE contact_id_seq OWNER TO htdnewweb;

--
-- Name: contact_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE contact_id_seq OWNED BY contact.id;


--
-- Name: coupon; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE coupon (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    code character varying,
    discount_type character varying,
    discount_amount character varying,
    min_amount character varying,
    num_uses_all character varying,
    uses_per_user character varying,
    start_time character varying,
    end_time character varying
);


ALTER TABLE coupon OWNER TO htdnewweb;

--
-- Name: coupon_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE coupon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE coupon_id_seq OWNER TO htdnewweb;

--
-- Name: coupon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE coupon_id_seq OWNED BY coupon.id;


--
-- Name: credit; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE credit (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    description character varying,
    price character varying,
    value character varying
);


ALTER TABLE credit OWNER TO htdnewweb;

--
-- Name: credit_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE credit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE credit_id_seq OWNER TO htdnewweb;

--
-- Name: credit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE credit_id_seq OWNED BY credit.id;


--
-- Name: faq_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE faq_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE faq_id_seq OWNER TO postgres;

--
-- Name: faq; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE faq (
    id integer DEFAULT nextval('faq_id_seq'::regclass) NOT NULL,
    insert_time timestamp with time zone DEFAULT now(),
    active integer DEFAULT 1,
    question character varying,
    answer character varying,
    display_order integer
);


ALTER TABLE faq OWNER TO postgres;

--
-- Name: hipaa_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE hipaa_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hipaa_id_seq OWNER TO root;

--
-- Name: hipaa; Type: TABLE; Schema: public; Owner: root; Tablespace: 
--

CREATE TABLE hipaa (
    id integer DEFAULT nextval('hipaa_id_seq'::regclass) NOT NULL,
    insert_time timestamp with time zone DEFAULT now(),
    active integer DEFAULT 1,
    content character varying
);


ALTER TABLE hipaa OWNER TO root;

--
-- Name: home_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE home_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE home_id_seq OWNER TO postgres;

--
-- Name: home; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE home (
    id integer DEFAULT nextval('home_id_seq'::regclass) NOT NULL,
    insert_time timestamp with time zone DEFAULT now(),
    active integer DEFAULT 1,
    main_title character varying,
    section character varying,
    content character varying,
    featured_image1 character varying,
    featured_image2 character varying,
    featured_image3 character varying
);


ALTER TABLE home OWNER TO postgres;

--
-- Name: jewelry; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE jewelry (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    description character varying,
    price numeric(10,2),
    currency_code character varying,
    quantity integer,
    tags character varying,
    materials character varying,
    images text,
    variation text,
    meta_title character varying,
    meta_keywords character varying,
    meta_description character varying,
    metal_colors text DEFAULT '{"14kt_gold":{"White":0,"Yellow":0,"Rose":0},"18kt_gold":{"White":0,"Yellow":0,"Rose":0},"Platinum":{"White":0,"Yellow":0,"Rose":0}}'::text,
    metals character varying DEFAULT '14kt_gold, 18kt_gold, Platinum'::character varying,
    total_14kt_price numeric(10,2) DEFAULT 0,
    total_18kt_price numeric(10,2) DEFAULT 0,
    total_plat_price numeric(10,2) DEFAULT 0,
    slug character varying,
    semi_mount numeric(5,2),
    stone_breakdown character varying,
    stone_count integer,
    weight numeric(10,2),
    diamond_cwwt character varying,
    diamond_color character varying,
    diamond_quality character varying,
    _14kt numeric(10,2),
    _18kt numeric(10,2),
    platinum numeric(10,2),
    setting integer,
    polish integer,
    melee numeric(10,2),
    ship_from_cast numeric(10,2),
    ship_to_cust numeric(10,2),
    memo_duration integer DEFAULT 0,
    featured integer DEFAULT 0,
    discount integer DEFAULT 0
);


ALTER TABLE jewelry OWNER TO htdnewweb;

--
-- Name: jewelry_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE jewelry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE jewelry_id_seq OWNER TO htdnewweb;

--
-- Name: jewelry_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE jewelry_id_seq OWNED BY jewelry.id;


--
-- Name: media_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE media_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE media_id_seq OWNER TO postgres;

--
-- Name: media; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE media (
    id integer DEFAULT nextval('media_id_seq'::regclass) NOT NULL,
    insert_time timestamp with time zone DEFAULT now(),
    active integer DEFAULT 1,
    name character varying,
    logo character varying,
    url character varying
);


ALTER TABLE media OWNER TO postgres;

--
-- Name: office; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE office (
    id integer NOT NULL,
    insert_time timestamp with time zone DEFAULT now(),
    active integer DEFAULT 1,
    phone character varying,
    address character varying,
    address2 character varying,
    city character varying,
    zip character varying,
    label character varying,
    practice_id integer,
    email character varying,
    state character varying
);


ALTER TABLE office OWNER TO htdnewweb;

--
-- Name: order; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE "order" (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    bill_first_name character varying,
    bill_last_name character varying,
    bill_address character varying,
    bill_address2 character varying,
    bill_city character varying,
    bill_state character varying,
    bill_country character varying,
    bill_zip character varying,
    ship_first_name character varying,
    ship_last_name character varying,
    ship_address character varying,
    ship_address2 character varying,
    ship_city character varying,
    ship_state character varying,
    ship_country character varying,
    ship_zip character varying,
    cc_number character varying,
    cc_expiration_month character varying,
    cc_expiration_year character varying,
    cc_ccv character varying,
    tax character varying,
    tax_rate character varying,
    status character varying,
    tracking_number character varying,
    shipping_method character varying,
    user_id integer,
    coupon_code character varying,
    gift_card character varying,
    email character varying,
    shipping_cost real,
    payment_method integer,
    subtotal real,
    total real,
    viewed boolean,
    phone character varying,
    coupon_type integer,
    coupon_amount numeric,
    note character varying,
    user_role character varying,
    ref_num character varying,
    order_type integer,
    signature character varying
);


ALTER TABLE "order" OWNER TO htdnewweb;

--
-- Name: order_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE order_id_seq OWNER TO htdnewweb;

--
-- Name: order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE order_id_seq OWNED BY "order".id;


--
-- Name: order_product_option; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE order_product_option (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    order_product_id integer,
    data text,
    price numeric(10,2) DEFAULT 0
);


ALTER TABLE order_product_option OWNER TO htdnewweb;

--
-- Name: order_product_option_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE order_product_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE order_product_option_id_seq OWNER TO htdnewweb;

--
-- Name: order_product_option_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE order_product_option_id_seq OWNED BY order_product_option.id;


--
-- Name: order_products; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE order_products (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    order_id integer,
    quantity integer,
    unit_price real,
    product_map_id integer NOT NULL
);


ALTER TABLE order_products OWNER TO htdnewweb;

--
-- Name: order_products_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE order_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE order_products_id_seq OWNER TO htdnewweb;

--
-- Name: order_products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE order_products_id_seq OWNED BY order_products.id;


--
-- Name: page; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE page (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    title character varying,
    slug character varying,
    description character varying,
    featured_image character varying,
    meta_title character varying,
    meta_keywords character varying,
    meta_description character varying,
    date_modified character varying,
    content text
);


ALTER TABLE page OWNER TO htdnewweb;

--
-- Name: page_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE page_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE page_id_seq OWNER TO htdnewweb;

--
-- Name: page_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE page_id_seq OWNED BY page.id;


--
-- Name: payment_profiles; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE payment_profiles (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    user_id integer,
    first_name character varying,
    last_name character varying,
    phone character varying,
    address character varying,
    address2 character varying,
    city character varying,
    state character varying,
    country character varying,
    zip character varying,
    cc_number character varying,
    cc_expiration_month character varying,
    cc_expiration_year character varying,
    cc_ccv character varying,
    label character varying
);


ALTER TABLE payment_profiles OWNER TO htdnewweb;

--
-- Name: payment_profiles_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE payment_profiles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE payment_profiles_id_seq OWNER TO htdnewweb;

--
-- Name: payment_profiles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE payment_profiles_id_seq OWNED BY payment_profiles.id;


--
-- Name: practice_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE practice_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE practice_id_seq OWNER TO htdnewweb;

--
-- Name: practice; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE practice (
    id integer DEFAULT nextval('practice_id_seq'::regclass) NOT NULL,
    insert_time timestamp without time zone DEFAULT now(),
    active integer DEFAULT 1,
    name character varying,
    slug character varying,
    original_name character varying
);


ALTER TABLE practice OWNER TO htdnewweb;

--
-- Name: privacy_policy_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE privacy_policy_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE privacy_policy_id_seq OWNER TO root;

--
-- Name: privacy_policy; Type: TABLE; Schema: public; Owner: root; Tablespace: 
--

CREATE TABLE privacy_policy (
    id integer DEFAULT nextval('privacy_policy_id_seq'::regclass) NOT NULL,
    insert_time timestamp with time zone DEFAULT now(),
    active integer DEFAULT 1,
    content character varying
);


ALTER TABLE privacy_policy OWNER TO root;

--
-- Name: product; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE product (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    sku character varying,
    price character varying,
    meta_title character varying,
    meta_keywords character varying,
    meta_description character varying,
    featured_image character varying,
    colors character varying,
    featured smallint,
    shape character varying,
    clarity character varying,
    weight character varying,
    lab character varying,
    cut_grade character varying,
    polish character varying,
    symmetry character varying,
    fluor character varying,
    rapaport_price character varying,
    total character varying,
    certificate character varying,
    length character varying,
    width character varying,
    depth character varying,
    depth_percent character varying,
    table_percent character varying,
    girdle character varying,
    culet character varying,
    description character varying,
    origin character varying,
    memo_status character varying,
    inscription character varying,
    certificate_file character varying,
    slug character varying,
    wholesale_price numeric(10,2) DEFAULT 0,
    video_link character varying,
    quantity integer,
    memo_duration integer DEFAULT 0,
    vendor character varying,
    discount integer DEFAULT 0
);


ALTER TABLE product OWNER TO htdnewweb;

--
-- Name: product_categories; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE product_categories (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    category_id integer,
    product_map_id integer NOT NULL
);


ALTER TABLE product_categories OWNER TO htdnewweb;

--
-- Name: product_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE product_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_categories_id_seq OWNER TO htdnewweb;

--
-- Name: product_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE product_categories_id_seq OWNED BY product_categories.id;


--
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_id_seq OWNER TO htdnewweb;

--
-- Name: product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE product_id_seq OWNED BY product.id;


--
-- Name: product_images; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE product_images (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    image character varying,
    display_order integer,
    product_map_id integer NOT NULL
);


ALTER TABLE product_images OWNER TO htdnewweb;

--
-- Name: product_images_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE product_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_images_id_seq OWNER TO htdnewweb;

--
-- Name: product_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE product_images_id_seq OWNED BY product_images.id;


--
-- Name: product_map; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE product_map (
    id integer NOT NULL,
    active integer DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    product_id integer,
    product_type_id integer
);


ALTER TABLE product_map OWNER TO htdnewweb;

--
-- Name: product_map_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE product_map_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_map_id_seq OWNER TO htdnewweb;

--
-- Name: product_map_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE product_map_id_seq OWNED BY product_map.id;


--
-- Name: quotes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE quotes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE quotes_id_seq OWNER TO postgres;

--
-- Name: quotes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE quotes (
    id integer DEFAULT nextval('quotes_id_seq'::regclass) NOT NULL,
    insert_time timestamp with time zone DEFAULT now(),
    active integer DEFAULT 1,
    quote character varying,
    by_name character varying
);


ALTER TABLE quotes OWNER TO postgres;

--
-- Name: register_request; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE register_request (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    first_name character varying,
    last_name character varying,
    company character varying,
    email character varying,
    phone character varying,
    full_address character varying,
    status integer DEFAULT 0
);


ALTER TABLE register_request OWNER TO htdnewweb;

--
-- Name: register_request_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE register_request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE register_request_id_seq OWNER TO htdnewweb;

--
-- Name: register_request_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE register_request_id_seq OWNED BY register_request.id;


--
-- Name: reviews_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE reviews_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE reviews_id_seq OWNER TO postgres;

--
-- Name: reviews; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE reviews (
    id integer DEFAULT nextval('reviews_id_seq'::regclass) NOT NULL,
    insert_time timestamp with time zone DEFAULT now(),
    active integer DEFAULT 1,
    name character varying,
    review_comment character varying
);


ALTER TABLE reviews OWNER TO postgres;

--
-- Name: ring; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE ring (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    price numeric(10,2),
    default_metal character varying,
    default_color character varying,
    dwt character varying,
    semi_mount_weight character varying,
    stone_breakdown character varying,
    center_stone_weight character varying,
    center_stone_shape character varying,
    total_stones character varying,
    semi_mount_diamond_weight character varying,
    diamond_color character varying,
    diamond_quality character varying,
    cost_per_dwt character varying,
    fourteenkt_gold_price character varying,
    eighteenkt_gold_price character varying,
    platinum_price character varying,
    website_fourteenkt_gold_price character varying,
    website_eighteenkt_gold_price character varying,
    website_platinum_price character varying,
    slug character varying,
    min_carat numeric(5,2) DEFAULT 0.0,
    max_carat numeric(5,2) DEFAULT 100,
    shape character varying,
    video_link character varying,
    metals character varying DEFAULT '14kt_gold, 18kt_gold, Platinum'::character varying,
    colors character varying DEFAULT 'White,Yellow,Rose'::character varying,
    metal_colors text DEFAULT '{"14kt_gold":{"White":1,"Yellow":1,"Rose":1},"18kt_gold":{"White":1,"Yellow":1,"Rose":0},"Platinum":{"White":1,"Yellow":0,"Rose":0}}'::text,
    setting_labor numeric(5,2),
    center_stone_setting_labor numeric(5,2),
    polish_labor numeric(5,2),
    head_to_add numeric(5,2),
    melee_cost numeric(5,2),
    shipping_from_cast numeric(5,2),
    shipping_to_customer numeric(5,2),
    total_14kt_cost numeric(10,2),
    total_18kt_cost numeric(10,2),
    total_plat_cost numeric(10,2),
    setting_style character varying,
    options text,
    alias character varying,
    meta_title character varying,
    meta_keywords character varying,
    meta_description character varying,
    description character varying,
    memo_duration integer DEFAULT 0,
    featured integer DEFAULT 0,
    discount integer DEFAULT 0
);


ALTER TABLE ring OWNER TO htdnewweb;

--
-- Name: ring_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE ring_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ring_id_seq OWNER TO htdnewweb;

--
-- Name: ring_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE ring_id_seq OWNED BY ring.id;


--
-- Name: ring_material; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE ring_material (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    price numeric(10,2) DEFAULT 0,
    min_size numeric(10,2) DEFAULT 4.00,
    max_size numeric(10,2) DEFAULT 9.00,
    size_step numeric(10,2) DEFAULT 0.25,
    size_step_price numeric(10,2) DEFAULT 10
);


ALTER TABLE ring_material OWNER TO htdnewweb;

--
-- Name: ring_material_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE ring_material_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ring_material_id_seq OWNER TO htdnewweb;

--
-- Name: ring_material_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE ring_material_id_seq OWNED BY ring_material.id;


--
-- Name: role; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE role (
    id integer NOT NULL,
    active smallint DEFAULT 1 NOT NULL,
    insert_time timestamp without time zone DEFAULT now() NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE role OWNER TO htdnewweb;

--
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE role_id_seq OWNER TO htdnewweb;

--
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE role_id_seq OWNED BY role.id;


--
-- Name: shipping_method; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE shipping_method (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    is_default boolean DEFAULT false,
    name character varying,
    cart_subtotal_range_min numeric(10,0),
    cost numeric(10,0)
);


ALTER TABLE shipping_method OWNER TO htdnewweb;

--
-- Name: shipping_method_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE shipping_method_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE shipping_method_id_seq OWNER TO htdnewweb;

--
-- Name: shipping_method_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE shipping_method_id_seq OWNED BY shipping_method.id;


--
-- Name: team_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE team_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE team_id_seq OWNER TO postgres;

--
-- Name: team; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE team (
    id integer DEFAULT nextval('team_id_seq'::regclass) NOT NULL,
    insert_time timestamp with time zone DEFAULT now(),
    active integer DEFAULT 1,
    name character varying,
    "position" character varying,
    image character varying,
    description character varying
);


ALTER TABLE team OWNER TO postgres;

--
-- Name: transaction; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE transaction (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    order_id integer,
    authorize_net_data text,
    ref_trans_id character varying
);


ALTER TABLE transaction OWNER TO htdnewweb;

--
-- Name: transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE transaction_id_seq OWNER TO htdnewweb;

--
-- Name: transaction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE transaction_id_seq OWNED BY transaction.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE "user" (
    id integer NOT NULL,
    active smallint DEFAULT 1 NOT NULL,
    insert_time timestamp without time zone DEFAULT now() NOT NULL,
    email character varying NOT NULL,
    password character varying,
    hash character varying,
    first_name character varying,
    last_name character varying,
    zip numeric,
    phone character varying,
    dob timestamp without time zone,
    bank_name character varying,
    routing_number character varying,
    bank_address character varying,
    bank_city character varying,
    voided_check character varying,
    photo character varying,
    wholesale_id integer DEFAULT 0,
    status integer,
    company character varying,
    full_address character varying,
    setup integer DEFAULT 0,
    username character varying
);


ALTER TABLE "user" OWNER TO htdnewweb;

--
-- Name: user_favorite; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE user_favorite (
    id integer NOT NULL,
    active smallint DEFAULT 1 NOT NULL,
    insert_time timestamp without time zone DEFAULT now() NOT NULL,
    user_id integer,
    provider_id integer
);


ALTER TABLE user_favorite OWNER TO htdnewweb;

--
-- Name: user_favorite_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE user_favorite_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_favorite_id_seq OWNER TO htdnewweb;

--
-- Name: user_favorite_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE user_favorite_id_seq OWNED BY user_favorite.id;


--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_id_seq OWNER TO htdnewweb;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE user_id_seq OWNED BY "user".id;


--
-- Name: user_roles; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE user_roles (
    id integer NOT NULL,
    active smallint DEFAULT 1 NOT NULL,
    insert_time timestamp without time zone DEFAULT now() NOT NULL,
    user_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE user_roles OWNER TO htdnewweb;

--
-- Name: user_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE user_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_roles_id_seq OWNER TO htdnewweb;

--
-- Name: user_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE user_roles_id_seq OWNED BY user_roles.id;


--
-- Name: wedding_band; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE wedding_band (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    slug character varying,
    price numeric(10,2) DEFAULT 0,
    category character varying,
    diamond_color character varying,
    diamond_quality character varying,
    video_link character varying,
    metals character varying DEFAULT '14kt_gold, 18kt_gold, Platinum'::character varying,
    colors character varying DEFAULT 'White,Yellow,Rose'::character varying,
    metal_colors text DEFAULT '{"14kt_gold":{"White":1,"Yellow":1,"Rose":1},"18kt_gold":{"White":1,"Yellow":1,"Rose":1},"Platinum":{"White":1,"Yellow":0,"Rose":0}}'::text,
    setting_labor numeric(5,2),
    polish_labor numeric(5,2),
    head_to_add numeric(5,2),
    melee_cost numeric(5,2),
    shipping_from_cast numeric(5,2),
    shipping_to_customer numeric(5,2),
    total_14kt_cost numeric(10,2),
    total_18kt_cost numeric(10,2),
    total_plat_cost numeric(10,2),
    options text,
    setting_style character varying,
    alias character varying,
    meta_title character varying,
    meta_keywords character varying,
    meta_description character varying,
    memo_duration integer DEFAULT 0
);


ALTER TABLE wedding_band OWNER TO htdnewweb;

--
-- Name: wedding_band_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE wedding_band_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE wedding_band_id_seq OWNER TO htdnewweb;

--
-- Name: wedding_band_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE wedding_band_id_seq OWNED BY wedding_band.id;


--
-- Name: wholesale; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE wholesale (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    company character varying,
    job_title character varying,
    dba character varying,
    incorporation character varying,
    referrer character varying,
    phone character varying,
    alt_phone character varying,
    status integer DEFAULT 0,
    address character varying,
    address2 character varying,
    payment character varying,
    memo_duration integer DEFAULT 24,
    site character varying,
    logo character varying,
    discount text,
    tax_id integer
);


ALTER TABLE wholesale OWNER TO htdnewweb;

--
-- Name: wholesale_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE wholesale_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE wholesale_id_seq OWNER TO htdnewweb;

--
-- Name: wholesale_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE wholesale_id_seq OWNED BY wholesale.id;


--
-- Name: wholesale_items; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE wholesale_items (
    id integer NOT NULL,
    active integer DEFAULT 1,
    insert_time timestamp without time zone,
    price character varying,
    wholesale_id integer,
    product_id integer
);


ALTER TABLE wholesale_items OWNER TO htdnewweb;

--
-- Name: wholesale_items_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE wholesale_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE wholesale_items_id_seq OWNER TO htdnewweb;

--
-- Name: wholesale_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE wholesale_items_id_seq OWNED BY wholesale_items.id;


--
-- Name: wishlist; Type: TABLE; Schema: public; Owner: htdnewweb; Tablespace: 
--

CREATE TABLE wishlist (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    user_id integer,
    data text
);


ALTER TABLE wishlist OWNER TO htdnewweb;

--
-- Name: wishlist_id_seq; Type: SEQUENCE; Schema: public; Owner: htdnewweb
--

CREATE SEQUENCE wishlist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE wishlist_id_seq OWNER TO htdnewweb;

--
-- Name: wishlist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: htdnewweb
--

ALTER SEQUENCE wishlist_id_seq OWNED BY wishlist.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY address ALTER COLUMN id SET DEFAULT nextval('address_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY admin_roles ALTER COLUMN id SET DEFAULT nextval('admin_roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY administrator ALTER COLUMN id SET DEFAULT nextval('administrator_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY banner ALTER COLUMN id SET DEFAULT nextval('banner_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY category ALTER COLUMN id SET DEFAULT nextval('category_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY config ALTER COLUMN id SET DEFAULT nextval('config_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY contact ALTER COLUMN id SET DEFAULT nextval('contact_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY coupon ALTER COLUMN id SET DEFAULT nextval('coupon_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY credit ALTER COLUMN id SET DEFAULT nextval('credit_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY jewelry ALTER COLUMN id SET DEFAULT nextval('jewelry_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY "order" ALTER COLUMN id SET DEFAULT nextval('order_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY order_product_option ALTER COLUMN id SET DEFAULT nextval('order_product_option_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY order_products ALTER COLUMN id SET DEFAULT nextval('order_products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY page ALTER COLUMN id SET DEFAULT nextval('page_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY payment_profiles ALTER COLUMN id SET DEFAULT nextval('payment_profiles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY product ALTER COLUMN id SET DEFAULT nextval('product_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY product_categories ALTER COLUMN id SET DEFAULT nextval('product_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY product_images ALTER COLUMN id SET DEFAULT nextval('product_images_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY product_map ALTER COLUMN id SET DEFAULT nextval('product_map_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY register_request ALTER COLUMN id SET DEFAULT nextval('register_request_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY ring ALTER COLUMN id SET DEFAULT nextval('ring_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY ring_material ALTER COLUMN id SET DEFAULT nextval('ring_material_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY role ALTER COLUMN id SET DEFAULT nextval('role_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY shipping_method ALTER COLUMN id SET DEFAULT nextval('shipping_method_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY transaction ALTER COLUMN id SET DEFAULT nextval('transaction_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY "user" ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY user_favorite ALTER COLUMN id SET DEFAULT nextval('user_favorite_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY user_roles ALTER COLUMN id SET DEFAULT nextval('user_roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY wedding_band ALTER COLUMN id SET DEFAULT nextval('wedding_band_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY wholesale ALTER COLUMN id SET DEFAULT nextval('wholesale_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY wholesale_items ALTER COLUMN id SET DEFAULT nextval('wholesale_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: htdnewweb
--

ALTER TABLE ONLY wishlist ALTER COLUMN id SET DEFAULT nextval('wishlist_id_seq'::regclass);


--
-- Data for Name: address; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--



--
-- Name: address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('address_id_seq', 1, false);


--
-- Data for Name: admin_roles; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--

INSERT INTO admin_roles VALUES (1, 1, '2015-08-21 12:40:22.392', 1, 1);


--
-- Name: admin_roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('admin_roles_id_seq', 1, true);


--
-- Data for Name: administrator; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--

INSERT INTO administrator VALUES (1, 1, '2015-08-21 12:40:22.254', 'Master', 'User', 'americangrowndiamonds@gmail.com', 'emagid', 'e337bb3bc37b0e2224d0c9b7819dd4bf35b2d164ff349c7a61a4e06fce3abab3', '900a780666f25081412061dd16f87804', 'Content,Pages,Blogs,Newsletter,Banners,Home Contents,Media Logos,Quotes,FAQs,Users,Contacts,Privacy Policies,Hipaa,Reviews,Teams,Practice Info,Offices,Patients,Appointments,System,Email List,Configs,Administrators', NULL, NULL);


--
-- Name: administrator_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('administrator_id_seq', 1, true);


--
-- Data for Name: banner; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--

INSERT INTO banner VALUES (1, 0, '2018-06-06 14:44:07.355267', 'test', NULL, 'abc', NULL, false, 0, NULL, 1);
INSERT INTO banner VALUES (2, 1, '2018-06-06 20:18:21.588658', 'the dentist now <br /> comes to you. <br /> (it''s about time)', '5b1879cdb7f9d_banner_inside.jpg', '<p>we believe everyone<br />
should have easy access<br />
to great dental care</p>

<p>that&#39;s why we built HENRY</p>
', 'home', false, 0, NULL, 1);
INSERT INTO banner VALUES (9, 1, '2018-06-19 12:44:05.206147', '', '5b2932d53cac2_IMG_6393.JPG', '', 'experience', false, 1, NULL, 4);
INSERT INTO banner VALUES (8, 1, '2018-06-19 12:38:14.647067', '', '5b2932e6b2eb2_IMG_6337.JPG', '', 'experience', false, 1, NULL, 3);
INSERT INTO banner VALUES (5, 1, '2018-06-06 20:46:30.129009', 'why <span>HENRY</span>', '5b23f9f4eec4a_banner_inside.jpg', '<p>a better option for<br />
companies and for patients</p>

<p>In 2017 the CDC issued a report which stated 36% of Americans with dental insurance have not gone to the dentist in over 1 year. Studies have shown every $1 spent on preventative dental care, saves patients $50 in late stage dental needs.</p>

<p>HENRY solves this massive healthcare problem by bringing a state-of-the-art mobile dental practice to corporate offices, providing employees with onsite in-network dental care.</p>
', 'whyhenry', false, 0, NULL, 1);
INSERT INTO banner VALUES (6, 1, '2018-06-19 12:27:54.466105', '', '5b2930cf4a11b_IMG_5979.JPG', '', 'experience', false, 1, NULL, 1);
INSERT INTO banner VALUES (7, 1, '2018-06-19 12:36:01.393794', '', '5b2930f163274_IMG_6044.JPG', '', 'experience', false, 1, NULL, 2);
INSERT INTO banner VALUES (4, 1, '2018-06-06 20:43:02.192424', 'what to expect', '5b23f6615e290_banner_inside.jpg', '<p>We&#39;ve invested a lot of thought inside each clinic to provide the greatest level of care to our patients and their results.</p>

<ul>
	<li>Cerec Scanner&nbsp;&nbsp;</li>
	<li>Apteryx Intraoral Camera</li>
	<li>Nomad Handheld X-Ray</li>
	<li>Midmark Panoramic X-Ray</li>
	<li>Apple TV featuring hbo &amp; netflix</li>
	<li>BOSE noise-cancelling headphones</li>
</ul>

<p>&nbsp;</p>
', 'experience', false, 0, NULL, 1);
INSERT INTO banner VALUES (3, 1, '2018-06-06 20:35:57.531106', 'who we are', '5b23f12940378_banner_left.jpg', '<p><strong>In 2016 HENRY&nbsp;set out to reimagine the dental experience.</strong> Recognizing the challenges employees face with busy work schedules, and complicated insurance plans. HENRY brings a state-of-the-art mobile dental practice to corporate offices, providing employees with onsite in-network dental care.</p>

<p>HENRY works with companies that value employee health and wellness, and integrates seamlessly with all insurance carriers. There is no cost to companies or employees to bring HENRY onsite.</p>

<p>By the end of 2018, HENRY will have 3 mobile practices servicing New Jersey along with a brick-and-mortar specialty practice on the top floor of the new Summit Medical building located in New Providence, NJ covering all service types including preventative, general, advanced dentistry, and oral surgery.</p>
', 'about', false, 0, NULL, 1);


--
-- Name: banner_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('banner_id_seq', 9, true);


--
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--



--
-- Name: category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('category_id_seq', 1, false);


--
-- Data for Name: config; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--

INSERT INTO config VALUES (2, 1, '2015-08-21 12:04:19.995', 'Meta Description', '​HENRY The Dentist is a state-of-the-art mobile dental practice, delivering an in-network employee benefit to corporate offices in New Jersey.');
INSERT INTO config VALUES (3, 1, '2015-08-21 12:04:19.995', 'Meta Keywords', 'HENRY The Dentist, Mobile Dental Practice, New Jersey Dentist');
INSERT INTO config VALUES (1, 1, '2015-08-21 12:04:19.995', 'Meta Title', 'HENRY The Dentist | Mobile Dental Practice | Serving New Jersey');


--
-- Name: config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('config_id_seq', 1, false);


--
-- Data for Name: contact; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--

INSERT INTO contact VALUES (1, 1, '2018-06-06 18:00:36.969629', 'Mit', 'Shah', '9874563210', 'mitaksh@emagid.com', 'emagid', 'Test message');
INSERT INTO contact VALUES (2, 1, '2018-06-07 13:20:14.506025', 'Mitaksh', 'Shah', '9733425479', 'mitaksh@student.fdu.edu', 'fairleigh dickinson university', 'j cmsslmcsklcncsnmkcsn kjnjnjdmnkjkcmbsjkcnkmn');
INSERT INTO contact VALUES (3, 1, '2018-06-20 18:34:16.410906', 'sean', 'cooper', '7187187118', 'sean@emagid.com', 'emagid', 'TEST');
INSERT INTO contact VALUES (4, 1, '2018-06-21 09:06:26.237048', 'Alex ', 'KETCHESON', '9492149338', 'ALEX@HENRYTHEDENTIST.COM', 'HENRY', 'Testing ');
INSERT INTO contact VALUES (5, 1, '2018-06-21 15:23:25.755327', 'sean', 'cooper', '7328281614', 'sean@emagid.com', 'eMagid', 'test');
INSERT INTO contact VALUES (6, 1, '2018-06-27 06:18:22.999792', 'Finley', 'Mehler', '519-645-2534', 'finley.mehler@hotmail.com', 'Finley Mehler', 'Looking to rank higher on google? Search no further - visit http://rankersparadise.com');
INSERT INTO contact VALUES (7, 1, '2018-06-27 15:15:47.966532', 'ANH', 'LE', '6099020744 or 6095804673', 'aNH.LE@FIRMENICH.COM', 'FIRMENICH INC', 'Hello

I''m Anh Le from Firmenich Inc. I''m your new patient from your recent visited to Firmenich Inc., Plainsboro, NJ (05.21.2018 to 05.24.2018)

I usually have my dental cleaning every 3 months (4 times per year).

Can you please inform me when your Team will return to Firmenich Inc., Plainsboro, NJ or I can go to your closest mobile site in New Jersey?

Thank you

Anh Le

');
INSERT INTO contact VALUES (8, 1, '2018-06-29 10:07:49.714952', 'Alexandria', 'Ketcheson', '9492149338', 'alex@henrythedentist.com', 'Henry', 'Testing mobile site. ');
INSERT INTO contact VALUES (9, 1, '2018-06-29 10:10:41.005599', 'alex', 'ketcheson', '000000000', 'alex@henrythedentist.com', 'henry', 'Testing desktop site. ');
INSERT INTO contact VALUES (10, 1, '2018-07-05 10:21:18.40071', 'Layla', 'Shields', '4438408642', 'lshields@employee1.net', 'Employee One Benefit Solutions', 'I have a few clients in the Baltimore MD area that are looking for mobile dentists for their employees to use.  Any chance you may be expanding a little further south??? 

Thanks!');
INSERT INTO contact VALUES (11, 1, '2018-07-05 11:25:11.996473', 'Robert Hatton', 'Hatton', '646', 'rhatton@mailbox.org', '-', 'Hello Dr. Bradshaw,

We can make your website rank on page one of Google for any keyword you''d like (e.g. "Orthodontists in Jersey City").

Interested in a video analysis of your online presence—recorded specifically for you—explaining how?

Regards,
Robert Hatton

');
INSERT INTO contact VALUES (12, 1, '2018-07-06 15:15:41.936232', 'Hemal', 'Chokshi', '7329803368', 'chokshih@lixilamericas.com', 'American Standard Brands', 'Hi There, 

Just wanted a bit more information on your mobile dentist program. We are hosting a health and wellness fair for our employees in September and wanted to see if you could make it out. 

If you had some material you could send us so I could go over with our benefits team that would be great. 

Thanks,
Hemal ');
INSERT INTO contact VALUES (13, 1, '2018-07-09 11:16:32.143722', 'tameka', 'Hobson', '908 868-6555', 'tameka.hobson@verizonwireless.com', '', 'Good morning

I had an exam when the mobile dentist came to the law office in my building about 2 weeks ago.  After my visit they advised me that the system was down and they were unable to give me my referral.  I called someone either the end of June or the very beginning of July but no one has called me back.  Can someone please send me my referral so I can have a copy of the visit sent over to the dentist so I can get the things they advised me to have done, taken care of.  I am at 100 Southgate Parkway Morristown NJ 07960, which is the address the mobile dentist came to.  Please call me with any questions.

Thank you');
INSERT INTO contact VALUES (14, 1, '2018-07-09 16:52:54.317457', 'Carolyn', 'seddon', '8565524806', 'cseddon@permainc.com', 'conner strong and buckelew', 'One of the small fillings that was done on the top left has come out, when will the mobile dental office be back to our company?');
INSERT INTO contact VALUES (15, 1, '2018-07-10 10:57:06.245556', 'Kiyana', 'McKenzie', '6095359491', 'kiyana.mckenzie-cw@otsuka-us.com', 'otsuka', 'I see that this will be a mobile dentist on site in July and wondered as a consultant whether you could use my insurance? 

Please advise.

Kiyana');
INSERT INTO contact VALUES (16, 1, '2018-07-10 11:39:27.049523', 'Atiya', 'Easterling ', '9733642174', 'atiya.easterling@beaconhealthoptions.com', 'Beacon Health Options', 'How do I request information or partnership to work with our company? Do you need the HRBP contact information to get started?');
INSERT INTO contact VALUES (17, 1, '2018-07-12 10:12:01.315', 'Susan', 'Williams', '610-222-6056', 'SusanDl9d@gmail.com', 'Your  Spokes Video', 'Hello, after visiting your website I wanted to let you know that we work with businesses like yours to publish a custom made promotional & marketing video that features your company online.
 
This video would tell the story of your company.
 
The 90 second video below shows you what this custom made video can do for your business.
 
To watch the video please visit:
http://www.videopromodeals.com/?=henrythedentist.com
 
Visit today, and we’ll send you a free marketing report for your company.
 
Thanks for your time.
 
Sincerely,
 
-Susan Williams 
Your – Video Spokes
860 1st Avenue
King of Prussia PA, 19406








 
If you would like to stop any further marketing messages please visit: http://stop-marketing.top/?site=henrythedentist.com



');
INSERT INTO contact VALUES (18, 1, '2018-07-13 08:17:49.523328', 'Sanjeev ', 'Yadav', '7077060205', 'seo@googlemybusiness.co', 'Seo Services', 'Hello and Good Day

I am Sanjeev Yadav, Marketing Manager with a reputable online marketing company based in India.

We can fairly quickly promote your website to the top of the search rankings with no long term contracts!

We can place your website on top of the Natural Listings on Google, Yahoo and MSN. Our Search Engine Optimization team delivers more top rankings than anyone else and we can prove it. We do not use "link farms" or "black hat" methods that Google and the other search engines frown upon and can use to de-list or ban your site. The techniques are proprietary, involving some valuable closely held trade secrets. Our prices are less than half of what other companies charge.

We would be happy to send you a proposal using the top search phrases for your area of expertise. Please contact me at your convenience so we can start saving you some money.

In order for us to respond to your request for information, please include your company’s website address (mandatory) and or phone number.

So let me know if you would like me to mail you more details or schedule a call. We''ll be pleased to serve you.
I look forward to your mail

Thanks and Regards
Sanjeev Yadav');
INSERT INTO contact VALUES (19, 1, '2018-07-17 02:07:01.847683', 'Reviews', 'me@allhatmedia4u.com', 'Reviews', 'Reviews', 'Reviews', 'Hi there, I am sure you are aware having consumer reviews posted on review sites is a great way to get even more clients and sales. 
 Difficulty is sometimes clients do not remember to leave you a glowing review. That is where we come in, i want to post your consumer reviews on the top review sites get more information at 
 http://www.allhatsmedia.info
Reviews http://www.allhatsmedia.info');
INSERT INTO contact VALUES (20, 1, '2018-07-17 02:10:06.435884', 'Randy', 'Sinclair', '416-385-3200', 'Randy@TalkWithLead.com', 'TalkWithLead.com', 'Hi,

My name is Randy and I was looking at a few different sites online and came across your site henrythedentist.com.  I must say - your website is very impressive.  I found your website on the first page of the Search Engine. 

Have you noticed that 70 percent of visitors who leave your website will never return?  In most cases, this means that 95 percent to 98 percent of your marketing efforts are going to waste, not to mention that you are losing more money in customer acquisition costs than you need to.
 
As a business person, the time and money you put into your marketing efforts is extremely valuable.  So why let it go to waste?  Our users have seen staggering improvements in conversions with insane growths of 150 percent going upwards of 785 percent. Are you ready to unlock the highest conversion revenue from each of your website visitors?  

TalkWithLead is a widget which captures a website visitor’s Name, Email address and Phone Number and then calls you immediately, so that you can talk to the Lead exactly when they are live on your website — while they''re hot!
  
Try the TalkWithLead Live Demo now to see exactly how it works.  Visit: https://www.talkwithlead.com/Contents/LiveDemo.aspx

When targeting leads, speed is essential - there is a 100x decrease in Leads when a Lead is contacted within 30 minutes vs being contacted within 5 minutes.

If you would like to talk to me about this service, please give me a call.  We do offer a 14 days free trial.  

Thanks and Best Regards,
Randy');
INSERT INTO contact VALUES (21, 1, '2018-07-17 10:01:06.682284', 'Matthew', 'Connolly', '9082175426', 'matthew.connolly@otsuka-us.com', 'Otsuka', 'Hi,

I scheduled an appointment for a cleaning on July 20 @11am.  I am also interested in getting my teeth whitened.  Can you please provide with some information on that?  If possible, I''d like to get my teeth whitened that same day.

Thanks,
Matt Connolly');
INSERT INTO contact VALUES (22, 1, '2018-07-17 11:30:59.420738', 'Monique', 'jerram', '201-306-5639', 'jerramm@sharpsec.com', 'Sharp', 'Good morning,

This is Monique Jerram. I need to cancel my 12pm appointment for invisalign consult for this afternoon at Sharp. 

Thank you,
Monique');
INSERT INTO contact VALUES (23, 1, '2018-07-17 16:32:19.643403', 'Renata', 'Farias-Rios', '732-277-0553', 'renata_farias-Rios@mtf.org', 'MTF', 'I would like to know more about how this works. I am looking for a family dentist. I know you come to my office, but I need to find a dentist for my husband as well. He is also on my insurance plan, but he cannot make it to my office. Is there a office that we can visit? How can we look up any background information on the dentist?

thanks

Renata');
INSERT INTO contact VALUES (24, 1, '2018-07-18 10:52:07.487957', 'Ajay', 'marathe', '732-261-9063', 'amarathe@verisk.com', 'verisk', 'Hello Henry,

I work for Verisk and would like to find out when next you expect to be onsite at my company?  

I need to have a cavity filled ASAP and my preference would be to utilize your services if possible. Please advise so I can decide whether it makes sense for me to register myself as a new patient at this time.

Kind regards,
Ajay Marathe');


--
-- Name: contact_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('contact_id_seq', 24, true);


--
-- Data for Name: coupon; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--



--
-- Name: coupon_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('coupon_id_seq', 1, false);


--
-- Data for Name: credit; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--



--
-- Name: credit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('credit_id_seq', 1, false);


--
-- Data for Name: faq; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO faq VALUES (2, '2018-06-06 17:22:21.467044-04', 1, 'What type of dental plans does HENRY take?', '<p>HENRY is in-network with all PPO plans. HENRY accepts family members of employees, and employees who are on a spousal plan from a different company. HENRY also accepts cash patients without insurance.</p>
', 2);
INSERT INTO faq VALUES (3, '2018-06-06 17:24:03.108799-04', 1, 'What if an employee needs follow-up care?', '<p>We are usually able to accommodate procedures on the same day or on a consecutive day that we&rsquo;re onsite. If patients require post-visit care, we have 3 options:</p>

<ol>
	<li>We can schedule an employee for a follow-up appointment for the next time we&rsquo;ll be onsite (typically every 3-4 months)</li>
	<li>We can schedule a patient when we&rsquo;re at a site nearby</li>
	<li>If there are enough employees who need an appointment sooner than 3-4 months, we can schedule a separate day to come on site and see them.</li>
</ol>
', 3);
INSERT INTO faq VALUES (4, '2018-06-06 17:24:30.010006-04', 1, 'Is all information HIPAA compliant?', '<p>Yes, all patient records are managed in a secure HIPAA compliant system</p>
', 4);
INSERT INTO faq VALUES (5, '2018-06-06 17:24:53.025483-04', 1, 'Will our employees see the same dental team?', '<p>Yes, we schedule the same team onsite for all of our visits at each corporate location</p>
', 5);
INSERT INTO faq VALUES (7, '2018-06-06 17:25:56.957476-04', 1, 'How does HENRY provide information to a patient’s general dentist?', '<p>Upon request, we will share a patient&rsquo;s information with their general dentist via a HIPAA compliant, secured email.</p>
', 7);
INSERT INTO faq VALUES (8, '2018-06-06 17:26:27.363101-04', 1, 'How do patient records get transferred or received in advance of care?', '<p>A patient will either ask their previous dentist to send us their medical records in advance of visiting HENRY, or the patient will let us know in advance and we will work with their previous dentist to obtain their records.</p>
', 8);
INSERT INTO faq VALUES (11, '2018-06-06 17:29:05.887712-04', 1, 'Does it cost incremental money to the employees?', '<p>Having HENRY at your office does not cost incremental money for the employee or the company.</p>
', 11);
INSERT INTO faq VALUES (12, '2018-06-06 17:29:25.723866-04', 1, 'Can our kids see HENRY?', '<p>HENRY can see spouses and children 12 years and older.</p>
', 12);
INSERT INTO faq VALUES (6, '2018-06-06 17:25:12.703835-04', 0, 'Is there a minimum amount of employees required for HENRY to visit my company?', '<p>There is no minimum number of patients required per day. Typically, HENRY visits offices with greater than 500 employees per site.</p>
', 6);
INSERT INTO faq VALUES (1, '2018-06-06 17:21:24.609891-04', 1, 'Is there a minimum amount of employees required for HENRY to visit my company?', '<p>There is no minimum number of patients required per day. Typically, HENRY visits offices with greater than 500 employees per site.</p>
', 1);
INSERT INTO faq VALUES (9, '2018-06-06 17:27:48.376231-04', 1, 'How are referrals handled for services outside of HENRY’s scope?', '<p>We are able to perform most treatments typically performed by a General Dentistry practice, including preventative, restorative and orthodontic services. For Specialty treatments not performed by a General Dentist, such as root canals or extractions, we refer patients to an in-network Specialist (e.g. Periodontist, Endodontist, Oral Surgeon), or HENRY&rsquo;s Specialty Practice location.</p>
', 9);
INSERT INTO faq VALUES (10, '2018-06-06 17:28:30.887012-04', 1, 'Does it cost our company to have HENRY on-site?', '<p>There is no cost to your company to have HENRY on-site providing services to your employees.</p>
', 10);


--
-- Name: faq_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('faq_id_seq', 12, true);


--
-- Data for Name: hipaa; Type: TABLE DATA; Schema: public; Owner: root
--

INSERT INTO hipaa VALUES (1, '2018-06-20 18:41:25.957551-04', 1, '<p><strong>NOTICE OF PRIVACY PRACTICES</strong></p>

<p>&nbsp;</p>

<p><strong>THIS NOTICE DESCRIBES HOW MEDICAL INFORMATION ABOUT YOU MAY BE USED AND DISCLOSED AND HOW YOU CAN GET ACCESS TO THIS INFORMATION. PLEASE REVIEW IT CAREFULLY.</strong></p>

<p>&nbsp;</p>

<p>This Notice describes the privacy practices of the medical practice operating in the state of New Jersey as HENRY The Dentist (the &ldquo;Practice&rdquo;). The Practice is required by law to maintain the privacy of medical and health information about you (&ldquo;Protected Health Information&rdquo; or &ldquo;PHI&rdquo;) and to provide you with this Notice of the Practice&rsquo;s legal duties and privacy practices with respect to PHI. When the Practice uses or discloses PHI, the Practice is required to abide by the terms of this Notice (or other notice in effect at the time of the use or disclosure).</p>

<p>&nbsp;</p>

<p><strong>How the Practice May Use and Disclose Your PHI</strong></p>

<p>The following categories describe ways the Practice may use and disclose your PHI (however, not every</p>

<p>use or disclosure in a category is listed). Your written authorization is not required before the Practice</p>

<p>may use or disclose your PHI for the purposes listed below, unless otherwise noted.</p>

<p>&nbsp;</p>

<p><strong>Treatment &ndash; </strong>The Practice uses PHI to provide treatment and other services to you &ndash; for example, to</p>

<p>diagnose and treat your injury or illness. With your consent, the Practice may disclose information about</p>

<p>you to other health care providers who are involved in your care and treatment.</p>

<p>&nbsp;</p>

<p><strong>Payment &ndash; </strong>The Practice may use, and with your consent, disclose your PHI so that the services you</p>

<p>receive may be billed and payment collected from you, an insurance company or third party payor. For</p>

<p>example, the Practice may disclose your PHI to file claims and obtain payment from your health insurer</p>

<p>for the medical services provided by the Practice. With your consent, the Practice also may disclose PHI</p>

<p>to other health care providers so that they may seek payment for services they rendered to you.</p>

<p>&nbsp;</p>

<p><strong>Health Care Operations &ndash; </strong>The Practice may use, and with your consent, disclose your PHI as necessary</p>

<p>to support the day-to-day activities and management of the Practice. For example, the Practice may use</p>

<p>and disclose your PHI for purposes of internal administration and planning, quality review and</p>

<p>improvement, legal services, etc.</p>

<p>&nbsp;</p>

<p><strong>Information Related to Your Care &ndash; </strong>The Practice may use your PHI to communicate with you about</p>

<p>products or services relating to your treatment, case management or care coordination, or alternative</p>

<p>treatments, therapies, providers or care settings. The Practice also may use your PHI to identify health-related services and products provided by the Practice that may be beneficial to your health and then</p>

<p>contact you about the services and products. The Practice will not use or disclose your PHI for purposes</p>

<p>of marketing (as defined by federal privacy laws) without first obtaining your prior authorization.</p>

<p>&nbsp;</p>

<p><strong>Communication with Family and Others &ndash;</strong>The Practice may disclose your PHI to a family member,</p>

<p>other relative, close personal friend or others who are identified by you, who are involved in your care or payment for your care, when you are present for, or otherwise available prior to, the disclosure, and you do not object to such disclosure after being given the opportunity to do so. The Practice also may disclose your PHI to such person with your verbal agreement or written consent. If you are incapacitated or in an emergency circumstance, the health care providers at the Practice may exercise their professional judgment to determine whether a disclosure is in your best interest. If the Practice discloses PHI in such event, the Practice would disclose only PHI that we believe is directly relevant to the person&rsquo;s involvement with your health care or with payment related to your health care. The Practice also may disclose your PHI in order to notify (or assist in notifying) such persons of your location, general condition or death.</p>

<p>&nbsp;</p>

<p><strong>Public Health Reporting &ndash; </strong>Your PHI may be disclosed for public health purposes as required by law. For</p>

<p>instance, the Practice is required to: (1) report cases of child abuse and neglect, elder abuse, disabled</p>

<p>persons abuse, rape, and sexual assault; (2) report medical information for the purpose of preventing or controlling disease, injury or disability; (3) report information about products and services under the</p>

<p>jurisdiction of the U.S. Food and Drug Administration; (4) report information to your insurer and/or the</p>

<p>Massachusetts Industrial Accident Board (and any party involved in the Workers&rsquo; Compensation matter)</p>

<p>as required under laws addressing work-related illnesses and injuries or workplace medical surveillance;</p>

<p>if we know or have reason to believe that you are infected with a venereal disease, to alert your</p>

<p>fianc&eacute;e, if you are engaged, or your spouse, if you are married; and (6) file a death certificate.</p>

<p>&nbsp;</p>

<p><strong>Health Oversight Activities &ndash; </strong>Your PHI may be disclosed to health oversight agencies as required by law.</p>

<p>Health oversight activities include audit, investigation, inspection, licensure or disciplinary actions, and</p>

<p>civil, criminal or administrative proceedings or actions. The Practice also is required to disclose your PHI</p>

<p>to the Secretary of Health and Human Services, upon request, to determine our compliance with the</p>

<p>Health Insurance Portability and Accountability Act.</p>

<p>&nbsp;</p>

<p><strong>Health or Safety &ndash; </strong>The Practice may use or disclose PHI to prevent or lessen a serious and imminent</p>

<p>danger to you or to others if the disclosure is to a person who is reasonably able to lessen or prevent the</p>

<p>threat, including the target of the threat.</p>

<p>&nbsp;</p>

<p><strong>Judicial and Administrative Proceedings &ndash; </strong>The Practice may disclose PHI in the course of a judicial or</p>

<p>administrative proceeding in response to a legal order or other lawful process.</p>

<p>&nbsp;</p>

<p><strong>Law Enforcement Officials &ndash; </strong>Your PHI may be disclosed to the police or other law enforcement officials</p>

<p>as required or permitted by law or in compliance with a court order or a grand jury or administrative</p>

<p>subpoena accompanied by a court order.</p>

<p>&nbsp;</p>

<p><strong>Specialized Government Functions &ndash; </strong>The Practice may use and disclose your PHI to units of the</p>

<p>government with special functions, such as the U.S. military or the U.S. Department of State under certain circumstances as required by law.</p>

<p>&nbsp;</p>

<p><strong>Ordered Examinations &ndash; </strong>The Practice may release your PHI when required to report findings from an</p>

<p>examination ordered by a court or detention facility.</p>

<p>&nbsp;</p>

<p><strong>Decedents &ndash; </strong>The Practice may disclose your PHI to a coroner or medical examiner as authorized by law.</p>

<p>&nbsp;</p>

<p><strong>Organ and Tissue Procurement &ndash; </strong>If you are an organ donor, the Practice may disclose your PHI to</p>

<p>organizations that facilitate organ, eye or tissue procurement, banking or transplantation.</p>

<p>&nbsp;</p>

<p><strong>Research &ndash; </strong>The Practice may use or disclose your PHI without your consent or authorization for research</p>

<p>purposes if an Institutional Review Board/Privacy Board approves a waiver of authorization for such use</p>

<p>or disclosure.</p>

<p>&nbsp;</p>

<p><strong>Required by Law &ndash; </strong>The Practice may use and disclose your PHI when required to do so by federal, state</p>

<p>or local law.</p>

<p>&nbsp;</p>

<p><strong>Sale of PHI, Marketing, and Other Uses and Disclosures Require Your Authorization &ndash; </strong>The Practice</p>

<p>will not sell your PHI or otherwise use or disclose it for purposes of marketing (as defined by federal</p>

<p>privacy laws) without obtaining your prior written authorization. Furthermore, use or disclosure of your</p>

<p>PHI for any purpose other than those listed above requires your written authorization or that of your</p>

<p>legal representative. We will not deny medical treatment if you do not sign the authorization.</p>

<p>Furthermore, you may revoke the authorization at any time, in writing. If you revoke your authorization,</p>

<p>we will no longer use or disclose information about you for the reason covered by your written</p>

<p>revocation.</p>

<p>&nbsp;</p>

<p><strong>Highly Confidential Information &ndash; </strong>Federal and state law require special privacy protections for certain</p>

<p>highly confidential information about you (&ldquo;Highly Confidential Information&rdquo;), including: (1) your</p>

<p>HIV/AIDS status; (2) genetic testing information; (3) substance abuse (alcohol or drug) treatment or</p>

<p>rehabilitation information; (4) confidential communications with a psychotherapist, psychologist, social</p>

<p>worker, sexual assault counselor, domestic violence counselor, or other allied mental health professional, or human services professional; (5) venereal disease information; (6) mammography records; (7) mental health community program records; (8) research involving controlled substances; (9) abortion consent form(s); and (10) family planning services. In order for us to disclose your Highly Confidential Information, we must obtain your separate, specific written consent and/or authorization unless we are otherwise permitted by law to make such disclosure. Most uses and disclosures involving Psychotherapy Notes (as defined in the Federal privacy regulations) require your authorization. If you are an emancipated minor, certain information relating to your treatment or diagnosis may be considered &ldquo;Highly Confidential Information&rdquo; and as a result will not be disclosed to your parent or guardian without your consent. Your consent is not required, however, if a physician reasonably believes your condition to be so serious that your life or limb is endangered. Under such circumstances, we may notify your parents or legal guardian of the condition, and will inform you of any such notification. Please note that if you are a parent or legal guardian of an emancipated minor, certain portions of the emancipated minor&rsquo;s medical record (or, in certain instances, the entire medical record) may not be accessible to you.</p>

<p>&nbsp;</p>

<p><strong>Your Rights Regarding Your PHI</strong></p>

<p>Although your health records are the physical property of the Practice, you have certain rights with</p>

<p>regard to the information we maintain about you in those records.</p>

<p>&nbsp;</p>

<p><strong>Notice &ndash; </strong>You have the right to receive a paper copy of this Notice (even if you have agreed to receive this Notice electronically).</p>

<p>&nbsp;</p>

<p><strong>Revoke Your Authorization &ndash; </strong>You have the right to revoke your authorization (or consent) to our</p>

<p>use/disclosure of your PHI, as long as you make your request in writing to the Practice. You can revoke</p>

<p>your authorization (or consent) for future disclosures, but not for any disclosures made prior to when you first gave your authorization (or consent).</p>

<p>&nbsp;</p>

<p><strong>Request Restrictions &ndash; </strong>You have the right to request restrictions on uses and disclosures of your PHI: (i)</p>

<p>for treatment, payment and health care operations; (ii) to individuals (such as a family member, other</p>

<p>relative, close personal friend or any other person identified by you) involved with your care or with</p>

<p>payment related to your care, or (iii) to notify or assist in the notification of such individuals regarding</p>

<p>your location and general condition. The Practice will consider your request; however, we are not</p>

<p>required to agree to the restriction (with one limited exception relating to disclosures to a health plan</p>

<p>where you pay out of pocket in full for the health care item or service). Restrictions we have agreed to do not apply to disclosures that are made mandatory by health oversight activities or law. If you wish to</p>

<p>request restrictions, please obtain a request form from, and submit the completed form to, our Practice</p>

<p>Administrator. We will send you a written response.</p>

<p>&nbsp;</p>

<p><strong>Receive Confidential Communications &ndash; </strong>You have the right to receive confidential communications of</p>

<p>your PHI from the Practice by alternative means or at alternative locations. We are required to</p>

<p>accommodate any reasonable request you make. Requests must be submitted in writing to the Practice.</p>

<p>&nbsp;</p>

<p><strong>Inspect and Copy Your PHI &ndash; </strong>You have the right to inspect and copy your PHI that we hold in a</p>

<p>designated record set. This usually includes medical records (excluding psychotherapy notes) and billing</p>

<p>records. To the extent that electronic health records are available, you have a right to an electronic copy</p>

<p>of your record, and, if you choose, to direct us to transmit a copy of the electronic health record to a</p>

<p>designated individual or entity. We may charge a fee for copies of your records. If you wish to access</p>

<p>your records, please obtain a record request form from, and submit the completed form to, our Practice</p>

<p>Administrator. Questions about fees may be directed to our Practice Administrator as well.</p>

<p>&nbsp;</p>

<p><strong>Amend Your PHI &ndash; </strong>You have a right to request that we amend your PHI if you feel that the information</p>

<p>we have is inaccurate or incomplete, as long as the Practice created the information you wish to amend.</p>

<p>We will not make changes to medical information created by another health care provider or changes that would make your medical record inaccurate or incomplete. If you wish to request an amendment to your records, please obtain an amendment request form from, and submit the completed form to, our Practice Administrator.</p>

<p>&nbsp;</p>

<p><strong>Accounting and Access Report &ndash; </strong>You have a right to receive a list of how and to whom certain of your</p>

<p>medical information has been disclosed, called an &ldquo;accounting of disclosures&rdquo;. The accounting does not</p>

<p>include disclosures of your PHI that pertain to treatment, payment or health care operations. To the</p>

<p>extent that we use or maintain your PHI in an electronic designated record set, you also have a right to</p>

<p>receive an access report indicating who has accessed such PHI (including access for purposes of</p>

<p>treatment, payment, and health care operations) during a period of time up to three years prior to the</p>

<p>date of your request. We will provide an access report relating to such disclosures made by us and all of</p>

<p>our Business Associates. If you would like to request an accounting and/or an access report, please</p>

<p>obtain a request form from, and submit the completed form to, our Practice Administrator.</p>

<p>&nbsp;</p>

<p><strong>Notice of a Breach &ndash; </strong>You have a right to receive a breach notification that complies with applicable</p>

<p>Federal and State laws and regulations in the event of a breach of your unsecured PHI.</p>

<p>&nbsp;</p>

<p><strong>Revisions to the Practice&rsquo;s Privacy Policies and Practices</strong></p>

<p>The Practice is required by law to: make sure that the privacy of your PHI is maintained, provide you with this Notice of our legal duties and privacy practices and abide by the terms of the Notice that is currently in effect. The Practice reserves the right to change its privacy policies and practices, including this Notice, and to make the new policies and practices, including the revised Notice provisions, effective for all PHI that we maintain. We will post a copy of the current Notice in our office. You may request a copy of it at any time.</p>

<p>&nbsp;</p>

<p><strong>Questions Regarding the Privacy of Your Health Information</strong></p>

<p>If you have questions regarding information contained in this Notice, if you would like to obtain</p>

<p>additional information about our privacy practices, or if you wish to exercise your rights as listed in this</p>

<p>Notice, you may contact our Practice Administrator.</p>

<p>&nbsp;</p>

<p><strong>How to File a Complaint</strong></p>

<p>If you would like to submit a comment or complaint about our privacy practices, you can do so by</p>

<p>contacting our Practice Administrator. You may also contact the Secretary of the Department of Health</p>

<p>and Human Services. You will not be penalized or otherwise retaliated against for filing a complaint.</p>

<p>Office for Civil Rights</p>

<p>Department of Health and Human Services</p>

<p>Attn: Patient Safety Act</p>

<p>200 Independence Ave., SW, Rm. 509F</p>

<p>Washington, D.C. 20201</p>

<p>Email: ocrmail.@hhs.gov</p>

<p>&nbsp;</p>

<p><strong>Practice Contact Information</strong></p>

<p>You may contact our Practice Administrator at:</p>

<p>Phone Number: 551.999.2226</p>

<p>Email Address: hello@HENRYthedentist.com.com</p>

<p>Mailing Address: 50 E Mount Pleasant Ave, Livingston NJ 07039</p>

<p><em>Effective Date</em></p>

<p><em>This revised Notice is effective as of May 1, 2018.</em></p>
');


--
-- Name: hipaa_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('hipaa_id_seq', 1, true);


--
-- Data for Name: home; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO home VALUES (2, '2018-06-06 14:31:59.008314-04', 0, '5b18289f14680_banner_left.jpg', '<p>test</p>
', '<p>test</p>
', NULL, NULL, NULL);
INSERT INTO home VALUES (1, '2018-06-06 14:17:34.957175-04', 0, '5b184a040f361_banner_inside.jpg', 'the dentist now<br />
comes to you.
<br />
(it''s about time)
', 'we believe everyone<br />
should have easy access <br />
to great dental care<br />

that''s why we built HENRY
', NULL, NULL, NULL);
INSERT INTO home VALUES (3, '2018-06-07 12:34:58.773162-04', 1, 'our promise', 'middle', '{"consistent quality and standards":"<p>We pride ourselves in creating professional relationships with employees. To achieve that, each corporate building will see the same medical staff every visit, so you won&#39;t have to worry about meeting someone new each time.<\/p>\r\n","brand experience":"<p>We know going to the dentist can be scary, so we&#39;ve added a lot of features to minimize the fear like Netflix, HBO and noise-canceling BOSE headphones.<\/p>\r\n","ease of insurance navigation":"<p>We know insurance policies are confusing, so we take it upon ourselves to map out your coverage before each visit.<\/p>\r\n"}', NULL, NULL, NULL);
INSERT INTO home VALUES (4, '2018-06-19 15:40:36.61965-04', 1, 'how it works for patients', 'right', '{"":""}', '5b295c34ac1fa_form.png', '5b295c34b1faa_dental_tool.png', '5b295c34b761e_tool.png');
INSERT INTO home VALUES (5, '2018-06-19 15:41:46.839953-04', 1, 'how it works for hr', 'left', '{"":"<p>Speak with a HENRY representative who will walk you through our scheduling process and how we integrate with your company&#39;s PPO plan.<\/p>\r\n"}', NULL, NULL, NULL);


--
-- Name: home_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('home_id_seq', 5, true);


--
-- Data for Name: jewelry; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--



--
-- Name: jewelry_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('jewelry_id_seq', 1, false);


--
-- Data for Name: media; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO media VALUES (6, '2018-06-06 15:54:37.875285-04', 1, 'Hoboken Girl', '5b295dc4b499e_hg_logo.png', 'https://hobokengirl.com/shaka-bowl-anniversary-coditums-free-coding-class-hoboken-jersey-city-news-october-22-2017/?mc_cid=aacda9d1fa&mc_eid=190e01f2e3');
INSERT INTO media VALUES (5, '2018-06-06 15:54:01.84485-04', 1, 'NJ Biz', '5b295ddd584e8_njbiz_logo.png', 'http://www.njbiz.com/article/20171023/NJBIZ01/171029945/mobile-dental-clinic-launches-in-northern-new-jersey');
INSERT INTO media VALUES (4, '2018-06-06 15:53:12.859343-04', 1, 'nj', '5b295de95c49e_nj_logo.png', 'http://www.nj.com/hudson/index.ssf/2017/10/delivery_service_promotes_employees_mobile_dentist.html');
INSERT INTO media VALUES (3, '2018-06-06 15:52:27.848785-04', 1, 'crain', '5b295df3dee4e_crain_logo.png', '');
INSERT INTO media VALUES (2, '2018-06-06 15:51:46.024748-04', 1, 'dt', '5b295dff2314a_dt_logo.png', 'http://www.dentistrytoday.com/news/industrynews/item/2512-henry-mobile-office-now-serving-professionals-in-new-jersey');
INSERT INTO media VALUES (1, '2018-06-06 15:50:51.88152-04', 1, 'Hudson', '5b295e09c78bd_hudson_logo.png', 'http://hudsonreporter.com/view/full_story/27496089/article-Dentist-on-wheels--Mobile-dental-office-serves-corporate-employees--needs-?instance=north_bergen_top_story');
INSERT INTO media VALUES (7, '2018-07-11 11:51:09.406-04', 1, 'SHRM-LI', '5b46276b3076c_shrm_logo.png', 'https://www.shrm.org/resourcesandtools/hr-topics/benefits/pages/bringing-personal-services-to-work.aspx');


--
-- Name: media_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('media_id_seq', 7, true);


--
-- Data for Name: office; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--

INSERT INTO office VALUES (104, '2017-09-26 12:55:21.798907-04', 1, '5519992226', '354 Eisenhower Pkwy Suite 1850', '', 'Livingston', '07039', 'Livingston, NJ', 317, '', 'NJ');
INSERT INTO office VALUES (105, '2017-09-26 12:55:21.801293-04', 1, '5519992226', '399 Jefferson Rd', '', 'Parsippany', '07054', 'Parsippany, NJ', 318, 'hello@HENRYthedentist.com', 'NJ');
INSERT INTO office VALUES (106, '2017-09-27 11:14:00.019763-04', 1, '5519992226', '1639 NJ-10', '', 'Parsippany', '07054', 'Parsippany, NJ', 319, 'hello@HENRYthedentist.com', 'NJ');
INSERT INTO office VALUES (107, '2017-09-27 11:14:00.022208-04', 1, '5519992226', '500 Hills Dr #300', '', 'Bedminster', '07921', 'Bedminster, NJ', 320, 'Hello@HENRYthedentist,com', 'NJ');
INSERT INTO office VALUES (108, '2017-09-27 11:14:00.02454-04', 1, '5519992226', ' 399 Interpace Pkwy', '', 'Parsippany', '07054', 'Parsippany, NJ', 321, '', 'NJ');
INSERT INTO office VALUES (120, '2018-02-20 12:04:59.397204-05', 1, '9734906600', '305 Madison Ave', '', 'Morristown', '07960', 'Morristown, NJ', 332, 'hello@henrythedentist.com', 'NJ');
INSERT INTO office VALUES (124, '2018-03-15 13:05:43.969137-04', 1, '5519992226', '401 NJ-73 #300', '', 'Marlton', '08053', 'Marlton, NJ', 336, '', 'NJ');
INSERT INTO office VALUES (126, '2018-03-29 12:18:14.295582-04', 1, '5519992226', '107 College Rd E', '', 'Princeton', '08540', 'Princeton, NJ', 338, 'hello@henrythedentist.com', 'NJ');
INSERT INTO office VALUES (101, '2017-08-21 12:08:47.541036-04', 1, '5519992226', '121 Woodcrest Rd', '', 'Cherry Hill', '08003', 'Cherry Hill, NJ', 314, 'hello@HENRYthedentist.com', 'NJ');
INSERT INTO office VALUES (112, '2017-10-30 16:14:41.741208-04', 1, '5519992226', 'Pine Brook', '', 'Pine Brook', '07058', 'Pine Brook, NJ', 325, '', 'NJ');
INSERT INTO office VALUES (121, '2018-02-27 12:00:01.606214-05', 1, '5519992226', '44 Whippany Corp Center', '', 'Morristown', '07960', 'Morristown, NJ', 333, 'hello@henrythedentist.com', 'NJ');
INSERT INTO office VALUES (114, '2017-12-12 20:27:59.68147-05', 1, '5519992226', 'TRAC Intermodal', '', 'Princeton', '08540', 'Princeton, NJ', 327, 'hello@HENRYthedentist.com', 'NJ');
INSERT INTO office VALUES (127, '2018-04-15 10:15:41.900757-04', 1, '5519992226', '100 Southgate Pkwy', '', 'Morristown', '07960', 'Morristown, NJ', 339, 'hello@henrythedentist.com', 'NJ');
INSERT INTO office VALUES (128, '2018-04-15 10:15:41.902784-04', 1, '5519992226', '615 Pavonia Ave', '', 'Jersey City', '07306', 'Jersey City, NJ', 340, 'hello@henrythedentist.com', 'NJ');
INSERT INTO office VALUES (125, '2018-03-21 13:46:04.827863-04', 1, '5519992226', '200 Crossing Bvd', '', 'Bridgewater', '08806', 'Bridgewater, NJ', 337, 'hello@henrythedentist.com', 'NJ');
INSERT INTO office VALUES (1, '2018-06-03 13:35:16.548543-04', 0, '1234567890 ext.25000', 'This Is A Free Online Calculator Which Counts The2', 'Suite C-250', 'Albion', '50005', 'Albion, IN', 350, 'rahulkhot77@yahoo.com', 'IN');
INSERT INTO office VALUES (140, '2018-06-03 13:35:16.620235-04', 0, '123456789', 'Enosis Address', '', 'Enosis City', '', 'Enosis City, AA', 355, '', 'AA');
INSERT INTO office VALUES (134, '2018-05-14 22:32:08.278194-04', 1, '5519992226', '100 Somerset Corporate Blvd', '', 'Bridgewater', '08807', 'Bridgewater, NJ', 346, '', 'NJ');
INSERT INTO office VALUES (135, '2018-05-14 22:32:08.280146-04', 1, '5519992226', '5 Giralda Farms, Dodge Dr', '', 'Madison', '07940', 'Madison, NJ', 347, 'hello@henrythedentist.com', 'NJ');
INSERT INTO office VALUES (103, '2017-09-05 11:00:22.184586-04', 0, '5519992226', '11 Luis Munoz Marin Blvd.', '', 'Jersey City', '07302', 'Jersey City, NJ', 316, 'hello@HENRYthedentist.com', 'NJ');
INSERT INTO office VALUES (115, '2018-06-03 13:35:16.585764-04', 0, '3121231231', '123 Fair Valley,Suite 2', '', 'Sarasota', '34234', 'Sarasota, FL', 352, '', 'FL');
INSERT INTO office VALUES (144, '2018-06-03 13:35:16.631821-04', 0, '6565656565 ext.22232', 'Test Data Analysis', '', 'APO', '96520', 'APO, AP', 359, '', 'AP');
INSERT INTO office VALUES (136, '2018-05-22 11:30:32.336801-04', 1, '5519992226', '250 Pehle Ave', '', 'Saddle Brook', '07663', 'Saddle Brook, NJ', 348, 'hello@henrythedentist.com', 'NJ');
INSERT INTO office VALUES (137, '2018-05-22 11:30:32.339052-04', 1, '5519992226', '23 Main St', '', 'Holmdel', '07733', 'Holmdel, NJ', 349, 'hello@HENRYthedentist.com', 'NJ');
INSERT INTO office VALUES (145, '2018-06-03 13:35:16.633792-04', 0, '6532223232 ext.23232', 'Sprint 11... Production Test Fazlul', '', 'Test Sprint 11', '96520', 'Test Sprint 11, AA', 360, 'testproductionie11@sprint11.com', 'AA');
INSERT INTO office VALUES (146, '2018-06-03 13:35:16.635845-04', 0, '6656565656 ext.56565', 'Dental Office', '', 'APO', '96520', 'APO, AP', 361, '', 'AP');
INSERT INTO office VALUES (147, '2018-06-03 13:35:16.637813-04', 0, '6656565665 ext.65655', 'Test', 'Ana', 'APO', '96520', 'APO, AP', 362, '', 'AP');
INSERT INTO office VALUES (148, '2018-06-03 13:35:16.63979-04', 0, '4545654654', 'Test Address', '', 'Empire State', '10001', 'Empire State, NY', 363, '', 'NY');
INSERT INTO office VALUES (149, '2018-06-03 13:35:16.641811-04', 0, '8765432123 ext.32323', 'Updated Address', '', 'Enosis city', '96852', 'Enosis city, AA', 364, '', 'AA');
INSERT INTO office VALUES (150, '2018-06-03 13:35:16.644117-04', 0, '7147897454', 'Planetdds', '', 'South Main', '92707', 'South Main, CA', 365, '', 'CA');
INSERT INTO office VALUES (151, '2018-06-03 13:35:16.646244-04', 0, '9874587458', '975 Easton Road', '', 'Irvine', '92606', 'Irvine, CA', 366, '', 'CA');
INSERT INTO office VALUES (154, '2018-06-03 13:35:16.652648-04', 0, '5454545454', 'Test Address', '', 'Empire State', '10001', 'Empire State, NY', 369, '', 'NY');
INSERT INTO office VALUES (155, '2018-06-03 13:35:16.654648-04', 0, '5511515151', 'Test Address', '', 'Empire State', '10001', 'Empire State, NY', 370, '', 'NY');
INSERT INTO office VALUES (156, '2018-06-03 13:35:16.656696-04', 0, '5455115151', 'Test Address', '', 'G P O', '10001', 'G P O, NY', 371, '', 'NY');
INSERT INTO office VALUES (157, '2018-06-03 13:35:16.658711-04', 0, '5155151515', 'Test Address', '', 'Empire State', '10001', 'Empire State, NY', 372, '', 'NY');
INSERT INTO office VALUES (158, '2018-06-03 13:35:16.660725-04', 0, '6666262626', 'Test Address', '', 'Empire State', '10001', 'Empire State, NY', 373, '', 'NY');
INSERT INTO office VALUES (159, '2018-06-03 13:35:16.662754-04', 0, '5432132132 ext.21212', 'Test Dhaka', 'Data', 'FPO', '96520', 'FPO, AP', 374, '', 'AP');
INSERT INTO office VALUES (160, '2018-06-03 13:35:16.664719-04', 0, '4545454545', 'Test Address', '', 'Greeley Square', '10001', 'Greeley Square, NY', 375, '', 'NY');
INSERT INTO office VALUES (161, '2018-06-03 13:35:16.666695-04', 0, '555555555', 'Test Address', '', 'Empire State', '10001', 'Empire State, NY', 376, '', 'NY');
INSERT INTO office VALUES (131, '2018-04-23 11:44:26.406243-04', 1, '5519992226', 'Florham Park', '', 'Florham Park', '07932', 'Florham Park, NJ', 343, 'hello@henrythedentist.com', 'NJ');
INSERT INTO office VALUES (132, '2018-04-23 11:44:26.408449-04', 1, '5519992226', 'Florham Park', '', 'Florham Park', '07932', 'Florham Park, NJ', 344, 'hello@henrythedentist.com', 'NJ');
INSERT INTO office VALUES (162, '2018-06-03 13:35:16.668676-04', 0, '2112465656', 'Test Address', '', 'Empire State', '10001', 'Empire State, NY', 377, '', 'NY');
INSERT INTO office VALUES (163, '2018-06-03 13:35:16.670655-04', 0, '6565656565 ext.56565', 'Test Offices', '', 'FPO', '96520', 'FPO, AP', 378, '', 'AP');
INSERT INTO office VALUES (164, '2018-06-03 13:35:16.672612-04', 0, '9874587458', 'Irvine At Demand', '', 'Irvine', '92606', 'Irvine, CA', 379, '', 'CA');
INSERT INTO office VALUES (165, '2018-06-03 13:35:16.674608-04', 0, '6556565656 ext.65656', 'Test Address', '', 'Empire State', '10001', 'Empire State, NY', 380, '', 'NY');
INSERT INTO office VALUES (166, '2018-06-03 13:35:16.676509-04', 0, '5545454545 ext.54545', 'Test Address', '', 'Empire State', '10001', 'Empire State, NY', 381, '', 'NY');
INSERT INTO office VALUES (167, '2018-06-03 13:35:16.678522-04', 0, '6565656565 ext.65656', 'Test Address', '', 'Empire State', '10001', 'Empire State, NY', 382, '', 'NY');
INSERT INTO office VALUES (168, '2018-06-03 13:35:16.680447-04', 0, '4545454545', 'Test Addresss', '', 'Empire State', '10001', 'Empire State, NY', 383, '', 'NY');
INSERT INTO office VALUES (169, '2018-06-03 13:35:16.682409-04', 0, '6545454587 ext.87878', 'Dhaka - Enosis', '', 'Hono', '96820', 'Hono, HI', 384, 'testData@ymail.com', 'HI');
INSERT INTO office VALUES (170, '2018-06-03 13:35:16.684351-04', 0, '5456456454 ext.54545', 'Test Address', '', 'APO', '96520', 'APO, AP', 385, '', 'AP');
INSERT INTO office VALUES (171, '2018-06-03 13:35:16.686394-04', 0, '3565656565 ext.65656', 'AHJSDGKh', '', 'FPO', '96520', 'FPO, AP', 386, '', 'AP');
INSERT INTO office VALUES (172, '2018-06-03 13:35:16.688331-04', 0, '1111111111', '1 Help', '', 'Costa Mesa', '92626', 'Costa Mesa, CA', 387, '', 'CA');
INSERT INTO office VALUES (173, '2018-06-03 13:35:16.690258-04', 0, '1345454545', '111 Main', '', 'Costa Mesa', '92626', 'Costa Mesa, CA', 388, '', 'CA');
INSERT INTO office VALUES (174, '2018-06-03 13:35:16.69222-04', 0, '1234564564', '123 Main', '', 'Costa Mesa', '92626', 'Costa Mesa, CA', 389, '', 'CA');
INSERT INTO office VALUES (175, '2018-06-03 13:35:16.694164-04', 0, '1111111111', 'Address 1', '', 'Irvine', '92606', 'Irvine, CA', 390, '', 'CA');
INSERT INTO office VALUES (176, '2018-06-03 13:35:16.696151-04', 0, '9989898989 ext.89898', 'Data Analysis', '', 'Dhaka', '96520', 'Dhaka, AA', 391, 'nitor.fazi@infote.com', 'AA');
INSERT INTO office VALUES (180, '2018-06-03 13:35:16.736096-04', 0, '2121212121', 'Test Address', '', 'G P O', '10001', 'G P O, NY', 392, '', 'NY');
INSERT INTO office VALUES (181, '2018-06-03 13:35:16.738692-04', 0, '5454545454 ext.45454', 'Test Enosis', '', 'FPO', '96520', 'FPO, AP', 393, '', 'AP');
INSERT INTO office VALUES (182, '2018-06-03 13:35:16.740831-04', 0, '6565656565 ext.56565', 'Data Address', '', 'FPO', '96520', 'FPO, AP', 394, 'kumar@ymail.com', 'AP');
INSERT INTO office VALUES (183, '2018-06-03 13:35:16.74296-04', 0, '5445454545', 'Test Address', 'Test Address', 'Irvine', '10001', 'Irvine, AA', 395, '', 'AA');
INSERT INTO office VALUES (184, '2018-06-03 13:35:16.745195-04', 0, '5545454545', 'Test Address', '', 'Empire State', '10001', 'Empire State, NY', 396, '', 'NY');
INSERT INTO office VALUES (185, '2018-06-03 13:35:16.747246-04', 0, '1231231231', 'Aswfdas', '', 'Empire State', '10001', 'Empire State, NY', 397, '', 'NY');
INSERT INTO office VALUES (186, '2018-06-03 13:35:16.749392-04', 0, '1231231234', 'Asdasdasdasd', '', 'Empire State', '10001', 'Empire State, NY', 398, '', 'NY');
INSERT INTO office VALUES (187, '2018-06-03 13:35:16.751358-04', 0, '1234567891', 'Asdf', '', 'Empire State', '10001', 'Empire State, NY', 399, '', 'NY');
INSERT INTO office VALUES (188, '2018-06-03 13:35:16.75331-04', 0, '1231231323', 'Asdf', '', 'Empire State', '10001', 'Empire State, NY', 400, '', 'NY');
INSERT INTO office VALUES (189, '2018-06-03 13:35:16.755285-04', 0, '1231231231', 'Asdf', '', 'Empire State', '10001', 'Empire State, NY', 401, '', 'NY');
INSERT INTO office VALUES (190, '2018-06-03 13:35:16.757438-04', 0, '123413241234123', 'Asd', '', 'Asdfasdf', '10001', 'Asdfasdf, ', 402, '', '');
INSERT INTO office VALUES (191, '2018-06-03 13:35:16.759421-04', 0, '1231231231', 'Asdf', '', 'Empire State', '10001', 'Empire State, NY', 403, '', 'NY');
INSERT INTO office VALUES (192, '2018-06-03 13:35:16.769156-04', 0, '5456454545 ext.56454', 'Test Regression Test', '', 'FPO', '96520', 'FPO, AP', 404, '', 'AP');
INSERT INTO office VALUES (193, '2018-06-03 13:35:16.771748-04', 0, '1231231234', 'Abcdefgh', '', 'Empire State', '10001', 'Empire State, NY', 405, '', 'NY');
INSERT INTO office VALUES (194, '2018-06-03 13:35:16.773994-04', 0, '1111111111', '123 Main', '', 'Costa Mesa', '92626', 'Costa Mesa, CA', 406, '', 'CA');
INSERT INTO office VALUES (195, '2018-06-03 13:35:16.776167-04', 0, '1231231234', 'Asdf', '', 'Empire State', '10001', 'Empire State, NY', 407, '', 'NY');
INSERT INTO office VALUES (196, '2018-06-03 13:35:16.77813-04', 0, '4654456465 ext.45645', 'AsdAS', '', 'FPO', '96520', 'FPO, AP', 408, '', 'AP');
INSERT INTO office VALUES (197, '2018-06-03 13:35:16.780086-04', 0, '1231231234', 'Asdf', '', 'Empire State', '10001', 'Empire State, NY', 409, '', 'NY');
INSERT INTO office VALUES (198, '2018-06-03 13:35:16.782067-04', 0, '1234123412', 'ASDF', '', 'Empire State', '10001', 'Empire State, NY', 410, '', 'NY');
INSERT INTO office VALUES (141, '2018-06-03 13:35:16.622337-04', 0, '123456789', 'Enosis Address', '', 'Enosis City', '', 'Enosis City, AA', 356, '', 'AA');
INSERT INTO office VALUES (142, '2018-06-03 13:35:16.624372-04', 0, '2333232323 ext.2010', 'Test Address', '', 'APO', '96520', 'APO, AP', 357, '', 'AP');
INSERT INTO office VALUES (143, '2018-06-03 13:35:16.629798-04', 0, '4564654654', 'Test Address', '', 'North Chatham', '121321321', 'North Chatham, NY', 358, '', 'NY');
INSERT INTO office VALUES (199, '2018-06-03 13:35:16.783991-04', 0, '0023213515', 'A Test Sample MA Address', '', 'Agawam', '01001', 'Agawam, MA', 411, '', 'MA');
INSERT INTO office VALUES (200, '2018-06-03 13:35:16.786017-04', 0, '1321321321 ext.13121', 'Asdf', '', 'New York City', '10001', 'New York City, NY', 412, '', 'NY');
INSERT INTO office VALUES (201, '2018-06-03 13:35:16.788123-04', 0, '2341234123', 'Asdf', '', 'Empire State', '10001', 'Empire State, NY', 413, '', 'NY');
INSERT INTO office VALUES (202, '2018-06-03 13:35:16.790202-04', 0, '1111111111 ext.22222', 'Asdf', 'Qwer', 'Empire State', '10001', 'Empire State, NY', 414, 'asdf@gmail.com', 'NY');
INSERT INTO office VALUES (203, '2018-06-03 13:35:16.79253-04', 0, '1111111111 ext.22222', 'Asdf', 'Qwer', 'Empire State', '10001', 'Empire State, NY', 415, 'asdf@gmail.com', 'NY');
INSERT INTO office VALUES (204, '2018-06-03 13:35:16.794571-04', 0, '1111111111 ext.22222', 'Asdf', 'Qwer', 'Empire State', '10001', 'Empire State, NY', 416, 'asdf@gmail.com', 'NY');
INSERT INTO office VALUES (205, '2018-06-03 13:35:16.796501-04', 0, '1111111111 ext.22222', 'Asdf', '', 'Empire State', '10001', 'Empire State, NY', 417, 'asd@gmail.com', 'NY');
INSERT INTO office VALUES (206, '2018-06-03 13:35:16.79843-04', 0, '1111111111 ext.22222', 'Asdf', '', 'Empire State', '10001', 'Empire State, NY', 418, '', 'NY');
INSERT INTO office VALUES (207, '2018-06-03 13:35:16.80032-04', 0, '9798798787 ext.78979', 'Hide Office', '', 'APO', '96520', 'APO, AP', 419, '', 'AP');
INSERT INTO office VALUES (208, '2018-06-03 13:35:16.802248-04', 0, '8888888888', 'Main Street LoCal', '', 'Irvine', '92620', 'Irvine, CA', 420, '', 'CA');
INSERT INTO office VALUES (209, '2018-06-03 13:35:16.804161-04', 0, '9999999999', 'Xx', '', 'Irvine', '92606', 'Irvine, CA', 421, '', 'CA');
INSERT INTO office VALUES (210, '2018-06-03 13:35:16.806062-04', 0, '1111111111', 'Asdf', '', 'Empire State', '10001', 'Empire State, NY', 422, '', 'NY');
INSERT INTO office VALUES (211, '2018-06-03 13:35:16.807943-04', 0, '1111111111 ext.22222', 'This Is Test', 'This Is Test', 'Albion', '50005', 'Albion, IA', 423, 'asdf@gmail.com', 'IA');
INSERT INTO office VALUES (100, '2017-08-21 12:08:47.538528-04', 0, '646-204-7303', '70 Greene Street', '', 'Jersey City', '07302', 'Jersey City, NJ', 313, '', 'NJ');
INSERT INTO office VALUES (152, '2018-06-03 13:35:16.648545-04', 0, '9874587458', '25 Main Street', '', 'Santa Ana', '92707', 'Santa Ana, CA', 367, '', 'CA');
INSERT INTO office VALUES (153, '2018-06-03 13:35:16.650611-04', 0, '6532321232 ext.32132', 'Mike Dental Office', '', 'FPO', '96520', 'FPO, AP', 368, 'hoque3034@gmail.com', 'AP');
INSERT INTO office VALUES (212, '2018-06-03 13:35:16.809829-04', 0, '1111111111 ext.22222', 'Like Any Other Social Media Site Facebook Has Leng', 'Like Any Other Social Media Site Facebook Has Leng', 'Washington', '20002', 'Washington, DC', 424, 'Like any other social media site Facebook has length require', 'DC');
INSERT INTO office VALUES (213, '2018-06-03 13:35:16.811844-04', 0, '9874587448', 'MAIN', '', 'Irvine', '92620', 'Irvine, CA', 425, '', 'CA');
INSERT INTO office VALUES (214, '2018-06-03 13:35:16.813789-04', 0, '1231231231', '123 Stree', '', 'Irvine', '92606', 'Irvine, CA', 426, '', 'CA');
INSERT INTO office VALUES (215, '2018-06-03 13:35:16.815724-04', 0, '9878787878', '98', '', 'Irvine', '92620', 'Irvine, CA', 427, '', 'CA');
INSERT INTO office VALUES (216, '2018-06-03 13:35:16.817689-04', 0, '1111111111', 'Asdf', '', 'Washington', '20002', 'Washington, DC', 428, '', 'DC');
INSERT INTO office VALUES (217, '2018-06-03 13:35:16.819599-04', 0, '1231231231', '1234', '', 'Irvine', '92606', 'Irvine, CA', 429, '', 'CA');
INSERT INTO office VALUES (218, '2018-06-03 13:35:16.821545-04', 0, '1111111111 ext.22222', 'Like Any Other Social Media Site Facebook Has Leng', 'Like Any Other Social Media Site Facebook Has Leng', 'Washington', '20002', 'Washington, DC', 430, 'asdf@gmail.com', 'DC');
INSERT INTO office VALUES (219, '2018-06-03 13:35:16.823515-04', 0, '1111111111 ext.22222', 'Like Any Other Social Media Site Facebook Has Leng', 'Like Any Other Social Media Site Facebook Has Leng', 'Empire State', '10001', 'Empire State, NY', 431, 'asdf@gmail.com', 'NY');
INSERT INTO office VALUES (220, '2018-06-03 13:35:16.825519-04', 0, '1111111111', 'A', 'A', 'Dhaka', '', 'Dhaka, AA', 432, '', 'AA');
INSERT INTO office VALUES (221, '2018-06-03 13:35:16.827472-04', 0, '1111111111', 'An Area Which Is Rarely Used In Facebook Is The No', '', 'Washington', '20002', 'Washington, DC', 433, '', 'DC');
INSERT INTO office VALUES (222, '2018-06-03 13:35:16.829349-04', 0, '1111111111 ext.22222', 'Test Address 1', 'Test Address 2', 'Empire State', '10001', 'Empire State, NY', 434, 'asdf@gmail.com', 'NY');
INSERT INTO office VALUES (223, '2018-06-03 13:35:16.831244-04', 0, '2222222222 ext.11111', 'Address 1', 'Address 2', 'Washington', '20002', 'Washington, DC', 435, 'asdfasdf@gmail.com', 'DC');
INSERT INTO office VALUES (224, '2018-06-03 13:35:16.833136-04', 0, '1111111111 ext.22222', 'Asdf', 'Asdf', 'Empire State', '10001', 'Empire State, NY', 436, 'asdf@gmail.com', 'NY');
INSERT INTO office VALUES (225, '2018-06-03 13:35:16.835027-04', 0, '1111111111 ext.22222', 'Asdf', 'Asdf', 'Washington', '20002', 'Washington, DC', 437, 'asdf@gmail.com', 'DC');
INSERT INTO office VALUES (226, '2018-06-03 13:35:16.837075-04', 0, '1231231231', 'Office Test 123', '', 'Irvine', '92606', 'Irvine, CA', 438, '', 'CA');
INSERT INTO office VALUES (118, '2018-01-19 11:40:35.22579-05', 1, '2026921100', ' 100 Tice Blvd Woodcliff Lake', '', 'Woodcliff Lake', '07677', 'Woodcliff Lake, NJ', 330, 'hello@henrythedentist.com', 'NJ');
INSERT INTO office VALUES (129, '2018-04-15 10:15:41.904691-04', 1, '5519992226', '250 Plainsboro Rd', '', 'Plainsboro', '08536', 'Plainsboro, NJ', 341, 'hello@henrythedentist.com', 'NJ');
INSERT INTO office VALUES (119, '2018-01-19 11:40:35.228419-05', 1, '5519992226', '545 Washington Blvd', '', 'Jersey City', '07310', 'Jersey City, NJ', 331, 'hello@henrythedentist.com', 'NJ');
INSERT INTO office VALUES (116, '2018-01-15 18:16:11.950571-05', 1, '7326610202', '125 May St', '', 'Edison', '08837', 'Edison, NJ', 328, 'hello@henrythedentist.com', 'NJ');
INSERT INTO office VALUES (117, '2018-01-15 18:16:11.952934-05', 1, '7326610780', '425 Raritan Center Pkwy', '', 'Edison', '08837', 'Edison, NJ', 329, 'hello@henrythedentist.com', 'NJ');
INSERT INTO office VALUES (110, '2017-09-27 11:14:00.029161-04', 1, '5519992226', '508 Carnegie Center,', '', 'Princeton', '08540', 'Princeton, NJ', 323, '', 'NJ');
INSERT INTO office VALUES (102, '2017-08-21 12:08:47.543288-04', 1, '5519992226', '100 Jefferson Rd', '', 'Parsippany', '07054', 'Parsippany, NJ', 315, '', 'NJ');
INSERT INTO office VALUES (130, '2018-04-15 10:15:41.909743-04', 1, '5519992226', '100 Paragon Drive', '', 'Montvale', '07645', 'Montvale, NJ', 342, 'hello@henrythedentist.com', 'NJ');
INSERT INTO office VALUES (109, '2017-09-27 11:14:00.026866-04', 1, '5519992226', '545 Washington Blvd', '', 'Jersey City', '07310', 'Jersey City, NJ', 322, '', 'NJ');
INSERT INTO office VALUES (123, '2018-03-15 13:05:43.966796-04', 1, '5519992226', 'One Tower Center Boulevard, 14th Floor', '', 'East Brunswick', '08816', 'East Brunswick, NJ', 335, 'hello@henythedentist.com', 'NJ');
INSERT INTO office VALUES (111, '2017-09-27 13:59:07.073291-04', 0, '5519992226', '11 Marin Blv', '', 'Jersey City', '00000', 'Jersey City, AA', 324, '', 'AA');
INSERT INTO office VALUES (228, '2018-06-03 13:35:16.839312-04', 0, '1232132132', '123 Main', '', 'Irvine', '92618', 'Irvine, CA', 439, '', 'CA');
INSERT INTO office VALUES (229, '2018-06-03 13:35:16.841544-04', 0, '1231231232', '123 Main', '', 'Irvine', '92618', 'Irvine, CA', 440, '', 'CA');
INSERT INTO office VALUES (230, '2018-06-03 13:35:16.843602-04', 0, '1231231231', '123 Main', '', 'Irvine', '92618', 'Irvine, CA', 441, '', 'CA');
INSERT INTO office VALUES (231, '2018-06-03 13:35:16.845493-04', 0, '3110', '1232 fef Ave', '', 'Los ANgeles', '90221', 'Los ANgeles, CA', 442, '', 'CA');
INSERT INTO office VALUES (232, '2018-06-03 13:35:16.847396-04', 0, '3110', '1232 fef Ave', '', 'Los ANgeles', '90221', 'Los ANgeles, CA', 443, '', 'CA');
INSERT INTO office VALUES (233, '2018-06-03 13:35:16.84929-04', 0, '1231231231', '123 Main', '', 'Irvine', '92618', 'Irvine, CA', 444, '', 'CA');
INSERT INTO office VALUES (234, '2018-06-03 13:35:16.851208-04', 0, '1111111111 ext.22222', 'This Is Test 1', 'This Is Test 2', 'Albion', '50005', 'Albion, IA', 445, 'asdf@gmail.com', 'IA');
INSERT INTO office VALUES (235, '2018-06-03 13:35:16.853435-04', 0, '1111111111 ext.22222', 'Private Messaging Is One Of The Main Ways That Pe1', 'Private Messaging Is One Of The Main Ways That Pe2', 'New York City', '10001', 'New York City, NY', 446, 'rahul_khot@yahoo.com', 'NY');
INSERT INTO office VALUES (236, '2018-06-03 13:35:16.855524-04', 0, '4444444444', '123 Main', '', 'Irvine', '92618', 'Irvine, CA', 447, '', 'CA');
INSERT INTO office VALUES (237, '2018-06-03 13:35:16.857557-04', 0, '1231231231', '116 Mangrove Banks', '', 'Irvine', '92620', 'Irvine, CA', 448, 'rahulkhot77@gmail.com', 'CA');
INSERT INTO office VALUES (122, '2018-02-27 12:29:43.333887-05', 0, '5519992226', '1769 Hooper Ave.', '', 'Toms River', '08753', 'Toms River, NJ', 334, '', 'NJ');
INSERT INTO office VALUES (138, '2018-06-25 15:02:06.34619-04', 1, '5519992226', '73 Mountainview Blvd', '', 'Basking Ridge', '07920', 'Basking Ridge, NJ', 455, 'hello@henrythedentist.com', 'NJ');
INSERT INTO office VALUES (113, '2017-12-12 20:27:59.679331-05', 1, '5519992226', '1 Washington Pl', '', 'Newark', '07102', 'Newark, NJ', 326, '', 'NJ');
INSERT INTO office VALUES (139, '2018-06-25 15:33:15.445314-04', 1, '5519992226', '7 Volvo Dr', '', 'Rockleigh', '07647', 'Rockleigh, NJ', 456, 'hello@henrythedentist.com', 'NJ');
INSERT INTO office VALUES (133, '2018-05-14 22:32:08.276187-04', 1, '5519992226', '445 South St', '', 'Morristown', '07960', 'Morristown, NJ', 454, 'hello@henrythedentist.com', 'NJ');


--
-- Data for Name: order; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--



--
-- Name: order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('order_id_seq', 1, false);


--
-- Data for Name: order_product_option; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--



--
-- Name: order_product_option_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('order_product_option_id_seq', 1, false);


--
-- Data for Name: order_products; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--



--
-- Name: order_products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('order_products_id_seq', 1, false);


--
-- Data for Name: page; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--

INSERT INTO page VALUES (3, 1, '2018-06-20 18:31:42.286311', 'Privacy Policy', 'privacy-policy', '<p>dwdwefwfwef</p>
', NULL, '', '', '', NULL, NULL);
INSERT INTO page VALUES (2, 0, '2018-06-07 11:25:26.153274', '', '', '', NULL, '', '', '', NULL, '{"Test":"<p>cknsc<\/p>\r\n","Test2":"<p>hjbsnkcllskc<\/p>\r\n"}');
INSERT INTO page VALUES (1, 0, '2018-06-06 12:54:16.806623', 'Test', 'test', '<p>In 2016 HENRY The Dentist set out to reimagine the dental experience. Recognizing the challenges employees face with busy work schedules, and complicated insurance plans. HENRY brings a state-of-the-art mobile dental clinic to corporate offices, providing employees with onsite in network dental care.</p>

<p><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>

<p><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; HENRY works with companies that value emplyoee health and welness, and integrates seamlessly with all insurance carriers. There is no cost to companies or employees to bring HENRY onsite.</p>

<p><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>

<p><br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; By the end of 2018, HENRY will have 3 mobile clinics servicing New Jersey along with a brick- and-mortar specality clinic, which will allow HENRY to provide a full 360&deg; patient experience covering all service types from preventative, general, and advanced dentistry.</p>
', '5b196d39509be_banner_inside.jpg', '', '', '', NULL, NULL);


--
-- Name: page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('page_id_seq', 3, true);


--
-- Data for Name: payment_profiles; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--



--
-- Name: payment_profiles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('payment_profiles_id_seq', 1, false);


--
-- Data for Name: practice; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--

INSERT INTO practice VALUES (335, '2018-03-15 13:05:43.932998', 1, 'WithumSmith+Brown - East Brunswick', 'withumsmith-brown', 'WithumSmith+Brown');
INSERT INTO practice VALUES (316, '2017-09-05 11:00:22.174707', 1, 'Training', 'training', 'Training');
INSERT INTO practice VALUES (334, '2018-02-27 12:29:43.295965', 1, 'JBJ Health Fair', 'jbj-health-fair', 'JBJ Health Fair');
INSERT INTO practice VALUES (313, '2017-08-21 12:08:47.488225', 1, 'HENRY The Dentist', 'henry-the-dentist', 'HENRY The Dentist');
INSERT INTO practice VALUES (341, '2018-04-15 10:15:41.903951', 1, 'Firmenich - Plainsboro', 'firmenich-plainsboro', 'Firmenich - Plainsboro');
INSERT INTO practice VALUES (342, '2018-04-15 10:15:41.908821', 1, 'SHARP - Montvale', 'sharp-montvale', 'SHARP - Montvale');
INSERT INTO practice VALUES (315, '2017-08-21 12:08:47.542292', 1, 'PNY Technologies - Parsippany', 'pny-technologies-parsippany', 'PNY Technologies - Parsippany');
INSERT INTO practice VALUES (345, '2018-05-14 22:32:08.264551', 1, 'Covanta/ Mercer/ Marsh/ Guy Carpenter', 'covanta-mercer-marsh-guy-carpenter', 'Covanta/ Mercer/ Marsh/ Guy Carpenter');
INSERT INTO practice VALUES (322, '2017-09-27 11:14:00.025815', 1, 'Verisk - Jersey City', 'verisk-jersey-city', 'Verisk - Jersey City');
INSERT INTO practice VALUES (323, '2017-09-27 11:14:00.028098', 1, 'Otsuka - Princeton', 'otsuka-princeton', 'Otsuka - Princeton');
INSERT INTO practice VALUES (324, '2017-09-27 13:59:07.071206', 1, 'Training Office - Jersey City', 'test-office-marin', 'Test Office Marin');
INSERT INTO practice VALUES (326, '2017-12-12 20:27:59.655336', 1, 'Audible - Newark', 'audible-newark', 'Audible- Newark');
INSERT INTO practice VALUES (328, '2018-01-15 18:16:11.927553', 1, 'MTF - 125 May St', 'mtf-125-may-st', 'MTF - 125 May St');
INSERT INTO practice VALUES (329, '2018-01-15 18:16:11.95201', 1, 'MTF - Raritan Center', 'mtf-raritan-center', 'MTF - Raritan Center');
INSERT INTO practice VALUES (330, '2018-01-19 11:40:35.179338', 1, 'Eisai - Woodcliff Lake', 'eisai-woodcliff-lake', 'Eisai- Woodcliff Lake');
INSERT INTO practice VALUES (331, '2018-01-19 11:40:35.227335', 1, 'HENRY Pop-Up - 545 Washington Blvd', 'henry-pop-up', 'HENRY Pop-Up');
INSERT INTO practice VALUES (390, '2018-06-03 13:35:16.693387', 1, 'Office A', 'office-a', 'Office A');
INSERT INTO practice VALUES (389, '2018-06-03 13:35:16.691424', 1, 'Irvine Offices 20', 'irvine-offices-20', 'Irvine Offices 20');
INSERT INTO practice VALUES (388, '2018-06-03 13:35:16.689496', 1, 'Irvine Offices 123', 'irvine-offices-123', 'Irvine Offices 123');
INSERT INTO practice VALUES (386, '2018-06-03 13:35:16.685521', 1, 'Test363', 'test363', 'Test363');
INSERT INTO practice VALUES (385, '2018-06-03 13:35:16.683576', 1, 'TestEnosis', 'testenosis', 'TestEnosis');
INSERT INTO practice VALUES (384, '2018-06-03 13:35:16.681641', 1, 'TestOfficeSprint15', 'testofficesprint15', 'TestOfficeSprint15');
INSERT INTO practice VALUES (383, '2018-06-03 13:35:16.679684', 1, 'Test Enosis Qa', 'test-enosis-qa', 'Test Enosis Qa');
INSERT INTO practice VALUES (382, '2018-06-03 13:35:16.677746', 1, 'TestProduction', 'testproduction', 'TestProduction');
INSERT INTO practice VALUES (381, '2018-06-03 13:35:16.675774', 1, 'NEWT', 'newt', 'NEWT');
INSERT INTO practice VALUES (380, '2018-06-03 13:35:16.673818', 1, 'EnosisTT', 'enosistt', 'EnosisTT');
INSERT INTO practice VALUES (378, '2018-06-03 13:35:16.669871', 1, 'TestOfficesDat', 'testofficesdat', 'TestOfficesDat');
INSERT INTO practice VALUES (377, '2018-06-03 13:35:16.667887', 1, 'TestTXRegression1', 'testtxregression1', 'TestTXRegression1');
INSERT INTO practice VALUES (376, '2018-06-03 13:35:16.665916', 1, 'Test Office Enosis 5', 'test-office-enosis-5', 'Test Office Enosis 5');
INSERT INTO practice VALUES (375, '2018-06-03 13:35:16.663927', 1, 'Test Regression', 'test-regression', 'Test Regression');
INSERT INTO practice VALUES (373, '2018-06-03 13:35:16.659917', 1, 'Test Office 3', 'test-office-3', 'Test Office 3');
INSERT INTO practice VALUES (372, '2018-06-03 13:35:16.657924', 1, 'Test Office 2', 'test-office-2', 'Test Office 2');
INSERT INTO practice VALUES (371, '2018-06-03 13:35:16.655871', 1, 'Test Office 1', 'test-office-1', 'Test Office 1');
INSERT INTO practice VALUES (370, '2018-06-03 13:35:16.653846', 1, 'Test Regression 1', 'test-regression-1', 'Test Regression 1');
INSERT INTO practice VALUES (368, '2018-06-03 13:35:16.649766', 1, 'Mike Regression Dental Office', 'mike-regression-dental-office', 'Mike Regression Dental Office');
INSERT INTO practice VALUES (367, '2018-06-03 13:35:16.647696', 1, 'Sharmila''s Dentistry', 'sharmila-s-dentistry', 'Sharmila''s Dentistry');
INSERT INTO practice VALUES (365, '2018-06-03 13:35:16.643351', 1, 'Office Solution Reach 1(Please Do Not Use)', 'office-solution-reach-1-please-do-not-use-', 'Office Solution Reach 1(Please Do Not Use)');
INSERT INTO practice VALUES (364, '2018-06-03 13:35:16.640957', 1, 'X Office', 'x-office', 'X Office');
INSERT INTO practice VALUES (363, '2018-06-03 13:35:16.639008', 1, 'Test Office Name', 'test-office-name', 'Test Office Name');
INSERT INTO practice VALUES (362, '2018-06-03 13:35:16.637019', 1, 'RegressionTestSp12', 'regressiontestsp12', 'RegressionTestSp12');
INSERT INTO practice VALUES (361, '2018-06-03 13:35:16.634994', 1, 'Test Rx Office', 'test-rx-office', 'Test Rx Office');
INSERT INTO practice VALUES (359, '2018-06-03 13:35:16.631003', 1, 'TestFazlul', 'testfazlul', 'TestFazlul');
INSERT INTO practice VALUES (358, '2018-06-03 13:35:16.625584', 1, 'Test Office In Production', 'test-office-in-production', 'Test Office In Production');
INSERT INTO practice VALUES (357, '2018-06-03 13:35:16.623528', 1, 'TestLiveO', 'testliveo', 'TestLiveO');
INSERT INTO practice VALUES (356, '2018-06-03 13:35:16.621462', 1, 'Test Automation3/30/2016 10:56:47 PM', 'test-automation3-30-2016-10-56-47-pm', 'Test Automation3/30/2016 10:56:47 PM');
INSERT INTO practice VALUES (354, '2018-06-03 13:35:16.617325', 1, 'Test Automation3/30/2016 9:12:33 PM', 'test-automation3-30-2016-9-12-33-pm', 'Test Automation3/30/2016 9:12:33 PM');
INSERT INTO practice VALUES (353, '2018-06-03 13:35:16.615223', 1, 'Test Automation3/27/2016 8:48:49 PM', 'test-automation3-27-2016-8-48-49-pm', 'Test Automation3/27/2016 8:48:49 PM');
INSERT INTO practice VALUES (352, '2018-06-03 13:35:16.584908', 1, 'Dent', 'dent', 'Dent');
INSERT INTO practice VALUES (351, '2018-06-03 13:35:16.56681', 1, 'San Diego', 'san-diego', 'San Diego');
INSERT INTO practice VALUES (350, '2018-06-03 13:35:16.515027', 1, 'Irvine Dentistry', 'irvine-dentistry', 'Irvine Dentistry');
INSERT INTO practice VALUES (448, '2018-06-03 13:35:16.856727', 1, 'Office Test1', 'office-test1', 'Office Test1');
INSERT INTO practice VALUES (447, '2018-06-03 13:35:16.854735', 1, 'DupeDocTest', 'dupedoctest', 'DupeDocTest');
INSERT INTO practice VALUES (446, '2018-06-03 13:35:16.852603', 1, '4344_Office', '4344_office', '4344_Office');
INSERT INTO practice VALUES (445, '2018-06-03 13:35:16.850452', 1, '4142_Office', '4142_office', '4142_Office');
INSERT INTO practice VALUES (444, '2018-06-03 13:35:16.848561', 1, 'The Office Part 1', 'the-office-part-1', 'The Office Part 1');
INSERT INTO practice VALUES (443, '2018-06-03 13:35:16.846656', 1, 'TestYo2', 'testyo2', 'TestYo2');
INSERT INTO practice VALUES (318, '2017-09-26 12:55:21.800233', 1, 'Pinnacle Foods - Parsippany', 'pinnacle-foods-parsippany', 'Pinnacle Foods - Parsippany');
INSERT INTO practice VALUES (319, '2017-09-27 11:14:00.00171', 1, 'Delta Dental - Parsippany', 'delta-dental-parsippany', 'Delta Dental - Parsippany');
INSERT INTO practice VALUES (320, '2017-09-27 11:14:00.021074', 1, 'Peapack Gladstone Bank - Bedminster', 'peapack-gladstone-bank-bedminste', 'Peapack Gladstone Bank - Bedminste');
INSERT INTO practice VALUES (321, '2017-09-27 11:14:00.023558', 1, 'RB - Parsippany', 'rb-parsippany', 'RB - Parsippany');
INSERT INTO practice VALUES (325, '2017-10-30 16:14:41.690847', 1, 'Pitney Bowes - Pine Brook', 'pitney-bowes', 'Pitney Bowes');
INSERT INTO practice VALUES (327, '2017-12-12 20:27:59.680631', 1, 'TRAC Intermodal - Princeton', 'trac-intermodal-princeton', 'TRAC Intermodal - Princeton');
INSERT INTO practice VALUES (332, '2018-02-20 12:04:59.372978', 1, 'Crum & Forster - Morristown', 'crum-forster-morristown', 'Crum & Forster - Morristown');
INSERT INTO practice VALUES (333, '2018-02-27 12:00:01.580435', 1, '44 Whippany Corp Center', '44-whippany-corp-center', '44 Whippany Corp Center');
INSERT INTO practice VALUES (336, '2018-03-15 13:05:43.968162', 1, 'Conner Strong - Marlton', 'conner-strong-marlton', 'Conner Strong- Marlton');
INSERT INTO practice VALUES (337, '2018-03-21 13:46:04.800348', 1, 'Synchronoss - Bridgewater', 'synchronoss', 'Synchronoss');
INSERT INTO practice VALUES (338, '2018-03-29 12:18:14.269649', 1, 'Dr Reddy''s - Princeton', 'dr-reddy-s', 'Dr Reddy''s');
INSERT INTO practice VALUES (340, '2018-04-15 10:15:41.90202', 1, 'KRE - Journal Squared', 'kre-journal-squared', 'KRE - Journal Squared');
INSERT INTO practice VALUES (343, '2018-04-23 11:44:26.377529', 1, 'Park Ave Campus - LOT 1B', 'florham-park-lot-1b', 'Florham Park- LOT 1B');
INSERT INTO practice VALUES (344, '2018-04-23 11:44:26.407556', 1, 'Park Ave Campus - LOT 5', 'florham-park-lot-5', 'Florham Park- LOT 5');
INSERT INTO practice VALUES (346, '2018-05-14 22:32:08.277416', 1, 'Allegran - Branchburg', 'allegran-branchburg', 'Allegran- Branchburg');
INSERT INTO practice VALUES (347, '2018-05-14 22:32:08.279361', 1, 'Allergan - Madison', 'allergan-madison', 'Allergan- Madison');
INSERT INTO practice VALUES (348, '2018-05-22 11:30:32.297753', 1, 'Park 80 West', 'park-80-west', 'Park 80 West');
INSERT INTO practice VALUES (349, '2018-05-22 11:30:32.338236', 1, 'Vonage - Holmdel', 'vonage-holmdel', 'Vonage - Holmdel');
INSERT INTO practice VALUES (442, '2018-06-03 13:35:16.844768', 1, 'TestYo', 'testyo', 'TestYo');
INSERT INTO practice VALUES (441, '2018-06-03 13:35:16.842833', 1, 'The New Office V3', 'the-new-office-v3', 'The New Office V3');
INSERT INTO practice VALUES (440, '2018-06-03 13:35:16.840682', 1, 'The Office New V2', 'the-office-new-v2', 'The Office New V2');
INSERT INTO practice VALUES (439, '2018-06-03 13:35:16.838337', 1, 'The New Office', 'the-new-office', 'The New Office');
INSERT INTO practice VALUES (438, '2018-06-03 13:35:16.836217', 1, 'Office Test 123', 'office-test-123', 'Office Test 123');
INSERT INTO practice VALUES (437, '2018-06-03 13:35:16.834303', 1, 'Office_40.1_EoY', 'office_40-1_eoy', 'Office_40.1_EoY');
INSERT INTO practice VALUES (436, '2018-06-03 13:35:16.832406', 1, '40.1 EoY', '40-1-eoy', '40.1 EoY');
INSERT INTO practice VALUES (435, '2018-06-03 13:35:16.830512', 1, '3940_Office', '3940_office', '3940_Office');
INSERT INTO practice VALUES (434, '2018-06-03 13:35:16.828626', 1, '3738_Office', '3738_office', '3738_Office');
INSERT INTO practice VALUES (433, '2018-06-03 13:35:16.826727', 1, 'An Area Which Is Rarely Used In Facebook Is The No', 'an-area-which-is-rarely-used-in-facebook-is-the-no', 'An Area Which Is Rarely Used In Facebook Is The No');
INSERT INTO practice VALUES (432, '2018-06-03 13:35:16.824691', 1, 'A', 'a', 'A');
INSERT INTO practice VALUES (431, '2018-06-03 13:35:16.822764', 1, '3536_Office', '3536_office', '3536_Office');
INSERT INTO practice VALUES (430, '2018-06-03 13:35:16.820779', 1, '3334_Office', '3334_office', '3334_Office');
INSERT INTO practice VALUES (429, '2018-06-03 13:35:16.818889', 1, 'New Office Version 33', 'new-office-version-33', 'New Office Version 33');
INSERT INTO practice VALUES (428, '2018-06-03 13:35:16.816931', 1, 'PEPSI', 'pepsi', 'PEPSI');
INSERT INTO practice VALUES (427, '2018-06-03 13:35:16.814964', 1, 'MyOffice', 'myoffice', 'MyOffice');
INSERT INTO practice VALUES (426, '2018-06-03 13:35:16.812995', 1, 'Office TestOffice', 'office-testoffice', 'Office TestOffice');
INSERT INTO practice VALUES (425, '2018-06-03 13:35:16.811054', 1, 'SMILY', 'smily', 'SMILY');
INSERT INTO practice VALUES (424, '2018-06-03 13:35:16.809104', 1, '3132_Office_1', '3132_office_1', '3132_Office_1');
INSERT INTO practice VALUES (423, '2018-06-03 13:35:16.80722', 1, '3132_Office', '3132_office', '3132_Office');
INSERT INTO practice VALUES (422, '2018-06-03 13:35:16.805322', 1, 'A_O', 'a_o', 'A_O');
INSERT INTO practice VALUES (421, '2018-06-03 13:35:16.803417', 1, 'Txx', 'txx', 'Txx');
INSERT INTO practice VALUES (420, '2018-06-03 13:35:16.801509', 1, 'LoCalSmile', 'localsmile', 'LoCalSmile');
INSERT INTO practice VALUES (419, '2018-06-03 13:35:16.799591', 1, 'HideIn', 'hidein', 'HideIn');
INSERT INTO practice VALUES (418, '2018-06-03 13:35:16.797668', 1, 'A_Office', 'a_office', 'A_Office');
INSERT INTO practice VALUES (417, '2018-06-03 13:35:16.795759', 1, '2930_Office_2', '2930_office_2', '2930_Office_2');
INSERT INTO practice VALUES (416, '2018-06-03 13:35:16.793827', 1, '2930_Office_1', '2930_office_1', '2930_Office_1');
INSERT INTO practice VALUES (415, '2018-06-03 13:35:16.791699', 1, '2930_Officep', '2930_officep', '2930_Officep');
INSERT INTO practice VALUES (414, '2018-06-03 13:35:16.789373', 1, '2728_Office', '2728_office', '2728_Office');
INSERT INTO practice VALUES (413, '2018-06-03 13:35:16.787297', 1, 'TestTest', 'testtest', 'TestTest');
INSERT INTO practice VALUES (412, '2018-06-03 13:35:16.785182', 1, '2526_Office', '2526_office', '2526_Office');
INSERT INTO practice VALUES (411, '2018-06-03 13:35:16.783246', 1, 'TestMAoffice', 'testmaoffice', 'TestMAoffice');
INSERT INTO practice VALUES (410, '2018-06-03 13:35:16.78127', 1, '2324_Office', '2324_office', '2324_Office');
INSERT INTO practice VALUES (409, '2018-06-03 13:35:16.779319', 1, '2122_Office', '2122_office', '2122_Office');
INSERT INTO practice VALUES (408, '2018-06-03 13:35:16.777364', 1, 'ASD', 'asd', 'ASD');
INSERT INTO practice VALUES (407, '2018-06-03 13:35:16.775361', 1, 'R_Office', 'r_office', 'R_Office');
INSERT INTO practice VALUES (406, '2018-06-03 13:35:16.773119', 1, 'Beanitos', 'beanitos', 'Beanitos');
INSERT INTO practice VALUES (405, '2018-06-03 13:35:16.770697', 1, 'New ABC Office', 'new-abc-office', 'New ABC Office');
INSERT INTO practice VALUES (404, '2018-06-03 13:35:16.760596', 1, 'Test_Regression_Sprint_19', 'test_regression_sprint_19', 'Test_Regression_Sprint_19');
INSERT INTO practice VALUES (403, '2018-06-03 13:35:16.758657', 1, 'AA_US', 'aa_us', 'AA_US');
INSERT INTO practice VALUES (402, '2018-06-03 13:35:16.756538', 1, 'A_UK', 'a_uk', 'A_UK');
INSERT INTO practice VALUES (401, '2018-06-03 13:35:16.754492', 1, 'A_US', 'a_us', 'A_US');
INSERT INTO practice VALUES (400, '2018-06-03 13:35:16.752541', 1, '11111111111', '11111111111', '11111111111');
INSERT INTO practice VALUES (399, '2018-06-03 13:35:16.750615', 1, 'AA', 'aa', 'AA');
INSERT INTO practice VALUES (398, '2018-06-03 13:35:16.748508', 1, 'A New Office', 'a-new-office', 'A New Office');
INSERT INTO practice VALUES (397, '2018-06-03 13:35:16.746425', 1, 'AAAA_New', 'aaaa_new', 'AAAA_New');
INSERT INTO practice VALUES (396, '2018-06-03 13:35:16.744308', 1, 'Test_Fattah_Wahid_1', 'test_fattah_wahid_1', 'Test_Fattah_Wahid_1');
INSERT INTO practice VALUES (395, '2018-06-03 13:35:16.742068', 1, 'Test_Wahid_Dental_2', 'test_wahid_dental_2', 'Test_Wahid_Dental_2');
INSERT INTO practice VALUES (394, '2018-06-03 13:35:16.739975', 1, 'TestXp', 'testxp', 'TestXp');
INSERT INTO practice VALUES (393, '2018-06-03 13:35:16.737735', 1, 'TestXpOffices', 'testxpoffices', 'TestXpOffices');
INSERT INTO practice VALUES (392, '2018-06-03 13:35:16.697331', 1, 'TestOfficeEnosisqa', 'testofficeenosisqa', 'TestOfficeEnosisqa');
INSERT INTO practice VALUES (391, '2018-06-03 13:35:16.695355', 1, 'TestProductionOffices', 'testproductionoffices', 'TestProductionOffices');
INSERT INTO practice VALUES (387, '2018-06-03 13:35:16.687567', 1, 'Irvine Top Dentistry', 'irvine-top-dentistry', 'Irvine Top Dentistry');
INSERT INTO practice VALUES (379, '2018-06-03 13:35:16.671825', 1, 'Office DemandForce(Please Do Not Use)', 'office-demandforce-please-do-not-use-', 'Office DemandForce(Please Do Not Use)');
INSERT INTO practice VALUES (374, '2018-06-03 13:35:16.661925', 1, 'A&A Dental Office', 'a-a-dental-office', 'A&A Dental Office');
INSERT INTO practice VALUES (369, '2018-06-03 13:35:16.651835', 1, 'Test_Regression', 'test_regression', 'Test_Regression');
INSERT INTO practice VALUES (366, '2018-06-03 13:35:16.645333', 1, 'Office Solution Reach 2(Please Do Not Use)', 'office-solution-reach-2-please-do-not-use-', 'Office Solution Reach 2(Please Do Not Use)');
INSERT INTO practice VALUES (360, '2018-06-03 13:35:16.632987', 1, 'Test Office In Productin IE 11 Sprint 11', 'test-office-in-productin-ie-11-sprint-11', 'Test Office In Productin IE 11 Sprint 11');
INSERT INTO practice VALUES (355, '2018-06-03 13:35:16.619389', 1, 'Test Automation3/30/2016 9:40:13 PM', 'test-automation3-30-2016-9-40-13-pm', 'Test Automation3/30/2016 9:40:13 PM');
INSERT INTO practice VALUES (453, '2018-06-03 18:12:07.14901', 1, 'WithumSmith+Brown - East Brunswick', 'withumsmith-brown-east-brunswick', 'WithumSmith+Brown - East Brunswick');
INSERT INTO practice VALUES (452, '2018-06-03 18:12:07.142112', 1, 'HENRY Pop-Up - 545 Washington Blvd', 'henry-pop-up-545-washington-blvd', 'HENRY Pop-Up - 545 Washington Blvd');
INSERT INTO practice VALUES (451, '2018-06-03 18:12:07.140186', 1, 'Eisai - Woodcliff Lake', 'eisai-woodcliff-lake-1', 'Eisai - Woodcliff Lake');
INSERT INTO practice VALUES (450, '2018-06-03 18:12:07.13308', 1, 'Audible - Newark', 'audible-newark-1', 'Audible - Newark');
INSERT INTO practice VALUES (449, '2018-06-03 18:12:07.112359', 1, 'Training Office - Jersey City', 'training-office-jersey-city', 'Training Office - Jersey City');
INSERT INTO practice VALUES (314, '2017-08-21 12:08:47.540042', 1, 'Pinnacle Foods - Cherry Hill', 'pinnacle-foods-cherry-hill', 'Pinnacle Foods - Cherry Hill');
INSERT INTO practice VALUES (317, '2017-09-26 12:55:21.765411', 1, 'WISS & Co. - Livingston', 'wiss-livingston', 'WISS - Livingston');
INSERT INTO practice VALUES (339, '2018-04-15 10:15:41.88143', 1, 'Porizio, Bromberg, & Newman - Morristown', 'porizio-bromberg-newman-morristown', 'Porizio, Bromberg, & Newman - Morristown');
INSERT INTO practice VALUES (457, '2018-06-25 14:58:26.613225', 1, 'Cooper University Hospital - Camden', 'cooper-university-hospital-camden', 'Cooper University Hospital - Camden');
INSERT INTO practice VALUES (456, '2018-06-25 14:58:26.603688', 1, 'Creston - Rockleigh', 'creston-rockleigh', 'Creston - Rockleigh');
INSERT INTO practice VALUES (454, '2018-06-25 14:58:26.551223', 1, 'Covanta - Morristown', 'covanta-morristown', 'Covanta - Morristown');
INSERT INTO practice VALUES (455, '2018-06-25 14:58:26.591948', 1, 'Affinity Credit Union - Basking Ridge', 'affinity-credit-union-basking-ridge', 'Affinity Credit Union - Basking Ridge');


--
-- Name: practice_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('practice_id_seq', 457, true);


--
-- Data for Name: privacy_policy; Type: TABLE DATA; Schema: public; Owner: root
--

INSERT INTO privacy_policy VALUES (1, '2018-06-20 18:39:25.161135-04', 1, '<p><strong>Privacy Policy</strong></p>

<p>This privacy policy is effective as of May 1, 2018. It describes how HENRY The Dentist handles information we learn from you on our website (www.HENRYthedentist.com) (the &ldquo;Website&rdquo; or &ldquo;Site&rdquo;) or in our clinics. By using our Website and visiting our clinics, you agree to the use, disclosure and procedures this Privacy Policy describes.</p>

<p>&nbsp;</p>

<p>If you have any questions about this policy, please contact us at hello@HENRYthedentist.com.</p>

<p>&nbsp;</p>

<p><strong>What Information do we collect?</strong></p>

<p>The information we collect depends on what you do when you visit our Website. We only collect information when you visit our Website and you choose to provide us with such information. The information we collect from you may be personally identifiable information (&ldquo;Personal Information&rdquo;), protected health information (&ldquo;Health Information&rdquo;) and non-personally identifiable information (i.e., information that cannot be used to identify who you are) (&ldquo;Non-Personal Information&rdquo;).</p>

<p>&nbsp;</p>

<p><strong>Personal Information:</strong>&nbsp;</p>

<p>We collect Personal Information you provide to us in connection with requests for materials or services made through the Website, logging information on a launch page, purchases you make on our Website, and your participation in surveys on the Website. From time to time, we may provide you the opportunity to participate in contests or in other voluntary activities through the Website. We may request personal information from you, along with other information, in order for you to participate. Participation in these activities is completely voluntary; you can elect not to disclose your personal information by not participating in the activity.</p>

<p>&nbsp;</p>

<p>The Personal Information we may collect from you includes:</p>

<ul>
	<li>Your name</li>
	<li>Any medical information relevant to your visit</li>
	<li>Your contact information, such as your phone number or email</li>
	<li>Your credit card information (although we use a third party payment processor, as described below), billing address and shipping address, if you make purchases from HENRY The Dentist. Our payment processor, that credit card issuers require to have sufficient security, stores and processes your credit card or other payment information using industry-standard security measures. By making a purchase through the Site, you give us consent to use and provide your financial information as we consider necessary to process the transaction.</li>
	<li>Live chats and customer service phone calls</li>
	<li>In-clinic surveillance video</li>
	<li>Pictures taken by our team in-clinic</li>
	<li>Survey responses to the extent you include Personal Information</li>
</ul>

<p>&nbsp;</p>

<p><strong>Health Information:</strong></p>

<p>We are required by applicable federal and state law to maintain the privacy of your Health Information. Please see our <a href="http://henry.mymagid.net/hipaa"><strong>HIPAA Privacy Statement</strong></a> for information on our privacy practices, our legal duties, and your rights concerning your Health Information. Any Health Information we collect from you on this site will be handled in accordance with the terms of our HIPAA Privacy Statement.</p>

<p>&nbsp;</p>

<p><strong>Non-Personal Information:</strong></p>

<p>We may automatically gather certain Non-Personal Information for every visitor to our site and store it in log files. We may collect and store this information on an individual basis and in aggregate, or combined, form. We may also collect both user-specific and aggregate information on what pages visitors access or visit.</p>

<p>&nbsp;</p>

<p>Non-Personal Information we collect from you may include:</p>

<ul>
	<li>The name of your internet service provider</li>
	<li>The IP address of the computer you use to access our Website</li>
	<li>The type of browser software you are using</li>
	<li>The operating system you are using</li>
	<li>The date and time you access our Website</li>
	<li>The website address, if any, that linked you to our Website</li>
	<li>The website address, if any, you leave our website and travel to</li>
	<li>Your home page customization preferences</li>
	<li>Search engine search terms and advertising clicks/actions</li>
	<li>Weblogs and other clickstream data</li>
	<li>Survey responses to the extent you do not include Personal Information</li>
	<li>Other non-personally identifiable traffic data</li>
</ul>

<p>&nbsp;</p>

<p><strong>Communications You Initiate with Us </strong></p>

<p>If you contact us in person, by phone, email, social media, or by some other means (either through our Site or through Third Party Services), we may keep a record of your contact information and correspondence for later reference to help improve our service. When we send you emails, we may track whether you open them to figure out how to deliver more delightful and helpful emails and improve our Site. We may send emails or regular mail to you in connection with your transaction or business relationship with us. We may send email to you concerning special offers, promotions, appointment reminders. If you do not wish to receive special offers or promotional mailings, you may be removed from our email or regular mail lists through the use of opt-out.</p>

<p>&nbsp;</p>

<p><strong>Third Party Services </strong><br />
Please note that this Privacy Policy does not apply to third party websites (&ldquo;Third Party Services&rdquo;) that we do not own or control such as Facebook, Twitter, Pinterest, Instagram and Google+, even if you access them through our Site. We encourage you to carefully review the privacy policies of any Third Party Services you access so that you know how they will use and share your information.</p>

<p>&nbsp;</p>

<p>This Privacy Policy applies only to information collected by this website. From time to time, this website may link you to other sites or Third Party Services that are not owned by HENRY The Dentist. We do not control the collection or use of any information, including personally-identifying information, that occurs during your visit to the Third Party Services. Further, we make no representations about the privacy policies or practices of Third Party Services, and HENRY The Dentist is not responsible for the privacy practices of Third Party Services. Be careful of disclosing any of your personally identifiable information when leaving our site. We encourage you to be aware when you leave our site and to read the privacy statements of each and every website that collects personally identifiable information.</p>

<p>&nbsp;</p>

<p><strong>Information We Automatically Collect </strong><br />
Like most websites, our Site may incorporate technology such as &ldquo;pixel tags&rdquo;, &ldquo;web beacons&rdquo;, and &ldquo;cookies&rdquo; that allow us to track the actions of users of our Website. Pixel tags, web beacons and cookies collect Non-Personal Information. &#39;Cookies&#39; allow us to provide information that is targeted to your interests and make your online experience more convenient. Cookies are small bits of electronic information that a website sends to a visitor&#39;s browser and are stored on your hard drive. We may use cookies or similar tools to learn about your preferences and Internet session information, to record information including your likely state of residence, the web pages that you access and for how long, whether you return to the Site, and whether a particular email is opened. We do not receive or record any personal information in the cookies we use and are never tied to anything that could be used to identify you. We may encode our cookies so that only we can interpret the information stored in them. Although most web browsers automatically accept cookies, you may choose to change your browser to block cookies that or to notify you when you are sent a cookie. Most cookies are &#39;session cookies,&#39; meaning that they are automatically deleted at the end of a session and you are always free to decline our cookies if your browser permits. You can still use the Site if your browser is set to reject cookies.</p>

<p>&nbsp;</p>

<p><em>Log Information.</em> Our servers automatically track certain information about you as you use our Site. This information may include the URL that you just came from, which URL you go to next, what browser you are using, and your IP address. Our site logs do generate certain kinds of non-identifying site usage data, such as the number of hits and visits to our Site. This information is used for internal purposes by technical support staff to provide better services to the public and may also be provided to others, but the statistics contain no personal information and cannot be used to gather such information.</p>

<p>&nbsp;</p>

<p><strong>How We Use the Information We Gather </strong></p>

<p>We primarily use the information we collect and store to enhance the Website and your experience with HENRY The Dentist. Except if we sell all or a portion of our business, as described below, we never rent or sell your Personal Information or Health Information. If we share your Personal Information, we do so only as described below. The sharing of Health Information is covered by our Notice of Privacy Practices.</p>

<p>&nbsp;</p>

<p><em>Internal Use to Provide Services to You.</em> We may use Personal Information to provide the services to you. Some ways we may internally use your information include:</p>

<ul>
	<li>To facilitate the sale and delivery of your purchases</li>
	<li>To contact you when necessary</li>
	<li>To respond to your comments or questions</li>
	<li>To provide you with additional information according to your preferences</li>
	<li>To customize and personalize your experience with HENRY The Dentist</li>
	<li>To generate anonymous statistics to help us improve the customer experience</li>
	<li>To make the Services easier and more convenient for you (such as by prepopulating forms when you have already provided identical information)</li>
	<li>To provide recommendations to you</li>
	<li>To send you information and marketing materials about services and products available through our Services</li>
	<li>To train our team members</li>
	<li>For our other internal business purposes</li>
</ul>

<p>&nbsp;</p>

<p><em>Non-Personal Information.</em><br />
We may share or disclose aggregated Non-Personal Information on our Site, with service providers, and/or with other persons we conduct business with. These service providers and other persons may also share with us aggregated Non-Personal Information that they have independently developed or acquired. Additionally, we may combine aggregate information from the pixel tags, web beacons and cookies we discussed above with similar data we collect from other visitors to help us improve our Services, but that aggregate, anonymous information cannot be linked back to you.</p>

<p>&nbsp;</p>

<p><em>Opting Out.</em> If you decide you don&rsquo;t want to receive email or other mail from us, you can select the &ldquo;opt out&rdquo; provision in our communications to unsubscribe. Unsubscribing will stop you from receiving most types of communication, but it may not apply to emails about orders or transactions you place through the Services or to respond to your specific request, such as password recovery. We will not share your Personal Information with third parties who wish to market other products and services to you.</p>

<p>&nbsp;</p>

<p><strong>Agents:</strong></p>

<p>We may share your personal information with employees and service providers that we engage for the purpose of processing information on our and your behalf. We will not disclose your Personal Information to unaffiliated third parties. We may, however, provide your Personal Information to our employees and service providers to enable them to perform certain services for us, including:</p>

<ul>
	<li>Order fulfillment</li>
	<li>Payment processing</li>
	<li>Website-related services such as web hosting</li>
	<li>Improvement of website-related services and features</li>
	<li>Maintenance services</li>
	<li>Distribution of advertisements and other marketing materials on our behalf</li>
</ul>

<p>Our employees and service providers may only use the Personal Information we share with for what is necessary to assist us. We require these entities to comply with appropriate confidentiality and security measures.</p>

<p>&nbsp;</p>

<p><em>Business Transfers.</em> We may choose to buy or sell assets. In these types of transactions, customer information may be transferred.</p>

<p>&nbsp;</p>

<p><em>Protection of Us and Others.</em> We reserve the right to access, read, preserve, and disclose any information that we reasonably believe is necessary to comply with law or court order; cooperate with law enforcement; enforce or apply our Terms of Use and other agreements; or protect the rights, property, or safety of HENRY The Dentist, our employees, our users, or others.</p>

<p>&nbsp;</p>

<p><em>With Your Consent.</em> Except as we&rsquo;ve stated above, we will notify you before we share your Personal Information with any third parties, and you will be able to prevent the sharing of this information.</p>

<p>&nbsp;</p>

<p><strong>We are Committed to Making Sure Your Information is Protected:</strong></p>

<p>The security of your personal information is important to us. We follow generally accepted industry standards to protect the personal information submitted to us, and to guard that information against loss, misuse or alteration. For example, we limit access to authorized employees and third parties who have agreed to follow confidentiality obligations we give them. Also, we only allow parties to access your Personal Information for permitted business purposes. When you enter sensitive and/or personally-identifying information, we encrypt transmissions involving such information using secure protocols. Please note, however, that no method of transmission over the internet, or method of electronic storage, is 100% secure. Therefore, while we use commercially-reasonable means to protect your personal information, we cannot guarantee its absolute security. Any information that you provide to us is done so entirely at your own risk.</p>

<p>&nbsp;</p>

<p><strong>Changes to Our Policy: </strong></p>

<p>From time to time, we may review this Privacy Policy to make sure it complies with applicable law and conforms to changes in our business. If our information practices change materially at some time in the future, we will post the changes to our website and revise our Policy accordingly. If we do revise this Privacy Policy, we will update the &ldquo;Effective Date&rdquo; at the top of this page so that you can tell if it has changed since your last visit. Please review this Privacy Policy regularly to ensure that you are aware of its terms. Your use of our Services constitutes your acceptance of the terms of the Privacy Policy as amended or revised by us from time to time.</p>

<p>&nbsp;</p>

<p><strong>Questions or Concerns:</strong></p>

<p>If you have any questions or believe that this site is not following its stated information policy, you please contact us at <a href="mailto:hello@HENRYthedentist.com">hello@HENRYthedentist.com</a>.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>
');


--
-- Name: privacy_policy_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('privacy_policy_id_seq', 1, true);


--
-- Data for Name: product; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--

INSERT INTO product VALUES (3, 1, '2017-07-11 18:35:23.002169', 'Fifteen', NULL, '23', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO product VALUES (4, 1, '2017-07-11 18:49:18.262943', '454', NULL, '33', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<p>33</p>
', NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO product VALUES (5, 1, '2017-07-11 18:52:03.644402', 'hooho', NULL, '554', '', '', '', '59655693e6923_dollar.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO product VALUES (0, 1, '2016-07-19 18:08:17.471233', '1.00 CTS H SI1 Round Lab Grown Diamond', '10664', '3575', NULL, NULL, NULL, NULL, 'H', NULL, 'RB', 'SI1', '1', 'IGI', '', 'Very Good', 'Excellent', 'NONE', NULL, '6500', '10299101', '6.54', '6.5', '3.85', '0.591', '0.605', 'EXTREMELY THIN TO THICK, FACETED', 'NONE', NULL, 'Lab Created', '', '10299101', 'http://www.washingtondiamondscorp.com/certificates/LG10299101.pdf', '10664', 0.00, NULL, 1, 0, 'Israel', 0);
INSERT INTO product VALUES (1, 1, '2017-07-11 13:03:40.998839', 'Question Mark Cut', NULL, '44.33', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO product VALUES (2, 1, '2017-07-11 15:42:07.545583', 'mark', NULL, '5', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, 0, NULL, NULL);


--
-- Data for Name: product_categories; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--



--
-- Name: product_categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('product_categories_id_seq', 1, false);


--
-- Name: product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('product_id_seq', 5, true);


--
-- Data for Name: product_images; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--



--
-- Name: product_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('product_images_id_seq', 1, false);


--
-- Data for Name: product_map; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--



--
-- Name: product_map_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('product_map_id_seq', 1, false);


--
-- Data for Name: quotes; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO quotes VALUES (1, '2018-06-06 16:27:17.787916-04', 1, '<p><strong>HENRY</strong>&nbsp;The Dentist is for dentistry what Uber is for taxis, Warby Parker is for eyeglasses and Amazon is for commerce. A disruptor that&#39;s completely changing the game and fully improving the consumer experience. I got a checkup day one and fixed two cavities day two. The practice is really chic, clean and extremely convenient (I was back to my desk within an hour both times).&nbsp;</p>

<p>&nbsp;</p>

<p><strong>-Caroline L.&nbsp;</strong></p>
', '');


--
-- Name: quotes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('quotes_id_seq', 1, true);


--
-- Data for Name: register_request; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--



--
-- Name: register_request_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('register_request_id_seq', 1, false);


--
-- Data for Name: reviews; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO reviews VALUES (2, '2018-06-07 03:00:14.003679-04', 1, 'Tamara', '<p>I had an AMAZING experience with Henry the Dentist and would love to share! I don&rsquo;t actually enjoy going to the dentist, especially if I have to take off from work or wake up early on a Saturday to go. Henry the Dentist saves the day! They literally drive to your office while you&rsquo;re working. I easily scheduled my appointment through a link sent to my email from HR; Time slots were available all day so it will work whether you have a fixed lunch or flexible schedule.</p>
');
INSERT INTO reviews VALUES (1, '2018-06-07 02:59:02.753006-04', 1, 'Marcell S.', '<p>I had an AMAZING experience with Henry the Dentist and would love to share! I don&rsquo;t actually enjoy going to the dentist, especially if I have to take off from work or wake up early on a Saturday to go. Henry the Dentist saves the day! They literally drive to your office while you&rsquo;re working. I easily scheduled my appointment through a link sent to my email from HR; Time slots were available all day so it will work whether you have a fixed lunch or flexible schedule.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>
');


--
-- Name: reviews_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('reviews_id_seq', 2, true);


--
-- Data for Name: ring; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--



--
-- Name: ring_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('ring_id_seq', 1, false);


--
-- Data for Name: ring_material; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--



--
-- Name: ring_material_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('ring_material_id_seq', 1, false);


--
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--

INSERT INTO role VALUES (1, 1, '2015-08-21 12:05:07.458', 'admin');
INSERT INTO role VALUES (3, 1, '2015-08-21 12:05:07.458', 'provider');
INSERT INTO role VALUES (2, 1, '2015-08-21 12:05:07.458', 'customer');
INSERT INTO role VALUES (4, 1, '2016-03-07 18:47:03.590473', 'wholesale');


--
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('role_id_seq', 1, true);


--
-- Data for Name: shipping_method; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--



--
-- Name: shipping_method_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('shipping_method_id_seq', 1, false);


--
-- Data for Name: team; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO team VALUES (1, '2018-06-19 14:00:00.596867-04', 1, 'Justin Joffe ', 'Founder, CEO ', '5b2946eb6cb31_crop_7_mini.jpg', 'Justin Joffe is the CEO and visionary behind HENRY. He may not be a dentist, but he has a great smile and tons of experience behind building successful brands! Since graduating from Harvard Business School, Justin spent the last 10 years building reputable companies in the US and Canada, most recently serving as CEO at Hudson Blvd. Group building a national portfolio of retail brands. Justin brings his passion for providing unmatched client experience and has thought through every element in the HENRY practices. Justin believes in community involvement and giving back, which is why for the last 4 years he’s challenged himself to complete a 100 mile bike ride to raise money for SMILOW Cancer Hospital, and sits as an advisor on several charitable organizations. HENRY is his crowning achievement!');
INSERT INTO team VALUES (5, '2018-06-19 14:28:05.031691-04', 1, 'Dr. Rafael Rodriguez', 'Associate Dentist HENRY I ', '5b294c1c1d8da_crop_15_mini.jpg', 'After studying at NYU School of Dentistry and a two-year residency at Woodhul Hospital, Dr. Rafael D. Rodriguez first began practicing as a professional Dentist establishing his office in the Bronx, NY. Extensive medical knowledge combined with an ongoing curiosity to learn about the latest trends are what define his success. Dr. Rodriguez has been married to His wife, Maria for 22 years and they have a daughter who is currently attending college. Every year Dr. Rodriguez and his family promote a fundraising event called “Stand 4 A Child” providing school supplies to children in need in the Dominican Republic.  He is also involved annually in a summer health fair festival at Gospel Tabernacle in North Bergen''s charity outreach offering dental services to the community and congregants.');
INSERT INTO team VALUES (4, '2018-06-19 14:26:03.149045-04', 1, 'Alexandria Ketcheson', 'Director of Brand and Marketing ', '5b294abb324e8_crop_5_mini.jpg', 'Before joining HENRY, Alex spent the last 7 years at Drybar, one of the country’s premier blowout bars. Her most recent role was in their Marketing department as the Director of Events and Brand Partnerships, overseeing events in 100 locations across the country. Alex brings immense knowledge and pride around building a brand’s voice and industry credibility. She’s enthusiastic about entering into the healthcare space and setting a high-bar on patient experience in the industry (which she feels needs a real boost)! In her free time, you will see her running along the Hudson or jet-setting around the world. Oh, and she’s the wife to HENRY’s founder, Justin. ');
INSERT INTO team VALUES (3, '2018-06-19 14:13:07.157725-04', 1, 'Michael Prenininger', 'Director of Business Development ', '5b2947b33e8b0_crop_11_mini.jpg', 'Michael joins the HENRY team with an extensive background in the pharmaceutical industry. He most recently worked at CDM Advertising Agency, where he helped Fortune 500 clients improve their brand strategy and profitability. He''s passionate about building relationships, hails from Texas and comes from a family that has worked in healthcare for generations - helping people is part of his DNA. Michael''s enthusiasm, energy (and massive network!) made him the obvious choice to head HENRY''s business development. He also wins the award for being the most fit HENRY employee, enjoying Crossfit classes and competing in local competitions. Michael loves spending time with his wife, Devin and feisty Scottish Terrier, General Patton.');
INSERT INTO team VALUES (2, '2018-06-19 14:03:44.491394-04', 1, 'Dr. Jeffery Rappaport ', 'Dental Director', '5b2946359095d_crop_4_mini.jpg', 'Dr. Rappaport is HENRY''s Dental Director, armed with a Doctorate of Dental Surgery from New York University. Jeff brings years of experience at private practices throughout New York and New Jersey. His obsession for elevated standards and patient care led him to join the HENRY mission and bring cutting edge dental technology and physicians to each practice. As a busy father of two, Jeff enjoys keeping an active lifestyle and is an avid skier. In the summer you''ll find Jeff and his wife Michelle (who happens to be an orthodontist) at their home in the Hamptons. ');
INSERT INTO team VALUES (6, '2018-06-19 14:29:02.526289-04', 1, 'Dr. Ayanna Bradshaw-Sydnor ', 'Associate Dentist HENRY II ', '5b2be1994ee4b_Bradshaw_headshot_mini.jpg', 'Dr. Ayanna Bradshaw is originally from Greensboro, North Carolina, which created her very charming southern accent! Dr. Ayanna Bradshaw is not short of education, she completed her bachelor of science degree from Florida Agricultural and Mechanical University in Florida, then went on to complete a fellowship at the Centers for Disease Control as she pursued a Masters of Public Health from Emory University.   During her time in Atlanta, Dr. Bradshaw discovered her passion for dentistry after shadowing multiple health professionals and being inspired by a pediatric dentist. Dr. Bradshaw earned her dental degree from the University of North Carolina at Chapel Hill in 2009 and completed a general practice residency with Bronx Lebanon Hospital Center in Bronx, New York. Dr. Bradshaw has worked in New Jersey for the past five years as a General Dentist treating adults and children, and is a member of the American Dental Association, New Jersey Dental Association, and Essex County Dental Society. She enjoys spending time with her husband and daughter, visiting with family and friends, traveling, and decorating for special events.');


--
-- Name: team_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('team_id_seq', 6, true);


--
-- Data for Name: transaction; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--



--
-- Name: transaction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('transaction_id_seq', 1, false);


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--

INSERT INTO "user" VALUES (611, 0, '2017-07-10 18:24:19.194598', 'g@emagid.com', 'ced30dfe12ef0f72a13aeb1c133a8cced10091a8d6d3854762b204fc4eb7f954', '8b6db5332a24cff12695201243fc2008', 'emagid', 'tsst', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "user" VALUES (612, 0, '2017-07-10 18:29:16.728938', 'foranigan@gmail.com', 'adaa19f2d66248ecdfa2770a1ac2dc65db70b69d800041216eb1bff138f43c59', '2455628f40c7154af2734f15e9bd6fd8', 'emagid', 'testt', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "user" VALUES (613, 1, '2017-07-11 11:45:25.970299', 'freeze@gothcorp.com', 'bf6ec5f362b83ecdc51c1ab69e232f7d4ef6193ab82a0032030f73cb7e7839d8', '2c854513eecf46e00b9e4b18f1b446d3', 'Victor', 'Fries', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 86, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "user" VALUES (616, 1, '2017-07-12 10:27:37.297147', 'faake@emagid.com', NULL, NULL, 'Not', 'Here', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO "user" VALUES (617, 1, '2017-07-12 10:35:11.816144', 'Fake@emagid.com', NULL, NULL, 'Hero', 'her', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO "user" VALUES (610, 0, '2017-07-10 18:17:43.065593', 'Garrett@emagid.com', '6e16c7b60c5ca9340478cf05387b8056790e39bed22dd0bdd911936774ab241f', '8f2549a587326f0aebe2eb2e9ef03631', 'emagid', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "user" VALUES (618, 0, '2017-07-12 11:03:53.572623', 'Garrett@emagid.com', 'd27d3c562cea00dc11f0e8232fc80c7282b97fd72d30b22d653ba85718e4b901', '3a5af5f6385f15add30045281c50f4c8', 'Simon', 'eMagid', NULL, '6464609362', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93, NULL, '', '123 fake st', NULL, NULL);
INSERT INTO "user" VALUES (615, 0, '2017-07-11 21:20:03.003272', 'garrett@emagid.com', 'c0e2825aaf43d94b398ae2981e0a4fa127bcb4e0936a92db1ea829506bb40e35', '7779154e76e6c4c1681cf227cc4036f0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 89, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "user" VALUES (619, 0, '2017-07-12 11:05:18.530482', 'garrett@emagid.com', NULL, NULL, 'Haro', 'Brimly', NULL, '342423', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 95, NULL, '', '123 fake st', NULL, NULL);
INSERT INTO "user" VALUES (620, 0, '2017-07-12 11:06:26.919394', 'garrett@emagid.com', NULL, NULL, 'Haro', 'eMagid', NULL, '6464609362', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 97, NULL, 'uh', '123 fake st', NULL, NULL);
INSERT INTO "user" VALUES (621, 0, '2017-07-12 11:08:37.473861', 'garrett@emagid.com', NULL, NULL, 'Haro', 'Brimly', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 98, NULL, '', '123 fake st', NULL, NULL);
INSERT INTO "user" VALUES (630, 1, '2017-07-12 20:05:14.363418', 'garrett@emagid.com', '3630b732424e91ffbbf88ffa8636e5d707086353f68171d2580f50e0d09512fc', '72691b7c0d0941506449c5bc4aa56c5d', '', 'filterface', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 110, NULL, NULL, NULL, NULL, 'Organon');
INSERT INTO "user" VALUES (623, 0, '2017-07-12 12:47:29.832172', 'fake@emagid.com', 'd6bcd465781aa4166b715ce0477645113a8999964317aae6ceb02184348a1cdf', '42703406fab4fc105af8703185aef81c', 'Juro', 'eMagid', NULL, '6464609362', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 100, NULL, 'uh', '123 fake st', 0, NULL);
INSERT INTO "user" VALUES (624, 1, '2017-07-12 13:14:23.279456', 'fake@emagid.com', '7d121af73c9538a952669583b1d02aceaaaf728663356928cac2cc3b3b91c815', '6e49243625bdb103c307a5b745c7458f', 'Juro', 'eMagid', NULL, '6464609362', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, NULL, 'uh', '123 fake st', 0, NULL);
INSERT INTO "user" VALUES (622, 0, '2017-07-12 12:20:47.350516', 'garrett@emagid.com', '0060d36911e686b125b54f4f776a84fbe4f030d433bd4e4f1b612e4141de857c', '31147c8d2c339dc950ed3ca76c3bcdae', 'FARKER TARKER', 'eMagid', NULL, '6464609362', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 99, NULL, '', '123 fake st', 0, NULL);
INSERT INTO "user" VALUES (626, 1, '2017-07-12 15:08:19.652474', 'faaake@emagid.com', '8ea8ef7359d84f30225ed9b63e0d989ca91c1fe642dd86127394111a6aa9812e', '9ff0d5eebda0575778eff09ff50f9e0b', 'Holton', 'Seller', NULL, '6464609362', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 105, NULL, '', '105 Fake Town Lane', 0, NULL);
INSERT INTO "user" VALUES (627, 1, '2017-07-12 15:26:21.673362', 'garrettfake@emagid.com', '10f21cb56dff6b7a6bd4e26e3bc3fe7ee3e795a85783af4d68952e40fe98cd14', '67513e27b22a58b57b6654ee962f008d', 'Haro', 'eMagid', NULL, '6464609362', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 107, NULL, 'uh', '123 fake st', 0, NULL);
INSERT INTO "user" VALUES (628, 1, '2017-07-12 15:27:16.032472', 'fakegarrett@emagid.com', '92b988ca29c93972007fd3962f826bdf2b9098bb242156ccd13b71e98a236ce7', 'c0330e11b8a5b92630e1b5da121ad5b6', 'Haro', 'eMagid', NULL, '6464609362', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 108, NULL, 'uh', '123 fake st', 0, NULL);
INSERT INTO "user" VALUES (614, 1, '2017-07-11 16:26:53.625218', 'simon@emagid.com', 'ca3b83fbb7c9cb123863f5d79698cbd2de1a94cb567f9455b3208d54134d13cf', 'fb90ec1d5bcce6d1e4f4a1a3b4465ae8', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Unduly');
INSERT INTO "user" VALUES (625, 1, '2017-07-12 13:18:20.615031', 'garret@emagid.com', '0fe25b9b465c67e0fb9a614ca3f4596d752e59452bc2b5bd5ecea0cb70dc7b82', 'ae60b6748f66aefe26d0d860dea874c0', 'Simon', 'eMagid', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO "user" VALUES (629, 1, '2017-07-12 19:22:09.046546', 'GREAK@emagid.com', '1044293ce5bdbd9da7502181ec78f7285100cd208e43267eac888246d33fd3d1', '66fb7dc6c175de7c740cfbc5dc665c63', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'filterface');


--
-- Data for Name: user_favorite; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--



--
-- Name: user_favorite_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('user_favorite_id_seq', 1, false);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('user_id_seq', 630, true);


--
-- Data for Name: user_roles; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--

INSERT INTO user_roles VALUES (51, 1, '2015-09-15 16:01:06.154', 74, 2);
INSERT INTO user_roles VALUES (52, 1, '2015-09-16 11:24:19.274', 75, 2);
INSERT INTO user_roles VALUES (53, 1, '2015-11-06 12:41:13.428782', 81, 2);
INSERT INTO user_roles VALUES (54, 1, '2015-11-12 12:40:52.478316', 82, 2);
INSERT INTO user_roles VALUES (55, 1, '2015-11-13 19:26:38.332046', 83, 2);
INSERT INTO user_roles VALUES (56, 1, '2015-11-16 11:01:01.941693', 84, 2);
INSERT INTO user_roles VALUES (57, 1, '2016-02-02 11:25:49.98874', 85, 2);
INSERT INTO user_roles VALUES (58, 1, '2016-03-25 15:02:10.600641', 86, 2);
INSERT INTO user_roles VALUES (59, 1, '2016-04-11 17:22:11.185447', 87, 4);
INSERT INTO user_roles VALUES (60, 1, '2016-04-12 12:17:03.862326', 88, 4);
INSERT INTO user_roles VALUES (61, 1, '2016-04-13 12:40:10.046119', 89, 4);
INSERT INTO user_roles VALUES (62, 1, '2016-04-14 15:56:40.024307', 90, 2);
INSERT INTO user_roles VALUES (63, 1, '2016-04-22 12:32:14.365196', 91, 2);
INSERT INTO user_roles VALUES (64, 1, '2016-04-22 12:37:29.838462', 92, 4);
INSERT INTO user_roles VALUES (65, 1, '2016-04-28 09:47:35.131271', 93, 4);
INSERT INTO user_roles VALUES (66, 1, '2016-05-02 18:00:37.926248', 94, 2);
INSERT INTO user_roles VALUES (67, 1, '2016-05-03 12:33:17.670198', 95, 2);
INSERT INTO user_roles VALUES (68, 1, '2016-05-03 12:34:26.294319', 96, 2);
INSERT INTO user_roles VALUES (69, 1, '2016-05-03 12:36:44.372915', 97, 2);
INSERT INTO user_roles VALUES (70, 1, '2016-05-03 12:40:45.63819', 98, 2);
INSERT INTO user_roles VALUES (71, 1, '2016-05-03 12:42:29.843719', 99, 2);
INSERT INTO user_roles VALUES (72, 1, '2016-05-03 15:39:33.41612', 100, 2);
INSERT INTO user_roles VALUES (73, 1, '2016-05-03 15:40:21.979051', 101, 2);
INSERT INTO user_roles VALUES (74, 1, '2016-05-03 15:41:55.147192', 102, 2);
INSERT INTO user_roles VALUES (75, 1, '2016-05-03 17:10:01.343247', 103, 2);
INSERT INTO user_roles VALUES (76, 1, '2016-05-03 17:15:20.562538', 104, 2);
INSERT INTO user_roles VALUES (77, 1, '2016-05-03 17:20:13.358521', 105, 2);
INSERT INTO user_roles VALUES (78, 1, '2016-05-03 17:21:39.358722', 106, 2);
INSERT INTO user_roles VALUES (79, 1, '2016-05-04 04:47:29.838569', 107, 2);
INSERT INTO user_roles VALUES (80, 1, '2016-05-11 08:46:31.365002', 108, 2);
INSERT INTO user_roles VALUES (81, 1, '2016-05-12 11:38:44.408103', 109, 4);
INSERT INTO user_roles VALUES (82, 1, '2016-05-13 09:46:17.868978', 110, 4);
INSERT INTO user_roles VALUES (83, 1, '2016-05-13 20:32:34.123945', 111, 2);
INSERT INTO user_roles VALUES (84, 1, '2016-05-17 18:11:07.271271', 112, 2);
INSERT INTO user_roles VALUES (85, 1, '2016-05-21 16:54:42.144201', 113, 4);
INSERT INTO user_roles VALUES (86, 1, '2016-05-23 13:16:28.887928', 114, 4);
INSERT INTO user_roles VALUES (87, 1, '2016-05-23 14:14:14.814685', 115, 4);
INSERT INTO user_roles VALUES (88, 1, '2016-05-23 21:18:21.832431', 116, 2);
INSERT INTO user_roles VALUES (89, 1, '2016-05-24 15:13:21.394086', 117, 4);
INSERT INTO user_roles VALUES (90, 1, '2016-05-24 15:16:49.983825', 118, 2);
INSERT INTO user_roles VALUES (91, 1, '2016-05-25 14:00:02.016356', 119, 2);
INSERT INTO user_roles VALUES (92, 1, '2016-05-28 04:26:19.26212', 120, 2);
INSERT INTO user_roles VALUES (93, 1, '2016-05-30 09:33:44.337989', 121, 2);
INSERT INTO user_roles VALUES (94, 1, '2016-05-30 11:42:02.744245', 122, 2);
INSERT INTO user_roles VALUES (95, 1, '2016-05-30 13:46:25.330518', 123, 2);
INSERT INTO user_roles VALUES (96, 1, '2016-05-30 19:31:27.014349', 124, 2);
INSERT INTO user_roles VALUES (97, 1, '2016-05-30 20:08:35.760792', 125, 2);
INSERT INTO user_roles VALUES (98, 1, '2016-06-03 15:39:44.720792', 126, 4);
INSERT INTO user_roles VALUES (99, 1, '2016-06-08 12:41:08.460891', 127, 4);
INSERT INTO user_roles VALUES (100, 1, '2016-06-08 14:42:12.819399', 128, 4);
INSERT INTO user_roles VALUES (101, 1, '2016-06-08 14:48:56.685618', 129, 2);
INSERT INTO user_roles VALUES (102, 1, '2016-06-08 18:03:47.428855', 130, 4);
INSERT INTO user_roles VALUES (103, 1, '2016-06-08 19:05:18.232028', 131, 4);
INSERT INTO user_roles VALUES (104, 1, '2016-06-08 19:37:54.587433', 132, 4);
INSERT INTO user_roles VALUES (105, 1, '2016-06-09 07:27:37.302694', 133, 2);
INSERT INTO user_roles VALUES (106, 1, '2016-06-09 10:52:43.524964', 134, 4);
INSERT INTO user_roles VALUES (107, 1, '2016-06-12 11:35:24.092019', 135, 2);
INSERT INTO user_roles VALUES (108, 1, '2016-06-13 16:22:51.619886', 136, 2);
INSERT INTO user_roles VALUES (109, 1, '2016-06-14 11:48:03.034502', 137, 2);
INSERT INTO user_roles VALUES (110, 1, '2016-06-14 11:48:19.125745', 138, 2);
INSERT INTO user_roles VALUES (111, 1, '2016-06-14 12:06:29.44825', 139, 2);
INSERT INTO user_roles VALUES (112, 1, '2016-06-14 12:14:55.640761', 140, 2);
INSERT INTO user_roles VALUES (113, 1, '2016-06-14 12:27:23.62754', 141, 2);
INSERT INTO user_roles VALUES (114, 1, '2016-06-14 12:41:23.30926', 142, 2);
INSERT INTO user_roles VALUES (115, 1, '2016-06-14 12:42:47.12658', 143, 2);
INSERT INTO user_roles VALUES (116, 1, '2016-06-14 13:27:37.548418', 144, 2);
INSERT INTO user_roles VALUES (117, 1, '2016-06-14 14:00:52.224856', 145, 2);
INSERT INTO user_roles VALUES (118, 1, '2016-06-14 14:06:35.52136', 146, 2);
INSERT INTO user_roles VALUES (119, 1, '2016-06-14 14:13:46.456503', 147, 2);
INSERT INTO user_roles VALUES (120, 1, '2016-06-14 14:22:20.986674', 148, 2);
INSERT INTO user_roles VALUES (121, 1, '2016-06-14 14:31:27.478948', 149, 2);
INSERT INTO user_roles VALUES (122, 1, '2016-06-14 14:32:33.400433', 150, 2);
INSERT INTO user_roles VALUES (123, 1, '2016-06-14 14:33:10.251918', 151, 2);
INSERT INTO user_roles VALUES (124, 1, '2016-06-14 14:33:57.757026', 152, 2);
INSERT INTO user_roles VALUES (125, 1, '2016-06-14 14:35:54.367403', 153, 2);
INSERT INTO user_roles VALUES (126, 1, '2016-06-14 14:49:11.466086', 154, 2);
INSERT INTO user_roles VALUES (127, 1, '2016-06-14 14:55:30.556398', 155, 2);
INSERT INTO user_roles VALUES (128, 1, '2016-06-14 14:57:41.245471', 156, 4);
INSERT INTO user_roles VALUES (129, 1, '2016-06-14 15:04:36.959324', 157, 2);
INSERT INTO user_roles VALUES (130, 1, '2016-06-14 15:05:50.674369', 158, 2);
INSERT INTO user_roles VALUES (131, 1, '2016-06-14 15:06:49.151837', 159, 2);
INSERT INTO user_roles VALUES (132, 1, '2016-06-14 15:15:18.787115', 160, 2);
INSERT INTO user_roles VALUES (133, 1, '2016-06-14 15:15:52.656659', 161, 2);
INSERT INTO user_roles VALUES (134, 1, '2016-06-14 15:18:59.632813', 162, 2);
INSERT INTO user_roles VALUES (135, 1, '2016-06-14 15:27:32.914782', 163, 4);
INSERT INTO user_roles VALUES (136, 1, '2016-06-14 15:27:49.822664', 164, 2);
INSERT INTO user_roles VALUES (137, 1, '2016-06-14 15:39:51.839206', 165, 2);
INSERT INTO user_roles VALUES (138, 1, '2016-06-14 15:40:54.784693', 166, 2);
INSERT INTO user_roles VALUES (139, 1, '2016-06-14 15:44:13.440184', 167, 2);
INSERT INTO user_roles VALUES (140, 1, '2016-06-14 16:01:39.464303', 168, 2);
INSERT INTO user_roles VALUES (141, 1, '2016-06-14 16:09:48.351276', 169, 2);
INSERT INTO user_roles VALUES (142, 1, '2016-06-14 16:13:58.096268', 170, 2);
INSERT INTO user_roles VALUES (143, 1, '2016-06-14 16:16:01.40943', 171, 2);
INSERT INTO user_roles VALUES (144, 1, '2016-06-14 16:16:39.814156', 172, 2);
INSERT INTO user_roles VALUES (145, 1, '2016-06-14 16:17:59.859664', 173, 2);
INSERT INTO user_roles VALUES (146, 1, '2016-06-14 16:21:36.527218', 174, 2);
INSERT INTO user_roles VALUES (147, 1, '2016-06-14 16:22:00.715747', 175, 2);
INSERT INTO user_roles VALUES (148, 1, '2016-06-14 16:23:42.647179', 176, 2);
INSERT INTO user_roles VALUES (149, 1, '2016-06-14 16:25:10.835102', 177, 2);
INSERT INTO user_roles VALUES (150, 1, '2016-06-14 16:35:35.652978', 178, 2);
INSERT INTO user_roles VALUES (151, 1, '2016-06-14 16:36:26.058061', 179, 2);
INSERT INTO user_roles VALUES (152, 1, '2016-06-14 16:43:35.227633', 180, 2);
INSERT INTO user_roles VALUES (153, 1, '2016-06-14 16:43:36.826572', 181, 2);
INSERT INTO user_roles VALUES (154, 1, '2016-06-14 16:47:21.768355', 182, 2);
INSERT INTO user_roles VALUES (155, 1, '2016-06-14 17:04:32.672554', 183, 2);
INSERT INTO user_roles VALUES (156, 1, '2016-06-14 17:08:02.842073', 184, 2);
INSERT INTO user_roles VALUES (157, 1, '2016-06-14 17:10:41.894494', 185, 2);
INSERT INTO user_roles VALUES (158, 1, '2016-06-14 17:11:18.498463', 186, 2);
INSERT INTO user_roles VALUES (159, 1, '2016-06-14 17:11:41.892062', 187, 2);
INSERT INTO user_roles VALUES (160, 1, '2016-06-14 17:12:41.991718', 188, 2);
INSERT INTO user_roles VALUES (161, 1, '2016-06-14 17:15:45.835163', 189, 2);
INSERT INTO user_roles VALUES (162, 1, '2016-06-14 17:18:58.731137', 190, 2);
INSERT INTO user_roles VALUES (163, 1, '2016-06-14 17:24:03.295928', 191, 2);
INSERT INTO user_roles VALUES (164, 1, '2016-06-14 17:29:53.18511', 192, 2);
INSERT INTO user_roles VALUES (165, 1, '2016-06-14 17:38:16.500732', 193, 2);
INSERT INTO user_roles VALUES (166, 1, '2016-06-14 17:42:12.000479', 194, 2);
INSERT INTO user_roles VALUES (167, 1, '2016-06-14 17:48:42.300559', 195, 2);
INSERT INTO user_roles VALUES (168, 1, '2016-06-14 17:49:12.500565', 196, 2);
INSERT INTO user_roles VALUES (169, 1, '2016-06-14 17:56:14.027462', 197, 2);
INSERT INTO user_roles VALUES (170, 1, '2016-06-14 18:04:24.181664', 198, 2);
INSERT INTO user_roles VALUES (171, 1, '2016-06-14 18:06:04.167054', 199, 2);
INSERT INTO user_roles VALUES (172, 1, '2016-06-14 18:07:43.63578', 200, 2);
INSERT INTO user_roles VALUES (173, 1, '2016-06-14 18:12:58.056735', 201, 2);
INSERT INTO user_roles VALUES (174, 1, '2016-06-14 18:16:31.937082', 202, 2);
INSERT INTO user_roles VALUES (175, 1, '2016-06-14 18:25:22.027555', 203, 2);
INSERT INTO user_roles VALUES (176, 1, '2016-06-14 18:33:01.781636', 204, 2);
INSERT INTO user_roles VALUES (177, 1, '2016-06-14 18:59:15.697914', 205, 2);
INSERT INTO user_roles VALUES (178, 1, '2016-06-14 19:00:18.427521', 206, 2);
INSERT INTO user_roles VALUES (179, 1, '2016-06-14 19:00:30.177319', 207, 4);
INSERT INTO user_roles VALUES (180, 1, '2016-06-14 19:00:58.508688', 208, 4);
INSERT INTO user_roles VALUES (181, 1, '2016-06-14 19:03:04.632856', 209, 2);
INSERT INTO user_roles VALUES (182, 1, '2016-06-14 19:08:30.272852', 210, 2);
INSERT INTO user_roles VALUES (183, 1, '2016-06-14 19:11:10.009594', 211, 2);
INSERT INTO user_roles VALUES (184, 1, '2016-06-14 19:11:17.151053', 212, 2);
INSERT INTO user_roles VALUES (185, 1, '2016-06-14 19:15:09.491139', 213, 2);
INSERT INTO user_roles VALUES (186, 1, '2016-06-14 19:19:21.658688', 214, 2);
INSERT INTO user_roles VALUES (187, 1, '2016-06-14 19:19:45.466139', 215, 2);
INSERT INTO user_roles VALUES (188, 1, '2016-06-14 19:23:27.194805', 216, 2);
INSERT INTO user_roles VALUES (189, 1, '2016-06-14 19:27:41.183293', 217, 2);
INSERT INTO user_roles VALUES (190, 1, '2016-06-14 19:32:02.514792', 218, 2);
INSERT INTO user_roles VALUES (191, 1, '2016-06-14 19:37:19.670615', 219, 2);
INSERT INTO user_roles VALUES (192, 1, '2016-06-14 19:39:00.057425', 220, 2);
INSERT INTO user_roles VALUES (193, 1, '2016-06-14 19:39:23.056271', 221, 2);
INSERT INTO user_roles VALUES (194, 1, '2016-06-14 19:41:05.844425', 222, 2);
INSERT INTO user_roles VALUES (195, 1, '2016-06-14 19:47:06.449858', 223, 2);
INSERT INTO user_roles VALUES (196, 1, '2016-06-14 19:47:37.068732', 224, 2);
INSERT INTO user_roles VALUES (197, 1, '2016-06-14 19:48:17.210556', 225, 2);
INSERT INTO user_roles VALUES (198, 1, '2016-06-14 19:52:25.448095', 226, 2);
INSERT INTO user_roles VALUES (199, 1, '2016-06-14 19:55:57.348272', 227, 2);
INSERT INTO user_roles VALUES (200, 1, '2016-06-14 19:57:18.970837', 228, 2);
INSERT INTO user_roles VALUES (201, 1, '2016-06-14 20:01:03.382055', 229, 2);
INSERT INTO user_roles VALUES (202, 1, '2016-06-14 20:01:04.507083', 230, 2);
INSERT INTO user_roles VALUES (203, 1, '2016-06-14 20:03:49.738174', 231, 2);
INSERT INTO user_roles VALUES (204, 1, '2016-06-14 20:04:48.110412', 232, 2);
INSERT INTO user_roles VALUES (205, 1, '2016-06-14 20:11:48.148271', 233, 2);
INSERT INTO user_roles VALUES (206, 1, '2016-06-14 20:20:41.517016', 234, 2);
INSERT INTO user_roles VALUES (207, 1, '2016-06-14 20:24:49.646622', 235, 2);
INSERT INTO user_roles VALUES (208, 1, '2016-06-14 20:33:09.779995', 236, 2);
INSERT INTO user_roles VALUES (209, 1, '2016-06-14 20:52:11.341415', 237, 2);
INSERT INTO user_roles VALUES (210, 1, '2016-06-14 20:58:03.941179', 238, 2);
INSERT INTO user_roles VALUES (211, 1, '2016-06-14 21:03:00.012764', 239, 2);
INSERT INTO user_roles VALUES (212, 1, '2016-06-14 21:05:56.853449', 240, 2);
INSERT INTO user_roles VALUES (213, 1, '2016-06-14 21:06:32.378287', 241, 2);
INSERT INTO user_roles VALUES (214, 1, '2016-06-14 21:13:10.047519', 242, 2);
INSERT INTO user_roles VALUES (215, 1, '2016-06-14 21:14:41.02721', 243, 2);
INSERT INTO user_roles VALUES (216, 1, '2016-06-14 21:19:19.822801', 244, 2);
INSERT INTO user_roles VALUES (217, 1, '2016-06-14 21:33:22.310824', 245, 2);
INSERT INTO user_roles VALUES (218, 1, '2016-06-14 21:38:24.947363', 246, 2);
INSERT INTO user_roles VALUES (219, 1, '2016-06-14 21:39:06.165188', 247, 2);
INSERT INTO user_roles VALUES (220, 1, '2016-06-14 21:40:42.868918', 248, 2);
INSERT INTO user_roles VALUES (221, 1, '2016-06-14 21:47:26.084465', 249, 2);
INSERT INTO user_roles VALUES (222, 1, '2016-06-14 21:50:16.233595', 250, 4);
INSERT INTO user_roles VALUES (223, 1, '2016-06-14 21:51:09.311729', 251, 2);
INSERT INTO user_roles VALUES (224, 1, '2016-06-14 21:52:31.881425', 252, 2);
INSERT INTO user_roles VALUES (225, 1, '2016-06-14 21:54:02.15683', 253, 2);
INSERT INTO user_roles VALUES (226, 1, '2016-06-14 22:11:33.101616', 254, 2);
INSERT INTO user_roles VALUES (227, 1, '2016-06-14 22:40:10.892683', 255, 2);
INSERT INTO user_roles VALUES (228, 1, '2016-06-14 22:43:48.451864', 256, 2);
INSERT INTO user_roles VALUES (229, 1, '2016-06-14 22:46:36.309125', 257, 2);
INSERT INTO user_roles VALUES (230, 1, '2016-06-14 22:50:23.655095', 258, 2);
INSERT INTO user_roles VALUES (231, 1, '2016-06-14 23:06:16.232407', 259, 2);
INSERT INTO user_roles VALUES (232, 1, '2016-06-14 23:07:06.534111', 260, 2);
INSERT INTO user_roles VALUES (233, 1, '2016-06-14 23:09:24.698614', 261, 2);
INSERT INTO user_roles VALUES (234, 1, '2016-06-14 23:12:29.897237', 262, 2);
INSERT INTO user_roles VALUES (235, 1, '2016-06-14 23:12:53.87016', 263, 2);
INSERT INTO user_roles VALUES (236, 1, '2016-06-14 23:19:26.664912', 264, 2);
INSERT INTO user_roles VALUES (237, 1, '2016-06-14 23:36:05.520338', 265, 2);
INSERT INTO user_roles VALUES (238, 1, '2016-06-14 23:45:00.257872', 266, 2);
INSERT INTO user_roles VALUES (239, 1, '2016-06-14 23:46:06.990278', 267, 2);
INSERT INTO user_roles VALUES (240, 1, '2016-06-14 23:52:05.52538', 268, 2);
INSERT INTO user_roles VALUES (241, 1, '2016-06-14 23:53:42.261221', 269, 2);
INSERT INTO user_roles VALUES (242, 1, '2016-06-14 23:57:07.241799', 270, 2);
INSERT INTO user_roles VALUES (243, 1, '2016-06-14 23:59:22.509826', 271, 2);
INSERT INTO user_roles VALUES (244, 1, '2016-06-15 00:05:20.226208', 272, 2);
INSERT INTO user_roles VALUES (245, 1, '2016-06-15 00:08:57.882488', 273, 2);
INSERT INTO user_roles VALUES (246, 1, '2016-06-15 00:11:26.778345', 274, 2);
INSERT INTO user_roles VALUES (247, 1, '2016-06-15 00:16:34.164791', 275, 2);
INSERT INTO user_roles VALUES (248, 1, '2016-06-15 00:16:44.167208', 276, 2);
INSERT INTO user_roles VALUES (249, 1, '2016-06-15 00:18:38.674613', 277, 2);
INSERT INTO user_roles VALUES (250, 1, '2016-06-15 00:19:44.12317', 278, 2);
INSERT INTO user_roles VALUES (251, 1, '2016-06-15 00:20:14.05943', 279, 2);
INSERT INTO user_roles VALUES (252, 1, '2016-06-15 00:26:09.344816', 280, 2);
INSERT INTO user_roles VALUES (253, 1, '2016-06-15 00:28:15.031695', 281, 2);
INSERT INTO user_roles VALUES (254, 1, '2016-06-15 00:45:49.286432', 282, 2);
INSERT INTO user_roles VALUES (255, 1, '2016-06-15 00:54:28.149595', 283, 2);
INSERT INTO user_roles VALUES (256, 1, '2016-06-15 01:00:37.451929', 284, 2);
INSERT INTO user_roles VALUES (257, 1, '2016-06-15 01:00:59.637272', 285, 2);
INSERT INTO user_roles VALUES (258, 1, '2016-06-15 01:03:47.106012', 286, 2);
INSERT INTO user_roles VALUES (259, 1, '2016-06-15 01:07:59.141619', 287, 2);
INSERT INTO user_roles VALUES (260, 1, '2016-06-15 01:31:24.900982', 288, 2);
INSERT INTO user_roles VALUES (261, 1, '2016-06-15 01:49:20.745832', 289, 2);
INSERT INTO user_roles VALUES (262, 1, '2016-06-15 01:50:43.5918', 290, 2);
INSERT INTO user_roles VALUES (263, 1, '2016-06-15 02:26:08.146755', 291, 2);
INSERT INTO user_roles VALUES (264, 1, '2016-06-15 02:56:46.042956', 292, 2);
INSERT INTO user_roles VALUES (265, 1, '2016-06-15 03:13:47.306683', 293, 2);
INSERT INTO user_roles VALUES (266, 1, '2016-06-15 03:17:07.236805', 294, 2);
INSERT INTO user_roles VALUES (267, 1, '2016-06-15 03:57:31.482308', 295, 2);
INSERT INTO user_roles VALUES (268, 1, '2016-06-15 04:27:23.371154', 296, 2);
INSERT INTO user_roles VALUES (269, 1, '2016-06-15 05:23:06.367818', 297, 2);
INSERT INTO user_roles VALUES (270, 1, '2016-06-15 05:53:39.237681', 298, 2);
INSERT INTO user_roles VALUES (271, 1, '2016-06-15 06:16:47.148608', 299, 2);
INSERT INTO user_roles VALUES (272, 1, '2016-06-15 06:26:02.287058', 300, 4);
INSERT INTO user_roles VALUES (273, 1, '2016-06-15 07:00:40.618952', 301, 2);
INSERT INTO user_roles VALUES (274, 1, '2016-06-15 07:02:51.919051', 302, 2);
INSERT INTO user_roles VALUES (275, 1, '2016-06-15 07:18:38.652292', 303, 2);
INSERT INTO user_roles VALUES (276, 1, '2016-06-15 07:19:15.389021', 304, 2);
INSERT INTO user_roles VALUES (277, 1, '2016-06-15 08:18:20.668561', 305, 2);
INSERT INTO user_roles VALUES (278, 1, '2016-06-15 08:23:15.895844', 306, 2);
INSERT INTO user_roles VALUES (279, 1, '2016-06-15 08:40:50.147899', 307, 2);
INSERT INTO user_roles VALUES (280, 1, '2016-06-15 08:46:29.972463', 308, 2);
INSERT INTO user_roles VALUES (281, 1, '2016-06-15 08:51:53.936413', 309, 2);
INSERT INTO user_roles VALUES (282, 1, '2016-06-15 09:06:21.908884', 310, 2);
INSERT INTO user_roles VALUES (283, 1, '2016-06-15 09:19:06.312314', 311, 2);
INSERT INTO user_roles VALUES (284, 1, '2016-06-15 09:20:44.557264', 312, 4);
INSERT INTO user_roles VALUES (285, 1, '2016-06-15 09:23:35.340258', 313, 2);
INSERT INTO user_roles VALUES (286, 1, '2016-06-15 09:29:43.123996', 314, 2);
INSERT INTO user_roles VALUES (287, 1, '2016-06-15 09:47:21.389452', 315, 2);
INSERT INTO user_roles VALUES (288, 1, '2016-06-15 09:47:21.842803', 316, 2);
INSERT INTO user_roles VALUES (289, 1, '2016-06-15 09:48:33.468868', 317, 2);
INSERT INTO user_roles VALUES (290, 1, '2016-06-15 09:50:45.149057', 318, 2);
INSERT INTO user_roles VALUES (291, 1, '2016-06-15 09:52:30.868756', 319, 2);
INSERT INTO user_roles VALUES (292, 1, '2016-06-15 10:00:21.710562', 320, 2);
INSERT INTO user_roles VALUES (293, 1, '2016-06-15 10:01:15.299437', 321, 2);
INSERT INTO user_roles VALUES (294, 1, '2016-06-15 10:12:47.840293', 322, 2);
INSERT INTO user_roles VALUES (295, 1, '2016-06-15 10:15:53.322647', 323, 2);
INSERT INTO user_roles VALUES (296, 1, '2016-06-15 10:16:20.910432', 324, 2);
INSERT INTO user_roles VALUES (297, 1, '2016-06-15 10:20:01.908281', 325, 2);
INSERT INTO user_roles VALUES (298, 1, '2016-06-15 10:26:31.822298', 326, 2);
INSERT INTO user_roles VALUES (299, 1, '2016-06-15 10:27:33.367112', 327, 2);
INSERT INTO user_roles VALUES (300, 1, '2016-06-15 10:28:15.082693', 328, 2);
INSERT INTO user_roles VALUES (301, 1, '2016-06-15 10:30:06.64397', 329, 2);
INSERT INTO user_roles VALUES (302, 1, '2016-06-15 10:30:18.805444', 330, 2);
INSERT INTO user_roles VALUES (303, 1, '2016-06-15 10:37:55.105041', 331, 2);
INSERT INTO user_roles VALUES (304, 1, '2016-06-15 10:47:25.225851', 332, 2);
INSERT INTO user_roles VALUES (305, 1, '2016-06-15 10:49:03.550756', 333, 2);
INSERT INTO user_roles VALUES (306, 1, '2016-06-15 10:54:20.93238', 334, 2);
INSERT INTO user_roles VALUES (307, 1, '2016-06-15 10:54:54.827401', 335, 2);
INSERT INTO user_roles VALUES (308, 1, '2016-06-15 10:55:25.63602', 336, 2);
INSERT INTO user_roles VALUES (309, 1, '2016-06-15 11:22:50.273149', 337, 2);
INSERT INTO user_roles VALUES (310, 1, '2016-06-15 11:23:42.33095', 338, 2);
INSERT INTO user_roles VALUES (311, 1, '2016-06-15 11:37:12.612618', 339, 2);
INSERT INTO user_roles VALUES (312, 1, '2016-06-15 11:40:46.655652', 340, 2);
INSERT INTO user_roles VALUES (313, 1, '2016-06-15 11:56:21.918203', 341, 2);
INSERT INTO user_roles VALUES (314, 1, '2016-06-15 12:15:46.524664', 342, 2);
INSERT INTO user_roles VALUES (315, 1, '2016-06-15 12:21:50.232273', 343, 2);
INSERT INTO user_roles VALUES (316, 1, '2016-06-15 12:26:02.220269', 344, 2);
INSERT INTO user_roles VALUES (317, 1, '2016-06-15 12:32:42.509348', 345, 2);
INSERT INTO user_roles VALUES (318, 1, '2016-06-15 12:36:04.878458', 346, 2);
INSERT INTO user_roles VALUES (319, 1, '2016-06-15 12:44:32.830607', 347, 2);
INSERT INTO user_roles VALUES (320, 1, '2016-06-15 12:59:47.255466', 348, 2);
INSERT INTO user_roles VALUES (321, 1, '2016-06-15 13:03:31.532791', 349, 2);
INSERT INTO user_roles VALUES (322, 1, '2016-06-15 13:04:44.898245', 350, 4);
INSERT INTO user_roles VALUES (323, 1, '2016-06-15 13:49:19.813315', 351, 2);
INSERT INTO user_roles VALUES (324, 1, '2016-06-15 13:58:00.446118', 352, 2);
INSERT INTO user_roles VALUES (325, 1, '2016-06-15 14:19:03.190969', 353, 2);
INSERT INTO user_roles VALUES (326, 1, '2016-06-15 14:21:21.826568', 354, 2);
INSERT INTO user_roles VALUES (327, 1, '2016-06-15 14:44:36.371858', 355, 2);
INSERT INTO user_roles VALUES (328, 1, '2016-06-15 15:00:23.039087', 356, 2);
INSERT INTO user_roles VALUES (329, 1, '2016-06-15 15:00:35.524799', 357, 2);
INSERT INTO user_roles VALUES (330, 1, '2016-06-15 15:01:15.76371', 358, 2);
INSERT INTO user_roles VALUES (331, 1, '2016-06-15 15:04:53.06437', 359, 4);
INSERT INTO user_roles VALUES (332, 1, '2016-06-15 15:15:30.328065', 360, 2);
INSERT INTO user_roles VALUES (333, 1, '2016-06-15 15:31:52.85292', 361, 4);
INSERT INTO user_roles VALUES (334, 1, '2016-06-15 15:41:00.601417', 362, 2);
INSERT INTO user_roles VALUES (335, 1, '2016-06-15 15:46:40.162773', 363, 2);
INSERT INTO user_roles VALUES (336, 1, '2016-06-15 15:50:44.620624', 364, 2);
INSERT INTO user_roles VALUES (337, 1, '2016-06-15 16:02:37.163397', 365, 2);
INSERT INTO user_roles VALUES (338, 1, '2016-06-15 16:17:20.492803', 366, 2);
INSERT INTO user_roles VALUES (339, 1, '2016-06-15 16:43:04.709402', 367, 2);
INSERT INTO user_roles VALUES (340, 1, '2016-06-15 17:04:01.868528', 368, 2);
INSERT INTO user_roles VALUES (341, 1, '2016-06-15 17:32:26.808341', 369, 2);
INSERT INTO user_roles VALUES (342, 1, '2016-06-15 17:45:51.899359', 370, 2);
INSERT INTO user_roles VALUES (343, 1, '2016-06-15 17:49:44.073286', 371, 2);
INSERT INTO user_roles VALUES (344, 1, '2016-06-15 17:54:43.700567', 372, 2);
INSERT INTO user_roles VALUES (345, 1, '2016-06-15 17:55:41.364594', 373, 2);
INSERT INTO user_roles VALUES (346, 1, '2016-06-15 18:14:56.002322', 374, 2);
INSERT INTO user_roles VALUES (347, 1, '2016-06-15 19:08:15.329579', 375, 2);
INSERT INTO user_roles VALUES (348, 1, '2016-06-15 19:11:11.799561', 376, 2);
INSERT INTO user_roles VALUES (349, 1, '2016-06-15 19:21:00.35545', 377, 2);
INSERT INTO user_roles VALUES (350, 1, '2016-06-15 19:44:24.562603', 378, 2);
INSERT INTO user_roles VALUES (351, 1, '2016-06-15 20:01:03.833522', 379, 2);
INSERT INTO user_roles VALUES (352, 1, '2016-06-15 20:03:08.464078', 380, 2);
INSERT INTO user_roles VALUES (353, 1, '2016-06-15 21:32:00.364829', 381, 2);
INSERT INTO user_roles VALUES (354, 1, '2016-06-15 21:46:56.329793', 382, 2);
INSERT INTO user_roles VALUES (355, 1, '2016-06-15 22:02:48.639564', 383, 4);
INSERT INTO user_roles VALUES (356, 1, '2016-06-15 22:10:52.706762', 384, 2);
INSERT INTO user_roles VALUES (357, 1, '2016-06-15 22:30:41.8639', 385, 2);
INSERT INTO user_roles VALUES (358, 1, '2016-06-15 22:37:28.259103', 386, 2);
INSERT INTO user_roles VALUES (359, 1, '2016-06-15 23:08:46.00956', 387, 4);
INSERT INTO user_roles VALUES (360, 1, '2016-06-15 23:15:55.956643', 388, 2);
INSERT INTO user_roles VALUES (361, 1, '2016-06-16 00:27:26.12909', 389, 2);
INSERT INTO user_roles VALUES (362, 1, '2016-06-16 00:37:40.709383', 390, 2);
INSERT INTO user_roles VALUES (363, 1, '2016-06-16 04:23:19.52022', 391, 2);
INSERT INTO user_roles VALUES (364, 1, '2016-06-16 05:30:49.606142', 392, 2);
INSERT INTO user_roles VALUES (365, 1, '2016-06-16 09:12:10.112195', 393, 2);
INSERT INTO user_roles VALUES (366, 1, '2016-06-16 10:11:33.521934', 394, 2);
INSERT INTO user_roles VALUES (367, 1, '2016-06-16 10:17:52.905204', 395, 2);
INSERT INTO user_roles VALUES (368, 1, '2016-06-16 10:29:10.618839', 396, 2);
INSERT INTO user_roles VALUES (369, 1, '2016-06-16 11:28:46.07417', 397, 2);
INSERT INTO user_roles VALUES (370, 1, '2016-06-16 14:09:25.829972', 398, 2);
INSERT INTO user_roles VALUES (371, 1, '2016-06-16 14:13:52.717741', 399, 2);
INSERT INTO user_roles VALUES (372, 1, '2016-06-16 14:35:58.373735', 400, 2);
INSERT INTO user_roles VALUES (373, 1, '2016-06-16 16:19:27.864721', 401, 2);
INSERT INTO user_roles VALUES (374, 1, '2016-06-16 16:29:08.641262', 402, 2);
INSERT INTO user_roles VALUES (375, 1, '2016-06-16 16:33:26.53225', 403, 2);
INSERT INTO user_roles VALUES (376, 1, '2016-06-16 17:28:06.244664', 404, 2);
INSERT INTO user_roles VALUES (377, 1, '2016-06-16 17:28:42.710727', 405, 2);
INSERT INTO user_roles VALUES (378, 1, '2016-06-16 17:55:43.243034', 406, 2);
INSERT INTO user_roles VALUES (379, 1, '2016-06-16 19:52:30.943542', 407, 2);
INSERT INTO user_roles VALUES (380, 1, '2016-06-17 03:26:42.260367', 408, 2);
INSERT INTO user_roles VALUES (381, 1, '2016-06-17 07:55:59.774574', 409, 2);
INSERT INTO user_roles VALUES (382, 1, '2016-06-17 08:35:41.530643', 410, 2);
INSERT INTO user_roles VALUES (383, 1, '2016-06-17 08:45:59.745754', 411, 2);
INSERT INTO user_roles VALUES (384, 1, '2016-06-17 09:13:20.488827', 412, 2);
INSERT INTO user_roles VALUES (385, 1, '2016-06-17 09:41:32.840794', 413, 2);
INSERT INTO user_roles VALUES (386, 1, '2016-06-17 09:42:15.706383', 414, 2);
INSERT INTO user_roles VALUES (387, 1, '2016-06-17 10:08:46.136241', 415, 2);
INSERT INTO user_roles VALUES (388, 1, '2016-06-17 11:20:27.759749', 416, 2);
INSERT INTO user_roles VALUES (389, 1, '2016-06-17 12:44:45.003643', 417, 2);
INSERT INTO user_roles VALUES (390, 1, '2016-06-17 12:46:38.549036', 418, 2);
INSERT INTO user_roles VALUES (391, 1, '2016-06-17 14:24:24.311852', 419, 2);
INSERT INTO user_roles VALUES (392, 1, '2016-06-17 14:34:49.33794', 420, 2);
INSERT INTO user_roles VALUES (393, 1, '2016-06-17 15:53:15.68075', 421, 2);
INSERT INTO user_roles VALUES (394, 1, '2016-06-17 16:09:47.683469', 422, 2);
INSERT INTO user_roles VALUES (395, 1, '2016-06-17 16:20:06.687273', 423, 2);
INSERT INTO user_roles VALUES (396, 1, '2016-06-17 17:26:06.48877', 424, 2);
INSERT INTO user_roles VALUES (397, 1, '2016-06-17 17:26:11.075146', 425, 2);
INSERT INTO user_roles VALUES (398, 1, '2016-06-17 17:46:21.781982', 426, 2);
INSERT INTO user_roles VALUES (399, 1, '2016-06-17 18:09:21.872724', 427, 2);
INSERT INTO user_roles VALUES (400, 1, '2016-06-17 18:13:26.328454', 428, 2);
INSERT INTO user_roles VALUES (401, 1, '2016-06-17 18:20:22.562157', 429, 2);
INSERT INTO user_roles VALUES (402, 1, '2016-06-17 18:41:15.379442', 430, 2);
INSERT INTO user_roles VALUES (403, 1, '2016-06-17 18:41:39.047762', 431, 2);
INSERT INTO user_roles VALUES (404, 1, '2016-06-17 18:49:04.222351', 432, 2);
INSERT INTO user_roles VALUES (405, 1, '2016-06-17 19:05:24.227757', 433, 2);
INSERT INTO user_roles VALUES (406, 1, '2016-06-17 19:05:56.358161', 434, 2);
INSERT INTO user_roles VALUES (407, 1, '2016-06-17 19:06:10.79807', 435, 2);
INSERT INTO user_roles VALUES (408, 1, '2016-06-17 19:10:57.947786', 436, 2);
INSERT INTO user_roles VALUES (409, 1, '2016-06-17 19:20:35.26276', 437, 2);
INSERT INTO user_roles VALUES (410, 1, '2016-06-17 19:39:18.208558', 438, 2);
INSERT INTO user_roles VALUES (411, 1, '2016-06-17 19:44:16.263525', 439, 2);
INSERT INTO user_roles VALUES (412, 1, '2016-06-17 19:50:43.073233', 440, 2);
INSERT INTO user_roles VALUES (413, 1, '2016-06-17 20:00:23.66828', 441, 2);
INSERT INTO user_roles VALUES (414, 1, '2016-06-17 20:17:19.622781', 442, 2);
INSERT INTO user_roles VALUES (415, 1, '2016-06-17 20:21:26.742002', 443, 2);
INSERT INTO user_roles VALUES (416, 1, '2016-06-17 20:22:48.141888', 444, 2);
INSERT INTO user_roles VALUES (417, 1, '2016-06-17 20:33:50.369991', 445, 2);
INSERT INTO user_roles VALUES (418, 1, '2016-06-17 20:34:53.631728', 446, 2);
INSERT INTO user_roles VALUES (419, 1, '2016-06-17 20:58:34.819728', 447, 2);
INSERT INTO user_roles VALUES (420, 1, '2016-06-17 21:04:28.218722', 448, 2);
INSERT INTO user_roles VALUES (421, 1, '2016-06-17 21:39:42.571768', 449, 2);
INSERT INTO user_roles VALUES (422, 1, '2016-06-17 22:19:23.154358', 450, 2);
INSERT INTO user_roles VALUES (423, 1, '2016-06-17 22:23:49.455769', 451, 2);
INSERT INTO user_roles VALUES (424, 1, '2016-06-17 23:03:23.068431', 452, 2);
INSERT INTO user_roles VALUES (425, 1, '2016-06-17 23:13:20.76872', 453, 2);
INSERT INTO user_roles VALUES (426, 1, '2016-06-18 00:05:16.001055', 454, 2);
INSERT INTO user_roles VALUES (427, 1, '2016-06-18 00:26:06.563128', 455, 2);
INSERT INTO user_roles VALUES (428, 1, '2016-06-18 00:44:20.025938', 456, 2);
INSERT INTO user_roles VALUES (429, 1, '2016-06-18 01:07:09.263453', 457, 2);
INSERT INTO user_roles VALUES (430, 1, '2016-06-18 01:14:58.02089', 458, 2);
INSERT INTO user_roles VALUES (431, 1, '2016-06-18 01:45:39.092813', 459, 2);
INSERT INTO user_roles VALUES (432, 1, '2016-06-18 02:49:17.378129', 460, 2);
INSERT INTO user_roles VALUES (433, 1, '2016-06-18 04:03:11.309793', 461, 2);
INSERT INTO user_roles VALUES (434, 1, '2016-06-18 04:12:50.072375', 462, 2);
INSERT INTO user_roles VALUES (435, 1, '2016-06-18 05:53:53.737621', 463, 2);
INSERT INTO user_roles VALUES (436, 1, '2016-06-18 05:55:23.95078', 464, 2);
INSERT INTO user_roles VALUES (437, 1, '2016-06-18 05:57:04.144895', 465, 2);
INSERT INTO user_roles VALUES (438, 1, '2016-06-18 05:59:33.353794', 466, 2);
INSERT INTO user_roles VALUES (439, 1, '2016-06-18 07:29:32.594462', 467, 2);
INSERT INTO user_roles VALUES (440, 1, '2016-06-18 07:57:01.39655', 468, 2);
INSERT INTO user_roles VALUES (441, 1, '2016-06-18 08:34:47.577914', 469, 2);
INSERT INTO user_roles VALUES (442, 1, '2016-06-18 08:42:52.62752', 470, 4);
INSERT INTO user_roles VALUES (443, 1, '2016-06-18 08:50:18.513893', 471, 4);
INSERT INTO user_roles VALUES (444, 1, '2016-06-18 09:10:16.87328', 472, 2);
INSERT INTO user_roles VALUES (445, 1, '2016-06-18 09:23:21.253631', 473, 2);
INSERT INTO user_roles VALUES (446, 1, '2016-06-18 10:06:49.318991', 474, 2);
INSERT INTO user_roles VALUES (447, 1, '2016-06-18 11:30:05.140271', 475, 2);
INSERT INTO user_roles VALUES (448, 1, '2016-06-18 11:35:24.348516', 476, 2);
INSERT INTO user_roles VALUES (449, 1, '2016-06-18 11:39:48.301245', 477, 2);
INSERT INTO user_roles VALUES (450, 1, '2016-06-18 11:50:58.52255', 478, 2);
INSERT INTO user_roles VALUES (451, 1, '2016-06-18 11:52:19.247965', 479, 2);
INSERT INTO user_roles VALUES (452, 1, '2016-06-18 12:10:16.075106', 480, 2);
INSERT INTO user_roles VALUES (453, 1, '2016-06-18 12:36:39.009306', 481, 2);
INSERT INTO user_roles VALUES (454, 1, '2016-06-18 12:39:53.558756', 482, 2);
INSERT INTO user_roles VALUES (455, 1, '2016-06-18 12:40:28.229156', 483, 2);
INSERT INTO user_roles VALUES (456, 1, '2016-06-18 12:45:12.254877', 484, 2);
INSERT INTO user_roles VALUES (457, 1, '2016-06-18 12:55:11.282895', 485, 2);
INSERT INTO user_roles VALUES (458, 1, '2016-06-18 12:59:22.132981', 486, 2);
INSERT INTO user_roles VALUES (459, 1, '2016-06-18 13:05:38.755172', 487, 2);
INSERT INTO user_roles VALUES (460, 1, '2016-06-18 13:20:53.934015', 488, 2);
INSERT INTO user_roles VALUES (461, 1, '2016-06-18 13:30:03.316881', 489, 2);
INSERT INTO user_roles VALUES (462, 1, '2016-06-18 13:54:28.222737', 490, 2);
INSERT INTO user_roles VALUES (463, 1, '2016-06-18 14:03:36.499725', 491, 2);
INSERT INTO user_roles VALUES (464, 1, '2016-06-18 14:22:42.563864', 492, 2);
INSERT INTO user_roles VALUES (465, 1, '2016-06-18 14:30:06.797557', 493, 2);
INSERT INTO user_roles VALUES (466, 1, '2016-06-18 14:52:25.515875', 494, 2);
INSERT INTO user_roles VALUES (467, 1, '2016-06-18 15:06:20.057053', 495, 2);
INSERT INTO user_roles VALUES (468, 1, '2016-06-18 15:20:53.849086', 496, 2);
INSERT INTO user_roles VALUES (469, 1, '2016-06-18 15:24:15.612095', 497, 2);
INSERT INTO user_roles VALUES (470, 1, '2016-06-18 15:30:41.05455', 498, 2);
INSERT INTO user_roles VALUES (471, 1, '2016-06-18 15:39:49.337926', 499, 2);
INSERT INTO user_roles VALUES (472, 1, '2016-06-18 15:42:52.842992', 500, 2);
INSERT INTO user_roles VALUES (473, 1, '2016-06-18 15:51:49.598966', 501, 2);
INSERT INTO user_roles VALUES (474, 1, '2016-06-18 16:18:33.590815', 502, 2);
INSERT INTO user_roles VALUES (475, 1, '2016-06-18 16:51:29.679367', 503, 2);
INSERT INTO user_roles VALUES (476, 1, '2016-06-18 17:44:41.296373', 504, 2);
INSERT INTO user_roles VALUES (477, 1, '2016-06-18 17:50:29.159266', 505, 2);
INSERT INTO user_roles VALUES (478, 1, '2016-06-18 18:12:52.986231', 506, 2);
INSERT INTO user_roles VALUES (479, 1, '2016-06-18 18:14:41.794313', 507, 2);
INSERT INTO user_roles VALUES (480, 1, '2016-06-18 18:16:33.969527', 508, 2);
INSERT INTO user_roles VALUES (481, 1, '2016-06-18 19:07:36.70171', 509, 2);
INSERT INTO user_roles VALUES (482, 1, '2016-06-18 20:27:24.680516', 510, 2);
INSERT INTO user_roles VALUES (483, 1, '2016-06-18 20:43:20.146011', 511, 2);
INSERT INTO user_roles VALUES (484, 1, '2016-06-18 21:01:59.849288', 512, 2);
INSERT INTO user_roles VALUES (485, 1, '2016-06-18 21:23:35.672674', 513, 2);
INSERT INTO user_roles VALUES (486, 1, '2016-06-18 21:36:45.281102', 514, 2);
INSERT INTO user_roles VALUES (487, 1, '2016-06-18 22:08:47.656969', 515, 2);
INSERT INTO user_roles VALUES (488, 1, '2016-06-18 22:09:59.822517', 516, 2);
INSERT INTO user_roles VALUES (489, 1, '2016-06-18 23:15:01.593001', 517, 2);
INSERT INTO user_roles VALUES (490, 1, '2016-06-18 23:36:31.284522', 518, 2);
INSERT INTO user_roles VALUES (491, 1, '2016-06-18 23:47:48.145918', 519, 2);
INSERT INTO user_roles VALUES (492, 1, '2016-06-19 00:43:33.741827', 520, 2);
INSERT INTO user_roles VALUES (493, 1, '2016-06-19 01:50:41.959712', 521, 2);
INSERT INTO user_roles VALUES (494, 1, '2016-06-19 10:29:20.232905', 522, 2);
INSERT INTO user_roles VALUES (495, 1, '2016-06-19 11:23:09.320676', 523, 2);
INSERT INTO user_roles VALUES (496, 1, '2016-06-19 11:53:44.331524', 524, 2);
INSERT INTO user_roles VALUES (497, 1, '2016-06-19 13:13:01.969893', 525, 2);
INSERT INTO user_roles VALUES (498, 1, '2016-06-19 13:42:47.814837', 526, 4);
INSERT INTO user_roles VALUES (499, 1, '2016-06-19 14:13:40.588218', 527, 2);
INSERT INTO user_roles VALUES (500, 1, '2016-06-19 15:02:00.061108', 528, 2);
INSERT INTO user_roles VALUES (501, 1, '2016-06-19 17:39:27.251807', 529, 2);
INSERT INTO user_roles VALUES (502, 1, '2016-06-19 18:23:07.215718', 530, 2);
INSERT INTO user_roles VALUES (503, 1, '2016-06-19 19:16:17.829124', 531, 2);
INSERT INTO user_roles VALUES (504, 1, '2016-06-19 20:35:39.707846', 532, 2);
INSERT INTO user_roles VALUES (505, 1, '2016-06-19 22:00:19.581596', 533, 2);
INSERT INTO user_roles VALUES (506, 1, '2016-06-19 22:45:09.247323', 534, 2);
INSERT INTO user_roles VALUES (507, 1, '2016-06-19 22:58:32.562222', 535, 2);
INSERT INTO user_roles VALUES (508, 1, '2016-06-19 23:18:37.821121', 536, 2);
INSERT INTO user_roles VALUES (509, 1, '2016-06-19 23:20:33.499799', 537, 2);
INSERT INTO user_roles VALUES (510, 1, '2016-06-19 23:59:41.354255', 538, 2);
INSERT INTO user_roles VALUES (511, 1, '2016-06-20 09:32:38.044325', 539, 2);
INSERT INTO user_roles VALUES (512, 1, '2016-06-20 09:51:03.335869', 540, 4);
INSERT INTO user_roles VALUES (513, 1, '2016-06-20 10:05:21.680762', 541, 2);
INSERT INTO user_roles VALUES (514, 1, '2016-06-20 11:19:03.523992', 542, 2);
INSERT INTO user_roles VALUES (515, 1, '2016-06-20 12:44:16.834688', 543, 2);
INSERT INTO user_roles VALUES (516, 1, '2016-06-20 13:22:36.360312', 544, 2);
INSERT INTO user_roles VALUES (517, 1, '2016-06-20 13:32:18.672386', 545, 2);
INSERT INTO user_roles VALUES (518, 1, '2016-06-20 14:21:53.353618', 546, 2);
INSERT INTO user_roles VALUES (519, 1, '2016-06-20 15:17:44.495928', 547, 2);
INSERT INTO user_roles VALUES (520, 1, '2016-06-20 15:58:38.716111', 548, 2);
INSERT INTO user_roles VALUES (521, 1, '2016-06-20 16:44:17.336423', 549, 4);
INSERT INTO user_roles VALUES (522, 1, '2016-06-20 18:15:19.77127', 550, 2);
INSERT INTO user_roles VALUES (523, 1, '2016-06-20 18:18:04.89257', 551, 2);
INSERT INTO user_roles VALUES (524, 1, '2016-06-20 18:45:01.807972', 552, 2);
INSERT INTO user_roles VALUES (525, 1, '2016-06-20 19:21:07.733215', 553, 2);
INSERT INTO user_roles VALUES (526, 1, '2016-06-20 19:21:45.808854', 554, 2);
INSERT INTO user_roles VALUES (527, 1, '2016-06-20 20:54:37.01221', 555, 2);
INSERT INTO user_roles VALUES (528, 1, '2016-06-21 09:55:12.18396', 556, 4);
INSERT INTO user_roles VALUES (529, 1, '2016-06-21 10:32:45.331904', 557, 2);
INSERT INTO user_roles VALUES (530, 1, '2016-06-21 14:00:04.332643', 558, 2);
INSERT INTO user_roles VALUES (531, 1, '2016-06-21 14:11:07.641575', 559, 4);
INSERT INTO user_roles VALUES (532, 1, '2016-06-21 14:14:39.251238', 560, 2);
INSERT INTO user_roles VALUES (533, 1, '2016-06-21 16:07:50.435196', 561, 2);
INSERT INTO user_roles VALUES (534, 1, '2016-06-22 05:20:20.473977', 562, 2);
INSERT INTO user_roles VALUES (535, 1, '2016-06-22 08:18:35.318721', 563, 2);
INSERT INTO user_roles VALUES (536, 1, '2016-06-22 13:34:32.037222', 564, 4);
INSERT INTO user_roles VALUES (537, 1, '2016-06-22 15:45:43.568546', 565, 2);
INSERT INTO user_roles VALUES (538, 1, '2016-06-22 19:42:02.093963', 566, 4);
INSERT INTO user_roles VALUES (539, 1, '2016-06-22 21:12:23.016792', 567, 2);
INSERT INTO user_roles VALUES (540, 1, '2016-06-23 06:29:02.441228', 568, 2);
INSERT INTO user_roles VALUES (541, 1, '2016-06-23 14:26:40.230705', 569, 4);
INSERT INTO user_roles VALUES (542, 1, '2016-06-23 16:45:57.966087', 570, 2);
INSERT INTO user_roles VALUES (543, 1, '2016-06-24 13:58:32.518944', 571, 2);
INSERT INTO user_roles VALUES (544, 1, '2016-06-25 23:54:00.391965', 572, 2);
INSERT INTO user_roles VALUES (545, 1, '2016-06-26 01:26:55.28956', 573, 4);
INSERT INTO user_roles VALUES (546, 1, '2016-06-26 02:23:17.686617', 574, 4);
INSERT INTO user_roles VALUES (547, 1, '2016-06-26 13:42:30.000157', 575, 2);
INSERT INTO user_roles VALUES (548, 1, '2016-06-27 10:40:37.027108', 576, 2);
INSERT INTO user_roles VALUES (549, 1, '2016-06-27 10:44:39.131129', 577, 2);
INSERT INTO user_roles VALUES (550, 1, '2016-06-27 17:07:18.686526', 578, 4);
INSERT INTO user_roles VALUES (551, 1, '2016-06-28 12:33:55.488477', 579, 2);
INSERT INTO user_roles VALUES (552, 1, '2016-06-28 23:46:28.687587', 580, 2);
INSERT INTO user_roles VALUES (553, 1, '2016-07-04 06:44:47.593038', 581, 2);
INSERT INTO user_roles VALUES (554, 1, '2016-07-04 12:52:27.674394', 582, 2);
INSERT INTO user_roles VALUES (555, 1, '2016-07-04 13:37:00.014121', 583, 4);
INSERT INTO user_roles VALUES (556, 1, '2016-07-04 14:15:40.382491', 584, 4);
INSERT INTO user_roles VALUES (557, 1, '2016-07-04 18:34:38.245605', 585, 2);
INSERT INTO user_roles VALUES (558, 1, '2016-07-04 20:38:57.672097', 586, 4);
INSERT INTO user_roles VALUES (559, 1, '2016-07-05 00:15:54.418875', 587, 2);
INSERT INTO user_roles VALUES (560, 1, '2016-07-05 16:47:16.776898', 588, 2);
INSERT INTO user_roles VALUES (561, 1, '2016-07-05 19:39:50.031326', 589, 2);
INSERT INTO user_roles VALUES (562, 1, '2016-07-06 01:17:23.930203', 590, 2);
INSERT INTO user_roles VALUES (563, 1, '2016-07-07 13:06:00.193539', 591, 2);
INSERT INTO user_roles VALUES (564, 1, '2016-07-07 22:57:06.853151', 592, 2);
INSERT INTO user_roles VALUES (565, 1, '2016-07-08 00:16:21.273109', 593, 2);
INSERT INTO user_roles VALUES (566, 1, '2016-07-09 20:24:38.865081', 594, 2);
INSERT INTO user_roles VALUES (567, 1, '2016-07-10 16:10:51.551811', 595, 2);
INSERT INTO user_roles VALUES (568, 1, '2016-07-10 18:09:55.325901', 596, 2);
INSERT INTO user_roles VALUES (569, 1, '2016-07-11 16:09:07.559941', 597, 4);
INSERT INTO user_roles VALUES (570, 1, '2016-07-14 13:14:49.57602', 598, 2);
INSERT INTO user_roles VALUES (571, 1, '2016-07-14 14:46:18.202316', 599, 2);
INSERT INTO user_roles VALUES (572, 1, '2016-07-14 21:49:26.400828', 600, 2);
INSERT INTO user_roles VALUES (573, 1, '2016-07-14 23:36:27.720178', 601, 4);
INSERT INTO user_roles VALUES (574, 1, '2016-07-15 09:41:47.487402', 602, 4);
INSERT INTO user_roles VALUES (575, 1, '2016-07-15 14:03:44.246278', 603, 4);
INSERT INTO user_roles VALUES (576, 1, '2016-07-16 14:57:54.049595', 604, 2);
INSERT INTO user_roles VALUES (577, 1, '2016-07-17 23:38:53.619638', 605, 4);
INSERT INTO user_roles VALUES (578, 1, '2016-07-19 00:06:18.418416', 606, 2);
INSERT INTO user_roles VALUES (579, 1, '2016-07-19 11:32:46.160554', 607, 2);
INSERT INTO user_roles VALUES (580, 1, '2016-07-20 11:26:03.470131', 608, 4);
INSERT INTO user_roles VALUES (581, 1, '2017-07-10 17:32:06.96419', 609, 2);
INSERT INTO user_roles VALUES (582, 1, '2017-07-10 18:17:43.068785', 610, 2);
INSERT INTO user_roles VALUES (583, 1, '2017-07-10 18:24:19.197389', 611, 2);
INSERT INTO user_roles VALUES (584, 1, '2017-07-10 18:29:16.732118', 612, 2);
INSERT INTO user_roles VALUES (585, 1, '2017-07-11 11:45:25.9853', 613, 4);
INSERT INTO user_roles VALUES (586, 1, '2017-07-11 16:26:53.716223', 614, 4);
INSERT INTO user_roles VALUES (587, 1, '2017-07-11 21:20:03.039274', 615, 4);
INSERT INTO user_roles VALUES (588, 1, '2017-07-12 11:03:53.587623', 618, 4);
INSERT INTO user_roles VALUES (589, 1, '2017-07-12 11:05:18.550483', 619, 4);
INSERT INTO user_roles VALUES (590, 1, '2017-07-12 11:06:26.922394', 620, 4);
INSERT INTO user_roles VALUES (591, 1, '2017-07-12 11:08:37.476861', 621, 4);
INSERT INTO user_roles VALUES (592, 1, '2017-07-12 12:20:47.354516', 622, 4);
INSERT INTO user_roles VALUES (593, 1, '2017-07-12 12:47:29.842173', 623, 4);
INSERT INTO user_roles VALUES (594, 1, '2017-07-12 13:14:23.393463', 624, 4);
INSERT INTO user_roles VALUES (595, 1, '2017-07-12 13:18:20.619031', 625, 4);
INSERT INTO user_roles VALUES (596, 1, '2017-07-12 15:08:19.660475', 626, 4);
INSERT INTO user_roles VALUES (597, 1, '2017-07-12 15:26:21.681363', 627, 4);
INSERT INTO user_roles VALUES (598, 1, '2017-07-12 15:27:16.063473', 628, 4);
INSERT INTO user_roles VALUES (599, 1, '2017-07-12 19:22:09.080548', 629, 4);
INSERT INTO user_roles VALUES (600, 1, '2017-07-12 20:05:14.367418', 630, 4);


--
-- Name: user_roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('user_roles_id_seq', 600, true);


--
-- Data for Name: wedding_band; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--



--
-- Name: wedding_band_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('wedding_band_id_seq', 1, false);


--
-- Data for Name: wholesale; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--



--
-- Name: wholesale_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('wholesale_id_seq', 1, false);


--
-- Data for Name: wholesale_items; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--



--
-- Name: wholesale_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('wholesale_items_id_seq', 1, false);


--
-- Data for Name: wishlist; Type: TABLE DATA; Schema: public; Owner: htdnewweb
--



--
-- Name: wishlist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: htdnewweb
--

SELECT pg_catalog.setval('wishlist_id_seq', 1, false);


--
-- Name: address_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY address
    ADD CONSTRAINT address_pkey PRIMARY KEY (id);


--
-- Name: admin_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY admin_roles
    ADD CONSTRAINT admin_roles_pkey PRIMARY KEY (id);


--
-- Name: administrator_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY administrator
    ADD CONSTRAINT administrator_pkey PRIMARY KEY (id);


--
-- Name: banner_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY banner
    ADD CONSTRAINT banner_pkey PRIMARY KEY (id);


--
-- Name: category_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- Name: config_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY config
    ADD CONSTRAINT config_pkey PRIMARY KEY (id);


--
-- Name: contact_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY contact
    ADD CONSTRAINT contact_pkey PRIMARY KEY (id);


--
-- Name: coupon_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY coupon
    ADD CONSTRAINT coupon_pkey PRIMARY KEY (id);


--
-- Name: credit_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY credit
    ADD CONSTRAINT credit_pkey PRIMARY KEY (id);


--
-- Name: faq_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY faq
    ADD CONSTRAINT faq_pkey PRIMARY KEY (id);


--
-- Name: hipaa_pkey; Type: CONSTRAINT; Schema: public; Owner: root; Tablespace: 
--

ALTER TABLE ONLY hipaa
    ADD CONSTRAINT hipaa_pkey PRIMARY KEY (id);


--
-- Name: home_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY home
    ADD CONSTRAINT home_pkey PRIMARY KEY (id);


--
-- Name: media_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY media
    ADD CONSTRAINT media_pkey PRIMARY KEY (id);


--
-- Name: office_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY office
    ADD CONSTRAINT office_pkey PRIMARY KEY (id);


--
-- Name: order_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_pkey PRIMARY KEY (id);


--
-- Name: order_product_option_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY order_product_option
    ADD CONSTRAINT order_product_option_pkey PRIMARY KEY (id);


--
-- Name: order_products_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY order_products
    ADD CONSTRAINT order_products_pkey PRIMARY KEY (id);


--
-- Name: page_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY page
    ADD CONSTRAINT page_pkey PRIMARY KEY (id);


--
-- Name: payment_profiles_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY payment_profiles
    ADD CONSTRAINT payment_profiles_pkey PRIMARY KEY (id);


--
-- Name: practice_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY practice
    ADD CONSTRAINT practice_pkey PRIMARY KEY (id);


--
-- Name: preset_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY jewelry
    ADD CONSTRAINT preset_pkey PRIMARY KEY (id);


--
-- Name: privacy_pkey; Type: CONSTRAINT; Schema: public; Owner: root; Tablespace: 
--

ALTER TABLE ONLY privacy_policy
    ADD CONSTRAINT privacy_pkey PRIMARY KEY (id);


--
-- Name: product_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY product_categories
    ADD CONSTRAINT product_categories_pkey PRIMARY KEY (id);


--
-- Name: product_images_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY product_images
    ADD CONSTRAINT product_images_pkey PRIMARY KEY (id);


--
-- Name: product_map_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY product_map
    ADD CONSTRAINT product_map_pkey PRIMARY KEY (id);


--
-- Name: product_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- Name: quotes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY quotes
    ADD CONSTRAINT quotes_pkey PRIMARY KEY (id);


--
-- Name: reviews_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY reviews
    ADD CONSTRAINT reviews_pkey PRIMARY KEY (id);


--
-- Name: ring_material_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY ring_material
    ADD CONSTRAINT ring_material_pkey PRIMARY KEY (id);


--
-- Name: ring_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY ring
    ADD CONSTRAINT ring_pkey PRIMARY KEY (id);


--
-- Name: role_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- Name: shipping_method_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY shipping_method
    ADD CONSTRAINT shipping_method_pkey PRIMARY KEY (id);


--
-- Name: team_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY team
    ADD CONSTRAINT team_pkey PRIMARY KEY (id);


--
-- Name: transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY transaction
    ADD CONSTRAINT transaction_pkey PRIMARY KEY (id);


--
-- Name: user_favorite_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY user_favorite
    ADD CONSTRAINT user_favorite_pkey PRIMARY KEY (id);


--
-- Name: user_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: user_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY user_roles
    ADD CONSTRAINT user_roles_pkey PRIMARY KEY (id);


--
-- Name: wedding_band_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY wedding_band
    ADD CONSTRAINT wedding_band_pkey PRIMARY KEY (id);


--
-- Name: wholesale_items_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY wholesale_items
    ADD CONSTRAINT wholesale_items_pkey PRIMARY KEY (id);


--
-- Name: wholesale_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY wholesale
    ADD CONSTRAINT wholesale_pkey PRIMARY KEY (id);


--
-- Name: wishlist_pkey; Type: CONSTRAINT; Schema: public; Owner: htdnewweb; Tablespace: 
--

ALTER TABLE ONLY wishlist
    ADD CONSTRAINT wishlist_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: address; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE address FROM PUBLIC;
REVOKE ALL ON TABLE address FROM htdnewweb;
GRANT ALL ON TABLE address TO htdnewweb;
GRANT ALL ON TABLE address TO htdnewwe_user;
GRANT ALL ON TABLE address TO htdnewwe_db;


--
-- Name: address_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE address_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE address_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE address_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE address_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE address_id_seq TO postgres;
GRANT ALL ON SEQUENCE address_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE address_id_seq TO htdnewwe_db;


--
-- Name: admin_roles; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE admin_roles FROM PUBLIC;
REVOKE ALL ON TABLE admin_roles FROM htdnewweb;
GRANT ALL ON TABLE admin_roles TO htdnewweb;
GRANT ALL ON TABLE admin_roles TO htdnewwe_user;
GRANT ALL ON TABLE admin_roles TO htdnewwe_db;


--
-- Name: admin_roles_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE admin_roles_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE admin_roles_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE admin_roles_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE admin_roles_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE admin_roles_id_seq TO postgres;
GRANT ALL ON SEQUENCE admin_roles_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE admin_roles_id_seq TO htdnewwe_db;


--
-- Name: administrator; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE administrator FROM PUBLIC;
REVOKE ALL ON TABLE administrator FROM htdnewweb;
GRANT ALL ON TABLE administrator TO htdnewweb;
GRANT ALL ON TABLE administrator TO htdnewwe_user;
GRANT ALL ON TABLE administrator TO htdnewwe_db;


--
-- Name: administrator_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE administrator_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE administrator_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE administrator_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE administrator_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE administrator_id_seq TO postgres;
GRANT ALL ON SEQUENCE administrator_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE administrator_id_seq TO htdnewwe_db;


--
-- Name: banner; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE banner FROM PUBLIC;
REVOKE ALL ON TABLE banner FROM htdnewweb;
GRANT ALL ON TABLE banner TO htdnewweb;
GRANT ALL ON TABLE banner TO htdnewwe_user;
GRANT ALL ON TABLE banner TO htdnewwe_db;


--
-- Name: banner_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE banner_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE banner_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE banner_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE banner_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE banner_id_seq TO postgres;
GRANT ALL ON SEQUENCE banner_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE banner_id_seq TO htdnewwe_db;


--
-- Name: category; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE category FROM PUBLIC;
REVOKE ALL ON TABLE category FROM htdnewweb;
GRANT ALL ON TABLE category TO htdnewweb;
GRANT ALL ON TABLE category TO htdnewwe_user;
GRANT ALL ON TABLE category TO htdnewwe_db;


--
-- Name: category_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE category_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE category_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE category_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE category_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE category_id_seq TO postgres;
GRANT ALL ON SEQUENCE category_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE category_id_seq TO htdnewwe_db;


--
-- Name: config; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE config FROM PUBLIC;
REVOKE ALL ON TABLE config FROM htdnewweb;
GRANT ALL ON TABLE config TO htdnewweb;
GRANT ALL ON TABLE config TO htdnewwe_user;
GRANT ALL ON TABLE config TO htdnewwe_db;


--
-- Name: config_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE config_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE config_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE config_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE config_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE config_id_seq TO postgres;
GRANT ALL ON SEQUENCE config_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE config_id_seq TO htdnewwe_db;


--
-- Name: contact; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE contact FROM PUBLIC;
REVOKE ALL ON TABLE contact FROM htdnewweb;
GRANT ALL ON TABLE contact TO htdnewweb;
GRANT ALL ON TABLE contact TO htdnewwe_user;
GRANT ALL ON TABLE contact TO htdnewwe_db;


--
-- Name: contact_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE contact_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE contact_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE contact_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE contact_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE contact_id_seq TO htdnewwe_db;


--
-- Name: coupon; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE coupon FROM PUBLIC;
REVOKE ALL ON TABLE coupon FROM htdnewweb;
GRANT ALL ON TABLE coupon TO htdnewweb;
GRANT ALL ON TABLE coupon TO htdnewwe_user;
GRANT ALL ON TABLE coupon TO htdnewwe_db;


--
-- Name: coupon_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE coupon_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE coupon_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE coupon_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE coupon_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE coupon_id_seq TO postgres;
GRANT ALL ON SEQUENCE coupon_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE coupon_id_seq TO htdnewwe_db;


--
-- Name: credit; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE credit FROM PUBLIC;
REVOKE ALL ON TABLE credit FROM htdnewweb;
GRANT ALL ON TABLE credit TO htdnewweb;
GRANT ALL ON TABLE credit TO htdnewwe_user;
GRANT ALL ON TABLE credit TO htdnewwe_db;


--
-- Name: credit_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE credit_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE credit_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE credit_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE credit_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE credit_id_seq TO postgres;
GRANT ALL ON SEQUENCE credit_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE credit_id_seq TO htdnewwe_db;


--
-- Name: faq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE faq FROM PUBLIC;
REVOKE ALL ON TABLE faq FROM postgres;
GRANT ALL ON TABLE faq TO postgres;
GRANT ALL ON TABLE faq TO htdnewweb;
GRANT ALL ON TABLE faq TO htdnewwe_user;
GRANT ALL ON TABLE faq TO htdnewwe_db;


--
-- Name: home; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE home FROM PUBLIC;
REVOKE ALL ON TABLE home FROM postgres;
GRANT ALL ON TABLE home TO postgres;
GRANT ALL ON TABLE home TO htdnewweb;
GRANT ALL ON TABLE home TO htdnewwe_user;
GRANT ALL ON TABLE home TO htdnewwe_db;


--
-- Name: jewelry; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE jewelry FROM PUBLIC;
REVOKE ALL ON TABLE jewelry FROM htdnewweb;
GRANT ALL ON TABLE jewelry TO htdnewweb;
GRANT ALL ON TABLE jewelry TO PUBLIC;
GRANT ALL ON TABLE jewelry TO postgres;
GRANT ALL ON TABLE jewelry TO htdnewwe_user;
GRANT ALL ON TABLE jewelry TO htdnewwe_db;


--
-- Name: jewelry_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE jewelry_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE jewelry_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE jewelry_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE jewelry_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE jewelry_id_seq TO postgres;
GRANT ALL ON SEQUENCE jewelry_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE jewelry_id_seq TO htdnewwe_db;


--
-- Name: media; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE media FROM PUBLIC;
REVOKE ALL ON TABLE media FROM postgres;
GRANT ALL ON TABLE media TO postgres;
GRANT ALL ON TABLE media TO htdnewweb;
GRANT ALL ON TABLE media TO htdnewwe_user;
GRANT ALL ON TABLE media TO htdnewwe_db;


--
-- Name: office; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE office FROM PUBLIC;
REVOKE ALL ON TABLE office FROM htdnewweb;
GRANT ALL ON TABLE office TO htdnewweb;
GRANT ALL ON TABLE office TO htdnewwe_user WITH GRANT OPTION;
GRANT ALL ON TABLE office TO htdnewwe_db WITH GRANT OPTION;


--
-- Name: order; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE "order" FROM PUBLIC;
REVOKE ALL ON TABLE "order" FROM htdnewweb;
GRANT ALL ON TABLE "order" TO htdnewweb;
GRANT ALL ON TABLE "order" TO htdnewwe_user;
GRANT ALL ON TABLE "order" TO htdnewwe_db;


--
-- Name: order_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE order_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE order_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE order_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE order_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE order_id_seq TO htdnewwe_db;


--
-- Name: order_product_option; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE order_product_option FROM PUBLIC;
REVOKE ALL ON TABLE order_product_option FROM htdnewweb;
GRANT ALL ON TABLE order_product_option TO htdnewweb;
GRANT ALL ON TABLE order_product_option TO PUBLIC;
GRANT ALL ON TABLE order_product_option TO postgres;
GRANT ALL ON TABLE order_product_option TO htdnewwe_user;
GRANT ALL ON TABLE order_product_option TO htdnewwe_db;


--
-- Name: order_product_option_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE order_product_option_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE order_product_option_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE order_product_option_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE order_product_option_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE order_product_option_id_seq TO postgres;
GRANT ALL ON SEQUENCE order_product_option_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE order_product_option_id_seq TO htdnewwe_db;


--
-- Name: order_products; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE order_products FROM PUBLIC;
REVOKE ALL ON TABLE order_products FROM htdnewweb;
GRANT ALL ON TABLE order_products TO htdnewweb;
GRANT ALL ON TABLE order_products TO htdnewwe_user;
GRANT ALL ON TABLE order_products TO htdnewwe_db;


--
-- Name: order_products_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE order_products_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE order_products_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE order_products_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE order_products_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE order_products_id_seq TO htdnewwe_db;


--
-- Name: page; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE page FROM PUBLIC;
REVOKE ALL ON TABLE page FROM htdnewweb;
GRANT ALL ON TABLE page TO htdnewweb;
GRANT ALL ON TABLE page TO htdnewwe_user;
GRANT ALL ON TABLE page TO htdnewwe_db;


--
-- Name: page_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE page_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE page_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE page_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE page_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE page_id_seq TO postgres;
GRANT ALL ON SEQUENCE page_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE page_id_seq TO htdnewwe_db;


--
-- Name: payment_profiles; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE payment_profiles FROM PUBLIC;
REVOKE ALL ON TABLE payment_profiles FROM htdnewweb;
GRANT ALL ON TABLE payment_profiles TO htdnewweb;
GRANT ALL ON TABLE payment_profiles TO htdnewwe_user;
GRANT ALL ON TABLE payment_profiles TO htdnewwe_db;


--
-- Name: payment_profiles_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE payment_profiles_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE payment_profiles_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE payment_profiles_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE payment_profiles_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE payment_profiles_id_seq TO postgres;
GRANT ALL ON SEQUENCE payment_profiles_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE payment_profiles_id_seq TO htdnewwe_db;


--
-- Name: practice_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE practice_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE practice_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE practice_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE practice_id_seq TO htdnewwe_user WITH GRANT OPTION;
GRANT ALL ON SEQUENCE practice_id_seq TO htdnewwe_db WITH GRANT OPTION;


--
-- Name: practice; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE practice FROM PUBLIC;
REVOKE ALL ON TABLE practice FROM htdnewweb;
GRANT ALL ON TABLE practice TO htdnewweb;
GRANT ALL ON TABLE practice TO htdnewwe_user WITH GRANT OPTION;
GRANT ALL ON TABLE practice TO htdnewwe_db WITH GRANT OPTION;


--
-- Name: product; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE product FROM PUBLIC;
REVOKE ALL ON TABLE product FROM htdnewweb;
GRANT ALL ON TABLE product TO htdnewweb;
GRANT ALL ON TABLE product TO htdnewwe_user;
GRANT ALL ON TABLE product TO htdnewwe_db;


--
-- Name: product_categories; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE product_categories FROM PUBLIC;
REVOKE ALL ON TABLE product_categories FROM htdnewweb;
GRANT ALL ON TABLE product_categories TO htdnewweb;
GRANT ALL ON TABLE product_categories TO htdnewwe_user;
GRANT ALL ON TABLE product_categories TO htdnewwe_db;


--
-- Name: product_categories_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE product_categories_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE product_categories_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE product_categories_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE product_categories_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE product_categories_id_seq TO htdnewwe_db;


--
-- Name: product_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE product_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE product_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE product_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE product_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE product_id_seq TO postgres;
GRANT ALL ON SEQUENCE product_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE product_id_seq TO htdnewwe_db;


--
-- Name: product_images; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE product_images FROM PUBLIC;
REVOKE ALL ON TABLE product_images FROM htdnewweb;
GRANT ALL ON TABLE product_images TO htdnewweb;
GRANT ALL ON TABLE product_images TO htdnewwe_user;
GRANT ALL ON TABLE product_images TO htdnewwe_db;


--
-- Name: product_images_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE product_images_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE product_images_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE product_images_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE product_images_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE product_images_id_seq TO htdnewwe_db;


--
-- Name: product_map; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE product_map FROM PUBLIC;
REVOKE ALL ON TABLE product_map FROM htdnewweb;
GRANT ALL ON TABLE product_map TO htdnewweb;
GRANT ALL ON TABLE product_map TO htdnewwe_user;
GRANT ALL ON TABLE product_map TO htdnewwe_db;


--
-- Name: product_map_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE product_map_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE product_map_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE product_map_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE product_map_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE product_map_id_seq TO postgres;
GRANT ALL ON SEQUENCE product_map_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE product_map_id_seq TO htdnewwe_db;


--
-- Name: quotes; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE quotes FROM PUBLIC;
REVOKE ALL ON TABLE quotes FROM postgres;
GRANT ALL ON TABLE quotes TO postgres;
GRANT ALL ON TABLE quotes TO htdnewweb;
GRANT ALL ON TABLE quotes TO htdnewwe_user;
GRANT ALL ON TABLE quotes TO htdnewwe_db;


--
-- Name: register_request; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE register_request FROM PUBLIC;
REVOKE ALL ON TABLE register_request FROM htdnewweb;
GRANT ALL ON TABLE register_request TO htdnewweb;
GRANT ALL ON TABLE register_request TO htdnewwe_user;
GRANT ALL ON TABLE register_request TO htdnewwe_db;


--
-- Name: register_request_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE register_request_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE register_request_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE register_request_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE register_request_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE register_request_id_seq TO htdnewwe_db;


--
-- Name: reviews; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE reviews FROM PUBLIC;
REVOKE ALL ON TABLE reviews FROM postgres;
GRANT ALL ON TABLE reviews TO postgres;
GRANT ALL ON TABLE reviews TO htdnewweb;
GRANT ALL ON TABLE reviews TO htdnewwe_user;
GRANT ALL ON TABLE reviews TO htdnewwe_db;


--
-- Name: ring; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE ring FROM PUBLIC;
REVOKE ALL ON TABLE ring FROM htdnewweb;
GRANT ALL ON TABLE ring TO htdnewweb;
GRANT ALL ON TABLE ring TO htdnewwe_user;
GRANT ALL ON TABLE ring TO htdnewwe_db;


--
-- Name: ring_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE ring_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE ring_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE ring_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE ring_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE ring_id_seq TO postgres;
GRANT ALL ON SEQUENCE ring_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE ring_id_seq TO htdnewwe_db;


--
-- Name: ring_material; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE ring_material FROM PUBLIC;
REVOKE ALL ON TABLE ring_material FROM htdnewweb;
GRANT ALL ON TABLE ring_material TO htdnewweb;
GRANT ALL ON TABLE ring_material TO htdnewwe_user;
GRANT ALL ON TABLE ring_material TO htdnewwe_db;


--
-- Name: ring_material_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE ring_material_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE ring_material_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE ring_material_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE ring_material_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE ring_material_id_seq TO htdnewwe_db;


--
-- Name: role; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE role FROM PUBLIC;
REVOKE ALL ON TABLE role FROM htdnewweb;
GRANT ALL ON TABLE role TO htdnewweb;
GRANT ALL ON TABLE role TO htdnewwe_user;
GRANT ALL ON TABLE role TO htdnewwe_db;


--
-- Name: role_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE role_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE role_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE role_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE role_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE role_id_seq TO htdnewwe_db;


--
-- Name: shipping_method; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE shipping_method FROM PUBLIC;
REVOKE ALL ON TABLE shipping_method FROM htdnewweb;
GRANT ALL ON TABLE shipping_method TO htdnewweb;
GRANT ALL ON TABLE shipping_method TO htdnewwe_user;
GRANT ALL ON TABLE shipping_method TO htdnewwe_db;


--
-- Name: shipping_method_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE shipping_method_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE shipping_method_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE shipping_method_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE shipping_method_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE shipping_method_id_seq TO postgres;
GRANT ALL ON SEQUENCE shipping_method_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE shipping_method_id_seq TO htdnewwe_db;


--
-- Name: team; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE team FROM PUBLIC;
REVOKE ALL ON TABLE team FROM postgres;
GRANT ALL ON TABLE team TO postgres;
GRANT ALL ON TABLE team TO htdnewweb;
GRANT ALL ON TABLE team TO htdnewwe_user;
GRANT ALL ON TABLE team TO htdnewwe_db;


--
-- Name: transaction; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE transaction FROM PUBLIC;
REVOKE ALL ON TABLE transaction FROM htdnewweb;
GRANT ALL ON TABLE transaction TO htdnewweb;
GRANT ALL ON TABLE transaction TO htdnewwe_user;
GRANT ALL ON TABLE transaction TO htdnewwe_db;


--
-- Name: transaction_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE transaction_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE transaction_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE transaction_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE transaction_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE transaction_id_seq TO postgres;
GRANT ALL ON SEQUENCE transaction_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE transaction_id_seq TO htdnewwe_db;


--
-- Name: user; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE "user" FROM PUBLIC;
REVOKE ALL ON TABLE "user" FROM htdnewweb;
GRANT ALL ON TABLE "user" TO htdnewweb;
GRANT ALL ON TABLE "user" TO htdnewwe_user;
GRANT ALL ON TABLE "user" TO htdnewwe_db;


--
-- Name: user_favorite; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE user_favorite FROM PUBLIC;
REVOKE ALL ON TABLE user_favorite FROM htdnewweb;
GRANT ALL ON TABLE user_favorite TO htdnewweb;
GRANT ALL ON TABLE user_favorite TO htdnewwe_user;
GRANT ALL ON TABLE user_favorite TO htdnewwe_db;


--
-- Name: user_favorite_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE user_favorite_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE user_favorite_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE user_favorite_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE user_favorite_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE user_favorite_id_seq TO htdnewwe_db;


--
-- Name: user_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE user_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE user_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE user_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE user_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE user_id_seq TO htdnewwe_db;


--
-- Name: user_roles; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE user_roles FROM PUBLIC;
REVOKE ALL ON TABLE user_roles FROM htdnewweb;
GRANT ALL ON TABLE user_roles TO htdnewweb;
GRANT ALL ON TABLE user_roles TO htdnewwe_user;
GRANT ALL ON TABLE user_roles TO htdnewwe_db;


--
-- Name: user_roles.id; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL(id) ON TABLE user_roles FROM PUBLIC;
REVOKE ALL(id) ON TABLE user_roles FROM htdnewweb;
GRANT ALL(id) ON TABLE user_roles TO PUBLIC;


--
-- Name: user_roles_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE user_roles_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE user_roles_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE user_roles_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE user_roles_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE user_roles_id_seq TO htdnewwe_db;


--
-- Name: wedding_band; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE wedding_band FROM PUBLIC;
REVOKE ALL ON TABLE wedding_band FROM htdnewweb;
GRANT ALL ON TABLE wedding_band TO htdnewweb;
GRANT ALL ON TABLE wedding_band TO htdnewwe_user;
GRANT ALL ON TABLE wedding_band TO htdnewwe_db;


--
-- Name: wedding_band_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE wedding_band_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE wedding_band_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE wedding_band_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE wedding_band_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE wedding_band_id_seq TO postgres;
GRANT ALL ON SEQUENCE wedding_band_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE wedding_band_id_seq TO htdnewwe_db;


--
-- Name: wholesale; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE wholesale FROM PUBLIC;
REVOKE ALL ON TABLE wholesale FROM htdnewweb;
GRANT ALL ON TABLE wholesale TO htdnewweb;
GRANT ALL ON TABLE wholesale TO htdnewwe_user;
GRANT ALL ON TABLE wholesale TO htdnewwe_db;


--
-- Name: wholesale_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE wholesale_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE wholesale_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE wholesale_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE wholesale_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE wholesale_id_seq TO htdnewwe_db;


--
-- Name: wholesale_items; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE wholesale_items FROM PUBLIC;
REVOKE ALL ON TABLE wholesale_items FROM htdnewweb;
GRANT ALL ON TABLE wholesale_items TO htdnewweb;
GRANT ALL ON TABLE wholesale_items TO htdnewwe_user;
GRANT ALL ON TABLE wholesale_items TO htdnewwe_db;


--
-- Name: wholesale_items_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE wholesale_items_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE wholesale_items_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE wholesale_items_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE wholesale_items_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE wholesale_items_id_seq TO htdnewwe_db;


--
-- Name: wishlist; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON TABLE wishlist FROM PUBLIC;
REVOKE ALL ON TABLE wishlist FROM htdnewweb;
GRANT ALL ON TABLE wishlist TO htdnewweb;
GRANT ALL ON TABLE wishlist TO htdnewwe_user;
GRANT ALL ON TABLE wishlist TO htdnewwe_db;


--
-- Name: wishlist_id_seq; Type: ACL; Schema: public; Owner: htdnewweb
--

REVOKE ALL ON SEQUENCE wishlist_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE wishlist_id_seq FROM htdnewweb;
GRANT ALL ON SEQUENCE wishlist_id_seq TO htdnewweb;
GRANT ALL ON SEQUENCE wishlist_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE wishlist_id_seq TO postgres;
GRANT ALL ON SEQUENCE wishlist_id_seq TO htdnewwe_user;
GRANT ALL ON SEQUENCE wishlist_id_seq TO htdnewwe_db;


--
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: -; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres REVOKE ALL ON TABLES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres REVOKE ALL ON TABLES  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres GRANT ALL ON TABLES  TO postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres GRANT ALL ON TABLES  TO htdnewweb;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres GRANT ALL ON TABLES  TO htdnewwe_user;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres GRANT ALL ON TABLES  TO htdnewwe_db;


--
-- PostgreSQL database dump complete
--

