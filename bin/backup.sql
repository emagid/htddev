--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5beta1
-- Dumped by pg_dump version 9.5beta1

-- Started on 2017-08-15 11:42:27

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 244 (class 3079 OID 12355)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2574 (class 0 OID 0)
-- Dependencies: 244
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 180 (class 1259 OID 133197)
-- Name: address; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE address (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    is_default boolean DEFAULT false,
    user_id integer,
    first_name character varying,
    last_name character varying,
    phone character varying,
    address character varying,
    address2 character varying,
    city character varying,
    state character varying,
    country character varying,
    zip character varying,
    label character varying
);


ALTER TABLE address OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 133206)
-- Name: address_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE address_id_seq OWNER TO postgres;

--
-- TOC entry 2575 (class 0 OID 0)
-- Dependencies: 181
-- Name: address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE address_id_seq OWNED BY address.id;


--
-- TOC entry 182 (class 1259 OID 133208)
-- Name: admin_roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE admin_roles (
    id integer NOT NULL,
    active smallint DEFAULT 1 NOT NULL,
    insert_time timestamp without time zone DEFAULT now() NOT NULL,
    admin_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE admin_roles OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 133213)
-- Name: admin_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE admin_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin_roles_id_seq OWNER TO postgres;

--
-- TOC entry 2577 (class 0 OID 0)
-- Dependencies: 183
-- Name: admin_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE admin_roles_id_seq OWNED BY admin_roles.id;


--
-- TOC entry 184 (class 1259 OID 133215)
-- Name: administrator; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE administrator (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    first_name character varying,
    last_name character varying,
    email character varying,
    username character varying,
    password character varying,
    hash character varying,
    permissions character varying,
    signup_ip character varying,
    update_time character varying
);


ALTER TABLE administrator OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 133223)
-- Name: administrator_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE administrator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE administrator_id_seq OWNER TO postgres;

--
-- TOC entry 2579 (class 0 OID 0)
-- Dependencies: 185
-- Name: administrator_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE administrator_id_seq OWNED BY administrator.id;


--
-- TOC entry 186 (class 1259 OID 133225)
-- Name: banner; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE banner (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    title character varying,
    image character varying,
    link character varying,
    date_modified character varying,
    is_deal boolean DEFAULT false,
    featured_id integer DEFAULT 0,
    url character varying,
    banner_order integer
);


ALTER TABLE banner OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 133235)
-- Name: banner_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE banner_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE banner_id_seq OWNER TO postgres;

--
-- TOC entry 2581 (class 0 OID 0)
-- Dependencies: 187
-- Name: banner_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE banner_id_seq OWNED BY banner.id;


--
-- TOC entry 188 (class 1259 OID 133237)
-- Name: category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE category (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    subtitle character varying,
    parent_category integer,
    description character varying,
    slug character varying,
    meta_title character varying,
    meta_keywords character varying,
    meta_description character varying,
    display_order integer,
    in_menu integer,
    image character varying
);


ALTER TABLE category OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 133245)
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE category_id_seq OWNER TO postgres;

--
-- TOC entry 2583 (class 0 OID 0)
-- Dependencies: 189
-- Name: category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE category_id_seq OWNED BY category.id;


--
-- TOC entry 190 (class 1259 OID 133247)
-- Name: config; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE config (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    value character varying
);


ALTER TABLE config OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 133255)
-- Name: config_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE config_id_seq OWNER TO postgres;

--
-- TOC entry 2585 (class 0 OID 0)
-- Dependencies: 191
-- Name: config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE config_id_seq OWNED BY config.id;


--
-- TOC entry 192 (class 1259 OID 133257)
-- Name: contact; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE contact (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    email character varying,
    phone character varying,
    message text
);


ALTER TABLE contact OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 133265)
-- Name: contact_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE contact_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE contact_id_seq OWNER TO postgres;

--
-- TOC entry 2587 (class 0 OID 0)
-- Dependencies: 193
-- Name: contact_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE contact_id_seq OWNED BY contact.id;


--
-- TOC entry 194 (class 1259 OID 133267)
-- Name: coupon; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE coupon (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    code character varying,
    discount_type character varying,
    discount_amount character varying,
    min_amount character varying,
    num_uses_all character varying,
    uses_per_user character varying,
    start_time character varying,
    end_time character varying
);


ALTER TABLE coupon OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 133275)
-- Name: coupon_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE coupon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE coupon_id_seq OWNER TO postgres;

--
-- TOC entry 2588 (class 0 OID 0)
-- Dependencies: 195
-- Name: coupon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE coupon_id_seq OWNED BY coupon.id;


--
-- TOC entry 196 (class 1259 OID 133277)
-- Name: credit; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE credit (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    description character varying,
    price character varying,
    value character varying
);


ALTER TABLE credit OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 133285)
-- Name: credit_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE credit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE credit_id_seq OWNER TO postgres;

--
-- TOC entry 2590 (class 0 OID 0)
-- Dependencies: 197
-- Name: credit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE credit_id_seq OWNED BY credit.id;


--
-- TOC entry 198 (class 1259 OID 133287)
-- Name: jewelry; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE jewelry (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    description character varying,
    price numeric(10,2),
    currency_code character varying,
    quantity integer,
    tags character varying,
    materials character varying,
    images text,
    variation text,
    meta_title character varying,
    meta_keywords character varying,
    meta_description character varying,
    metal_colors text DEFAULT '{"14kt_gold":{"White":0,"Yellow":0,"Rose":0},"18kt_gold":{"White":0,"Yellow":0,"Rose":0},"Platinum":{"White":0,"Yellow":0,"Rose":0}}'::text,
    metals character varying DEFAULT '14kt_gold, 18kt_gold, Platinum'::character varying,
    total_14kt_price numeric(10,2) DEFAULT 0,
    total_18kt_price numeric(10,2) DEFAULT 0,
    total_plat_price numeric(10,2) DEFAULT 0,
    slug character varying,
    semi_mount numeric(5,2),
    stone_breakdown character varying,
    stone_count integer,
    weight numeric(10,2),
    diamond_cwwt character varying,
    diamond_color character varying,
    diamond_quality character varying,
    _14kt numeric(10,2),
    _18kt numeric(10,2),
    platinum numeric(10,2),
    setting integer,
    polish integer,
    melee numeric(10,2),
    ship_from_cast numeric(10,2),
    ship_to_cust numeric(10,2),
    memo_duration integer DEFAULT 0,
    featured integer DEFAULT 0,
    discount integer DEFAULT 0
);


ALTER TABLE jewelry OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 133301)
-- Name: jewelry_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE jewelry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE jewelry_id_seq OWNER TO postgres;

--
-- TOC entry 2593 (class 0 OID 0)
-- Dependencies: 199
-- Name: jewelry_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE jewelry_id_seq OWNED BY jewelry.id;


--
-- TOC entry 200 (class 1259 OID 133303)
-- Name: order; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "order" (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    bill_first_name character varying,
    bill_last_name character varying,
    bill_address character varying,
    bill_address2 character varying,
    bill_city character varying,
    bill_state character varying,
    bill_country character varying,
    bill_zip character varying,
    ship_first_name character varying,
    ship_last_name character varying,
    ship_address character varying,
    ship_address2 character varying,
    ship_city character varying,
    ship_state character varying,
    ship_country character varying,
    ship_zip character varying,
    cc_number character varying,
    cc_expiration_month character varying,
    cc_expiration_year character varying,
    cc_ccv character varying,
    tax character varying,
    tax_rate character varying,
    status character varying,
    tracking_number character varying,
    shipping_method character varying,
    user_id integer,
    coupon_code character varying,
    gift_card character varying,
    email character varying,
    shipping_cost real,
    payment_method integer,
    subtotal real,
    total real,
    viewed boolean,
    phone character varying,
    coupon_type integer,
    coupon_amount numeric,
    note character varying,
    user_role character varying,
    ref_num character varying,
    order_type integer,
    signature character varying
);


ALTER TABLE "order" OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 133311)
-- Name: order_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE order_id_seq OWNER TO postgres;

--
-- TOC entry 2595 (class 0 OID 0)
-- Dependencies: 201
-- Name: order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE order_id_seq OWNED BY "order".id;


--
-- TOC entry 202 (class 1259 OID 133313)
-- Name: order_product_option; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE order_product_option (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    order_product_id integer,
    data text,
    price numeric(10,2) DEFAULT 0
);


ALTER TABLE order_product_option OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 133322)
-- Name: order_product_option_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE order_product_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE order_product_option_id_seq OWNER TO postgres;

--
-- TOC entry 2597 (class 0 OID 0)
-- Dependencies: 203
-- Name: order_product_option_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE order_product_option_id_seq OWNED BY order_product_option.id;


--
-- TOC entry 204 (class 1259 OID 133324)
-- Name: order_products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE order_products (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    order_id integer,
    quantity integer,
    unit_price real,
    product_map_id integer NOT NULL
);


ALTER TABLE order_products OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 133329)
-- Name: order_products_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE order_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE order_products_id_seq OWNER TO postgres;

--
-- TOC entry 2599 (class 0 OID 0)
-- Dependencies: 205
-- Name: order_products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE order_products_id_seq OWNED BY order_products.id;


--
-- TOC entry 206 (class 1259 OID 133331)
-- Name: page; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE page (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    title character varying,
    slug character varying,
    description character varying,
    featured_image character varying,
    meta_title character varying,
    meta_keywords character varying,
    meta_description character varying,
    date_modified character varying,
    content text
);


ALTER TABLE page OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 133339)
-- Name: page_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE page_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE page_id_seq OWNER TO postgres;

--
-- TOC entry 2600 (class 0 OID 0)
-- Dependencies: 207
-- Name: page_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE page_id_seq OWNED BY page.id;


--
-- TOC entry 208 (class 1259 OID 133341)
-- Name: payment_profiles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE payment_profiles (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    user_id integer,
    first_name character varying,
    last_name character varying,
    phone character varying,
    address character varying,
    address2 character varying,
    city character varying,
    state character varying,
    country character varying,
    zip character varying,
    cc_number character varying,
    cc_expiration_month character varying,
    cc_expiration_year character varying,
    cc_ccv character varying,
    label character varying
);


ALTER TABLE payment_profiles OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 133349)
-- Name: payment_profiles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE payment_profiles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE payment_profiles_id_seq OWNER TO postgres;

--
-- TOC entry 2602 (class 0 OID 0)
-- Dependencies: 209
-- Name: payment_profiles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE payment_profiles_id_seq OWNED BY payment_profiles.id;


--
-- TOC entry 210 (class 1259 OID 133351)
-- Name: product; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    sku character varying,
    price character varying,
    meta_title character varying,
    meta_keywords character varying,
    meta_description character varying,
    featured_image character varying,
    colors character varying,
    featured smallint,
    shape character varying,
    clarity character varying,
    weight character varying,
    lab character varying,
    cut_grade character varying,
    polish character varying,
    symmetry character varying,
    fluor character varying,
    rapaport_price character varying,
    total character varying,
    certificate character varying,
    length character varying,
    width character varying,
    depth character varying,
    depth_percent character varying,
    table_percent character varying,
    girdle character varying,
    culet character varying,
    description character varying,
    origin character varying,
    memo_status character varying,
    inscription character varying,
    certificate_file character varying,
    slug character varying,
    wholesale_price numeric(10,2) DEFAULT 0,
    video_link character varying,
    quantity integer,
    memo_duration integer DEFAULT 0,
    vendor character varying,
    discount integer DEFAULT 0
);


ALTER TABLE product OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 133361)
-- Name: product_categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_categories (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    category_id integer,
    product_map_id integer NOT NULL
);


ALTER TABLE product_categories OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 133366)
-- Name: product_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE product_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_categories_id_seq OWNER TO postgres;

--
-- TOC entry 2604 (class 0 OID 0)
-- Dependencies: 212
-- Name: product_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE product_categories_id_seq OWNED BY product_categories.id;


--
-- TOC entry 213 (class 1259 OID 133368)
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_id_seq OWNER TO postgres;

--
-- TOC entry 2605 (class 0 OID 0)
-- Dependencies: 213
-- Name: product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE product_id_seq OWNED BY product.id;


--
-- TOC entry 214 (class 1259 OID 133370)
-- Name: product_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_images (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    image character varying,
    display_order integer,
    product_map_id integer NOT NULL
);


ALTER TABLE product_images OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 133378)
-- Name: product_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE product_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_images_id_seq OWNER TO postgres;

--
-- TOC entry 2607 (class 0 OID 0)
-- Dependencies: 215
-- Name: product_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE product_images_id_seq OWNED BY product_images.id;


--
-- TOC entry 216 (class 1259 OID 133380)
-- Name: product_map; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_map (
    id integer NOT NULL,
    active integer DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    product_id integer,
    product_type_id integer
);


ALTER TABLE product_map OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 133385)
-- Name: product_map_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE product_map_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_map_id_seq OWNER TO postgres;

--
-- TOC entry 2608 (class 0 OID 0)
-- Dependencies: 217
-- Name: product_map_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE product_map_id_seq OWNED BY product_map.id;


--
-- TOC entry 243 (class 1259 OID 133735)
-- Name: register_request; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE register_request (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    first_name character varying,
    last_name character varying,
    company character varying,
    email character varying,
    phone character varying,
    full_address character varying,
    status integer DEFAULT 0
);


ALTER TABLE register_request OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 133733)
-- Name: register_request_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE register_request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE register_request_id_seq OWNER TO postgres;

--
-- TOC entry 2610 (class 0 OID 0)
-- Dependencies: 242
-- Name: register_request_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE register_request_id_seq OWNED BY register_request.id;


--
-- TOC entry 218 (class 1259 OID 133387)
-- Name: ring; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE ring (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    price numeric(10,2),
    default_metal character varying,
    default_color character varying,
    dwt character varying,
    semi_mount_weight character varying,
    stone_breakdown character varying,
    center_stone_weight character varying,
    center_stone_shape character varying,
    total_stones character varying,
    semi_mount_diamond_weight character varying,
    diamond_color character varying,
    diamond_quality character varying,
    cost_per_dwt character varying,
    fourteenkt_gold_price character varying,
    eighteenkt_gold_price character varying,
    platinum_price character varying,
    website_fourteenkt_gold_price character varying,
    website_eighteenkt_gold_price character varying,
    website_platinum_price character varying,
    slug character varying,
    min_carat numeric(5,2) DEFAULT 0.0,
    max_carat numeric(5,2) DEFAULT 100,
    shape character varying,
    video_link character varying,
    metals character varying DEFAULT '14kt_gold, 18kt_gold, Platinum'::character varying,
    colors character varying DEFAULT 'White,Yellow,Rose'::character varying,
    metal_colors text DEFAULT '{"14kt_gold":{"White":1,"Yellow":1,"Rose":1},"18kt_gold":{"White":1,"Yellow":1,"Rose":0},"Platinum":{"White":1,"Yellow":0,"Rose":0}}'::text,
    setting_labor numeric(5,2),
    center_stone_setting_labor numeric(5,2),
    polish_labor numeric(5,2),
    head_to_add numeric(5,2),
    melee_cost numeric(5,2),
    shipping_from_cast numeric(5,2),
    shipping_to_customer numeric(5,2),
    total_14kt_cost numeric(10,2),
    total_18kt_cost numeric(10,2),
    total_plat_cost numeric(10,2),
    setting_style character varying,
    options text,
    alias character varying,
    meta_title character varying,
    meta_keywords character varying,
    meta_description character varying,
    description character varying,
    memo_duration integer DEFAULT 0,
    featured integer DEFAULT 0,
    discount integer DEFAULT 0
);


ALTER TABLE ring OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 133401)
-- Name: ring_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ring_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ring_id_seq OWNER TO postgres;

--
-- TOC entry 2611 (class 0 OID 0)
-- Dependencies: 219
-- Name: ring_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ring_id_seq OWNED BY ring.id;


--
-- TOC entry 220 (class 1259 OID 133403)
-- Name: ring_material; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE ring_material (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    price numeric(10,2) DEFAULT 0,
    min_size numeric(10,2) DEFAULT 4.00,
    max_size numeric(10,2) DEFAULT 9.00,
    size_step numeric(10,2) DEFAULT 0.25,
    size_step_price numeric(10,2) DEFAULT 10
);


ALTER TABLE ring_material OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 133416)
-- Name: ring_material_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ring_material_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ring_material_id_seq OWNER TO postgres;

--
-- TOC entry 2613 (class 0 OID 0)
-- Dependencies: 221
-- Name: ring_material_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ring_material_id_seq OWNED BY ring_material.id;


--
-- TOC entry 222 (class 1259 OID 133418)
-- Name: role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE role (
    id integer NOT NULL,
    active smallint DEFAULT 1 NOT NULL,
    insert_time timestamp without time zone DEFAULT now() NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE role OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 133426)
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE role_id_seq OWNER TO postgres;

--
-- TOC entry 2614 (class 0 OID 0)
-- Dependencies: 223
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE role_id_seq OWNED BY role.id;


--
-- TOC entry 224 (class 1259 OID 133428)
-- Name: shipping_method; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE shipping_method (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    is_default boolean DEFAULT false,
    name character varying,
    cart_subtotal_range_min numeric(10,0),
    cost numeric(10,0)
);


ALTER TABLE shipping_method OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 133437)
-- Name: shipping_method_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE shipping_method_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE shipping_method_id_seq OWNER TO postgres;

--
-- TOC entry 2615 (class 0 OID 0)
-- Dependencies: 225
-- Name: shipping_method_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE shipping_method_id_seq OWNED BY shipping_method.id;


--
-- TOC entry 226 (class 1259 OID 133439)
-- Name: transaction; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE transaction (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    order_id integer,
    authorize_net_data text,
    ref_trans_id character varying
);


ALTER TABLE transaction OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 133447)
-- Name: transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE transaction_id_seq OWNER TO postgres;

--
-- TOC entry 2617 (class 0 OID 0)
-- Dependencies: 227
-- Name: transaction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE transaction_id_seq OWNED BY transaction.id;


--
-- TOC entry 228 (class 1259 OID 133449)
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "user" (
    id integer NOT NULL,
    active smallint DEFAULT 1 NOT NULL,
    insert_time timestamp without time zone DEFAULT now() NOT NULL,
    email character varying NOT NULL,
    password character varying,
    hash character varying,
    first_name character varying,
    last_name character varying,
    zip numeric,
    phone character varying,
    dob timestamp without time zone,
    bank_name character varying,
    routing_number character varying,
    bank_address character varying,
    bank_city character varying,
    voided_check character varying,
    photo character varying,
    wholesale_id integer DEFAULT 0,
    status integer,
    company character varying,
    full_address character varying,
    setup integer DEFAULT 0,
    username character varying
);


ALTER TABLE "user" OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 133458)
-- Name: user_favorite; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE user_favorite (
    id integer NOT NULL,
    active smallint DEFAULT 1 NOT NULL,
    insert_time timestamp without time zone DEFAULT now() NOT NULL,
    user_id integer,
    provider_id integer
);


ALTER TABLE user_favorite OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 133463)
-- Name: user_favorite_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_favorite_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_favorite_id_seq OWNER TO postgres;

--
-- TOC entry 2619 (class 0 OID 0)
-- Dependencies: 230
-- Name: user_favorite_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_favorite_id_seq OWNED BY user_favorite.id;


--
-- TOC entry 231 (class 1259 OID 133465)
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_id_seq OWNER TO postgres;

--
-- TOC entry 2620 (class 0 OID 0)
-- Dependencies: 231
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_id_seq OWNED BY "user".id;


--
-- TOC entry 232 (class 1259 OID 133467)
-- Name: user_roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE user_roles (
    id integer NOT NULL,
    active smallint DEFAULT 1 NOT NULL,
    insert_time timestamp without time zone DEFAULT now() NOT NULL,
    user_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE user_roles OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 133472)
-- Name: user_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_roles_id_seq OWNER TO postgres;

--
-- TOC entry 2622 (class 0 OID 0)
-- Dependencies: 233
-- Name: user_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_roles_id_seq OWNED BY user_roles.id;


--
-- TOC entry 234 (class 1259 OID 133474)
-- Name: wedding_band; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE wedding_band (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    slug character varying,
    price numeric(10,2) DEFAULT 0,
    category character varying,
    diamond_color character varying,
    diamond_quality character varying,
    video_link character varying,
    metals character varying DEFAULT '14kt_gold, 18kt_gold, Platinum'::character varying,
    colors character varying DEFAULT 'White,Yellow,Rose'::character varying,
    metal_colors text DEFAULT '{"14kt_gold":{"White":1,"Yellow":1,"Rose":1},"18kt_gold":{"White":1,"Yellow":1,"Rose":1},"Platinum":{"White":1,"Yellow":0,"Rose":0}}'::text,
    setting_labor numeric(5,2),
    polish_labor numeric(5,2),
    head_to_add numeric(5,2),
    melee_cost numeric(5,2),
    shipping_from_cast numeric(5,2),
    shipping_to_customer numeric(5,2),
    total_14kt_cost numeric(10,2),
    total_18kt_cost numeric(10,2),
    total_plat_cost numeric(10,2),
    options text,
    setting_style character varying,
    alias character varying,
    meta_title character varying,
    meta_keywords character varying,
    meta_description character varying,
    memo_duration integer DEFAULT 0
);


ALTER TABLE wedding_band OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 133487)
-- Name: wedding_band_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE wedding_band_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE wedding_band_id_seq OWNER TO postgres;

--
-- TOC entry 2623 (class 0 OID 0)
-- Dependencies: 235
-- Name: wedding_band_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE wedding_band_id_seq OWNED BY wedding_band.id;


--
-- TOC entry 236 (class 1259 OID 133489)
-- Name: wholesale; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE wholesale (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    company character varying,
    job_title character varying,
    dba character varying,
    incorporation character varying,
    referrer character varying,
    phone character varying,
    alt_phone character varying,
    status integer DEFAULT 0,
    address character varying,
    address2 character varying,
    payment character varying,
    memo_duration integer DEFAULT 24,
    site character varying,
    logo character varying,
    discount text,
    tax_id integer
);


ALTER TABLE wholesale OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 133499)
-- Name: wholesale_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE wholesale_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE wholesale_id_seq OWNER TO postgres;

--
-- TOC entry 2625 (class 0 OID 0)
-- Dependencies: 237
-- Name: wholesale_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE wholesale_id_seq OWNED BY wholesale.id;


--
-- TOC entry 241 (class 1259 OID 133699)
-- Name: wholesale_items; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE wholesale_items (
    id integer NOT NULL,
    active integer DEFAULT 1,
    insert_time timestamp without time zone,
    price character varying,
    wholesale_id integer,
    product_id integer
);


ALTER TABLE wholesale_items OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 133697)
-- Name: wholesale_items_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE wholesale_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE wholesale_items_id_seq OWNER TO postgres;

--
-- TOC entry 2626 (class 0 OID 0)
-- Dependencies: 240
-- Name: wholesale_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE wholesale_items_id_seq OWNED BY wholesale_items.id;


--
-- TOC entry 238 (class 1259 OID 133501)
-- Name: wishlist; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE wishlist (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    user_id integer,
    data text
);


ALTER TABLE wishlist OWNER TO postgres;

--
-- TOC entry 239 (class 1259 OID 133509)
-- Name: wishlist_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE wishlist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE wishlist_id_seq OWNER TO postgres;

--
-- TOC entry 2627 (class 0 OID 0)
-- Dependencies: 239
-- Name: wishlist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE wishlist_id_seq OWNED BY wishlist.id;


--
-- TOC entry 2196 (class 2604 OID 133511)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY address ALTER COLUMN id SET DEFAULT nextval('address_id_seq'::regclass);


--
-- TOC entry 2199 (class 2604 OID 133512)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin_roles ALTER COLUMN id SET DEFAULT nextval('admin_roles_id_seq'::regclass);


--
-- TOC entry 2202 (class 2604 OID 133513)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY administrator ALTER COLUMN id SET DEFAULT nextval('administrator_id_seq'::regclass);


--
-- TOC entry 2207 (class 2604 OID 133514)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY banner ALTER COLUMN id SET DEFAULT nextval('banner_id_seq'::regclass);


--
-- TOC entry 2210 (class 2604 OID 133515)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY category ALTER COLUMN id SET DEFAULT nextval('category_id_seq'::regclass);


--
-- TOC entry 2213 (class 2604 OID 133516)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY config ALTER COLUMN id SET DEFAULT nextval('config_id_seq'::regclass);


--
-- TOC entry 2216 (class 2604 OID 133517)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contact ALTER COLUMN id SET DEFAULT nextval('contact_id_seq'::regclass);


--
-- TOC entry 2219 (class 2604 OID 133518)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY coupon ALTER COLUMN id SET DEFAULT nextval('coupon_id_seq'::regclass);


--
-- TOC entry 2222 (class 2604 OID 133519)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY credit ALTER COLUMN id SET DEFAULT nextval('credit_id_seq'::regclass);


--
-- TOC entry 2231 (class 2604 OID 133520)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY jewelry ALTER COLUMN id SET DEFAULT nextval('jewelry_id_seq'::regclass);


--
-- TOC entry 2236 (class 2604 OID 133521)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "order" ALTER COLUMN id SET DEFAULT nextval('order_id_seq'::regclass);


--
-- TOC entry 2240 (class 2604 OID 133522)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_product_option ALTER COLUMN id SET DEFAULT nextval('order_product_option_id_seq'::regclass);


--
-- TOC entry 2243 (class 2604 OID 133523)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_products ALTER COLUMN id SET DEFAULT nextval('order_products_id_seq'::regclass);


--
-- TOC entry 2246 (class 2604 OID 133524)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY page ALTER COLUMN id SET DEFAULT nextval('page_id_seq'::regclass);


--
-- TOC entry 2249 (class 2604 OID 133525)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY payment_profiles ALTER COLUMN id SET DEFAULT nextval('payment_profiles_id_seq'::regclass);


--
-- TOC entry 2254 (class 2604 OID 133526)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product ALTER COLUMN id SET DEFAULT nextval('product_id_seq'::regclass);


--
-- TOC entry 2258 (class 2604 OID 133527)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_categories ALTER COLUMN id SET DEFAULT nextval('product_categories_id_seq'::regclass);


--
-- TOC entry 2261 (class 2604 OID 133528)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_images ALTER COLUMN id SET DEFAULT nextval('product_images_id_seq'::regclass);


--
-- TOC entry 2264 (class 2604 OID 133529)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_map ALTER COLUMN id SET DEFAULT nextval('product_map_id_seq'::regclass);


--
-- TOC entry 2323 (class 2604 OID 133738)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY register_request ALTER COLUMN id SET DEFAULT nextval('register_request_id_seq'::regclass);


--
-- TOC entry 2273 (class 2604 OID 133530)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ring ALTER COLUMN id SET DEFAULT nextval('ring_id_seq'::regclass);


--
-- TOC entry 2283 (class 2604 OID 133531)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ring_material ALTER COLUMN id SET DEFAULT nextval('ring_material_id_seq'::regclass);


--
-- TOC entry 2286 (class 2604 OID 133532)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY role ALTER COLUMN id SET DEFAULT nextval('role_id_seq'::regclass);


--
-- TOC entry 2290 (class 2604 OID 133533)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY shipping_method ALTER COLUMN id SET DEFAULT nextval('shipping_method_id_seq'::regclass);


--
-- TOC entry 2293 (class 2604 OID 133534)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transaction ALTER COLUMN id SET DEFAULT nextval('transaction_id_seq'::regclass);


--
-- TOC entry 2297 (class 2604 OID 133535)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "user" ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);


--
-- TOC entry 2301 (class 2604 OID 133536)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_favorite ALTER COLUMN id SET DEFAULT nextval('user_favorite_id_seq'::regclass);


--
-- TOC entry 2304 (class 2604 OID 133537)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_roles ALTER COLUMN id SET DEFAULT nextval('user_roles_id_seq'::regclass);


--
-- TOC entry 2312 (class 2604 OID 133538)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY wedding_band ALTER COLUMN id SET DEFAULT nextval('wedding_band_id_seq'::regclass);


--
-- TOC entry 2317 (class 2604 OID 133539)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY wholesale ALTER COLUMN id SET DEFAULT nextval('wholesale_id_seq'::regclass);


--
-- TOC entry 2321 (class 2604 OID 133702)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY wholesale_items ALTER COLUMN id SET DEFAULT nextval('wholesale_items_id_seq'::regclass);


--
-- TOC entry 2320 (class 2604 OID 133540)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY wishlist ALTER COLUMN id SET DEFAULT nextval('wishlist_id_seq'::regclass);


--
-- TOC entry 2503 (class 0 OID 133197)
-- Dependencies: 180
-- Data for Name: address; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY address (id, active, insert_time, is_default, user_id, first_name, last_name, phone, address, address2, city, state, country, zip, label) FROM stdin;
\.


--
-- TOC entry 2629 (class 0 OID 0)
-- Dependencies: 181
-- Name: address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('address_id_seq', 1, false);


--
-- TOC entry 2505 (class 0 OID 133208)
-- Dependencies: 182
-- Data for Name: admin_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY admin_roles (id, active, insert_time, admin_id, role_id) FROM stdin;
1	1	2015-08-21 12:40:22.392	1	1
\.


--
-- TOC entry 2630 (class 0 OID 0)
-- Dependencies: 183
-- Name: admin_roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('admin_roles_id_seq', 1, true);


--
-- TOC entry 2507 (class 0 OID 133215)
-- Dependencies: 184
-- Data for Name: administrator; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY administrator (id, active, insert_time, first_name, last_name, email, username, password, hash, permissions, signup_ip, update_time) FROM stdin;
1	1	2015-08-21 12:40:22.254	Master	User	americangrowndiamonds@gmail.com	emagid	c41e2e70d691b9448518c8f1865c064e81cc8f41beefa0279b1e248afcf6b0f8	e415b46abfda6115e016a5b93a50ff10	Content,Pages,Blogs,Posts,Newsletter,Users,Patients,Companies,Appointments,System,Email List,Configs,Administrators	\N	\N
\.


--
-- TOC entry 2631 (class 0 OID 0)
-- Dependencies: 185
-- Name: administrator_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('administrator_id_seq', 1, true);


--
-- TOC entry 2509 (class 0 OID 133225)
-- Dependencies: 186
-- Data for Name: banner; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY banner (id, active, insert_time, title, image, link, date_modified, is_deal, featured_id, url, banner_order) FROM stdin;
\.


--
-- TOC entry 2632 (class 0 OID 0)
-- Dependencies: 187
-- Name: banner_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('banner_id_seq', 1, false);


--
-- TOC entry 2511 (class 0 OID 133237)
-- Dependencies: 188
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY category (id, active, insert_time, name, subtitle, parent_category, description, slug, meta_title, meta_keywords, meta_description, display_order, in_menu, image) FROM stdin;
\.


--
-- TOC entry 2633 (class 0 OID 0)
-- Dependencies: 189
-- Name: category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('category_id_seq', 1, false);


--
-- TOC entry 2513 (class 0 OID 133247)
-- Dependencies: 190
-- Data for Name: config; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY config (id, active, insert_time, name, value) FROM stdin;
1	1	2015-08-21 12:04:19.995	Meta Title	Lab Grown Diamonds | www.americangrowndiamonds.com
2	1	2015-08-21 12:04:19.995	Meta Description	American Grown Diamonds offers beautiful Eco-Friendly, Conflict-Free, Lab Grown Diamonds and Jewelry, Call (800) 678-1043
3	1	2015-08-21 12:04:19.995	Meta Keywords	Lab Grown Diamonds, Lab Grown Jewelry, Lab Grown Diamond Jewelry, American Grown, American Grown Diamonds, Conflict Free Diamonds, Eco Friendly Diamonds
\.


--
-- TOC entry 2634 (class 0 OID 0)
-- Dependencies: 191
-- Name: config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('config_id_seq', 1, false);


--
-- TOC entry 2515 (class 0 OID 133257)
-- Dependencies: 192
-- Data for Name: contact; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY contact (id, active, insert_time, name, email, phone, message) FROM stdin;
\.


--
-- TOC entry 2635 (class 0 OID 0)
-- Dependencies: 193
-- Name: contact_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('contact_id_seq', 1, false);


--
-- TOC entry 2517 (class 0 OID 133267)
-- Dependencies: 194
-- Data for Name: coupon; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY coupon (id, active, insert_time, name, code, discount_type, discount_amount, min_amount, num_uses_all, uses_per_user, start_time, end_time) FROM stdin;
\.


--
-- TOC entry 2636 (class 0 OID 0)
-- Dependencies: 195
-- Name: coupon_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('coupon_id_seq', 1, false);


--
-- TOC entry 2519 (class 0 OID 133277)
-- Dependencies: 196
-- Data for Name: credit; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY credit (id, active, insert_time, name, description, price, value) FROM stdin;
\.


--
-- TOC entry 2637 (class 0 OID 0)
-- Dependencies: 197
-- Name: credit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('credit_id_seq', 1, false);


--
-- TOC entry 2521 (class 0 OID 133287)
-- Dependencies: 198
-- Data for Name: jewelry; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY jewelry (id, active, insert_time, name, description, price, currency_code, quantity, tags, materials, images, variation, meta_title, meta_keywords, meta_description, metal_colors, metals, total_14kt_price, total_18kt_price, total_plat_price, slug, semi_mount, stone_breakdown, stone_count, weight, diamond_cwwt, diamond_color, diamond_quality, _14kt, _18kt, platinum, setting, polish, melee, ship_from_cast, ship_to_cust, memo_duration, featured, discount) FROM stdin;
\.


--
-- TOC entry 2638 (class 0 OID 0)
-- Dependencies: 199
-- Name: jewelry_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('jewelry_id_seq', 1, false);


--
-- TOC entry 2523 (class 0 OID 133303)
-- Dependencies: 200
-- Data for Name: order; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "order" (id, active, insert_time, bill_first_name, bill_last_name, bill_address, bill_address2, bill_city, bill_state, bill_country, bill_zip, ship_first_name, ship_last_name, ship_address, ship_address2, ship_city, ship_state, ship_country, ship_zip, cc_number, cc_expiration_month, cc_expiration_year, cc_ccv, tax, tax_rate, status, tracking_number, shipping_method, user_id, coupon_code, gift_card, email, shipping_cost, payment_method, subtotal, total, viewed, phone, coupon_type, coupon_amount, note, user_role, ref_num, order_type, signature) FROM stdin;
\.


--
-- TOC entry 2639 (class 0 OID 0)
-- Dependencies: 201
-- Name: order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('order_id_seq', 1, false);


--
-- TOC entry 2525 (class 0 OID 133313)
-- Dependencies: 202
-- Data for Name: order_product_option; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY order_product_option (id, active, insert_time, order_product_id, data, price) FROM stdin;
\.


--
-- TOC entry 2640 (class 0 OID 0)
-- Dependencies: 203
-- Name: order_product_option_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('order_product_option_id_seq', 1, false);


--
-- TOC entry 2527 (class 0 OID 133324)
-- Dependencies: 204
-- Data for Name: order_products; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY order_products (id, active, insert_time, order_id, quantity, unit_price, product_map_id) FROM stdin;
\.


--
-- TOC entry 2641 (class 0 OID 0)
-- Dependencies: 205
-- Name: order_products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('order_products_id_seq', 1, false);


--
-- TOC entry 2529 (class 0 OID 133331)
-- Dependencies: 206
-- Data for Name: page; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY page (id, active, insert_time, title, slug, description, featured_image, meta_title, meta_keywords, meta_description, date_modified, content) FROM stdin;
\.


--
-- TOC entry 2642 (class 0 OID 0)
-- Dependencies: 207
-- Name: page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('page_id_seq', 1, false);


--
-- TOC entry 2531 (class 0 OID 133341)
-- Dependencies: 208
-- Data for Name: payment_profiles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY payment_profiles (id, active, insert_time, user_id, first_name, last_name, phone, address, address2, city, state, country, zip, cc_number, cc_expiration_month, cc_expiration_year, cc_ccv, label) FROM stdin;
\.


--
-- TOC entry 2643 (class 0 OID 0)
-- Dependencies: 209
-- Name: payment_profiles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('payment_profiles_id_seq', 1, false);


--
-- TOC entry 2533 (class 0 OID 133351)
-- Dependencies: 210
-- Data for Name: product; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product (id, active, insert_time, name, sku, price, meta_title, meta_keywords, meta_description, featured_image, colors, featured, shape, clarity, weight, lab, cut_grade, polish, symmetry, fluor, rapaport_price, total, certificate, length, width, depth, depth_percent, table_percent, girdle, culet, description, origin, memo_status, inscription, certificate_file, slug, wholesale_price, video_link, quantity, memo_duration, vendor, discount) FROM stdin;
3	1	2017-07-11 18:35:23.002169	Fifteen	\N	23				\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N		\N	\N	\N	\N		\N	\N	\N	0	\N	\N
4	1	2017-07-11 18:49:18.262943	454	\N	33				\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	<p>33</p>\r\n	\N	\N	\N	\N		\N	\N	\N	0	\N	\N
5	1	2017-07-11 18:52:03.644402	hooho	\N	554				59655693e6923_dollar.png	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N		\N	\N	\N	\N		\N	\N	\N	0	\N	\N
0	1	2016-07-19 18:08:17.471233	1.00 CTS H SI1 Round Lab Grown Diamond	10664	3575	\N	\N	\N	\N	H	\N	RB	SI1	1	IGI		Very Good	Excellent	NONE	\N	6500	10299101	6.54	6.5	3.85	0.591	0.605	EXTREMELY THIN TO THICK, FACETED	NONE	\N	Lab Created		10299101	http://www.washingtondiamondscorp.com/certificates/LG10299101.pdf	10664	0.00	\N	1	0	Israel	0
1	1	2017-07-11 13:03:40.998839	Question Mark Cut	\N	44.33				\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N		\N	\N	\N	\N		\N	\N	\N	0	\N	\N
2	1	2017-07-11 15:42:07.545583	mark	\N	5				\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N		\N	\N	\N	\N		\N	\N	\N	0	\N	\N
\.


--
-- TOC entry 2534 (class 0 OID 133361)
-- Dependencies: 211
-- Data for Name: product_categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product_categories (id, active, insert_time, category_id, product_map_id) FROM stdin;
\.


--
-- TOC entry 2644 (class 0 OID 0)
-- Dependencies: 212
-- Name: product_categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('product_categories_id_seq', 1, false);


--
-- TOC entry 2645 (class 0 OID 0)
-- Dependencies: 213
-- Name: product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('product_id_seq', 5, true);


--
-- TOC entry 2537 (class 0 OID 133370)
-- Dependencies: 214
-- Data for Name: product_images; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product_images (id, active, insert_time, image, display_order, product_map_id) FROM stdin;
\.


--
-- TOC entry 2646 (class 0 OID 0)
-- Dependencies: 215
-- Name: product_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('product_images_id_seq', 1, false);


--
-- TOC entry 2539 (class 0 OID 133380)
-- Dependencies: 216
-- Data for Name: product_map; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY product_map (id, active, insert_time, product_id, product_type_id) FROM stdin;
\.


--
-- TOC entry 2647 (class 0 OID 0)
-- Dependencies: 217
-- Name: product_map_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('product_map_id_seq', 1, false);


--
-- TOC entry 2566 (class 0 OID 133735)
-- Dependencies: 243
-- Data for Name: register_request; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY register_request (id, active, insert_time, first_name, last_name, company, email, phone, full_address, status) FROM stdin;
\.


--
-- TOC entry 2648 (class 0 OID 0)
-- Dependencies: 242
-- Name: register_request_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('register_request_id_seq', 1, false);


--
-- TOC entry 2541 (class 0 OID 133387)
-- Dependencies: 218
-- Data for Name: ring; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ring (id, active, insert_time, name, price, default_metal, default_color, dwt, semi_mount_weight, stone_breakdown, center_stone_weight, center_stone_shape, total_stones, semi_mount_diamond_weight, diamond_color, diamond_quality, cost_per_dwt, fourteenkt_gold_price, eighteenkt_gold_price, platinum_price, website_fourteenkt_gold_price, website_eighteenkt_gold_price, website_platinum_price, slug, min_carat, max_carat, shape, video_link, metals, colors, metal_colors, setting_labor, center_stone_setting_labor, polish_labor, head_to_add, melee_cost, shipping_from_cast, shipping_to_customer, total_14kt_cost, total_18kt_cost, total_plat_cost, setting_style, options, alias, meta_title, meta_keywords, meta_description, description, memo_duration, featured, discount) FROM stdin;
\.


--
-- TOC entry 2649 (class 0 OID 0)
-- Dependencies: 219
-- Name: ring_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ring_id_seq', 1, false);


--
-- TOC entry 2543 (class 0 OID 133403)
-- Dependencies: 220
-- Data for Name: ring_material; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ring_material (id, active, insert_time, name, price, min_size, max_size, size_step, size_step_price) FROM stdin;
\.


--
-- TOC entry 2650 (class 0 OID 0)
-- Dependencies: 221
-- Name: ring_material_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ring_material_id_seq', 1, false);


--
-- TOC entry 2545 (class 0 OID 133418)
-- Dependencies: 222
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY role (id, active, insert_time, name) FROM stdin;
1	1	2015-08-21 12:05:07.458	admin
3	1	2015-08-21 12:05:07.458	provider
2	1	2015-08-21 12:05:07.458	customer
4	1	2016-03-07 18:47:03.590473	wholesale
\.


--
-- TOC entry 2651 (class 0 OID 0)
-- Dependencies: 223
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('role_id_seq', 1, true);


--
-- TOC entry 2547 (class 0 OID 133428)
-- Dependencies: 224
-- Data for Name: shipping_method; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY shipping_method (id, active, insert_time, is_default, name, cart_subtotal_range_min, cost) FROM stdin;
\.


--
-- TOC entry 2652 (class 0 OID 0)
-- Dependencies: 225
-- Name: shipping_method_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('shipping_method_id_seq', 1, false);


--
-- TOC entry 2549 (class 0 OID 133439)
-- Dependencies: 226
-- Data for Name: transaction; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY transaction (id, active, insert_time, order_id, authorize_net_data, ref_trans_id) FROM stdin;
\.


--
-- TOC entry 2653 (class 0 OID 0)
-- Dependencies: 227
-- Name: transaction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('transaction_id_seq', 1, false);


--
-- TOC entry 2551 (class 0 OID 133449)
-- Dependencies: 228
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "user" (id, active, insert_time, email, password, hash, first_name, last_name, zip, phone, dob, bank_name, routing_number, bank_address, bank_city, voided_check, photo, wholesale_id, status, company, full_address, setup, username) FROM stdin;
611	0	2017-07-10 18:24:19.194598	g@emagid.com	ced30dfe12ef0f72a13aeb1c133a8cced10091a8d6d3854762b204fc4eb7f954	8b6db5332a24cff12695201243fc2008	emagid	tsst	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N	\N	\N	\N
612	0	2017-07-10 18:29:16.728938	foranigan@gmail.com	adaa19f2d66248ecdfa2770a1ac2dc65db70b69d800041216eb1bff138f43c59	2455628f40c7154af2734f15e9bd6fd8	emagid	testt	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N	\N	\N	\N
613	1	2017-07-11 11:45:25.970299	freeze@gothcorp.com	bf6ec5f362b83ecdc51c1ab69e232f7d4ef6193ab82a0032030f73cb7e7839d8	2c854513eecf46e00b9e4b18f1b446d3	Victor	Fries	\N	\N	\N	\N	\N	\N	\N	\N	\N	86	\N	\N	\N	\N	\N
616	1	2017-07-12 10:27:37.297147	faake@emagid.com	\N	\N	Not	Here	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N
617	1	2017-07-12 10:35:11.816144	Fake@emagid.com	\N	\N	Hero	her	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N
610	0	2017-07-10 18:17:43.065593	Garrett@emagid.com	6e16c7b60c5ca9340478cf05387b8056790e39bed22dd0bdd911936774ab241f	8f2549a587326f0aebe2eb2e9ef03631	emagid	test	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N	\N	\N	\N
618	0	2017-07-12 11:03:53.572623	Garrett@emagid.com	d27d3c562cea00dc11f0e8232fc80c7282b97fd72d30b22d653ba85718e4b901	3a5af5f6385f15add30045281c50f4c8	Simon	eMagid	\N	6464609362	\N	\N	\N	\N	\N	\N	\N	93	\N		123 fake st	\N	\N
615	0	2017-07-11 21:20:03.003272	garrett@emagid.com	c0e2825aaf43d94b398ae2981e0a4fa127bcb4e0936a92db1ea829506bb40e35	7779154e76e6c4c1681cf227cc4036f0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	89	\N	\N	\N	\N	\N
619	0	2017-07-12 11:05:18.530482	garrett@emagid.com	\N	\N	Haro	Brimly	\N	342423	\N	\N	\N	\N	\N	\N	\N	95	\N		123 fake st	\N	\N
620	0	2017-07-12 11:06:26.919394	garrett@emagid.com	\N	\N	Haro	eMagid	\N	6464609362	\N	\N	\N	\N	\N	\N	\N	97	\N	uh	123 fake st	\N	\N
621	0	2017-07-12 11:08:37.473861	garrett@emagid.com	\N	\N	Haro	Brimly	\N		\N	\N	\N	\N	\N	\N	\N	98	\N		123 fake st	\N	\N
630	1	2017-07-12 20:05:14.363418	garrett@emagid.com	3630b732424e91ffbbf88ffa8636e5d707086353f68171d2580f50e0d09512fc	72691b7c0d0941506449c5bc4aa56c5d		filterface	\N	\N	\N	\N	\N	\N	\N	\N	\N	110	\N	\N	\N	\N	Organon
623	0	2017-07-12 12:47:29.832172	fake@emagid.com	d6bcd465781aa4166b715ce0477645113a8999964317aae6ceb02184348a1cdf	42703406fab4fc105af8703185aef81c	Juro	eMagid	\N	6464609362	\N	\N	\N	\N	\N	\N	\N	100	\N	uh	123 fake st	0	\N
624	1	2017-07-12 13:14:23.279456	fake@emagid.com	7d121af73c9538a952669583b1d02aceaaaf728663356928cac2cc3b3b91c815	6e49243625bdb103c307a5b745c7458f	Juro	eMagid	\N	6464609362	\N	\N	\N	\N	\N	\N	\N	101	\N	uh	123 fake st	0	\N
622	0	2017-07-12 12:20:47.350516	garrett@emagid.com	0060d36911e686b125b54f4f776a84fbe4f030d433bd4e4f1b612e4141de857c	31147c8d2c339dc950ed3ca76c3bcdae	FARKER TARKER	eMagid	\N	6464609362	\N	\N	\N	\N	\N	\N	\N	99	\N		123 fake st	0	\N
626	1	2017-07-12 15:08:19.652474	faaake@emagid.com	8ea8ef7359d84f30225ed9b63e0d989ca91c1fe642dd86127394111a6aa9812e	9ff0d5eebda0575778eff09ff50f9e0b	Holton	Seller	\N	6464609362	\N	\N	\N	\N	\N	\N	\N	105	\N		105 Fake Town Lane	0	\N
627	1	2017-07-12 15:26:21.673362	garrettfake@emagid.com	10f21cb56dff6b7a6bd4e26e3bc3fe7ee3e795a85783af4d68952e40fe98cd14	67513e27b22a58b57b6654ee962f008d	Haro	eMagid	\N	6464609362	\N	\N	\N	\N	\N	\N	\N	107	\N	uh	123 fake st	0	\N
628	1	2017-07-12 15:27:16.032472	fakegarrett@emagid.com	92b988ca29c93972007fd3962f826bdf2b9098bb242156ccd13b71e98a236ce7	c0330e11b8a5b92630e1b5da121ad5b6	Haro	eMagid	\N	6464609362	\N	\N	\N	\N	\N	\N	\N	108	\N	uh	123 fake st	0	\N
614	1	2017-07-11 16:26:53.625218	simon@emagid.com	ca3b83fbb7c9cb123863f5d79698cbd2de1a94cb567f9455b3208d54134d13cf	fb90ec1d5bcce6d1e4f4a1a3b4465ae8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	Unduly
625	1	2017-07-12 13:18:20.615031	garret@emagid.com	0fe25b9b465c67e0fb9a614ca3f4596d752e59452bc2b5bd5ecea0cb70dc7b82	ae60b6748f66aefe26d0d860dea874c0	Simon	eMagid	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N	\N	\N
629	1	2017-07-12 19:22:09.046546	GREAK@emagid.com	1044293ce5bdbd9da7502181ec78f7285100cd208e43267eac888246d33fd3d1	66fb7dc6c175de7c740cfbc5dc665c63	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	filterface
\.


--
-- TOC entry 2552 (class 0 OID 133458)
-- Dependencies: 229
-- Data for Name: user_favorite; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY user_favorite (id, active, insert_time, user_id, provider_id) FROM stdin;
\.


--
-- TOC entry 2654 (class 0 OID 0)
-- Dependencies: 230
-- Name: user_favorite_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('user_favorite_id_seq', 1, false);


--
-- TOC entry 2655 (class 0 OID 0)
-- Dependencies: 231
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('user_id_seq', 630, true);


--
-- TOC entry 2555 (class 0 OID 133467)
-- Dependencies: 232
-- Data for Name: user_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY user_roles (id, active, insert_time, user_id, role_id) FROM stdin;
51	1	2015-09-15 16:01:06.154	74	2
52	1	2015-09-16 11:24:19.274	75	2
53	1	2015-11-06 12:41:13.428782	81	2
54	1	2015-11-12 12:40:52.478316	82	2
55	1	2015-11-13 19:26:38.332046	83	2
56	1	2015-11-16 11:01:01.941693	84	2
57	1	2016-02-02 11:25:49.98874	85	2
58	1	2016-03-25 15:02:10.600641	86	2
59	1	2016-04-11 17:22:11.185447	87	4
60	1	2016-04-12 12:17:03.862326	88	4
61	1	2016-04-13 12:40:10.046119	89	4
62	1	2016-04-14 15:56:40.024307	90	2
63	1	2016-04-22 12:32:14.365196	91	2
64	1	2016-04-22 12:37:29.838462	92	4
65	1	2016-04-28 09:47:35.131271	93	4
66	1	2016-05-02 18:00:37.926248	94	2
67	1	2016-05-03 12:33:17.670198	95	2
68	1	2016-05-03 12:34:26.294319	96	2
69	1	2016-05-03 12:36:44.372915	97	2
70	1	2016-05-03 12:40:45.63819	98	2
71	1	2016-05-03 12:42:29.843719	99	2
72	1	2016-05-03 15:39:33.41612	100	2
73	1	2016-05-03 15:40:21.979051	101	2
74	1	2016-05-03 15:41:55.147192	102	2
75	1	2016-05-03 17:10:01.343247	103	2
76	1	2016-05-03 17:15:20.562538	104	2
77	1	2016-05-03 17:20:13.358521	105	2
78	1	2016-05-03 17:21:39.358722	106	2
79	1	2016-05-04 04:47:29.838569	107	2
80	1	2016-05-11 08:46:31.365002	108	2
81	1	2016-05-12 11:38:44.408103	109	4
82	1	2016-05-13 09:46:17.868978	110	4
83	1	2016-05-13 20:32:34.123945	111	2
84	1	2016-05-17 18:11:07.271271	112	2
85	1	2016-05-21 16:54:42.144201	113	4
86	1	2016-05-23 13:16:28.887928	114	4
87	1	2016-05-23 14:14:14.814685	115	4
88	1	2016-05-23 21:18:21.832431	116	2
89	1	2016-05-24 15:13:21.394086	117	4
90	1	2016-05-24 15:16:49.983825	118	2
91	1	2016-05-25 14:00:02.016356	119	2
92	1	2016-05-28 04:26:19.26212	120	2
93	1	2016-05-30 09:33:44.337989	121	2
94	1	2016-05-30 11:42:02.744245	122	2
95	1	2016-05-30 13:46:25.330518	123	2
96	1	2016-05-30 19:31:27.014349	124	2
97	1	2016-05-30 20:08:35.760792	125	2
98	1	2016-06-03 15:39:44.720792	126	4
99	1	2016-06-08 12:41:08.460891	127	4
100	1	2016-06-08 14:42:12.819399	128	4
101	1	2016-06-08 14:48:56.685618	129	2
102	1	2016-06-08 18:03:47.428855	130	4
103	1	2016-06-08 19:05:18.232028	131	4
104	1	2016-06-08 19:37:54.587433	132	4
105	1	2016-06-09 07:27:37.302694	133	2
106	1	2016-06-09 10:52:43.524964	134	4
107	1	2016-06-12 11:35:24.092019	135	2
108	1	2016-06-13 16:22:51.619886	136	2
109	1	2016-06-14 11:48:03.034502	137	2
110	1	2016-06-14 11:48:19.125745	138	2
111	1	2016-06-14 12:06:29.44825	139	2
112	1	2016-06-14 12:14:55.640761	140	2
113	1	2016-06-14 12:27:23.62754	141	2
114	1	2016-06-14 12:41:23.30926	142	2
115	1	2016-06-14 12:42:47.12658	143	2
116	1	2016-06-14 13:27:37.548418	144	2
117	1	2016-06-14 14:00:52.224856	145	2
118	1	2016-06-14 14:06:35.52136	146	2
119	1	2016-06-14 14:13:46.456503	147	2
120	1	2016-06-14 14:22:20.986674	148	2
121	1	2016-06-14 14:31:27.478948	149	2
122	1	2016-06-14 14:32:33.400433	150	2
123	1	2016-06-14 14:33:10.251918	151	2
124	1	2016-06-14 14:33:57.757026	152	2
125	1	2016-06-14 14:35:54.367403	153	2
126	1	2016-06-14 14:49:11.466086	154	2
127	1	2016-06-14 14:55:30.556398	155	2
128	1	2016-06-14 14:57:41.245471	156	4
129	1	2016-06-14 15:04:36.959324	157	2
130	1	2016-06-14 15:05:50.674369	158	2
131	1	2016-06-14 15:06:49.151837	159	2
132	1	2016-06-14 15:15:18.787115	160	2
133	1	2016-06-14 15:15:52.656659	161	2
134	1	2016-06-14 15:18:59.632813	162	2
135	1	2016-06-14 15:27:32.914782	163	4
136	1	2016-06-14 15:27:49.822664	164	2
137	1	2016-06-14 15:39:51.839206	165	2
138	1	2016-06-14 15:40:54.784693	166	2
139	1	2016-06-14 15:44:13.440184	167	2
140	1	2016-06-14 16:01:39.464303	168	2
141	1	2016-06-14 16:09:48.351276	169	2
142	1	2016-06-14 16:13:58.096268	170	2
143	1	2016-06-14 16:16:01.40943	171	2
144	1	2016-06-14 16:16:39.814156	172	2
145	1	2016-06-14 16:17:59.859664	173	2
146	1	2016-06-14 16:21:36.527218	174	2
147	1	2016-06-14 16:22:00.715747	175	2
148	1	2016-06-14 16:23:42.647179	176	2
149	1	2016-06-14 16:25:10.835102	177	2
150	1	2016-06-14 16:35:35.652978	178	2
151	1	2016-06-14 16:36:26.058061	179	2
152	1	2016-06-14 16:43:35.227633	180	2
153	1	2016-06-14 16:43:36.826572	181	2
154	1	2016-06-14 16:47:21.768355	182	2
155	1	2016-06-14 17:04:32.672554	183	2
156	1	2016-06-14 17:08:02.842073	184	2
157	1	2016-06-14 17:10:41.894494	185	2
158	1	2016-06-14 17:11:18.498463	186	2
159	1	2016-06-14 17:11:41.892062	187	2
160	1	2016-06-14 17:12:41.991718	188	2
161	1	2016-06-14 17:15:45.835163	189	2
162	1	2016-06-14 17:18:58.731137	190	2
163	1	2016-06-14 17:24:03.295928	191	2
164	1	2016-06-14 17:29:53.18511	192	2
165	1	2016-06-14 17:38:16.500732	193	2
166	1	2016-06-14 17:42:12.000479	194	2
167	1	2016-06-14 17:48:42.300559	195	2
168	1	2016-06-14 17:49:12.500565	196	2
169	1	2016-06-14 17:56:14.027462	197	2
170	1	2016-06-14 18:04:24.181664	198	2
171	1	2016-06-14 18:06:04.167054	199	2
172	1	2016-06-14 18:07:43.63578	200	2
173	1	2016-06-14 18:12:58.056735	201	2
174	1	2016-06-14 18:16:31.937082	202	2
175	1	2016-06-14 18:25:22.027555	203	2
176	1	2016-06-14 18:33:01.781636	204	2
177	1	2016-06-14 18:59:15.697914	205	2
178	1	2016-06-14 19:00:18.427521	206	2
179	1	2016-06-14 19:00:30.177319	207	4
180	1	2016-06-14 19:00:58.508688	208	4
181	1	2016-06-14 19:03:04.632856	209	2
182	1	2016-06-14 19:08:30.272852	210	2
183	1	2016-06-14 19:11:10.009594	211	2
184	1	2016-06-14 19:11:17.151053	212	2
185	1	2016-06-14 19:15:09.491139	213	2
186	1	2016-06-14 19:19:21.658688	214	2
187	1	2016-06-14 19:19:45.466139	215	2
188	1	2016-06-14 19:23:27.194805	216	2
189	1	2016-06-14 19:27:41.183293	217	2
190	1	2016-06-14 19:32:02.514792	218	2
191	1	2016-06-14 19:37:19.670615	219	2
192	1	2016-06-14 19:39:00.057425	220	2
193	1	2016-06-14 19:39:23.056271	221	2
194	1	2016-06-14 19:41:05.844425	222	2
195	1	2016-06-14 19:47:06.449858	223	2
196	1	2016-06-14 19:47:37.068732	224	2
197	1	2016-06-14 19:48:17.210556	225	2
198	1	2016-06-14 19:52:25.448095	226	2
199	1	2016-06-14 19:55:57.348272	227	2
200	1	2016-06-14 19:57:18.970837	228	2
201	1	2016-06-14 20:01:03.382055	229	2
202	1	2016-06-14 20:01:04.507083	230	2
203	1	2016-06-14 20:03:49.738174	231	2
204	1	2016-06-14 20:04:48.110412	232	2
205	1	2016-06-14 20:11:48.148271	233	2
206	1	2016-06-14 20:20:41.517016	234	2
207	1	2016-06-14 20:24:49.646622	235	2
208	1	2016-06-14 20:33:09.779995	236	2
209	1	2016-06-14 20:52:11.341415	237	2
210	1	2016-06-14 20:58:03.941179	238	2
211	1	2016-06-14 21:03:00.012764	239	2
212	1	2016-06-14 21:05:56.853449	240	2
213	1	2016-06-14 21:06:32.378287	241	2
214	1	2016-06-14 21:13:10.047519	242	2
215	1	2016-06-14 21:14:41.02721	243	2
216	1	2016-06-14 21:19:19.822801	244	2
217	1	2016-06-14 21:33:22.310824	245	2
218	1	2016-06-14 21:38:24.947363	246	2
219	1	2016-06-14 21:39:06.165188	247	2
220	1	2016-06-14 21:40:42.868918	248	2
221	1	2016-06-14 21:47:26.084465	249	2
222	1	2016-06-14 21:50:16.233595	250	4
223	1	2016-06-14 21:51:09.311729	251	2
224	1	2016-06-14 21:52:31.881425	252	2
225	1	2016-06-14 21:54:02.15683	253	2
226	1	2016-06-14 22:11:33.101616	254	2
227	1	2016-06-14 22:40:10.892683	255	2
228	1	2016-06-14 22:43:48.451864	256	2
229	1	2016-06-14 22:46:36.309125	257	2
230	1	2016-06-14 22:50:23.655095	258	2
231	1	2016-06-14 23:06:16.232407	259	2
232	1	2016-06-14 23:07:06.534111	260	2
233	1	2016-06-14 23:09:24.698614	261	2
234	1	2016-06-14 23:12:29.897237	262	2
235	1	2016-06-14 23:12:53.87016	263	2
236	1	2016-06-14 23:19:26.664912	264	2
237	1	2016-06-14 23:36:05.520338	265	2
238	1	2016-06-14 23:45:00.257872	266	2
239	1	2016-06-14 23:46:06.990278	267	2
240	1	2016-06-14 23:52:05.52538	268	2
241	1	2016-06-14 23:53:42.261221	269	2
242	1	2016-06-14 23:57:07.241799	270	2
243	1	2016-06-14 23:59:22.509826	271	2
244	1	2016-06-15 00:05:20.226208	272	2
245	1	2016-06-15 00:08:57.882488	273	2
246	1	2016-06-15 00:11:26.778345	274	2
247	1	2016-06-15 00:16:34.164791	275	2
248	1	2016-06-15 00:16:44.167208	276	2
249	1	2016-06-15 00:18:38.674613	277	2
250	1	2016-06-15 00:19:44.12317	278	2
251	1	2016-06-15 00:20:14.05943	279	2
252	1	2016-06-15 00:26:09.344816	280	2
253	1	2016-06-15 00:28:15.031695	281	2
254	1	2016-06-15 00:45:49.286432	282	2
255	1	2016-06-15 00:54:28.149595	283	2
256	1	2016-06-15 01:00:37.451929	284	2
257	1	2016-06-15 01:00:59.637272	285	2
258	1	2016-06-15 01:03:47.106012	286	2
259	1	2016-06-15 01:07:59.141619	287	2
260	1	2016-06-15 01:31:24.900982	288	2
261	1	2016-06-15 01:49:20.745832	289	2
262	1	2016-06-15 01:50:43.5918	290	2
263	1	2016-06-15 02:26:08.146755	291	2
264	1	2016-06-15 02:56:46.042956	292	2
265	1	2016-06-15 03:13:47.306683	293	2
266	1	2016-06-15 03:17:07.236805	294	2
267	1	2016-06-15 03:57:31.482308	295	2
268	1	2016-06-15 04:27:23.371154	296	2
269	1	2016-06-15 05:23:06.367818	297	2
270	1	2016-06-15 05:53:39.237681	298	2
271	1	2016-06-15 06:16:47.148608	299	2
272	1	2016-06-15 06:26:02.287058	300	4
273	1	2016-06-15 07:00:40.618952	301	2
274	1	2016-06-15 07:02:51.919051	302	2
275	1	2016-06-15 07:18:38.652292	303	2
276	1	2016-06-15 07:19:15.389021	304	2
277	1	2016-06-15 08:18:20.668561	305	2
278	1	2016-06-15 08:23:15.895844	306	2
279	1	2016-06-15 08:40:50.147899	307	2
280	1	2016-06-15 08:46:29.972463	308	2
281	1	2016-06-15 08:51:53.936413	309	2
282	1	2016-06-15 09:06:21.908884	310	2
283	1	2016-06-15 09:19:06.312314	311	2
284	1	2016-06-15 09:20:44.557264	312	4
285	1	2016-06-15 09:23:35.340258	313	2
286	1	2016-06-15 09:29:43.123996	314	2
287	1	2016-06-15 09:47:21.389452	315	2
288	1	2016-06-15 09:47:21.842803	316	2
289	1	2016-06-15 09:48:33.468868	317	2
290	1	2016-06-15 09:50:45.149057	318	2
291	1	2016-06-15 09:52:30.868756	319	2
292	1	2016-06-15 10:00:21.710562	320	2
293	1	2016-06-15 10:01:15.299437	321	2
294	1	2016-06-15 10:12:47.840293	322	2
295	1	2016-06-15 10:15:53.322647	323	2
296	1	2016-06-15 10:16:20.910432	324	2
297	1	2016-06-15 10:20:01.908281	325	2
298	1	2016-06-15 10:26:31.822298	326	2
299	1	2016-06-15 10:27:33.367112	327	2
300	1	2016-06-15 10:28:15.082693	328	2
301	1	2016-06-15 10:30:06.64397	329	2
302	1	2016-06-15 10:30:18.805444	330	2
303	1	2016-06-15 10:37:55.105041	331	2
304	1	2016-06-15 10:47:25.225851	332	2
305	1	2016-06-15 10:49:03.550756	333	2
306	1	2016-06-15 10:54:20.93238	334	2
307	1	2016-06-15 10:54:54.827401	335	2
308	1	2016-06-15 10:55:25.63602	336	2
309	1	2016-06-15 11:22:50.273149	337	2
310	1	2016-06-15 11:23:42.33095	338	2
311	1	2016-06-15 11:37:12.612618	339	2
312	1	2016-06-15 11:40:46.655652	340	2
313	1	2016-06-15 11:56:21.918203	341	2
314	1	2016-06-15 12:15:46.524664	342	2
315	1	2016-06-15 12:21:50.232273	343	2
316	1	2016-06-15 12:26:02.220269	344	2
317	1	2016-06-15 12:32:42.509348	345	2
318	1	2016-06-15 12:36:04.878458	346	2
319	1	2016-06-15 12:44:32.830607	347	2
320	1	2016-06-15 12:59:47.255466	348	2
321	1	2016-06-15 13:03:31.532791	349	2
322	1	2016-06-15 13:04:44.898245	350	4
323	1	2016-06-15 13:49:19.813315	351	2
324	1	2016-06-15 13:58:00.446118	352	2
325	1	2016-06-15 14:19:03.190969	353	2
326	1	2016-06-15 14:21:21.826568	354	2
327	1	2016-06-15 14:44:36.371858	355	2
328	1	2016-06-15 15:00:23.039087	356	2
329	1	2016-06-15 15:00:35.524799	357	2
330	1	2016-06-15 15:01:15.76371	358	2
331	1	2016-06-15 15:04:53.06437	359	4
332	1	2016-06-15 15:15:30.328065	360	2
333	1	2016-06-15 15:31:52.85292	361	4
334	1	2016-06-15 15:41:00.601417	362	2
335	1	2016-06-15 15:46:40.162773	363	2
336	1	2016-06-15 15:50:44.620624	364	2
337	1	2016-06-15 16:02:37.163397	365	2
338	1	2016-06-15 16:17:20.492803	366	2
339	1	2016-06-15 16:43:04.709402	367	2
340	1	2016-06-15 17:04:01.868528	368	2
341	1	2016-06-15 17:32:26.808341	369	2
342	1	2016-06-15 17:45:51.899359	370	2
343	1	2016-06-15 17:49:44.073286	371	2
344	1	2016-06-15 17:54:43.700567	372	2
345	1	2016-06-15 17:55:41.364594	373	2
346	1	2016-06-15 18:14:56.002322	374	2
347	1	2016-06-15 19:08:15.329579	375	2
348	1	2016-06-15 19:11:11.799561	376	2
349	1	2016-06-15 19:21:00.35545	377	2
350	1	2016-06-15 19:44:24.562603	378	2
351	1	2016-06-15 20:01:03.833522	379	2
352	1	2016-06-15 20:03:08.464078	380	2
353	1	2016-06-15 21:32:00.364829	381	2
354	1	2016-06-15 21:46:56.329793	382	2
355	1	2016-06-15 22:02:48.639564	383	4
356	1	2016-06-15 22:10:52.706762	384	2
357	1	2016-06-15 22:30:41.8639	385	2
358	1	2016-06-15 22:37:28.259103	386	2
359	1	2016-06-15 23:08:46.00956	387	4
360	1	2016-06-15 23:15:55.956643	388	2
361	1	2016-06-16 00:27:26.12909	389	2
362	1	2016-06-16 00:37:40.709383	390	2
363	1	2016-06-16 04:23:19.52022	391	2
364	1	2016-06-16 05:30:49.606142	392	2
365	1	2016-06-16 09:12:10.112195	393	2
366	1	2016-06-16 10:11:33.521934	394	2
367	1	2016-06-16 10:17:52.905204	395	2
368	1	2016-06-16 10:29:10.618839	396	2
369	1	2016-06-16 11:28:46.07417	397	2
370	1	2016-06-16 14:09:25.829972	398	2
371	1	2016-06-16 14:13:52.717741	399	2
372	1	2016-06-16 14:35:58.373735	400	2
373	1	2016-06-16 16:19:27.864721	401	2
374	1	2016-06-16 16:29:08.641262	402	2
375	1	2016-06-16 16:33:26.53225	403	2
376	1	2016-06-16 17:28:06.244664	404	2
377	1	2016-06-16 17:28:42.710727	405	2
378	1	2016-06-16 17:55:43.243034	406	2
379	1	2016-06-16 19:52:30.943542	407	2
380	1	2016-06-17 03:26:42.260367	408	2
381	1	2016-06-17 07:55:59.774574	409	2
382	1	2016-06-17 08:35:41.530643	410	2
383	1	2016-06-17 08:45:59.745754	411	2
384	1	2016-06-17 09:13:20.488827	412	2
385	1	2016-06-17 09:41:32.840794	413	2
386	1	2016-06-17 09:42:15.706383	414	2
387	1	2016-06-17 10:08:46.136241	415	2
388	1	2016-06-17 11:20:27.759749	416	2
389	1	2016-06-17 12:44:45.003643	417	2
390	1	2016-06-17 12:46:38.549036	418	2
391	1	2016-06-17 14:24:24.311852	419	2
392	1	2016-06-17 14:34:49.33794	420	2
393	1	2016-06-17 15:53:15.68075	421	2
394	1	2016-06-17 16:09:47.683469	422	2
395	1	2016-06-17 16:20:06.687273	423	2
396	1	2016-06-17 17:26:06.48877	424	2
397	1	2016-06-17 17:26:11.075146	425	2
398	1	2016-06-17 17:46:21.781982	426	2
399	1	2016-06-17 18:09:21.872724	427	2
400	1	2016-06-17 18:13:26.328454	428	2
401	1	2016-06-17 18:20:22.562157	429	2
402	1	2016-06-17 18:41:15.379442	430	2
403	1	2016-06-17 18:41:39.047762	431	2
404	1	2016-06-17 18:49:04.222351	432	2
405	1	2016-06-17 19:05:24.227757	433	2
406	1	2016-06-17 19:05:56.358161	434	2
407	1	2016-06-17 19:06:10.79807	435	2
408	1	2016-06-17 19:10:57.947786	436	2
409	1	2016-06-17 19:20:35.26276	437	2
410	1	2016-06-17 19:39:18.208558	438	2
411	1	2016-06-17 19:44:16.263525	439	2
412	1	2016-06-17 19:50:43.073233	440	2
413	1	2016-06-17 20:00:23.66828	441	2
414	1	2016-06-17 20:17:19.622781	442	2
415	1	2016-06-17 20:21:26.742002	443	2
416	1	2016-06-17 20:22:48.141888	444	2
417	1	2016-06-17 20:33:50.369991	445	2
418	1	2016-06-17 20:34:53.631728	446	2
419	1	2016-06-17 20:58:34.819728	447	2
420	1	2016-06-17 21:04:28.218722	448	2
421	1	2016-06-17 21:39:42.571768	449	2
422	1	2016-06-17 22:19:23.154358	450	2
423	1	2016-06-17 22:23:49.455769	451	2
424	1	2016-06-17 23:03:23.068431	452	2
425	1	2016-06-17 23:13:20.76872	453	2
426	1	2016-06-18 00:05:16.001055	454	2
427	1	2016-06-18 00:26:06.563128	455	2
428	1	2016-06-18 00:44:20.025938	456	2
429	1	2016-06-18 01:07:09.263453	457	2
430	1	2016-06-18 01:14:58.02089	458	2
431	1	2016-06-18 01:45:39.092813	459	2
432	1	2016-06-18 02:49:17.378129	460	2
433	1	2016-06-18 04:03:11.309793	461	2
434	1	2016-06-18 04:12:50.072375	462	2
435	1	2016-06-18 05:53:53.737621	463	2
436	1	2016-06-18 05:55:23.95078	464	2
437	1	2016-06-18 05:57:04.144895	465	2
438	1	2016-06-18 05:59:33.353794	466	2
439	1	2016-06-18 07:29:32.594462	467	2
440	1	2016-06-18 07:57:01.39655	468	2
441	1	2016-06-18 08:34:47.577914	469	2
442	1	2016-06-18 08:42:52.62752	470	4
443	1	2016-06-18 08:50:18.513893	471	4
444	1	2016-06-18 09:10:16.87328	472	2
445	1	2016-06-18 09:23:21.253631	473	2
446	1	2016-06-18 10:06:49.318991	474	2
447	1	2016-06-18 11:30:05.140271	475	2
448	1	2016-06-18 11:35:24.348516	476	2
449	1	2016-06-18 11:39:48.301245	477	2
450	1	2016-06-18 11:50:58.52255	478	2
451	1	2016-06-18 11:52:19.247965	479	2
452	1	2016-06-18 12:10:16.075106	480	2
453	1	2016-06-18 12:36:39.009306	481	2
454	1	2016-06-18 12:39:53.558756	482	2
455	1	2016-06-18 12:40:28.229156	483	2
456	1	2016-06-18 12:45:12.254877	484	2
457	1	2016-06-18 12:55:11.282895	485	2
458	1	2016-06-18 12:59:22.132981	486	2
459	1	2016-06-18 13:05:38.755172	487	2
460	1	2016-06-18 13:20:53.934015	488	2
461	1	2016-06-18 13:30:03.316881	489	2
462	1	2016-06-18 13:54:28.222737	490	2
463	1	2016-06-18 14:03:36.499725	491	2
464	1	2016-06-18 14:22:42.563864	492	2
465	1	2016-06-18 14:30:06.797557	493	2
466	1	2016-06-18 14:52:25.515875	494	2
467	1	2016-06-18 15:06:20.057053	495	2
468	1	2016-06-18 15:20:53.849086	496	2
469	1	2016-06-18 15:24:15.612095	497	2
470	1	2016-06-18 15:30:41.05455	498	2
471	1	2016-06-18 15:39:49.337926	499	2
472	1	2016-06-18 15:42:52.842992	500	2
473	1	2016-06-18 15:51:49.598966	501	2
474	1	2016-06-18 16:18:33.590815	502	2
475	1	2016-06-18 16:51:29.679367	503	2
476	1	2016-06-18 17:44:41.296373	504	2
477	1	2016-06-18 17:50:29.159266	505	2
478	1	2016-06-18 18:12:52.986231	506	2
479	1	2016-06-18 18:14:41.794313	507	2
480	1	2016-06-18 18:16:33.969527	508	2
481	1	2016-06-18 19:07:36.70171	509	2
482	1	2016-06-18 20:27:24.680516	510	2
483	1	2016-06-18 20:43:20.146011	511	2
484	1	2016-06-18 21:01:59.849288	512	2
485	1	2016-06-18 21:23:35.672674	513	2
486	1	2016-06-18 21:36:45.281102	514	2
487	1	2016-06-18 22:08:47.656969	515	2
488	1	2016-06-18 22:09:59.822517	516	2
489	1	2016-06-18 23:15:01.593001	517	2
490	1	2016-06-18 23:36:31.284522	518	2
491	1	2016-06-18 23:47:48.145918	519	2
492	1	2016-06-19 00:43:33.741827	520	2
493	1	2016-06-19 01:50:41.959712	521	2
494	1	2016-06-19 10:29:20.232905	522	2
495	1	2016-06-19 11:23:09.320676	523	2
496	1	2016-06-19 11:53:44.331524	524	2
497	1	2016-06-19 13:13:01.969893	525	2
498	1	2016-06-19 13:42:47.814837	526	4
499	1	2016-06-19 14:13:40.588218	527	2
500	1	2016-06-19 15:02:00.061108	528	2
501	1	2016-06-19 17:39:27.251807	529	2
502	1	2016-06-19 18:23:07.215718	530	2
503	1	2016-06-19 19:16:17.829124	531	2
504	1	2016-06-19 20:35:39.707846	532	2
505	1	2016-06-19 22:00:19.581596	533	2
506	1	2016-06-19 22:45:09.247323	534	2
507	1	2016-06-19 22:58:32.562222	535	2
508	1	2016-06-19 23:18:37.821121	536	2
509	1	2016-06-19 23:20:33.499799	537	2
510	1	2016-06-19 23:59:41.354255	538	2
511	1	2016-06-20 09:32:38.044325	539	2
512	1	2016-06-20 09:51:03.335869	540	4
513	1	2016-06-20 10:05:21.680762	541	2
514	1	2016-06-20 11:19:03.523992	542	2
515	1	2016-06-20 12:44:16.834688	543	2
516	1	2016-06-20 13:22:36.360312	544	2
517	1	2016-06-20 13:32:18.672386	545	2
518	1	2016-06-20 14:21:53.353618	546	2
519	1	2016-06-20 15:17:44.495928	547	2
520	1	2016-06-20 15:58:38.716111	548	2
521	1	2016-06-20 16:44:17.336423	549	4
522	1	2016-06-20 18:15:19.77127	550	2
523	1	2016-06-20 18:18:04.89257	551	2
524	1	2016-06-20 18:45:01.807972	552	2
525	1	2016-06-20 19:21:07.733215	553	2
526	1	2016-06-20 19:21:45.808854	554	2
527	1	2016-06-20 20:54:37.01221	555	2
528	1	2016-06-21 09:55:12.18396	556	4
529	1	2016-06-21 10:32:45.331904	557	2
530	1	2016-06-21 14:00:04.332643	558	2
531	1	2016-06-21 14:11:07.641575	559	4
532	1	2016-06-21 14:14:39.251238	560	2
533	1	2016-06-21 16:07:50.435196	561	2
534	1	2016-06-22 05:20:20.473977	562	2
535	1	2016-06-22 08:18:35.318721	563	2
536	1	2016-06-22 13:34:32.037222	564	4
537	1	2016-06-22 15:45:43.568546	565	2
538	1	2016-06-22 19:42:02.093963	566	4
539	1	2016-06-22 21:12:23.016792	567	2
540	1	2016-06-23 06:29:02.441228	568	2
541	1	2016-06-23 14:26:40.230705	569	4
542	1	2016-06-23 16:45:57.966087	570	2
543	1	2016-06-24 13:58:32.518944	571	2
544	1	2016-06-25 23:54:00.391965	572	2
545	1	2016-06-26 01:26:55.28956	573	4
546	1	2016-06-26 02:23:17.686617	574	4
547	1	2016-06-26 13:42:30.000157	575	2
548	1	2016-06-27 10:40:37.027108	576	2
549	1	2016-06-27 10:44:39.131129	577	2
550	1	2016-06-27 17:07:18.686526	578	4
551	1	2016-06-28 12:33:55.488477	579	2
552	1	2016-06-28 23:46:28.687587	580	2
553	1	2016-07-04 06:44:47.593038	581	2
554	1	2016-07-04 12:52:27.674394	582	2
555	1	2016-07-04 13:37:00.014121	583	4
556	1	2016-07-04 14:15:40.382491	584	4
557	1	2016-07-04 18:34:38.245605	585	2
558	1	2016-07-04 20:38:57.672097	586	4
559	1	2016-07-05 00:15:54.418875	587	2
560	1	2016-07-05 16:47:16.776898	588	2
561	1	2016-07-05 19:39:50.031326	589	2
562	1	2016-07-06 01:17:23.930203	590	2
563	1	2016-07-07 13:06:00.193539	591	2
564	1	2016-07-07 22:57:06.853151	592	2
565	1	2016-07-08 00:16:21.273109	593	2
566	1	2016-07-09 20:24:38.865081	594	2
567	1	2016-07-10 16:10:51.551811	595	2
568	1	2016-07-10 18:09:55.325901	596	2
569	1	2016-07-11 16:09:07.559941	597	4
570	1	2016-07-14 13:14:49.57602	598	2
571	1	2016-07-14 14:46:18.202316	599	2
572	1	2016-07-14 21:49:26.400828	600	2
573	1	2016-07-14 23:36:27.720178	601	4
574	1	2016-07-15 09:41:47.487402	602	4
575	1	2016-07-15 14:03:44.246278	603	4
576	1	2016-07-16 14:57:54.049595	604	2
577	1	2016-07-17 23:38:53.619638	605	4
578	1	2016-07-19 00:06:18.418416	606	2
579	1	2016-07-19 11:32:46.160554	607	2
580	1	2016-07-20 11:26:03.470131	608	4
581	1	2017-07-10 17:32:06.96419	609	2
582	1	2017-07-10 18:17:43.068785	610	2
583	1	2017-07-10 18:24:19.197389	611	2
584	1	2017-07-10 18:29:16.732118	612	2
585	1	2017-07-11 11:45:25.9853	613	4
586	1	2017-07-11 16:26:53.716223	614	4
587	1	2017-07-11 21:20:03.039274	615	4
588	1	2017-07-12 11:03:53.587623	618	4
589	1	2017-07-12 11:05:18.550483	619	4
590	1	2017-07-12 11:06:26.922394	620	4
591	1	2017-07-12 11:08:37.476861	621	4
592	1	2017-07-12 12:20:47.354516	622	4
593	1	2017-07-12 12:47:29.842173	623	4
594	1	2017-07-12 13:14:23.393463	624	4
595	1	2017-07-12 13:18:20.619031	625	4
596	1	2017-07-12 15:08:19.660475	626	4
597	1	2017-07-12 15:26:21.681363	627	4
598	1	2017-07-12 15:27:16.063473	628	4
599	1	2017-07-12 19:22:09.080548	629	4
600	1	2017-07-12 20:05:14.367418	630	4
\.


--
-- TOC entry 2656 (class 0 OID 0)
-- Dependencies: 233
-- Name: user_roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('user_roles_id_seq', 600, true);


--
-- TOC entry 2557 (class 0 OID 133474)
-- Dependencies: 234
-- Data for Name: wedding_band; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY wedding_band (id, active, insert_time, name, slug, price, category, diamond_color, diamond_quality, video_link, metals, colors, metal_colors, setting_labor, polish_labor, head_to_add, melee_cost, shipping_from_cast, shipping_to_customer, total_14kt_cost, total_18kt_cost, total_plat_cost, options, setting_style, alias, meta_title, meta_keywords, meta_description, memo_duration) FROM stdin;
\.


--
-- TOC entry 2657 (class 0 OID 0)
-- Dependencies: 235
-- Name: wedding_band_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('wedding_band_id_seq', 1, false);


--
-- TOC entry 2559 (class 0 OID 133489)
-- Dependencies: 236
-- Data for Name: wholesale; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY wholesale (id, active, insert_time, company, job_title, dba, incorporation, referrer, phone, alt_phone, status, address, address2, payment, memo_duration, site, logo, discount, tax_id) FROM stdin;
\.


--
-- TOC entry 2658 (class 0 OID 0)
-- Dependencies: 237
-- Name: wholesale_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('wholesale_id_seq', 1, false);


--
-- TOC entry 2564 (class 0 OID 133699)
-- Dependencies: 241
-- Data for Name: wholesale_items; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY wholesale_items (id, active, insert_time, price, wholesale_id, product_id) FROM stdin;
\.


--
-- TOC entry 2659 (class 0 OID 0)
-- Dependencies: 240
-- Name: wholesale_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('wholesale_items_id_seq', 1, false);


--
-- TOC entry 2561 (class 0 OID 133501)
-- Dependencies: 238
-- Data for Name: wishlist; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY wishlist (id, active, insert_time, user_id, data) FROM stdin;
\.


--
-- TOC entry 2660 (class 0 OID 0)
-- Dependencies: 239
-- Name: wishlist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('wishlist_id_seq', 1, false);


--
-- TOC entry 2328 (class 2606 OID 133545)
-- Name: address_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY address
    ADD CONSTRAINT address_pkey PRIMARY KEY (id);


--
-- TOC entry 2330 (class 2606 OID 133547)
-- Name: admin_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin_roles
    ADD CONSTRAINT admin_roles_pkey PRIMARY KEY (id);


--
-- TOC entry 2332 (class 2606 OID 133549)
-- Name: administrator_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY administrator
    ADD CONSTRAINT administrator_pkey PRIMARY KEY (id);


--
-- TOC entry 2334 (class 2606 OID 133551)
-- Name: banner_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY banner
    ADD CONSTRAINT banner_pkey PRIMARY KEY (id);


--
-- TOC entry 2336 (class 2606 OID 133553)
-- Name: category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- TOC entry 2338 (class 2606 OID 133555)
-- Name: config_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY config
    ADD CONSTRAINT config_pkey PRIMARY KEY (id);


--
-- TOC entry 2340 (class 2606 OID 133557)
-- Name: contact_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contact
    ADD CONSTRAINT contact_pkey PRIMARY KEY (id);


--
-- TOC entry 2342 (class 2606 OID 133559)
-- Name: coupon_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY coupon
    ADD CONSTRAINT coupon_pkey PRIMARY KEY (id);


--
-- TOC entry 2344 (class 2606 OID 133561)
-- Name: credit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY credit
    ADD CONSTRAINT credit_pkey PRIMARY KEY (id);


--
-- TOC entry 2348 (class 2606 OID 133563)
-- Name: order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_pkey PRIMARY KEY (id);


--
-- TOC entry 2350 (class 2606 OID 133565)
-- Name: order_product_option_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_product_option
    ADD CONSTRAINT order_product_option_pkey PRIMARY KEY (id);


--
-- TOC entry 2352 (class 2606 OID 133567)
-- Name: order_products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_products
    ADD CONSTRAINT order_products_pkey PRIMARY KEY (id);


--
-- TOC entry 2354 (class 2606 OID 133569)
-- Name: page_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY page
    ADD CONSTRAINT page_pkey PRIMARY KEY (id);


--
-- TOC entry 2356 (class 2606 OID 133571)
-- Name: payment_profiles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY payment_profiles
    ADD CONSTRAINT payment_profiles_pkey PRIMARY KEY (id);


--
-- TOC entry 2346 (class 2606 OID 133573)
-- Name: preset_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY jewelry
    ADD CONSTRAINT preset_pkey PRIMARY KEY (id);


--
-- TOC entry 2360 (class 2606 OID 133575)
-- Name: product_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_categories
    ADD CONSTRAINT product_categories_pkey PRIMARY KEY (id);


--
-- TOC entry 2362 (class 2606 OID 133577)
-- Name: product_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_images
    ADD CONSTRAINT product_images_pkey PRIMARY KEY (id);


--
-- TOC entry 2364 (class 2606 OID 133579)
-- Name: product_map_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_map
    ADD CONSTRAINT product_map_pkey PRIMARY KEY (id);


--
-- TOC entry 2358 (class 2606 OID 133581)
-- Name: product_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- TOC entry 2368 (class 2606 OID 133583)
-- Name: ring_material_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ring_material
    ADD CONSTRAINT ring_material_pkey PRIMARY KEY (id);


--
-- TOC entry 2366 (class 2606 OID 133585)
-- Name: ring_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ring
    ADD CONSTRAINT ring_pkey PRIMARY KEY (id);


--
-- TOC entry 2370 (class 2606 OID 133587)
-- Name: role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- TOC entry 2372 (class 2606 OID 133589)
-- Name: shipping_method_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY shipping_method
    ADD CONSTRAINT shipping_method_pkey PRIMARY KEY (id);


--
-- TOC entry 2374 (class 2606 OID 133591)
-- Name: transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transaction
    ADD CONSTRAINT transaction_pkey PRIMARY KEY (id);


--
-- TOC entry 2378 (class 2606 OID 133593)
-- Name: user_favorite_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_favorite
    ADD CONSTRAINT user_favorite_pkey PRIMARY KEY (id);


--
-- TOC entry 2376 (class 2606 OID 133595)
-- Name: user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- TOC entry 2380 (class 2606 OID 133597)
-- Name: user_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_roles
    ADD CONSTRAINT user_roles_pkey PRIMARY KEY (id);


--
-- TOC entry 2382 (class 2606 OID 133599)
-- Name: wedding_band_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY wedding_band
    ADD CONSTRAINT wedding_band_pkey PRIMARY KEY (id);


--
-- TOC entry 2388 (class 2606 OID 133708)
-- Name: wholesale_items_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY wholesale_items
    ADD CONSTRAINT wholesale_items_pkey PRIMARY KEY (id);


--
-- TOC entry 2384 (class 2606 OID 133601)
-- Name: wholesale_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY wholesale
    ADD CONSTRAINT wholesale_pkey PRIMARY KEY (id);


--
-- TOC entry 2386 (class 2606 OID 133603)
-- Name: wishlist_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY wishlist
    ADD CONSTRAINT wishlist_pkey PRIMARY KEY (id);


--
-- TOC entry 2573 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- TOC entry 2576 (class 0 OID 0)
-- Dependencies: 181
-- Name: address_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE address_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE address_id_seq FROM postgres;
GRANT ALL ON SEQUENCE address_id_seq TO postgres;
GRANT ALL ON SEQUENCE address_id_seq TO PUBLIC;


--
-- TOC entry 2578 (class 0 OID 0)
-- Dependencies: 183
-- Name: admin_roles_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE admin_roles_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE admin_roles_id_seq FROM postgres;
GRANT ALL ON SEQUENCE admin_roles_id_seq TO postgres;
GRANT ALL ON SEQUENCE admin_roles_id_seq TO PUBLIC;


--
-- TOC entry 2580 (class 0 OID 0)
-- Dependencies: 185
-- Name: administrator_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE administrator_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE administrator_id_seq FROM postgres;
GRANT ALL ON SEQUENCE administrator_id_seq TO postgres;
GRANT ALL ON SEQUENCE administrator_id_seq TO PUBLIC;


--
-- TOC entry 2582 (class 0 OID 0)
-- Dependencies: 187
-- Name: banner_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE banner_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE banner_id_seq FROM postgres;
GRANT ALL ON SEQUENCE banner_id_seq TO postgres;
GRANT ALL ON SEQUENCE banner_id_seq TO PUBLIC;


--
-- TOC entry 2584 (class 0 OID 0)
-- Dependencies: 189
-- Name: category_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE category_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE category_id_seq FROM postgres;
GRANT ALL ON SEQUENCE category_id_seq TO postgres;
GRANT ALL ON SEQUENCE category_id_seq TO PUBLIC;


--
-- TOC entry 2586 (class 0 OID 0)
-- Dependencies: 191
-- Name: config_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE config_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE config_id_seq FROM postgres;
GRANT ALL ON SEQUENCE config_id_seq TO postgres;
GRANT ALL ON SEQUENCE config_id_seq TO PUBLIC;


--
-- TOC entry 2589 (class 0 OID 0)
-- Dependencies: 195
-- Name: coupon_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE coupon_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE coupon_id_seq FROM postgres;
GRANT ALL ON SEQUENCE coupon_id_seq TO postgres;
GRANT ALL ON SEQUENCE coupon_id_seq TO PUBLIC;


--
-- TOC entry 2591 (class 0 OID 0)
-- Dependencies: 197
-- Name: credit_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE credit_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE credit_id_seq FROM postgres;
GRANT ALL ON SEQUENCE credit_id_seq TO postgres;
GRANT ALL ON SEQUENCE credit_id_seq TO PUBLIC;


--
-- TOC entry 2592 (class 0 OID 0)
-- Dependencies: 198
-- Name: jewelry; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE jewelry FROM PUBLIC;
REVOKE ALL ON TABLE jewelry FROM postgres;
GRANT ALL ON TABLE jewelry TO postgres;
GRANT ALL ON TABLE jewelry TO PUBLIC;


--
-- TOC entry 2594 (class 0 OID 0)
-- Dependencies: 199
-- Name: jewelry_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE jewelry_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE jewelry_id_seq FROM postgres;
GRANT ALL ON SEQUENCE jewelry_id_seq TO postgres;
GRANT ALL ON SEQUENCE jewelry_id_seq TO PUBLIC;


--
-- TOC entry 2596 (class 0 OID 0)
-- Dependencies: 202
-- Name: order_product_option; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE order_product_option FROM PUBLIC;
REVOKE ALL ON TABLE order_product_option FROM postgres;
GRANT ALL ON TABLE order_product_option TO postgres;
GRANT ALL ON TABLE order_product_option TO PUBLIC;


--
-- TOC entry 2598 (class 0 OID 0)
-- Dependencies: 203
-- Name: order_product_option_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE order_product_option_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE order_product_option_id_seq FROM postgres;
GRANT ALL ON SEQUENCE order_product_option_id_seq TO postgres;
GRANT ALL ON SEQUENCE order_product_option_id_seq TO PUBLIC;


--
-- TOC entry 2601 (class 0 OID 0)
-- Dependencies: 207
-- Name: page_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE page_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE page_id_seq FROM postgres;
GRANT ALL ON SEQUENCE page_id_seq TO postgres;
GRANT ALL ON SEQUENCE page_id_seq TO PUBLIC;


--
-- TOC entry 2603 (class 0 OID 0)
-- Dependencies: 209
-- Name: payment_profiles_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE payment_profiles_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE payment_profiles_id_seq FROM postgres;
GRANT ALL ON SEQUENCE payment_profiles_id_seq TO postgres;
GRANT ALL ON SEQUENCE payment_profiles_id_seq TO PUBLIC;


--
-- TOC entry 2606 (class 0 OID 0)
-- Dependencies: 213
-- Name: product_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE product_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE product_id_seq FROM postgres;
GRANT ALL ON SEQUENCE product_id_seq TO postgres;
GRANT ALL ON SEQUENCE product_id_seq TO PUBLIC;


--
-- TOC entry 2609 (class 0 OID 0)
-- Dependencies: 217
-- Name: product_map_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE product_map_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE product_map_id_seq FROM postgres;
GRANT ALL ON SEQUENCE product_map_id_seq TO postgres;
GRANT ALL ON SEQUENCE product_map_id_seq TO PUBLIC;


--
-- TOC entry 2612 (class 0 OID 0)
-- Dependencies: 219
-- Name: ring_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE ring_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE ring_id_seq FROM postgres;
GRANT ALL ON SEQUENCE ring_id_seq TO postgres;
GRANT ALL ON SEQUENCE ring_id_seq TO PUBLIC;


--
-- TOC entry 2616 (class 0 OID 0)
-- Dependencies: 225
-- Name: shipping_method_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE shipping_method_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE shipping_method_id_seq FROM postgres;
GRANT ALL ON SEQUENCE shipping_method_id_seq TO postgres;
GRANT ALL ON SEQUENCE shipping_method_id_seq TO PUBLIC;


--
-- TOC entry 2618 (class 0 OID 0)
-- Dependencies: 227
-- Name: transaction_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE transaction_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE transaction_id_seq FROM postgres;
GRANT ALL ON SEQUENCE transaction_id_seq TO postgres;
GRANT ALL ON SEQUENCE transaction_id_seq TO PUBLIC;


--
-- TOC entry 2621 (class 0 OID 0)
-- Dependencies: 232
-- Name: user_roles.id; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL(id) ON TABLE user_roles FROM PUBLIC;
REVOKE ALL(id) ON TABLE user_roles FROM postgres;
GRANT ALL(id) ON TABLE user_roles TO PUBLIC;


--
-- TOC entry 2624 (class 0 OID 0)
-- Dependencies: 235
-- Name: wedding_band_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE wedding_band_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE wedding_band_id_seq FROM postgres;
GRANT ALL ON SEQUENCE wedding_band_id_seq TO postgres;
GRANT ALL ON SEQUENCE wedding_band_id_seq TO PUBLIC;


--
-- TOC entry 2628 (class 0 OID 0)
-- Dependencies: 239
-- Name: wishlist_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE wishlist_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE wishlist_id_seq FROM postgres;
GRANT ALL ON SEQUENCE wishlist_id_seq TO postgres;
GRANT ALL ON SEQUENCE wishlist_id_seq TO PUBLIC;


-- Completed on 2017-08-15 11:42:29

--
-- PostgreSQL database dump complete
--

