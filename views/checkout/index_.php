<div class="banner cms" style="margin-bottom:0;">
	<div class="container">
		<div class="inner">
			<h1>Cart</h1>
		</div>
	</div>
</div>

<div class="container container_inner" style="margin-bottom:50px;">
			<div class="row">
				<div class="col-sm-14">
			<ol class="breadcrumb">
				<li>1. <a href="<?=SITE_URL?>cart"><b>View Cart</b></a></li>
				<li>2. <a href="<?=SITE_URL?>orders/checkout">Checkout</a></li>
				<li>3. <a href="<?=SITE_URL?>orders/checkout">Review/Submit</a></li>
			</ol>
		</div>
	</div>
</div> 

<div class="cart">
		<div class="container container_inner">
						<? if (count($model->cart->products) > 0) { ?>

			
				<div class="table-responsive">
					<table class="table table-products">

						<thead>
							<tr>
								<th width="5%">&nbsp;</th> 
								<th width="31%">Product</th>
								<th width="16%">Price</th>
								<th width="1%">QTY</th>
								<th width="1%"></th>
								<th width="16%">Total</th>
							</tr>
						</thead>


					<?php foreach ($model->cart->products as $product) {?>
 

						<tbody>
							<tr>
								
								<td width="5%">
									<a href="<?=SITE_URL?>cart/remove_product/<?=$product->key?>" class="close" aria-label="Delete Product">&times;</a>

								</td>
								
								 
								<!-- PRODUCT -->
								<td class="product_width" width="31%">
									<h4><a href=<?=SITE_URL.'products/'.$product->slug?>><?=$product->name?></a></h4> 
								</td>

								<!-- UNIT PRICE -->
								<td class="product_width" width="16%">$<?=number_format($product->price, 2)?></td>

								 
								
								<!-- ADD/REMOVE ONE -->
								<td width="1%">
									<a href="<?=SITE_URL?>cart/add/<?=$product->id?>" class="plus_sign" aria-label="Add One">&plus;</a>
									<br />
									<a href="<?=SITE_URL?>cart/remove_product/<?=$product->key?>" class="minus_sign" aria-label="Add One">&#45;</a>
								</td>

								<!-- TOTAL -->
								 
							</tr>

					<?php } ?>

							<tr>
								<td colspan="7" class="link-bottom">
									<!--<a href="<?=SITE_URL?>cart/remove_all" class="link-blue pull-left">Empty Cart</a>-->
							 		<a href="<?=SITE_URL?>" class="link-blue pull-right">Continue Shopping</a>
								</td>
							</tr>

						</tbody>
					</table>
				</div>

				<br /> <br />

				<div class="row cart-bottom">

					<div class="col-sm-12 col-md-8">
						<h3>Apply discounts</h3>
						<div class="input-group form-group">
							<input name="coupon-code" type="text" class="form-control" placeholder="Coupon Code" value="<?=(isset($_SESSION['coupon'])?$_SESSION['coupon']:'')?>" >
							<span class="input-group-btn">
								<button class="btn btn-primary apply-coupon" type="button">Apply</button>
							</span>
						</div><!-- /input-group -->
						<div class="input-group form-group"></div>
						<div class="input-group form-group">
							<input name="gift-card" type="text" class="form-control" placeholder="Gift Card">
							<span class="input-group-btn">
								<button class="btn btn-primary" type="button">Apply</button>
							</span>
						</div><!-- /input-group -->
					</div>

					<div class="col-sm-12 col-md-8">
						<h3>Shipping Options</h3>
						<!-- <div class="form-group">
							<div class="custom-select">
								<select name="country" id="country" class="form-control">
									<option value="0">COUNTRY</option>
									<option value="1">United States</option>
									<option value="2">Canada</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<div class="custom-select">
								<select name="state" id="state" class="form-control">
									<option value="0">STATE</option>
									<option value="1">Arizona</option>
									<option value="2">Alabama</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<input name="zip" type="text" class="form-control" placeholder="ZIP" />
						</div> -->

						<div class="form-group">
							<ul id="shipping-list" class="list-unstyled">
								<?
									$selected_method = $model->shipping_method; 
									foreach($model->shipping_methods as $shipping_method) { 
								?>
									<li><label for="s<?=$shipping_method->id?>"><input <?=($shipping_method->id == $selected_method)?'checked="checked"':'';?> type="radio" class="shipping-item" name="shipping" value="<?=$shipping_method->id?>" data-price="<?=$shipping_method->cost?>" />$<?=$shipping_method->cost?> <?=$shipping_method->name?></label></li>
								<? } ?>
							</ul>
						</div>

						<!-- <button type="submit" class="btn btn-primary btn-block">Update Total</button>
						<br /><br /> -->
					</div>

					<div class="col-sm-24 col-md-8">
						<table class="table table-cart">
							<tbody>
								<tr>
									<th>Subtotal:</th>
									<td> 
										$<span id="subtotal-value"><?= number_format($model->subtotal, 2) ?></span>
									</td>
								</tr>
								<tr>
									<th>Savings:</th>
									<td id="savings-value">$0.00</td>
								</tr>
								<tr>
									<th>Shipping:</th>
									<td>$<span id="shipping-value">0.00</span></td>
								</tr>
								<tr style="display:none;">
									<th>Tax:</th>
									<td>$<span id="tax-value">0.00</span></td>
								</tr>
								<tr>
									<th><h3>Grand Total</h3></th>
									<td><h3>$<span id="grand-total-value"><?=$model->cart->total?></span></h3></td>
								</tr>
							</tbody>
						</table>
						<hr />

						<? if (is_null($model->user)) { ?>								
							<a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target="#login">Secure Checkout</a>
							<!-- Guest Checkout -->
							<a href="<?=SITE_URL?>orders/checkout?type=guest" class="btn btn-primary btn-block">Guest Checkout</a>
						<? } else { ?>
							<a href="<?=SITE_URL?>orders/checkout" class="btn btn-primary btn-block">Checkout</a>
						<? } ?>

						<!--p>OR</p>
						<p><a href="#"><img src="/content/frontend/img/button_amazon.png" alt="Pay with Amazon" class="img-responsive" /></a></p>
						<p><a href="#"><img src="/content/frontend/img/button_paypal.png" alt="Pay with PayPal" class="img-responsive" /></a></p-->
					</div>
				</div>
			
					<? } else { ?>
		<h3 class="text-center">Your cart is empty, <a href="<?=SITE_URL?>" class="link-blue">Continue Shopping!</a></h3>
		<? } ?>

		</div>
	</div>

<?//= footer()?>

<script src="<?=FRONT_JS?>plugins/jquery.number.min.js"></script>

<script>
	$(function(){
		$('#shipping-list label').each(function(){ 
			if($('.iradio_square', this).hasClass('checked')){
				var value = $(this).find('input').attr('data-price');
				$('#shipping-value').text($.number(value, 2));
				$('#grand-total-value').text($.number(parseFloat($('#subtotal-value').html().replace(/,/g,'')) + value * 1, 2));
			}
		})
		$('input.shipping-item').on('ifChecked', function(){
			var value = $(this).attr('data-price');
			var id = $(this).val();
			$('#shipping-value').text($.number(value, 2));
			if ($('.apply-coupon').parent().prev().val() != ''){
				$('.apply-coupon').trigger('click');
			} else {
				$('#grand-total-value').text($.number(parseFloat($('#subtotal-value').html().replace(/,/g,'')) + value*1, 2));
			}
			$.ajax({
				url: '<?=SITE_URL?>cart/updateShipping',
				data: 'id='+id
			})
		})
		$('.apply-coupon').click(function(e){
			e.preventDefault();
			var code = $(this).parent().prev().val();
			$.getJSON('<?=SITE_URL?>cart/applyCoupon?code='+code, function(data){
				var total = parseFloat($('#subtotal-value').text().replace(/,/g,''));
				if (data.error.code == 0){
					$('.apply-coupon').parent().parent().next().text('');
					var savings = data.coupon.discount_amount;
					var shipping_and_tax = parseFloat($('#shipping-value').text().replace(/,/g,'')) + parseFloat($('#tax-value').text().replace(/,/g,''));
					if (data.coupon.discount_type == 1){ //$
						$('#grand-total-value').text(parseFloat(total - savings + shipping_and_tax).toFixed(2));
						$('#savings-value').text('$'+parseFloat(savings).toFixed(2));
					} else if (data.coupon.discount_type == 2){ //%
						$('#grand-total-value').text($.number(total * (1-savings/100) + shipping_and_tax, 2));
						$('#savings-value').text('$' + $.number(total * savings/100, 2));
					}
				} else {
					$('.apply-coupon').parent().parent().next().text(data.error.message);
					$('#grand-total-value').text($.number(parseFloat(total)+parseFloat($('#shipping-value').text().replace(/,/g,'')), 2));
					$('#savings-value').text('$0.00');
				}
			})
			return false;
		})
		if ($('.apply-coupon').parent().prev().val() != ''){
			$('.apply-coupon').trigger('click');
		}
 	})
</script>
