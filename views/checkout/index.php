<?
    function setDefaultValue($model, $inputName, $relations = null, $field = null)
    {
        if(isset($_SESSION['checkoutPost']) && isset($_SESSION['checkoutPost'][$inputName])){
            return $_SESSION['checkoutPost'][$inputName];
        } elseif($user = $model->user){
			if($inputName == 'email'){
				return $user->email;
			}
            if($relations && $field && $user->$relations()){
                return $user->$relations()->$field;
            }
        } else {
            return "";
        }
    }
?>
<form id='checkout-form' method="post" action="/checkout">
<div class = "checkout_content_wrapper">
	<div class = "col_left" style="display:none;">
		<div class = "order_summary">
			<h1>Order Summary</h1>
			<ul class = "products_order_summary">
				<? if (count($model->cart->products) > 0) { ?>
					<?php foreach($model->cart->products as $key => $item) {?>
						<li>
							<a href="<?=SITE_URL?>checkout/remove_product/<?=$key?>"  class = "remove_product">
								<svg class="modal_svg" xmlns="http://www.w3.org/2000/svg" version="1.1" width="32" height="32" viewBox="0 0 32 32">
									<path class="modal_svg_path" d="M31.158 32.118c-.246 0-.492-.093-.678-.282L.163 1.52c-.375-.375-.375-.983 0-1.358s.983-.375 1.358 0l30.317 30.316c.375.375.375.983 0 1.358-.187.188-.433.282-.678.282zM.842 32.118c-.246 0-.492-.093-.678-.282-.375-.375-.375-.983 0-1.358L30.48.162c.375-.375.983-.375 1.358 0s.375.983 0 1.358L1.52 31.836c-.186.188-.432.282-.677.282z" fill="#fff"></path>
								</svg>
							</a>
							<div class = "product_image">
								<?$productImage = $item->product->featuredImage();?>
								<img src="<?=!empty($productImage)?$productImage : FRONT_ASSETS."img/ring5.png"?>" />
							</div>
							<div class = "product_data">
								<h6><?=$item->product->name?></h6>
								<p class = "price">$<?=number_format(\Model\Product::getPriceByProducts([$item]), 2)?></p>
								<? if($item->productMap->product_type_id == 1){?>
									<p class = "cart-detail"><label>Carat:</label><?php echo strtoupper($item->product->weight)?> CT</p>
								<? } ?>
								<?if(isset($item->options)) {
									foreach ($item->options as $optionKey => $optionValue) {
										?>
										<p class="cart-detail"><label><?= $optionKey ?>
												:</label><?php echo strtoupper(str_replace('_', ' ', $optionValue)) ?>
										</p>
									<?}
								}?>
							</div>
						</li>
						<?if(isset($item->optionalProduct)){?>
							<li>
								<div class = "product_image">
									<?$productImage = $item->optionalProduct->featuredImage();?>
									<img src="<?=!empty($productImage)?$productImage : FRONT_ASSETS."img/ring5.png"?>" />
								</div>
								<div class = "product_data">
									<h6><?=$item->optionalProduct->name?></h6>
									<? if($item->optionalProduct->default_metal) { ?>
										<p class = "cart-detail"><label>Metal:</label><?php echo strtoupper($item->optionalProduct->default_metal)?></p>
									<? } ?>

									<? if($item->optionalProduct->default_color) { ?>
										<p class = "cart-detail"><label>Color:</label><?php echo strtoupper($item->optionalProduct->default_color)?></p>
									<? } ?>

									<? if(isset($item->size) && $item->size) { ?>
										<p class = "cart-detail"><label>Size:</label><?php echo strtoupper($item->size)?></p>
									<? } ?>
								</div>
							</li>
						<?php } ?>
						<?if(isset($item->ringMaterial)){?>
							<li>
								<div class = "product_image">
									<img src="<?=\Model\Ring_Setting::featureImage($item->ring_setting)?>" />
								</div>
								<div class = "product_data">
									<h6><?=$item->ringMaterial->name?></h6>
									<p class = "cart-detail"><label>Setting Style:</label><?php echo strtoupper(\Model\Ring_Setting::getName($item->ring_setting))?></p>

									<? if(isset($item->size) && $item->size) { ?>
										<p class = "cart-detail"><label>Size:</label><?php echo strtoupper($item->size)?></p>
									<? } ?>
								</div>
							</li>
						<?php } ?>
					<?php } ?>
				<?php } ?>
			</ul>
			<div class = "order_summary_data">
				<div>
					<div class = "left">Subtotal:</div>
					<div class = "right" id="subtotal-value">$<?=number_format($model->subtotal, 2);?></div>
				</div>
				<div>
					<div class = "left">Coupon:</div>
					<div class = "right">
						<input type="text" name="code" class="summary_data_text_input" value="<?=isset($_SESSION['coupon'])?$_SESSION['coupon']:''?>"/>
						<input type="button" value="Update" class="coupon_update_btn apply-coupon"/>
					</div>
				</div>
				<div>
					<div class = "left">Discount:</div>
					<div class = "right"><?php if($model->discountAmount > 0){ echo "-$".number_format($model->discountAmount,2);} else { echo "--";}?></div>
				</div>
				<div>
					<div class = "left">Shipping &#x26; Handling:</div>
					<div class = "right">
						<select class="agd_select" name="shipping_method">
                            <? foreach($model->shipping_methods as $method){ ?>
                                <option value="<?=$method->id?>" <?if($model->shipping_method == $method->id){ echo "selected";}?>>
                                    <?=$method->name?> -- $<?=number_format($method->cost, 2)?>
                                </option>
                            <? } ?>
						</select>
					</div>
				</div>
				<div>
					<div class = "left">Tax:</div>
					<div class = "right" id="tax-fee">--</div>
				</div>
				<div class = "final_data">
					<div class = "left">Total:</div>
					<div class = "right" id="total-value">$<?=number_format($model->total, 2);?></div>
<!--					<div class = "right" id="total-value">$--><?//=number_format($model->total, 2);?><!--</div>-->
				</div>
			</div>
		</div>
	</div>
	
	<div class ="col_right">
		<div class = "order_checkout_form_wrapper">
			<h1>Confirm Your Information</h1>
			<? if($model->user){?>
			<h5 class="float_right checkout_hello">Hello, <? echo $model->user->first_name; ?></h5>
			<? } else { ?>
<!--			<a class="float_right checkout_login_prompt show_signin_overlay">Log In</a>-->
			<? } ?>
			<div class = "input_row">
				<? if($model->transaction){ ?>
					<input type = "hidden" name = "id" value="<?=$model->transaction->id?>">
				<? } ?>
			</div>
			<div class = "input_row input_row_50">
				<div class = "width_50">
					<? if($model->transaction){ ?>
						<input type = "text" placeholder = "First name" name = "ship_first_name" value="<?=$model->transaction->ship_first_name; ?>" required>
					<? } else {?>
						<input type = "text" placeholder = "First name" name = "ship_first_name" value="<? echo setDefaultValue($model,'ship_first_name', 'address', 'first_name'); ?>" required>
					<? } ?>
				</div>

				<div class = "width_50">
					<? if($model->transaction){ ?>
						<input type = "text" placeholder = "Last name" name = "ship_last_name" value="<?=$model->transaction->ship_last_name?>" required>
					<? } else { ?>
						<input type = "text" placeholder = "Last name" name = "ship_last_name" value="<? echo setDefaultValue($model, 'ship_last_name', 'address', 'last_name'); ?>" required>
					<? } ?>
				</div>
			</div>
			<div class = "input_row">
				<? if($model->transaction){ ?>
					<input type = "text" placeholder = "Street address" name = "ship_address" value="<?=$model->transaction->ship_address?>" required>
				<? } else { ?>
					<input type = "text" placeholder = "Street address" name = "ship_address" value="<? echo setDefaultValue($model, 'ship_address', 'address', 'address'); ?>" required>
				<? } ?>
			</div>
            <div class = "input_row">
            	<? if($model->transaction){ ?>
                	<input type = "text" placeholder = "Apt#/Suite" name = "ship_address2" value="<?=$model->transaction->ship_address2?>">
                <? } else { ?>
                	<input type = "text" placeholder = "Apt#/Suite" name = "ship_address2" value="<? echo setDefaultValue($model, 'ship_address2', 'address', 'address2'); ?>">
                <? } ?>
            </div>
			<div class = "input_row">				
				<div class = "width_50" style="margin-right: 12px;">
					<? if($model->transaction){ ?>
						<input type = "text" placeholder = "City" name = "ship_city" value="<?=$model->transaction->ship_city ?>" required>
					<? } else { ?>
						<input type = "text" placeholder = "City" name = "ship_city" value="<? echo setDefaultValue($model, 'ship_city', 'address', 'city'); ?>" required>
					<? } ?>
				</div>
				<div class = "width_25">
					<select name="ship_state" size="1" class="agd_select" required>
						<option value="">State</option>
						<?php foreach(get_states() as $key => $value){ ?>
							<? if($model->transaction){ ?>
								<? $select = $model->transaction->ship_state == $key ? 'selected':''; ?>
								<option value="<?=$key?>" <?=$select?>><?=$value?></option>
							<? } else {?>
								<option value="<?=$key?>" <?if(setDefaultValue($model, 'ship_state', 'address', 'state') == $key){ echo "selected";}?>><?=$value?></option>
							<? } ?>
						<? } ?>
					</select>
				</div>
				<div class = "width_25">
					<? if($model->transaction){ ?>
						<input type = "text" placeholder = "Zip code" name = "ship_zip" value="<?=$model->transaction->ship_zip ?>" required>
					<? } else {?>
						<input type = "text" placeholder = "Zip code" name = "ship_zip" value="<? echo setDefaultValue($model, 'ship_zip', 'address', 'zip'); ?>" required>
					<? } ?>
				</div>
			</div>
			<div class = "input_row">
				<? if($model->transaction){ ?>
					<input type = "text" placeholder = "Phone number" name = "phone" value="<?=$model->transaction->phone?>" required>
				<? } else { ?>
					<input type = "text" placeholder = "Phone number" name = "phone" value="<? echo setDefaultValue($model, 'phone', 'address', 'phone'); ?>" required>
				<? } ?>
			</div>
			<div class = "input_row">
				<? if($model->transaction){ ?>
					<input type = "email" placeholder = "Email address" name = "email" value="<?=$model->transaction->email?>" required>
				<? } else { ?>
					<input type = "email" placeholder = "Email address" name = "email" value="<? echo setDefaultValue($model, 'email')?>" required>
				<? } ?>
			</div>

<!--
			<div class = "input_row">
				<input class="checkbox" type="checkbox" name="is_same" data-default="<//?echo setDefaultValue($model,'is_same') ?>"style="width: auto;" checked>Same billing address?
			</div>
-->

			<div class="separate_billing" style="display:none;">
				<h1 >Billing Address</h1>
				<div class = "input_row input_row_50">
					<div class = "width_50" >
						<input type = "text" placeholder = "First name" name = "bill_first_name" value="<? echo setDefaultValue($model,'bill_first_name', 'payment_profile', 'first_name'); ?>">
					</div>
					<div class = "width_50" >
						<input type = "text" placeholder = "Last name" name = "bill_last_name" value="<? echo setDefaultValue($model, 'bill_last_name', 'payment_profile', 'last_name'); ?>">
					</div>
				</div>
				<div class = "input_row" >
					<input type = "text" placeholder = "Street address" name = "bill_address" value="<? echo setDefaultValue($model, 'bill_address', 'payment_profile', 'address'); ?>">
				</div>
				<div class = "input_row" >
					<input type = "text" placeholder = "Street address" name = "bill_address2" value="<? echo setDefaultValue($model, 'bill_address2', 'payment_profile', 'address2'); ?>">
				</div>

				<div class = "input_row" >
					<div class = "width_50" style="margin-right: 12px;">
						<input type = "text" placeholder = "City" name = "bill_city" value="<? echo setDefaultValue($model, 'bill_city', 'payment_profile', 'city'); ?>">
					</div>
					<div class = "width_25">
						<select name="bill_state" size="1" class="agd_select">
							<option value="-1">State</option>
							<?php foreach(get_states() as $key => $value){ ?>
								<option value="<?=$key?>" <?if(setDefaultValue($model, 'bill_state', 'payment_profile', 'state') == $key){ echo "selected";}?>><?=$value?></option>
							<? } ?>
						</select>
					</div>
					<div class = "width_25">
						<input type = "text" placeholder = "Zip code" name = "bill_zip" value="<? echo setDefaultValue($model, 'bill_zip', 'payment_profile', 'zip'); ?>">
					</div>
				</div>
			</div>
			<div class="paymentDisplay">
				<h1>Payment Information</h1>
				<div class = "input_row">
					<div class = "width_65">
						<p>Please enter the payment amount</p>
						<? if($model->transaction){ ?>
							<p>$<?=$model->transaction->amount?></p>
						<? } else { ?>
							<input type = "text" placeholder = "Payment Amount" name = "amount">
						<? } ?>
					</div>
				</div>
			<!-- <div class = "input_row">

				<div class = "width_65">
					<input type = "text" placeholder = "Name as appears on card" name = "name_on_card">
				</div>
				<div class = "width_35" style = "padding-left:12px;">
					<input type = "text" placeholder = "Zip code" name = "billing_zip_code">
				</div>
				
			</div>
			<div class = "input_row">
				<div class = "width_65">
					<input type = "text" placeholder = "Card number" name = "cc_number" maxlength="16" id="credit_card_number">
				</div>
				<div class = "width_35">
					<svg version="1.1" id="Layer_1" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;" x="0px" y="0px" viewBox="618.7 14.5 430.4 54.5" enable-background="new 618.7 14.5 430.4 54.5" xml:space="preserve" class="credit_card_options">
	                    <g id="mc">
	                        <g>
	                            <path fill="#5565AF" d="M1049.1,61.3c0,3.7-3.1,6.7-6.9,6.7h-69.4c-3.9,0-6.9-2.9-6.9-6.7V21.2c0-3.7,3.1-6.7,6.9-6.7h69.4
	                                c3.9,0,6.9,2.9,6.9,6.7V61.3z"/>
	                            <path fill="#56B1D9" d="M1007.4,14.5"/>
	                            <g>
	                                <path fill="#EA564B" d="M1007.4,32.4c-2.5-2.8-6.2-4.5-10.4-4.5c-7.6,0-13.9,6-13.9,13.4s6.2,13.4,13.9,13.4
	                                    c4.2,0,7.9-1.7,10.4-4.5c-2.2-2.4-3.5-5.5-3.5-8.8C1004,37.9,1005.2,34.8,1007.4,32.4z"/>
	                                <path fill="#EA564B" d="M1007.4,32.4c-2.2,2.4-3.5,5.5-3.5,8.8s1.3,6.4,3.5,8.8c2.2-2.4,3.5-5.5,3.5-8.8S1009.5,34.8,1007.4,32.4
	                                    z"/>
	                            </g>
	                            <path fill="#E9D419" d="M1017.7,27.9c-4.2,0-7.9,1.7-10.4,4.5c-0.6,0.7-1.1,1.3-1.5,2.1h3.2c0.6,0.8,0.8,1.7,1.3,2.7h-5.5
	                                c-0.3,0.8-0.6,1.7-0.6,2.7h6.8c0,0.4,0.1,0.9,0.1,1.3c0,0.4,0,0.9-0.1,1.3h-6.8c0.1,0.9,0.3,1.9,0.6,2.7h5.7
	                                c-0.3,0.9-0.7,1.9-1.3,2.7h-3.2c0.4,0.8,1,1.5,1.5,2.1c2.5,2.8,6.2,4.5,10.4,4.5c7.6,0,13.9-6,13.9-13.4
	                                C1031.6,33.9,1025.5,27.9,1017.7,27.9z"/>
	                        </g>
	                    </g>
	                    <g id="visa">
	                        <g>
	                            <path fill="#F3F4F4" d="M932.7,61.3c0,3.7-3.1,6.7-6.9,6.7h-69.4c-3.9,0-6.9-2.9-6.9-6.7V21.2c0-3.7,3.1-6.7,6.9-6.7h69.4
	                                c3.9,0,6.9,2.9,6.9,6.7V61.3z"/>
	                            <path fill="#5565AF" d="M849.5,27.9v-6.7c0-3.7,3.2-6.7,7.2-6.7h68.9c4,0,7.2,2.9,7.2,6.7v6.7"/>
	                            <path fill="#E6A124" d="M932.7,54.6v7.4c0,4.2-3.2,5.9-7.2,5.9h-68.9c-4,0-7.2-1.8-7.2-5.9v-7.4"/>
	                            <g>
	                                <path fill="#5565AF" d="M873.5,45.8c0.6-1.5,1-2.5,1.3-2.9l4.7-9.2h3.5l-8,15.3h-3.6l-1.4-15.3h3.2l0.6,9.2c0,0.3,0,0.8,0,1.3
	                                    C873.5,44.9,873.5,45.4,873.5,45.8L873.5,45.8z"/>
	                                <path fill="#5565AF" d="M881.4,48.9l3.3-15.3h3.3l-3.3,15.3H881.4z"/>
	                                <path fill="#5565AF" d="M898.2,44.3c0,1.5-0.6,2.5-1.7,3.5c-1.1,0.8-2.5,1.2-4.3,1.2c-1.7,0-2.9-0.3-3.9-0.9v-2.8
	                                    c1.4,0.8,2.6,1.1,3.9,1.1c0.8,0,1.4-0.1,1.9-0.4c0.4-0.3,0.7-0.7,0.7-1.2c0-0.3,0-0.5-0.1-0.8c-0.1-0.3-0.3-0.4-0.4-0.7
	                                    c-0.1-0.1-0.7-0.5-1.4-1.1c-1-0.7-1.7-1.3-2.1-2c-0.4-0.7-0.6-1.3-0.6-2.1c0-0.9,0.3-1.7,0.7-2.4c0.4-0.7,1.1-1.2,1.9-1.6
	                                    c0.8-0.4,1.8-0.5,2.9-0.5c1.5,0,3.1,0.4,4.3,1.1l-1.3,2.4c-1.1-0.5-2.2-0.8-3.1-0.8c-0.6,0-1.1,0.1-1.5,0.5
	                                    c-0.4,0.3-0.6,0.7-0.6,1.2c0,0.4,0.1,0.8,0.4,1.1c0.3,0.3,0.8,0.8,1.7,1.2c0.8,0.5,1.5,1.2,1.9,1.9
	                                    C898.1,42.7,898.2,43.5,898.2,44.3z"/>
	                                <path fill="#5565AF" d="M908.9,45.3h-5.1l-1.8,3.6h-3.5l8.3-15.4h4l1.5,15.4h-3.2L908.9,45.3z M908.7,42.6l-0.3-3.6
	                                    c-0.1-0.9-0.1-1.7-0.1-2.7v-0.4c-0.3,0.8-0.7,1.7-1.3,2.7l-2.1,4H908.7z"/>
	                            </g>
	                            <path fill="#FFFFFF" d="M891.1,14.5"/>
	                        </g>
	                    </g>
	                    <g id="disc">
	                        <g>
	                            <path fill="#FFF9F0" d="M817.1,61.3c0,3.7-3.1,6.7-6.9,6.7h-69.4c-3.9,0-6.9-2.9-6.9-6.7V21.2c0-3.7,3.1-6.7,6.9-6.7h69.4
	                                c3.9,0,6.9,2.9,6.9,6.7V61.3z"/>
	                            <g>
	                                <path fill="#414042" d="M754.8,41.1c0,1.3-0.4,2.4-1.3,3.1c-0.8,0.7-1.9,1.1-3.5,1.1h-2.4v-8h2.6c1.4,0,2.5,0.4,3.2,1.1
	                                    C754.4,39,754.8,39.9,754.8,41.1z M752.8,41.2c0-1.7-0.8-2.7-2.4-2.7h-1v5.4h0.8C752,43.9,752.8,43,752.8,41.2z"/>
	                                <path fill="#414042" d="M756.3,45.3v-8h1.8v8.2h-1.8V45.3z"/>
	                                <path fill="#414042" d="M765.2,43.1c0,0.8-0.3,1.3-0.8,1.7c-0.6,0.4-1.3,0.7-2.2,0.7c-0.8,0-1.7-0.1-2.4-0.5v-1.6
	                                    c0.6,0.3,1.1,0.4,1.4,0.5c0.4,0.1,0.7,0.1,1.1,0.1c0.4,0,0.7-0.1,1-0.3c0.1-0.1,0.3-0.4,0.3-0.7c0-0.1,0-0.3-0.1-0.4
	                                    s-0.3-0.3-0.4-0.4c-0.1-0.1-0.6-0.3-1.1-0.5c-0.6-0.3-0.8-0.4-1.1-0.7c-0.3-0.3-0.4-0.4-0.6-0.8c-0.1-0.4-0.3-0.7-0.3-1.1
	                                    c0-0.7,0.3-1.3,0.7-1.7c0.6-0.4,1.3-0.7,2.1-0.7c0.4,0,0.8,0,1.3,0.1c0.4,0.1,0.8,0.3,1.3,0.4l-0.6,1.3c-0.4-0.1-0.8-0.3-1.1-0.4
	                                    c-0.3-0.1-0.6-0.1-0.8-0.1s-0.6,0.1-0.8,0.3c-0.1,0.1-0.3,0.4-0.3,0.5c0,0.1,0,0.3,0.1,0.4c0.1,0.1,0.1,0.3,0.4,0.3
	                                    s0.6,0.3,1.1,0.5c0.8,0.4,1.4,0.7,1.7,1.1C765,42,765.2,42.6,765.2,43.1z"/>
	                                <path fill="#414042" d="M770.3,38.4c-0.7,0-1.3,0.3-1.5,0.7c-0.4,0.5-0.6,1.2-0.6,2c0,1.9,0.7,2.7,2.1,2.7c0.6,0,1.3-0.1,2.2-0.4
	                                    v1.5c-0.7,0.3-1.5,0.4-2.4,0.4c-1.3,0-2.2-0.4-2.8-1.1c-0.7-0.7-1-1.7-1-3.1c0-0.8,0.1-1.6,0.4-2.3s0.8-1.1,1.4-1.5
	                                    c0.6-0.4,1.3-0.5,2.1-0.5c0.8,0,1.7,0.1,2.5,0.5l-0.6,1.3c-0.3-0.1-0.7-0.3-1-0.4C770.9,38.6,770.6,38.4,770.3,38.4z"/>
	                                <path fill="#414042" d="M786.8,37.2h1.8l-2.9,8.2h-1.9l-2.9-8.2h1.8l1.5,4.8c0.1,0.3,0.1,0.7,0.3,0.9c0.1,0.4,0.1,0.7,0.1,0.8
	                                    c0-0.4,0.1-0.9,0.4-1.7L786.8,37.2z"/>
	                                <path fill="#414042" d="M794.5,45.3h-4.9v-8h4.9v1.5h-3v1.7h2.9v1.5h-2.9V44h3.1L794.5,45.3L794.5,45.3z"/>
	                                <path fill="#414042" d="M798.1,42.2v3.1h-1.8v-8h2.5c1.1,0,1.9,0.1,2.5,0.7c0.6,0.4,0.8,1.1,0.8,1.9c0,0.5-0.1,0.9-0.4,1.3
	                                    c-0.3,0.4-0.7,0.7-1.1,0.9c1.3,1.9,2.1,3.1,2.5,3.5h-1.9l-2.1-3.1L798.1,42.2L798.1,42.2z M798.1,40.8h0.6c0.6,0,1-0.1,1.3-0.3
	                                    c0.3-0.1,0.4-0.5,0.4-0.8c0-0.4-0.1-0.7-0.4-0.8c-0.3-0.1-0.7-0.3-1.3-0.3h-0.6V40.8L798.1,40.8z"/>
	                                <ellipse fill="#E6A124" cx="776.8" cy="41.2" rx="4.2" ry="4"/>
	                            </g>
	                            <path fill="#E6A124" d="M817.1,51.3v10c0,0.9-0.1,1.7-0.6,2.5c-0.4,0.8-0.8,1.5-1.5,2.1c-0.7,0.7-1.4,1.1-2.2,1.5
	                                c-0.8,0.4-1.8,0.5-2.6,0.5h-31.2h-31.2L817.1,51.3z"/>
	                            <path opacity="8.000000e-02" fill="#FFFFFF" enable-background="new    " d="M815,16.5c1.3,1.2,2.1,2.9,2.1,4.7v20.1v20.1
	                                c0,1.9-0.8,3.5-2.1,4.7c-1.3,1.2-3.1,2-4.9,2h-34.7h-34.7c-1.9,0-3.6-0.8-4.9-2L815,16.5z"/>
	                        </g>
	                    </g>
	                    <g id="amex">
	                        <g>
	                            <g>
	                                <path fill="#5EC1EC" d="M701.9,61.3c0,3.7-3.1,6.7-6.9,6.7h-69.4c-3.9,0-6.9-2.9-6.9-6.7V21.2c0-3.7,3.1-6.7,6.9-6.7H695
	                                    c3.9,0,6.9,2.9,6.9,6.7V61.3z"/>
	                                <g>
	                                    <path fill="#FFFFFF" d="M643,47.8l-0.7-2.3h-4.2l-0.7,2.3h-3.9l4.2-12.4h4.6l4.3,12.4H643z M641.6,42.7l-0.6-2
	                                        c-0.1-0.4-0.3-1.1-0.4-1.7c-0.1-0.7-0.3-1.2-0.4-1.6c0,0.3-0.1,0.8-0.3,1.5c-0.1,0.7-0.6,2-1,3.9H641.6L641.6,42.7z"/>
	                                    <path fill="#FFFFFF" d="M653.8,47.8l-2.6-9H651c0.1,1.5,0.1,2.7,0.1,3.6v5.4h-3.1V35.4h4.6l2.6,8.8h0.1l2.6-8.8h4.6v12.4h-3.2
	                                        v-5.5c0-0.3,0-0.5,0-0.9c0-0.4,0-1.2,0.1-2.5h-0.1l-2.6,9L653.8,47.8L653.8,47.8z"/>
	                                    <path fill="#FFFFFF" d="M672.9,47.8h-7.6V35.4h7.6V38h-4.2v2h3.9v2.7h-3.9v2.4h4.2V47.8z"/>
	                                    <path fill="#FFFFFF" d="M686.8,47.8h-4l-2.5-3.9l-2.5,3.9h-3.9l4.3-6.3l-4-6h3.9l2.4,3.9l2.2-3.9h4l-4,6.3L686.8,47.8z"/>
	                                </g>
	                                <g>
	                                    <path fill="#FFFFFF" d="M643,47.8l-0.7-2.3h-4.2l-0.7,2.3h-3.9l4.2-12.4h4.6l4.3,12.4H643z M641.6,42.7l-0.6-2
	                                        c-0.1-0.4-0.3-1.1-0.4-1.7c-0.1-0.7-0.3-1.2-0.4-1.6c0,0.3-0.1,0.8-0.3,1.5c-0.1,0.7-0.6,2-1,3.9H641.6L641.6,42.7z"/>
	                                    <path fill="#FFFFFF" d="M653.8,47.8l-2.6-9H651c0.1,1.5,0.1,2.7,0.1,3.6v5.4h-3.1V35.4h4.6l2.6,8.8h0.1l2.6-8.8h4.6v12.4h-3.2
	                                        v-5.5c0-0.3,0-0.5,0-0.9c0-0.4,0-1.2,0.1-2.5h-0.1l-2.6,9L653.8,47.8L653.8,47.8z"/>
	                                    <path fill="#FFFFFF" d="M672.9,47.8h-7.6V35.4h7.6V38h-4.2v2h3.9v2.7h-3.9v2.4h4.2V47.8z"/>
	                                    <path fill="#FFFFFF" d="M686.8,47.8h-4l-2.5-3.9l-2.5,3.9h-3.9l4.3-6.3l-4-6h3.9l2.4,3.9l2.2-3.9h4l-4,6.3L686.8,47.8z"/>
	                                </g>
	                                <path fill="#56B1D9" d="M660.3,14.5"/>
	                            </g>
	                        </g>
	                    </g>
	                </svg>
				</div>

			</div>
			<div class = "input_row">
				<div class = "width_35 expiration_date_input_row">
					<input type = "text" placeholder = "MM" name="cc_expiration_month">
					<span>/</span>
					<input type = "text" placeholder = "YY" name="cc_expiration_year">
				</div>
				<div class = "width_65">
					<input class = "security_code" type = "text" placeholder = "CCV" name="cc_ccv">
	                 <div class = "show_ccv_help">
	                    <span>?</span>
	                </div>
	                <div class = "ccv_help_box">
	                    <p>For security reasons, please enter your card verification number (CVV) or card identification number (CID). Find this three-digit number on the back of your Visa, MasterCard and Discover cards in the signature area, or the four-digit number on the front of your American Express card, above the credit card number.</p>
	                    <span class = "ccv_tip_icons">
	                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	                                 viewBox="384.5 10.7 201.9 58.3" enable-background="new 384.5 10.7 201.9 58.3" xml:space="preserve">
	                            <g id="cvv_1_">
	                                <g>
	                                    <path fill="#C3C3C3" d="M468.3,59.9c0,3.7-3,6.5-6.8,6.5h-67.9c-3.8,0-6.8-2.9-6.8-6.5V20.6c0-3.7,3-6.5,6.8-6.5h67.9
	                                        c3.8,0,6.8,2.9,6.8,6.5L468.3,59.9C468.3,59.9,468.3,59.9,468.3,59.9z"/>
	                                </g>
	                                <rect x="386.8" y="22.3" fill="#222222" width="81.5" height="10.5"/>
	                                <rect x="393.2" y="38" fill="#FFFFFF" width="48.9" height="10.5"/>
	                                <rect x="397.1" y="41.9" fill="#222222" width="40.9" height="2.6"/>
	                                <rect x="449.8" y="38" fill="#FFFFFF" width="11.3" height="10.5"/>
	                                <rect x="451.1" y="41.9" fill="#222222" width="8.4" height="2.6"/>
	                                <ellipse fill="none" stroke="#FF0000" stroke-miterlimit="10" cx="455.4" cy="43.2" rx="10.6" ry="10.3"/>
	                            </g>
	                            <g id="cvv-amex_1_">
	                                <g id="cvv-amex">
	                                    <g>
	                                        <g>
	                                            <path fill="#5EC1EC" d="M586.4,59.7c0,3.8-3.1,6.7-7,6.7h-69.7c-3.9,0-7-3-7-6.7V19.4c0-3.8,3.1-6.7,7-6.7h69.7c3.9,0,7,3,7,6.7
	                                                V59.7z"/>
	                                            <g>
	                                                <path fill="#FFFFFF" d="M514.1,28.6l-0.4-1.6h-2.9l-0.4,1.6h-2.8l2.9-8.9h3.3l3.1,8.9H514.1z M513.1,25l-0.4-1.5
	                                                    c-0.1-0.3-0.1-0.7-0.3-1.2c-0.1-0.5-0.3-0.9-0.3-1.1c0,0.3-0.1,0.5-0.3,1.1c-0.1,0.5-0.4,1.3-0.7,2.8L513.1,25L513.1,25z"/>
	                                                <path fill="#FFFFFF" d="M521.7,28.6l-1.8-6.3l0,0c0.1,1.1,0.1,1.9,0.1,2.6v3.8h-2.2v-8.7h3.3l2,6.3l0,0l1.8-6.3h3.3v8.7h-2.2
	                                                    v-3.8c0-0.1,0-0.4,0-0.7c0-0.3,0-0.8,0.1-1.7l0,0l-1.8,6.2H521.7L521.7,28.6z"/>
	                                                <path fill="#FFFFFF" d="M535.4,28.6H530v-8.9h5.4v1.9h-2.9v1.3h2.8v1.9h-2.8v1.6h2.9L535.4,28.6L535.4,28.6z"/>
	                                                <path fill="#FFFFFF" d="M545.3,28.6h-2.9l-1.8-2.8l-1.8,2.8H536L539,24l-2.9-4.3h2.8l1.7,2.7l1.5-2.7h2.8l-2.9,4.6L545.3,28.6z
	                                                    "/>
	                                            </g>
	                                            <g>
	                                                <path fill="#FFFFFF" d="M514.1,28.6l-0.4-1.6h-2.9l-0.4,1.6h-2.8l2.9-8.9h3.3l3.1,8.9H514.1z M513.1,25l-0.4-1.5
	                                                    c-0.1-0.3-0.1-0.7-0.3-1.2c-0.1-0.5-0.3-0.9-0.3-1.1c0,0.3-0.1,0.5-0.3,1.1c-0.1,0.5-0.4,1.3-0.7,2.8L513.1,25L513.1,25z"/>
	                                                <path fill="#FFFFFF" d="M521.7,28.6l-1.8-6.3l0,0c0.1,1.1,0.1,1.9,0.1,2.6v3.8h-2.2v-8.7h3.3l2,6.3l0,0l1.8-6.3h3.3v8.7h-2.2
	                                                    v-3.8c0-0.1,0-0.4,0-0.7c0-0.3,0-0.8,0.1-1.7l0,0l-1.8,6.2H521.7L521.7,28.6z"/>
	                                                <path fill="#FFFFFF" d="M535.4,28.6H530v-8.9h5.4v1.9h-2.9v1.3h2.8v1.9h-2.8v1.6h2.9L535.4,28.6L535.4,28.6z"/>
	                                                <path fill="#FFFFFF" d="M545.3,28.6h-2.9l-1.8-2.8l-1.8,2.8H536L539,24l-2.9-4.3h2.8l1.7,2.7l1.5-2.7h2.8l-2.9,4.6L545.3,28.6z
	                                                    "/>
	                                            </g>
	                                        </g>
	                                        <rect x="507.5" y="43.9" display="none" fill="#FFFFFF" width="69.7" height="10.6"/>
	                                        <rect x="513.4" y="47.9" fill="#222222" width="56.1" height="2.7"/>
	                                        <rect x="565.3" y="31.4" fill="#FFFFFF" width="11.1" height="10.7"/>
	                                        <rect x="566.7" y="35.4" fill="#222222" width="8.4" height="2.7"/>
	                                    </g>
	                                    <ellipse fill="none" stroke="#FF0000" stroke-miterlimit="10" cx="570.8" cy="36.8" rx="10.9" ry="10.5"/>
	                                </g>
	                            </g>
	                        </svg>
	                    </span>
	                </div>
	            </div>
			</div>
			<div class = "input_row">
			</div> -->
			<div id="dropin-container" class='input_row'></div>
		</div>
		<input id="shipping" value="0" type="hidden" />
		<input id="tax" value="0" type="hidden" />
		<input id="nonce" name="nonce" type="hidden" />
		<input type = "submit" class = "checkout_submit_btn" value = "Confirm Purchase">
	</div>
</div>
</div>
</form>

<script src="<?=FRONT_JS?>plugins/jquery.number.min.js"></script>
<script src="https://js.braintreegateway.com/web/dropin/1.7.0/js/dropin.min.js"></script>
<script type="text/javascript">
	var button = $('.checkout_submit_btn');

	$(document).on('ready',function(){
		braintree.dropin.create({
			authorization: '<?=$model->token?>',
			container: '#dropin-container',
			// paypal: {
			//     flow: 'checkout',
			//     amount: '50',
			//     currency: 'USD'
			// }
		}, function (createErr, instance) {
			button.click(function (e) {
			    e.preventDefault();
				instance.requestPaymentMethod(function (err, payload) {
					// Submit payload.nonce to your server
					$("#nonce").val(payload.nonce);
					if(err == null){
						$('#checkout-form').submit();
					}
				});
			});
		});
	});

</script>
<script>
	$(function(){
		Number.prototype.formatMoney = function(c, d, t){
			var n = this,
				c = isNaN(c = Math.abs(c)) ? 2 : c,
				d = d == undefined ? "." : d,
				t = t == undefined ? "," : t,
				s = n < 0 ? "-" : "",
				i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
				j = (j = i.length) > 3 ? j % 3 : 0;
			return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
		};

		$('.apply-coupon').click(function(e){
			e.preventDefault();
			var code = $(this).parent().find('[name="code"]').val();
			$.getJSON('<?=SITE_URL?>checkout/applyCoupon?code='+code, function(data){
				console.log(data);
				var total = parseFloat($('#subtotal-value').text().replace(/,/g,''));
				if (data.error.code == 0){
//					$('.apply-coupon').parent().parent().next().text('');
					var savings = data.coupon.discount_amount;
					var shipping_and_tax = parseFloat($('#shipping-value').text().replace(/,/g,'')) + parseFloat($('#tax-value').text().replace(/,/g,''));
					if (data.coupon.discount_type == 1){ //$
//						$('#grand-total-value').text(parseFloat(total - savings + shipping_and_tax).toFixed(2));
//						$('#savings-value').text('$'+parseFloat(savings).toFixed(2));
					} else if (data.coupon.discount_type == 2){ //%
//						$('#grand-total-value').text($.number(total * (1-savings/100) + shipping_and_tax, 2));
//						$('#savings-value').text('$' + $.number(total * savings/100, 2));
					}
					window.location.reload();
				} else {
//					$('.apply-coupon').parent().parent().next().text(data.error.message);
//					$('#grand-total-value').text($.number(parseFloat(total)+parseFloat($('#shipping-value').text().replace(/,/g,'')), 2));
//					$('#savings-value').text('$0.00');
				}
			});
			return false;
		})
		if ($('.apply-coupon').parent().prev().val() != ''){
			$('.apply-coupon').trigger('click');
		}
		$(".checkbox").click(function() {
			$('.separate_billing').toggle();
		});

		window.onload = function() {
			if (localStorage.getItem('checked') == '1') {
				var total = <?echo $model->total?>;
				total *= 0.98;
//				$("#total-value").html("$"+Number(total).toLocaleString('en',{minimumFractionDigits:2}));
				$('.wireTransfer').prop('checked', true);
				$('.paymentDisplay').toggle();
			} else {
				$('.wireTransfer').prop('checked', false);
			}
		};

		$("[name='ship_state']").on('change', function(){
			var state = $(this).val();
			var shipping = $('#shipping').val();
			var totalText = "<?=number_format($model->total, 2)?>";
			totalText = totalText.replace('$', '').replace(',', '');
			var total = parseFloat(totalText);
			if(state == 'NY'){
				var taxFee = total * 0.08875;
				$('#tax').val(taxFee);
				$('#tax-fee').html('$' + taxFee.formatMoney(2));
				var newTotal = parseFloat(taxFee.formatMoney(2)) + total +  parseFloat(shipping);
				$('#total-value').html('$' + newTotal.formatMoney(2));
			} else if(state != '') {
				$('#tax-fee').html('--');
				$('#tax').val(0);
				var newTotal = total + parseFloat(shipping);
//				var my = parseFloat(total);
				$('#total-value').html('$' + newTotal.formatMoney(2));
			}
		});
		$("[name='ship_state']").trigger('change');
		if($(".checkbox").data('default')== 'on'){
			$(".checkbox").trigger('click');
		}
		$("[name='shipping_method']").on('change', function(){
			var fee = $(this).val();
			var totalText = "<?=number_format($model->total, 2)?>";
			totalText = totalText.replace('$', '').replace(',', '');
			var total = parseFloat(totalText);
			var tax = $('#tax').val();
			var myTax =  parseFloat(tax);
			var myTotal = myTax + total;
			var total = parseFloat(myTotal);
			if(fee == 5){
				$('#shipping').val(30);
				var newTotal = 30 + total;
				$('#total-value').html('$' + newTotal.formatMoney(2));
			}else{
				$('#shipping').val(0);
				$('#total-value').html('$' + total.formatMoney(2));
			}

		})
	})
</script>
