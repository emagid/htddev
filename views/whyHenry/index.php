<? $banner= \Model\Banner::getItem(null,['where'=>"page = 'whyhenry'"]); 
?>

<div class=" top_clear aboutus_content_wrapper">
	<!-- <div class="inner_hero home_hero" style="background-image:url('/content/frontend/assets/img/banner_left.jpg')">
        <div class="home_hero_overlay">
            <h2>why <span>HENRY</span></h2>
        </div>
	</div> -->

    <div class="inner_hero home_hero" style="background-image:url('<?= $banner->image ? UPLOAD_URL.'banners/'.$banner->image : '' ?>')">
        <div class="home_hero_overlay">
            <!-- <h2>why <span>HENRY</span></h2> -->
            <h2><?=$banner->title;?></h2>
        </div>
    </div>
    

    
        <div class="blue_statement">
                <div class="leftLines"></div>
    <div class="rightLines"></div>
            <div class="wrapper">
                <!-- <h4>a better option for <br>companies and for patients</h4>
                <div class="text">
                <p>In 2017 the CDC issues a report that stated 36% of Americans with dental insurance have not gone to the dentist in over 1 year.</p>
                <p>Every dollar spent on preventative dental care, saves patients $50 in late stage dental needs. Not to mention, we help keep your employees happy, and healthy.</p>
                </div> -->
                <div class="text">
                    <?=$banner->main_description;?>
                </div>
                
                <img src="/content/frontend/assets/img/Henry_Crown_Orange.png">
            </div>
            
    </div>
    
        <div class="carolina_statement">
        <h3 class="smaller_h3 white_font">why companies choose HENRY</h3>
        
        <div class="features">
            <div class="dental_features">
                <div class="stamp_year">
                    <div class="icon" style="background-image:url('/content/frontend/assets/img/perks.png');"></div>
                    <h6>Perks</h6>
                    <p>Health & Wellness Perk for Employees</p>
                </div>
            </div>
            <div class="dental_features">
                <div class="stamp_year">
                    <div class="icon" style="background-image:url('/content/frontend/assets/img/cost.png')"></div>
                    <h6>Cost</h6>
                    <p>$0 Incremental Cost to Company or Employee</p>
                </div>
            </div>
            <div class="dental_features">
                <div class="stamp_year">
                    <div class="icon" style="background-image:url('/content/frontend/assets/img/coverage1.png')"></div>
                    <h6>Coverage</h6>
                    <p>In-Network with All Major Insurance Providers</p>
                </div>
            </div>
            <div class="dental_features">
                <div class="stamp_year">
                    <div class="icon" style="background-image:url('/content/frontend/assets/img/time.png')"></div>
                    <h6>Time</h6>
                    <p>Saves Employee Time</p>
                </div>
            </div>
        </div>
    </div>
    <div class="full_width_bg" style="background-image:url('/content/frontend/assets/img/banner_left.jpg')"></div>
    
    <div class="feedback">
        <div class="wrapper">
            <h4 class="blue_font">why employees choose HENRY</h4>
            
            <div class="employee_feedback">
                <!-- <div class="feedback_left">
                    
                </div> -->


                <div class="feedback_left">
                    <h3 class="blue_font">&#65282;</h3>
                    <p class="blue_font">I had an AMAZING experience with HENRY the Dentist and would love to share! I don’t actually enjoy going to the dentist, especially if I have to take off from work or wake up early on a Saturday to go. HENRY the Dentist saves the day! They literally drive to your office while you’re working. I easily scheduled my appointment through a link sent to my email from HR; Time slots were available all day so it will work whether you have a fixed lunch or flexible schedule.</p>
                    <h6 class="blue_font"><strong>Marcell S.</strong></h6>
                </div>
                <div class="feedback_right">
                    <h3 class="white_font">&#65282;</h3>
                    <p class="white_font">Best. Dentist visit. Ever. HENRY came to our office, and the convenience couldn't be beat -- so I thought "how bad can it be?" Fast forward to my reclining in a chair with a built-in massager, watching Netflix using cordless noise-canceling headphones, while getting my teeth cleaned by the greatest dental hygienist in history. I was absolutely blown away by the kindness, attentiveness and professionalism of the staff. The facility/truck was spotless. All of the equipment was new and state of the art. Online scheduling that actually works. A total pleasure from start to finish.</p>
                    <h6 class="white_font"><strong>Reid A.</strong></h6>
                </div>
            </div>
            
            <div class="pdf" style="text-align:center;margin-top:50px;">
            <h4 class="blue_font">discover how employers are using onsite clinics</h4>
            <p style="margin:20px auto";>2018 Mercer study of US onsite medical clinics (click image below to view)</p>
            <a href="/content/frontend/assets/img/us-2018-worksite-medical-clinics-survey-report.pdf" target="_blank">
                <img src="/content/frontend/assets/img/article.png">
            </a>
            </div>
        </div>
    </div>
    
</div>

<script>
    $('.gallery_slider').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
        centerMode: true,
  centerPadding: '60px',
        adaptiveHeight: true,
        autoplay: true,
  autoplaySpeed: 3500
    });
	
</script>