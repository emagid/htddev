<div class="contactPageWrapper">

	<div class="main_contact_content_wrapper">
		<h1 class="content_main_header_text"><icon class="geo_diamond"></icon>How can we help you?</h1>
		<div class="row contacT_content_width">
			<div class="col contactFormWrapper">
				<div class="green_emph_box">
					<h4 class="gray_white_gradient genath">Contact Us</h4>
					<form method="post" action="/contact">
						<label for="full_name">Full Name<span>*</span></label>
						<input type="text" id="full_name" name="name">
						<label for="email">Email Address<span>*</span></label>
						<input type="email" id="email" name="email">
						<label for="phone">Phone Number</label>
						<input type="tel" id="phone" name="phone">
						<label for="message">Your Message<span>*</span></label>
						<textarea id="message" name="message"></textarea>
						<input type="submit" value="Submit" class="primaryBtn">
					</form>
				</div>
			</div>
			<div class='col copybranding'>
				<div>
					<h4>Send us a Message for any of the following, and more</h4>
					<p class="diamond_bullet">Custom jewelry requests.
						<span>Our team can custom design and handcraft any lab-grown jewelry that you and your loved one's wish</span>
					</p>
					<p class="diamond_bullet">Questions about any of our products</p>
					<p class="diamond_bullet">Press inquiries</p>
				</div>
				<div class="contact_options">
					<h4>Please feel free to reach us by...</h4>
					<div class="row row_of_2">
						<div class="col contact_option_phone">
							<h3>Phone</h3>
							<icon>
								<span>1.800.853.5590</span>
							</icon>
						</div>
						<div class="col contact_option_chat">
							<a href="javascript:void(0);" onclick="olark('api.box.expand')">
							<h3>Chat</h3>
							<icon>
							</icon>
							</a>
						</div>
						<div class="col contact_option_email">
							<a href="mailto:info@diamondlabgrown.com">
							<h3>Email</h3>
							<icon>
							</icon>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="map_slideout_panel">
			<div class="info">
				<p>Visit Our Flagship Store</p>
				<h2>Diamond Lab Grown</h2>
				<h2>301 W 38th St, New York, NY 10018</h2>
				<span>* Please contact us to find out which stores carry our products near you</span>
			</div>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3022.3321410052245!2d-73.99385028459373!3d40.75471897932731!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c259ad3e261cc1%3A0x24e65296503f8c78!2s301+W+38th+St%2C+New+York%2C+NY+10018!5e0!3m2!1sen!2sus!4v1470349161322" width="800" height="600" frameborder="0" style="border:0;width:100%!important;height:480px!important;" allowfullscreen></iframe>
		</div>		
	</div>	
</div>