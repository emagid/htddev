<div class="top_clear aboutus_content_wrapper">
	<!-- <div class="home_hero inner_hero" style="background-image:url('/content/frontend/assets/img/banner_left.jpg')">
        <div class="home_hero_overlay">
            <h2>who we are</h2>
        </div>
	</div> -->

    <div class="home_hero inner_hero" style="background-image:url('<?= $model->banner->image ? UPLOAD_URL.'banners/'.$model->banner->image : '' ?>')">
        <div class="home_hero_overlay">
            <!-- <h2>who we are</h2> -->
            <h2><?=$model->banner->title;?></h2>
        </div>
    </div>
    
    <div class="story_section">
        <div class="wrapper">
            <div class="story_graphic">
                <h1 class="blue_font">our story</h1>
                <img src="/content/frontend/assets/img/henry_staff.jpg">
            </div>
            <div class="story_text">


                <?=$model->banner->main_description;?>
                
            </div>
        </div>
    </div>
    
    <div class="timeline_section">
        <h3>timeline</h3>
        
        <div class="time_stamps">
            <div class="stamp">
                <div class="stamp_year" >
                    <h6>2016</h6>
                    <p>First napkin vision of HENRY was created</p>
                </div>
            </div>
            <div class="stamp">
                <div class="stamp_year">
                    <h6>2017</h6>
                    <p>HENRY I started to see our first patients</p>
                </div>
            </div>
            <div class="stamp">
                <div class="stamp_year">
                    <h6>2018</h6>
                    <p>Launch HENRY II, III, and Specialty Practice</p>
                </div>
            </div>
            <div class="stamp">
                <div class="stamp_year">
                    <h6>2019</h6>
                    <p>Expansion to Pennsylvania, Massachusetts, Connecticut, New York, and Ohio</p>
                </div>
            </div>
        </div>
    </div>
    
    <div class="team_section">
        <div class="wrapper">
        
            <h3 class="blue_font">meet the leadership team</h3>

            <div class="team_members">
                <? foreach($model->teams as $team){ ?>
                    <div class="staff">
                        <div class="headshot" style="background-image: url(<?= $team->image ? UPLOAD_URL.'teams/'.$team->image : '' ?>"></div>
                        <h6><?=$team->name?> <br><span><?=$team->position?></span></h6>
                        <div class="bio_hidden">
                            <div class="close">
                                <img src="/content/frontend/assets/img/cancel.png">
                            </div>
                            <img src="/content/frontend/assets/img/Henry_Crown_Orange.png">
                            <p><?=$team->description?></p>
                        </div>
                    </div>
                <? } ?>
                <!-- <div class="staff">
                    <div class="headshot"></div>
                    <h6>Justin Joffe <br><span>Founder, CEO</span></h6>
                </div>
                <div class="staff">
                    <div class="headshot"></div>
                    <h6>Jeffrey Rappaport <br><span>Dental Director</span></h6>
                </div>
                <div class="staff">
                    <div class="headshot"></div>
                    <h6>Michael Preininger <br><span>Director of Business Development</span></h6>
                </div>
                
                <div class="staff">
                    <div class="headshot"></div>
                    <h6>Alexandria Ketcheson <br><span>Director of Brand and Marketing</span></h6>
                </div>
                <div class="staff">
                    <div class="headshot"></div>
                    <h6>Jeffrey Rappaport <br><span>Dental Director</span></h6>
                </div>
                <div class="staff">
                    <div class="headshot"></div>
                    <h6>Michael Preininger <br><span>Director of Business Development</span></h6>
                </div> -->
            </div>
        </div>
    </div>

    <script>
    $(".staff").click(function(){
        $(this).toggleClass('active');
        $(".staff").toggle();
        $(this).show();
        
        $(".bio_hidden", this ).toggle();
        
        $('html,body').animate({
        scrollTop: $(".team_section").offset().top},
        'slow');
    });
    </script>
    
    
    

</div>