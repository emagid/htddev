<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->faq->id?>"/>
	<div class="row">
		<div class="col-md-16">
			<div class="box">
				<h4>General</h4>
				<div class="form-group">
					<label>Question</label>
					<?php echo $model->form->textAreaFor("question",["rows"=>"3"]); ?>
				</div>

				<div class="form-group">
					<label>Answer</label>
					<?php echo $model->form->textAreaFor("answer", ["class"=>"ckeditor"]);?>
				</div>

				<div class="form-group">
	                <label>Display Order</label>
	                <select name="display_order">
	                    <?foreach($model->displays as $display_order=>$text){?>
	                        <option value="<?=$display_order?>" <?=$display_order == $model->faq->display_order ? 'selected': ''?>><?=$text?></option>
	                    <?}?>
	                </select>
	            </div>
			</div>
		</div>

		<div class="col-lg-24">
			<button type="submit" class="btn btn-success btn-lg">Save</button>
		</div>
	</div>
</div>
</form>


<?= footer(); ?>


<script type='text/javascript'>
	var site_url = <?php echo json_encode(ADMIN_URL.'quotes/');?>;
</script>