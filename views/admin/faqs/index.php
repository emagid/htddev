<?php
 if(count($model->faqs)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
           <th width="30%">Question</th>
            <th width="30%">Answer</th> 
            <th width="5%">Display Order</th>
          <th width="15%" class="text-center">Edit</th>
          <th width="15%" class="text-center">Delete</th> 
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->faqs as $obj){ ?>
        <tr>
         <td>
         <a href="<?php echo ADMIN_URL; ?>faqs/update/<?php echo $obj->id; ?>">
            <?php echo $obj->question;?>
         </a>
      </td>
      <td>
        <a href="<?php echo ADMIN_URL; ?>faqs/update/<?php echo $obj->id; ?>">
        <?php echo $obj->answer;?> 
        </a>
      </td>
      <td>
        <a href="<?php echo ADMIN_URL; ?>faqs/update/<?php echo $obj->id; ?>">
        <?php echo $obj->display_order;?> 
        </a>
      </td>
         
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>faqs/update/<?= $obj->id ?>">
           <i class="icon-pencil"></i> 
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>faqs/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i> 
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'faqs';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>