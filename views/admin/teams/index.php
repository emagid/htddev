<?php
 if(count($model->teams)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
            <th width="20%">Logo</th>
            <th width="20%">Name</th> 
            <th width="20%">Position</th>
            <th width="20%">Description</th>
            <th width="15%" class="text-center">Edit</th>
            <th width="15%" class="text-center">Delete</th> 
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->teams as $obj){ ?>
        <tr>
         <td>
         <a href="<?php echo ADMIN_URL; ?>teams/update/<?php echo $obj->id; ?>">
            <img src="<?php echo UPLOAD_URL.'teams/'.$obj->image;?>" height="60px"/>
         </a>
      </td>
      <td>
        <a href="<?php echo ADMIN_URL; ?>teams/update/<?php echo $obj->id; ?>">
        <?php echo $obj->name;?> 
        </a>
      </td>
      <td>
        <a href="<?php echo ADMIN_URL; ?>teams/update/<?php echo $obj->id; ?>">
            <?=$obj->position;?>
        </a>
      </td>

      <td>
        <a href="<?php echo ADMIN_URL; ?>teams/update/<?php echo $obj->id; ?>">
            <?=$obj->description;?>
        </a>
      </td>
         
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>teams/update/<?= $obj->id ?>">
           <i class="icon-pencil"></i> 
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>teams/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i> 
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'teams';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>