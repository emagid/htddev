<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->team->id?>"/>
	<div class="row">
		<div class="col-md-16">
			<div class="box">
				<h4>General</h4>
				<div class="form-group">
					<label>Name</label>
					<?php echo $model->form->textBoxFor("name"); ?>
				</div>

				<div class="form-group">
					<label>Position</label>
					<?php echo $model->form->textBoxFor("position"); ?>
				</div>

				<div class="form-group">
					<label>Description</label>
					<?php echo $model->form->textBoxFor("description"); ?>
				</div>

				<div class="form-group">
					<label>Image</label>
					<p><small>(ideal image size is 1920 x 300)</small></p>
					<p><input type="file" name="image" class='image' /></p>
					<?php 
					$img_path = "";
					if($model->team->image != "" && file_exists(UPLOAD_PATH.'teams'. DS . $model->team->image)){
						$img_path = UPLOAD_URL . 'teams/' . $model->team->image;
						?>
						<div class="well well-sm pull-left">
							<img src="<?php echo $img_path; ?>" width="100" />
							<br />
							<a href="<?= ADMIN_URL.'teams/delete_image/'.$model->team->id.'/?image=1';?>" class="btn btn-default btn-xs">Delete</a>
							<input type="hidden" name="image" value="<?= $model->team->image ?>">
						</div>
					<?php } else if($model->team->image != ""){ 
						$img_path = UPLOAD_URL . 'teams/' . $model->team->image; ?>
						<div class="well well-sm pull-left">
							<img src="<?php echo $img_path; ?>" width="100" />
							<br />
							<a href="<?= ADMIN_URL.'teams/delete_image/'.$model->team->id.'/?image=1';?>" class="btn btn-default btn-xs">Delete</a>
						</div>
					<?php } ?>
					<div id='preview-container'></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>

		<div class="col-lg-24">
			<button type="submit" class="btn btn-success btn-lg">Save</button>
		</div>
	</div>
</div>
</form>


<?= footer(); ?>


<script type='text/javascript'>
	var site_url = <?php echo json_encode(ADMIN_URL.'teams/');?>;
	$(document).ready(function() {

		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					var img = $("<img />");
					img.attr('src',e.target.result);
					img.attr('alt','Uploaded Image');
					img.attr("width",'100');
					img.attr('height','100');
					$("#preview-container").html(img);
				};

				reader.readAsDataURL(input.files[0]);
			}
		}

		$("input.image").change(function(){
			readURL(this);
		});	
	});

</script>