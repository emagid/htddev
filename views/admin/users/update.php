<?php
$states = get_states();
unset($states['GU']);unset($states['PR']);unset($states['VI']);
?>
<div role="tabpanel">
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#account-info-tab" aria-controls="general" role="tab" data-toggle="tab">Account Information</a></li>
		<?php if($model->user->id>0) { ?>
			<li role="presentation"><a href="#addresses-tab" aria-controls="seo" role="tab" data-toggle="tab">Delivery Addresses</a></li>
			<li role="presentation"><a href="#payment-info-tab" aria-controls="categories" role="tab" data-toggle="tab">Payment</a></li>
			<? if (isset($model->orders) and count($model->orders) > 0) { ?>
				<li role="presentation"><a href="#orders-tab" aria-controls="collections" role="tab" data-toggle="tab">Orders</a></li>
			<? } ?>

			<? if ($model->user->wholesale_id != 0 && $model->user->wholesale_id != "") { ?>
				<li role="presentation"><a href="#wholesale-tab" aria-controls="collections" role="tab" data-toggle="tab">Wholesale Details</a></li>
			<? } ?>

			<li role="presentation"><a href="#items-tab" aria-controls="collections" role="tab" data-toggle="tab">Items</a></li>

		<?php } ?>
	</ul>
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="account-info-tab">
			<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
				<input type="hidden" name="id" value="<?php echo $model->user->id;?>" />
				<input type=hidden name="token" value="<?php echo get_token(); ?>" />
				<div class="row">
					<div class="col-md-12">
						<div class="box">
							<h4>Account Information</h4>
							<div class="form-group">
								<label>Email Address:</label>
                                <?php echo $model->form->editorFor("email");?>
                                <input type="hidden" name="wholesale_id" value="<?=$model->user->wholesale_id?>">
							</div>
							<div class="form-group">
								<label>Username:</label>
                                <?php echo $model->form->editorFor("username");?>
							</div>
							<div class="form-group">
								<label>First Name</label>
                                <?php echo $model->form->editorFor("first_name");?>
							</div>
							<div class="form-group">
								<label>Last Name</label>
                                <?php echo $model->form->editorFor("last_name");?>
							</div>
                            <div class="form-group">
                                <label>Company</label>
                                <?php echo $model->user->company;?>
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <?php echo $model->user->phone;?>
                            </div>
                            <div class="form-group">
                                <label>Full Address</label>
                                <?php echo $model->user->full_address;?>
                            </div>
							<div class="form-group">
								<label>Password (leave blank to keep current password)</label>
								<input type="password" name="password" />
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-save">Save</button>
							</div>
						</div>
					</div>
				</div>

			</form>
		</div>
		<?php if($model->user->id>0) { ?>
			<div role="tabpanel" class="tab-pane" id="addresses-tab">
				<div class="row">
					<div class="col-md-24">
						<div class="box">
							<a href="<?php echo ADMIN_URL.'users/address/-1?user_id='.$model->user->id;?>" class="btn btn-primary btn-add">Add New Address</a>
						</div>
					</div>
				</div>
				<div class="row">
					<?php foreach($model->addresses as $addr) { ?>
						<div class="col-md-6">
							<div class="box">
								<div class="form-group">
									<label>Label: </label>
									<?php echo $addr->label;?>
								</div>
								<div class="form-group">
									<label>First Name: </label>
									<?php echo $addr->first_name;?>
								</div>

								<div class="form-group">
									<label>Last Name: </label>
									<?php echo $addr->last_name;?>
								</div>
								<?php if ($addr->phone!="") { ?>
									<div class="form-group">
										<label>Phone Number: </label>
										<?php echo $addr->phone;?>
									</div>
								<?php } ?>
								<div class="form-group">
									<label>Address: </label>
									<?php echo $addr->address;?>
								</div>

								<div class="form-group">
									<label>Address 2: </label>
									<?php echo $addr->address2;?>
								</div>

								<div class="form-group">
									<label>City: </label>
									<?php echo $addr->city;?>
								</div>

								<div class="form-group">
									<label>State: </label>

									<?php echo $states[$addr->state];?>
								</div>

								<div class="form-group">
									<label>Zip Code: </label>
									<?php echo $addr->zip;?>
								</div>

								<div class="form-group">
									<label>Country: </label>
									<?php echo $addr->country;?>
								</div>

								<div class="form-group">
									<a class="btn-actions" href="<?php echo ADMIN_URL; ?>users/address/<?php echo $addr->id; ?>?user_id=<?php echo $model->user->id;?>">
										<i class="icon-pencil"></i>
									</a>
									<a class="btn-actions" href="<?php echo ADMIN_URL; ?>users/address_delete/<?php echo $addr->id; ?>?token_id=<?php echo get_token();?>?user_id=<?php echo $model->user->id;?>" onClick="return confirm('Are You Sure?');">
										<i class="icon-cancel-circled"></i>
									</a>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="payment-info-tab">
				<div class="row">
					<div class="col-md-24">
						<div class="box">
							<a href="<?php echo ADMIN_URL.'users/payment/-1?user_id='.$model->user->id;?>" class="btn btn-primary btn-add">Add Payment Method</a>
						</div>
					</div>
				</div>
				<div class="row">
					<?php foreach($model->payment_profiles as $payment_profile) { ?>
						<div class="col-md-6">
							<div class="box">
								<div class="form-group">
									<label>Label: </label>
									<?php echo $payment_profile->label;?>
								</div>
								<div class="form-group">
									<label>First Name: </label>
									<?php echo $payment_profile->first_name;?>
								</div>
								<div class="form-group">
									<label>Last Name: </label>
									<?php echo $payment_profile->last_name;?>
								</div>
								<?php if ($payment_profile->phone!="") { ?>
									<div class="form-group">
										<label>Phone Number: </label>
										<?php echo $payment_profile->phone;?>
									</div>
								<?php } ?>
								<div class="form-group">
									<label>Address: </label>
									<?php echo $payment_profile->address;?>
								</div>
								<div class="form-group">
									<label>Address 2: </label>
									<?php echo $payment_profile->address2;?>
								</div>
								<div class="form-group">
									<label>City: </label>
									<?php echo $payment_profile->city;?>
								</div>
								<div class="form-group">
									<label>State: </label>
									<?php echo $states[$payment_profile->state];?>
								</div>
								<div class="form-group">
									<label>Zip Code: </label>
									<?php echo $payment_profile->zip;?>
								</div>
								<div class="form-group">
									<label>Country: </label>
									<?php echo $payment_profile->country;?>
								</div>

								<div class="form-group">
									<label>Credit Card Number: </label>
									<?php echo $payment_profile->cc_number;?>
								</div>
								<div class="form-group">

								</div>

								<div class="form-group">
									<a class="btn-actions" href="<?php echo ADMIN_URL; ?>users/payment/<?php echo $payment_profile->id; ?>?user_id=<?php echo $model->user->id;?>">
										<i class="icon-pencil"></i>
									</a>
									<a class="btn-actions" href="<?php echo ADMIN_URL; ?>users/payment_delete/<?php echo $payment_profile->id; ?>?token_id=<?php echo get_token();?>?user_id=<?php echo $model->user->id;?>" onClick="return confirm('Are You Sure?');">
										<i class="icon-cancel-circled"></i>
									</a>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="wholesale-tab">
				<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
					<input type="hidden" name="id" value="<?php echo $model->user->id;?>" />
					<input type="hidden" name="wholesale_id" value="<?php echo $model->wholesale->id;?>" />
					<input type="hidden" name="type" value="wholesale" />
					<input type=hidden name="token" value="<?php echo get_token(); ?>" />
					<div class="row">
						<div class="col-md-12">
							<div class="box">
								<h4>Wholesale</h4>
								<div class="form-group">
									<label>Company</label>
									<input type="text" name="company" value="<?=$model->wholesale->company?>">
								</div>
								<div class="form-group">
									<label>Address</label>
									<input type="text" name="address" value="<?=$model->wholesale->address?>">
								</div>
								<div class="form-group">
									<label>Phone</label>
									<input type="text" name="phone" value="<?=$model->wholesale->phone?>">
								</div>
								<div class="form-group">
									<label>Status</label>
									<?$status = ['0'=>"Pending Approval", '1'=>"Approve", '2'=>"Refuse"];?>
									<select name="status">
										<?foreach($status as $key=>$stat){?>
											<option value="<?=$key?>" <?=$key == $model->wholesale->status?'selected':''?>><?=$stat?></option>
										<?}?>
									</select>
								</div>
								<div class="form-group">
									<label>Activation link</label>
									<?=SITE_DOMAIN."/account/activate/{$model->user->hash}"?>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-save">Save</button>
								</div>
							</div>
						</div>
					</div>

				</form>
			</div>
			<? if (isset($model->orders) and count($model->orders) > 0) { ?>
				<div role="tabpanel" class="tab-pane" id="orders-tab">
					<div class="box">
						<table class="table">
							<thead>
							<tr>
								<th>Order #</th>
								<th>Tracking #</th>
								<th>Status</th>
								<th>Bill Name</th>
								<th>Total</th>
								<th>View</th>
							</tr>
							</thead>
							<tbody>
							<?php foreach($model->orders as $obj) { ?>
								<tr>
									<td>
										<a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
											<?php echo $obj->id;?>
										</a>
									</td>
									<td>
										<a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
											<?php echo $obj->tracking_number;?>
										</a>
									</td>
									<td>
										<a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
											<?php echo $obj->status;?>
										</a>
									</td>
									<td>
										<a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
											<?php echo $obj->bill_first_name.' '. $obj->bill_last_name;?>
										</a>
									</td>
									<td>
										<a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
											$<?php echo number_format($obj->total, 2);?>
										</a>
									</td>
									<td>
										<a class="btn-actions" href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
											<i class="icon-eye"></i>
										</a>
									</td>
								</tr>
							<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			<? } ?>
			<div role="tabpanel" class="tab-pane" id="items-tab">
				<? $wholesaleProductIDs = [];
				$wholesalePartIDs = [];
				print_r($model->wholesale_ids);?>
				<div class="row">
					<div class="col-md-12">
						<div class="box">
							<h4>Product Selector</h4>
							<div class="row">
								<div class="col-sm-24">
									<div class="form-group">
										<? //print_r($model->productGrid);?>
										<select class="product_list form-control multiselect" multiple>
											<? foreach ($model->productGrid as $type => $prod) { ?>
												<optgroup label="<?= $type ?>">
													<? foreach ($prod as $item) {
														$wsKey = preg_match("/[\[,]".$item->id."[,\]]/",$model->wholesale_ids);
														if ($item->part != '1') {
															if($wsKey) $wholesaleProductIDs[] = $item->id;
															?>

															<option value="<?= $item->id ?>" data-price="<?=$item->price?>"><?= str_replace(["\r","\n"],'',strReplace($item->name, 100, '...')) ?></option>
														<? } else {
															if($wsKey) $wholesalePartIDs[] = $item->id;
														}
													} ?>
												</optgroup>
											<? } ?>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="box">
							<h4>Category Selector</h4>
							<div class="row">
								<div class="col-sm-24">
									<div class="form-group">
										<select class="category_list form-control multiselect" multiple>
											<? foreach ($model->categoryGrid as $cat) { ?>
												<option value="<?= $cat->id ?>" data-product_ids="<?=$cat->product_id?>"><?= str_replace(["\r","\n"],'',$cat->name)?></option>
											<? } ?>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="box">
							<h4>
								Items List
							</h4>
							<input id="uni_discount" type="number" max=100 min=0 placeholder="%discount">
							<input type="button" value="Apply Discount" onclick="applyDiscount()">
							<div class="row">
								<div class="form-group">
									<div class="col-sm-8">
										<label>Product Name</label>
									</div>
									<div class="col-sm-8">
										<label>MSRP</label>
									</div>
									<div class="col-sm-8">
										<label>Price</label>
									</div>
								</div>
							</div>
							<div class="wholesale_item_set">
								<? foreach ($model->wholesale_items as $item) {
									if ($product = \Model\Product::getItem($item->product_id)) {?>
										<div class="row">
											<div class="form-group">
												<input hidden name="product_id" value="<?= $item->product_id ?>">
												<div class="col-sm-8">
													<label></label>
													<div><?= $product->name ?></div>
												</div>
												<div class="col-sm-8">
													<label></label>
													<div><?= $product->price ?></div>
												</div>
												<div class="col-sm-8">
													<label></label>
													<input name="price" data-base="<?=$product->price?>" value="<?= $item->price ?>" class="form-control">
												</div>
											</div>
										</div>
									<? }
								}?>
							</div>
							<button type="button" class="btn save_items">Save Items</button>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
</div>
<?php footer();?>

<script type="text/javascript">
	var site_url = "<?php echo ADMIN_URL;?>";
	var user_id = <?php echo $model->user->id;?>;
	jQuery.validator.addMethod("validate_user_email",function(value,element) {
		var return_val = false;

		var data = {email:value,id: user_id};
		$.ajax({
			url: site_url+"users/validate_user_email",
			async: false,
			data: data,
			method: "GET",
			success: function(response) {
				if(response.success) {
					return_val = true;
				}
			}

		});
		return return_val;
	},"Email is invalid or already being used.");


	$("#user-form").validate({
		onkeyup: false,
		onfocusout: false,
		onclick: false,
		focusInvalid :false,
		rules: {
			email: {required:true},
			first_name: {required:true},
			last_name: {required:true},
		},
	});


	$('.coupon_send').change(function() {
		var product_id = $(this).attr("product");
		var user_id = $(this).attr("user");
		var coupon_id = $(this).val();



		$.ajax({
			url: '/admin/users/send_coupon/',
			method: 'POST',
			data: {
				product_id: product_id, user_id:user_id, coupon_id: coupon_id
			},
			success: function(data) {
				alert(data);

				/* window.location.replace("/checkout/");*/
			}
		});
	});

	$(".phone-us").inputmask("(999)999-9999");
</script>
<script type="text/javascript">
	$(document).ready(function () {
		$("input[name='name']").on('keyup', function (e) {
			var val = $(this).val();
			val = val.replace(/[^\w-]/g, '-');
			val = val.replace(/[-]+/g, '-');
			$("input[name='slug']").val(val.toLowerCase());
		});
		$('.product_list').multiselect({
			maxHeight: 415,
			checkboxName: '',
			includeSelectAllOption: true,
			enableCaseInsensitiveFiltering: true,
			buttonWidth: '100%',
			onSelectAll: function () {//Does not fire, outdated version of multiselect

			},
			onChange: function (option, checked) {
				if(option !== undefined) {//workaround for outdated multiselect
					var wholesaleItem = $('.wholesale_item_set').find('[name=product_id][value=' + option.val() + ']');
					if (wholesaleItem.length) {
						if (checked) {
							wholesaleItem.parent().show();
							wholesaleItem.prop('disabled', false);
						} else {
							wholesaleItem.parent().hide();
							wholesaleItem.prop('disabled', true);
						}
					} else {
						addWholesaleItem(option);
					}
					/** Item List Html */
				} else { //SelectAll option
					console.log('findme');
					var products = $('.product_list').parent().find('li').not('.multiselect-item,.multiselect-item-all,.group').find('input');
					var i = 7;
					products.each(function(){
						var active  = $(this).parents('li').hasClass('active');
						var item = $('.product_list').find('option[value='+$(this).val()+']');
						var wholesaleItem = $('.wholesale_item_set').find('[name=product_id][value=' + item.val() + ']');
						if (wholesaleItem.length) {
							if (checked) {
								wholesaleItem.parent().show();
								wholesaleItem.prop('disabled', false);
							} else {
								wholesaleItem.parent().hide();
								wholesaleItem.prop('disabled', true);
							}
						} else {
							if (checked){
								addWholesaleItem(item);
							}
						}
					});
				}
			}
		});
		$('.part_list').multiselect({
			maxHeight: 415,
			checkboxName: '',
			includeSelectAllOption: true,
			enableCaseInsensitiveFiltering: true,
			buttonWidth: '100%',
			onSelectAll: function () {//Does not fire, outdated version of multiselect

			},
			onChange: function (option, checked) {
				if(option !== undefined) {//workaround for outdated multiselect
					var wholesaleItem = $('.wholesale_item_set').find('[name=product_id][value=' + option.val() + ']');
					if (wholesaleItem.length) {
						if (checked) {
							wholesaleItem.parent().show();
							wholesaleItem.prop('disabled', false);
						} else {
							wholesaleItem.parent().hide();
							wholesaleItem.prop('disabled', true);
						}
					} else {
						addWholesaleItem(option);
					}
					/** Item List Html */
				} else { //SelectAll option
					console.log('findme');
					var parts = $('.part_list').parent().find('li').not('.multiselect-item,.multiselect-item-all,.group').find('input');
					var i = 7;
					parts.each(function(){
						var active  = $(this).parents('li').hasClass('active');
						var item = $('.part_list').find('option[value='+$(this).val()+']');
						var wholesaleItem = $('.wholesale_item_set').find('[name=product_id][value=' + item.val() + ']');
						if (wholesaleItem.length) {
							if (checked) {
								wholesaleItem.parent().show();
								wholesaleItem.prop('disabled', false);
							} else {
								wholesaleItem.parent().hide();
								wholesaleItem.prop('disabled', true);
							}
						} else {
							if (checked){
								addWholesaleItem(item);
							}
						}
					});
				}
			}
		});
		$('.category_list').multiselect({
			maxHeight: 415,
			checkboxName: '',
			enableCaseInsensitiveFiltering: true,
			buttonWidth: '100%',
			onChange: function (option, checked) {
				$.each(option.data('product_ids').split(','),function(i,e){
					var productSelect = $('.product_list,.part_list').parent().find('input[value='+e+']');
					if((checked && !productSelect.parents('li').hasClass('active')) || (!checked && productSelect.parents('li').hasClass('active'))){
						productSelect.trigger('click');
					}
				});
				/** Item List Html */
			}
		});
		$("select.multiselect").each(function (i, e) {
//            $(e).val('');
			var placeholder = $(e).data('placeholder');
			$(e).multiselect({
				nonSelectedText: placeholder,
				includeSelectAllOption: true,
				maxHeight: 415,
				checkboxName: '',
				enableCaseInsensitiveFiltering: true,
				buttonWidth: '100%'
			});
		});
		<?$wholesaleProductIDs = implode(",",$wholesaleProductIDs)?>
		<?$wholesalePartIDs = implode(",",$wholesalePartIDs)?>

		$('.product_list').val(<? echo "[".$wholesaleProductIDs."]" ?>);
		$('.part_list').val(<? echo "[".$wholesalePartIDs."]" ?>);
//        $('.product_list,.category_list').val(<?//=$model->wholesale_ids?>//);
		$('select.multiselect').multiselect('rebuild');

		$('.save_items').on('click',function(){
			var wholesaleItems = $('.wholesale_item_set').find('[name=product_id]:enabled');
			var wholesaleIds = [];
			wholesaleItems.each(function(i,e){
				e = $(e);
				var id = e.val();
				var price = e.siblings().find("[name=price]").val() ? e.siblings().find("[name=price]").val() : e.siblings().find('[name=price]').data('base');
				var obj = {id: id,price: price};
				wholesaleIds.push(obj)
			});
			console.log(wholesaleItems);
//			$.post('/admin/users/save_items',{id:<?//=$model->wholesale->id?>//,items:wholesaleIds},function(data){
//				console.log(data);
//			})
		});
	});
	function applyDiscount(){
		var discount = $("#uni_discount").val();
		discount = discount/100;
		//discount = 1-discount //alternate
		$('.wholesale_item_set input[name="price"]').each(function(price) {
			var fullVal = parseInt($(this).data('base'));
			var discountVal = fullVal * discount;
			discountVal = (discountVal).toFixed(2);
			$(this).val(discountVal);
		});
	}
	function addWholesaleItem (item) {//jquery object of the multiselect checkbox
		var html = '<div class="row">';
		html += '<div class="form-group">';
		html += '<input hidden name="product_id" value="' + item.val() + '">';
		html += '<div class="col-sm-8">';
		html += '<label></label>';
		html += '<div>' + item.text() + '</div>';
		html += '</div>';
		html += '<div class="col-sm-8">';
		html += '<label></label>';
		html += '<div>' + item.data('price') + '</div>';
		html += '</div>';
		html += '<div class="col-sm-8">';
		html += '<label></label>';
		html += '<input name="price" data-base="'+item.data('price')+'" value="" class="form-control">';
		html += '</div>';
		html += '</div>';
		html += '</div>';
		$('.wholesale_item_set').append(html);
	}
</script>