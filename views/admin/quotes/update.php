<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->quote->id?>"/>
	<div class="row">
		<div class="col-md-16">
			<div class="box">
				<h4>General</h4>
				<div class="form-group">
					<label>Quote</label>
					<?php echo $model->form->textAreaFor("quote",["class"=>"ckeditor"]); ?>
				</div>

				<div class="form-group">
					<label>Name of the Peron (Quote By) </label>
					<?php echo $model->form->textBoxFor("by_name"); ?>
					
				</div>
			</div>
		</div>

		<div class="col-lg-24">
			<button type="submit" class="btn btn-success btn-lg">Save</button>
		</div>
	</div>
</div>
</form>


<?= footer(); ?>


<script type='text/javascript'>
	var site_url = <?php echo json_encode(ADMIN_URL.'quotes/');?>;
</script>