<?php
 if(count($model->quotes)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
           <th width="40%">Quote</th>
            <th width="20%">By</th> 
          <th width="15%" class="text-center">Edit</th>
          <th width="15%" class="text-center">Delete</th> 
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->quotes as $obj){ ?>
        <tr>
         <td>
         <a href="<?php echo ADMIN_URL; ?>quotes/update/<?php echo $obj->id; ?>">
            <?php echo $obj->quote;?>
         </a>
      </td>
      <td>
        <a href="<?php echo ADMIN_URL; ?>quotes/update/<?php echo $obj->id; ?>">
        <?php echo $obj->by_name;?> 
        </a>
      </td>
         
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>quotes/update/<?= $obj->id ?>">
           <i class="icon-pencil"></i> 
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>quotes/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i> 
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'media_logos';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>