<div role="tabpanel">
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
	</ul>
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="general-tab">
			<form class="form" action="<?=$this->emagid->uri?>" method="post" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $model->transaction->id;?>" />
				<input type="hidden" name="token" value="<?php echo get_token();?>" />
				<div class="row">
					<div class="col-md-12">
						<div class="box">
							<h4>Transaction Information</h4>
							<div class="form-group">
								<label>First Name</label>
								<?php echo $model->form->textBoxFor("ship_first_name"); ?>
							</div>
							<div class="form-group">
								<label>Last Name</label>
								<?php echo $model->form->textBoxFor("ship_last_name"); ?>
							</div>
							<div class="form-group">
								<label>Email</label>
								<?php echo $model->form->textBoxFor("email"); ?>
							</div>
							<div class="form-group">
								<label>Phone</label>
								<?php echo $model->form->textBoxFor("phone"); ?>
							</div>
							<div class="form-group">
								<label>Amount</label>
								<?php echo $model->form->textBoxFor("amount"); ?>
							</div>
							<div class="form-group">
								<label>Payment Status</label>
								<select name="payment_status" class="form-control">
									<option value="select" selected disabled>Select</option>
	                                <?php foreach (\Model\Transaction::$pay_status as $status) {
	                                    $select = $model->transaction->payment_status == $status?'selected':''
	                                    ?>
	                                    <option value="<?php echo $status; ?>" <?php echo $select; ?> ><?php echo $status; ?></option>
	                                <?php } ?>
	                            </select>
							</div>
							<? if($model->transaction->id) { ?>
								<div class="form-group">
									<label>Type</label>
									<?php echo $model->form->textBoxFor("type",['readonly'=>true]); ?>
								</div>
								
								<div class="form-group">
									<label>Transaction Date</label>
									<input type="text" name="insert_time" value="<?php echo date('Y-m-d h:ia')?>" disabled>
								</div>
							<? } ?>
						</div>
					</div>
					<div class="col-md-12">
						<div class="box">
							<h4>Billing Address</h4>
							<div class="form-group">
								<label>Address 1</label>
								<?php echo $model->form->textBoxFor("ship_address"); ?>
							</div>
							<div class="form-group">
								<label>Address 2</label>
								<?php echo $model->form->textBoxFor("ship_address2"); ?>
							</div>
							<div class="form-group">
								<label>City</label>
								<?php echo $model->form->textBoxFor("ship_city"); ?>
							</div>
							<div class="form-group">
								<label>State</label>
								<?php echo $model->form->textBoxFor("ship_state"); ?>
							</div>
							<div class="form-group">
								<label>Country</label>
								<?php echo $model->form->textBoxFor("ship_country"); ?>
							</div>
							<div class="form-group">
								<label>Zip</label>
								<?php echo $model->form->textBoxFor("ship_zip"); ?>
							</div>
						</div>
					</div>
				</div>
				<button type="submit" class="btn btn-save">Save</button>
			</form>
		</div>
	</div>
</div>