<style>
    .originalProducts[data-status=paid] a:not(.btn-actions) {
        color: green;
    }
    .originalProducts[data-status=payment_failed] a:not(.btn-actions) {
        color: red;
    }
</style>
<div class="row">
    <div class="col-md-8">
        <div class="box">
            <div class="input-group">
                <label>Show on page:</label>
                <form>
                <select id='filter' class="form-control" name="status">
                    <option value="0">Paid and un-paid orders </option>
                    <?
                    foreach (\Model\Transaction::$pay_status as $i => $a) {?>
                        <option value="<?= $i+1 ?>" <?= isset($_GET['status']) && $_GET['status'] == $i+1 ? 'selected' : '' ?>><?= $a ?></option>
                    <? } ?>
                </select>
                </form>
            </div>
        </div>
    </div>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
</div>
<?php if (count($model->transactions) > 0) { ?>
    <div class="box box-table">
        <table id="data-list" class="table">
            <thead>
            <tr>
                <th width="10%">ID</th>
                <th width="20%">Name</th>
                <th width="20%">Email</th>
                <th width="10%">Date</th>
                <th width="30%">Amount</th>
                <th width="30%">Payment Status</th>
                <th width="15%" class="text-center">Edit</th>
                <th width="15%" class="text-center">Delete</th> 
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->transactions as $obj) { ?>
                <tr class="originalProducts" data-status="<?=strtolower(str_replace(' ','_',$obj->payment_status))?>">
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>transactions/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->id; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>transactions/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->full_name(); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>transactions/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->email; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>transactions/update/<?php echo $obj->id; ?>">
                            <?php echo date('M d,Y h:ia',strtotime($obj->get_insert_time())); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>transactions/update/<?php echo $obj->id; ?>">
                            $<?php echo $obj->amount; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>transactions/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->payment_status ?>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?= ADMIN_URL ?>transactions/update/<?= $obj->id ?>">
                        <i class="icon-pencil"></i> 
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?= ADMIN_URL ?>transactions/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i> 
                        </a>
                    </td>

                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } else { ?>
    <p>No Transactions to display</p>
<? } ?>

<?php footer(); ?>

<script>
    var site_url = '<?= ADMIN_URL.'transactions';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script>
    $('#filter').change(function(){
        $(this).parent().submit();
    });
</script>