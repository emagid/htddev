<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  	<input type="hidden" name="id" value="<?php echo $model->home_content->id?>"/>

  	<div role="tabpanel">
  		<ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
            <li role="presentation"><a href="#content-tab" aria-controls="content" role="tab" data-toggle="tab">Content</a></li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
				<div class="row">
					<div class="col-md-16">
						<div class="box">
							<h4>General</h4>
							<div class="form-group">
								<label>Content Title</label>
								<?php echo $model->form->textBoxFor("main_title"); ?>
							</div>
							<div class="form-group">
								<label>Select Section</label>
								<select id="lrsection" name="section">
									<option value="none" selected disabled>Select a section</option>
									<option value="left" <?= $model->home_content->section == 'left' ? 'selected' : '';?>>Section 1- Left</option>
									<option value="right" <?= $model->home_content->section == 'right' ? 'selected' : '';?>>Section 2- Right</option>
									<option value="middle" <?= $model->home_content->section == 'middle' ? 'selected' : '';?>>Section 3- Middle</option>
								</select>
							</div>
							<div id="content-icon1" class="form-group" style='display:none;'>
                                <label>Featured image 1 (Left/Right Top Section)</label>

                                <p>
                                    <small>(ideal featured image size is 1920 x 300)</small>
                                </p>
                                <?php
                                $img_path = "";
                                if ($model->home_content->featured_image1 != "") {
                                    $img_path = UPLOAD_URL . 'home_contents/' . $model->home_content->featured_image1;
                                }
                                ?>
                                <p><input id="image1" type="file" name="featured_image1" class='image'/></p>
                                <?php if ($model->home_content->featured_image1 != "") { ?>
                                    <div class="well well-sm pull-left">
                                        <img src="<?php echo $img_path; ?>" width="100"/>
                                        <br/>
                                        <a href="<?= ADMIN_URL . 'home_contents/delete_image/' . $model->home_content->id . '/?featured_image1=1'; ?>"
                                           class="btn btn-default btn-xs">Delete</a>
                                    </div>
                                <?php } ?>
                                <div id='preview-container1'></div>
                            </div>

                            <div id="content-icon2" class="form-group" style='display:none;'>
                                <label>Featured image 2 (Left/Right Middle Section)</label>

                                <p>
                                    <small>(ideal featured image size is 1920 x 300)</small>
                                </p>
                                <?php
                                $img_path = "";
                                if ($model->home_content->featured_image2 != "") {
                                    $img_path = UPLOAD_URL . 'home_contents/' . $model->home_content->featured_image2;
                                }
                                ?>
                                <p><input id="image2" type="file" name="featured_image2" class='image'/></p>
                                <?php if ($model->home_content->featured_image2 != "") { ?>
                                    <div class="well well-sm pull-left">
                                        <img src="<?php echo $img_path; ?>" width="100"/>
                                        <br/>
                                        <a href="<?= ADMIN_URL . 'home_contents/delete_image/' . $model->home_content->id . '/?featured_image2=1'; ?>"
                                           class="btn btn-default btn-xs">Delete</a>
                                    </div>
                                <?php } ?>
                                <div id='preview-container2'></div>
                            </div>

                            <div id="content-icon3" class="form-group" style='display:none;'>
                                <label>Featured image 3 (Left/Right Top Section)</label>

                                <p>
                                    <small>(ideal featured image size is 1920 x 300)</small>
                                </p>
                                <?php
                                $img_path = "";
                                if ($model->home_content->featured_image3 != "") {
                                    $img_path = UPLOAD_URL . 'home_contents/' . $model->home_content->featured_image3;
                                }
                                ?>
                                <p><input id="image3" type="file" name="featured_image3" class='image'/></p>
                                <?php if ($model->home_content->featured_image3 != "") { ?>
                                    <div class="well well-sm pull-left">
                                        <img src="<?php echo $img_path; ?>" width="100"/>
                                        <br/>
                                        <a href="<?= ADMIN_URL . 'home_contents/delete_image/' . $model->home_content->id . '/?featured_image3=1'; ?>"
                                           class="btn btn-default btn-xs">Delete</a>
                                    </div>
                                <?php } ?>
                                <div id='preview-container3'></div>
                            </div>
						</div>
					</div>	
				</div>
			</div>

			<div role="tabpanel" class="tab-pane" id="content-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div id="content_box" class="box">
                            <h4>Content Header and Description</h4>
                            <a id="add_desc" href="#">Add</a>
                            <? if(($content = $model->home_content->content) != ''){
                                $val = 0;
                                foreach(json_decode($content) as $key=>$value){
                                    ?>
                                    <div class="form-group">
                                        <input data-title-id="<?=$val?>" type="text" name="content_title[]" placeholder="Content Title" value="<?=$key?>">
                                        <textarea data-desc-id="<?=$val?>" id="editor_<?=$val?>" class="ckeditor" name="content_description[]" placeholder="Content Description"><?=$value?></textarea>
                                        <label>Order</label>
                                        <input type="number" name="content_order[]" value="<?=$val?>" placeholder="Enter display order"><br>
                                        <a href="#" class="delete_content">Delete</a>
                                    </div>
                                <?$val++;}?>
                            <?}?>
                        </div>
                    </div>
                </div>
            </div>
		</div>

		<div class="col-lg-24">
			<button type="submit" class="btn btn-success btn-lg">Save</button>
		</div>
	</div>
</form>


<?= footer(); ?>


<script type='text/javascript'>
	var site_url = <?php echo json_encode(ADMIN_URL.'home_content/');?>;
	$(document).ready(function() {

		function readURL1(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					var img = $("<img />");
					img.attr('src',e.target.result);
					img.attr('alt','Uploaded Image');
					img.attr("width",'100');
					img.attr('height','100');
					$("#preview-container1").html(img);
				};

				reader.readAsDataURL(input.files[0]);
			}
		}

		$("input#image1").change(function(){
			readURL1(this);
		});

		function readURL2(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					var img = $("<img />");
					img.attr('src',e.target.result);
					img.attr('alt','Uploaded Image');
					img.attr("width",'100');
					img.attr('height','100');
					$("#preview-container2").html(img);
				};

				reader.readAsDataURL(input.files[0]);
			}
		}

		$("input#image2").change(function(){
			readURL2(this);
		});

		function readURL3(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					var img = $("<img />");
					img.attr('src',e.target.result);
					img.attr('alt','Uploaded Image');
					img.attr("width",'100');
					img.attr('height','100');
					$("#preview-container3").html(img);
				};

				reader.readAsDataURL(input.files[0]);
			}
		}

		$("input#image3").change(function(){
			readURL3(this);
		});

		var val= <?=isset($val)?$val:0?>;
		$('#add_desc').on('click', function(){
			var content = $(
				'<div class="form-group"><input type="text" name="content_title[]" placeholder="Content Header"><textarea id="editor_'+val+'" class="ckeditor" name="content_description[]"></textarea><label>Order</label><input type="number" name="content_order[]" value="'+val+'" placeholder="Enter display order"><br><a href="#" class="delete_content">Delete</a></div>'
			);
			$('#content_box').append(content);
			triggerCK();
		});

		function triggerCK(){
			var editor = 'editor_'+val;
			CKEDITOR.replace(editor);
			val++;
		}	

		$(document).on('click','.delete_content', function(){
			$(this).parent().remove();
		})

		$('#lrsection').on('change', function(){
			// alert("Change");
			if(this.value == 'left' || this.value == 'right'){
				$("#content-icon1").show();
				$("#content-icon2").show();
				$("#content-icon3").show();
			} else {
				$("#content-icon1").hide();
				$("#content-icon2").hide();
				$("#content-icon3").hide();
			}
		})
	});

</script>