<?php
 if(count($model->reviews)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
           <th width="20%">Person Name</th>
            <th width="40%">Review</th> 
          <th width="5%" class="text-center">Edit</th>
          <th width="5%" class="text-center">Delete</th> 
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->reviews as $obj){ ?>
        <tr>
         <td>
         <a href="<?php echo ADMIN_URL; ?>reviews/update/<?php echo $obj->id; ?>">
            <?php echo $obj->name;?>
         </a>
      </td>
      <td>
        <a href="<?php echo ADMIN_URL; ?>reviews/update/<?php echo $obj->id; ?>">
        <?php echo $obj->review_comment;?> 
        </a>
      </td>
         
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>reviews/update/<?= $obj->id ?>">
           <i class="icon-pencil"></i> 
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>reviews/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i> 
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'reviews';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>