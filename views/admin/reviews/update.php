<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->review->id?>"/>
	<div class="row">
		<div class="col-md-16">
			<div class="box">
				<h4>General</h4>
				<div class="form-group">
					<label>Person Name</label>
					<?php echo $model->form->textAreaFor("name"); ?>
				</div>

				<div class="form-group">
					<label>Review Comment </label>
					<?php echo $model->form->textAreaFor("review_comment",["class"=>"ckeditor"]); ?>
					
				</div>
			</div>
		</div>

		<div class="col-lg-24">
			<button type="submit" class="btn btn-success btn-lg">Save</button>
		</div>
	</div>
</div>
</form>


<?= footer(); ?>


<script type='text/javascript'>
	var site_url = <?php echo json_encode(ADMIN_URL.'reviews/');?>;
</script>