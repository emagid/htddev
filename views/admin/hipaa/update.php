<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->hipaa->id?>"/>
	<div class="row">
		<div class="col-md-16">
			<div class="box">
				<h4>General</h4>
				
				<div class="form-group">
					<label>Content Description</label>
					<?php echo $model->form->textAreaFor("content", ["class"=>"ckeditor"]);?>
				</div>
			</div>
		</div>

		<div class="col-lg-24">
			<button type="submit" class="btn btn-success btn-lg">Save</button>
		</div>
	</div>
</div>
</form>


<?= footer(); ?>


<script type='text/javascript'>
	var site_url = <?php echo json_encode(ADMIN_URL.'hipaa/');?>;
</script>