<div class=" top_clear aboutus_content_wrapper contact_wrapper">

    <div class="booking">
                        <div class="leftLinesAlt"></div>
    <div class="rightLinesAlt"></div>
        <div class="wrapper">
            <div class="header_contact">
                <h4 class="blue_font hard">let's get you booked!</h4>
<!--                <p><a href="#"><ul>Already registered? Click here to log in.</ul></a></p>-->
            </div>
            
            <div class="services_section">
                <div class="services booking">
                    <div class="dental_service">
                        <div class="stamp_year">
                            <div class="henry_location">
                                <img class="feature" src="/content/frontend/assets/img/bus.jpg">
                                <div class="location_content">
                                    <h4>Is HENRY coming to your office?</h4>
                                    <p>Book your appointment here!</p>
                                    <div class="icon_row">
                                        <a class="header_link show_booking_overlay" id="booking">
                                            <img class="iconix" src="/content/frontend/assets/img/form_blue.png">
                                            <h6>Schedule an Appointment</h6>
                                        </a>
                                        <a class="header_link show_register_overlay" id="register">
                                                <img class="iconix" src="/content/frontend/assets/img/clipboard.png">
                                            <h6>New Patient Registration</h6>
                                        </a>
<!--
                                        <a class="header_link" id="myBtn">
                                            <img class="iconix" src="/content/frontend/assets/img/services.png">
                                            <h6>Services</h6>
                                        </a>
-->
                                    </div>
                                </div>
                            </div>
                        <div class="dental_service">
                            <div class="stamp_year">
                                <div class="henry_location">
                                    <img class="feature" src="/content/frontend/assets/img/newprov.png">
                                    <div class="location_content">
                                        <h4>HENRY The Dentist - New Providence, New Jersey</h4>
                                        <p>Book an appointment at our brand new brick-and-mortar practice, located at 890 Mountain Avenue, Suite 310</p>
                                        <div class="icon_row">
                                            <a class="header_link show_booking_overlay" onclick="directAppt(175);">
                                                <img class="iconix" src="/content/frontend/assets/img/form_blue.png">
                                                <h6>Schedule an Appointment</h6>
                                            </a>
                                            <a class="header_link show_register_overlay" onclick="directReg(175)">
                                                    <img class="iconix" src="/content/frontend/assets/img/clipboard.png">
                                                <h6>New Patient Registration</h6>
                                            </a>
<!--
                                            <a class="header_link" id="myBtn1">
                                                <img class="iconix" src="/content/frontend/assets/img/services.png">
                                                <h6>Services</h6>
                                            </a>
-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>


<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
      <div class="wrapper">
            <h3>New Providence, New Jersey Location</h3>
            <h4>Services Offered:</h4>
            <ul>
                <li><p>Cleanings</p></li>
                <li><p>X-rays</p></li>
                <li><p>Exams</p></li>
                <li><p>Sealants</p></li>
                <li><p>Fillings</p></li>
                <li><p>Crowns</p></li>
                <li><p>Night Guards</p></li>
                <li><p>Whitening</p></li>
                <li><p>Invisalign</p></li>
                <li><p>Periodontal</p></li>
                <li><p>Endodontics</p></li>
                <li><p>Advanced Orthodontics</p></li>
                <li><p>Cosmetic Dentistry</p></li>
            </ul>
      </div>
  </div>

</div>

<!-- The Modal -->
<div id="myModal1" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close1">&times;</span>
      <div class="wrapper">
            <h3>HENRY Mobile Clinic</h3>
            <h4>Services Offered:</h4>
            <ul>
                <li><p>Cleanings</p></li>
                <li><p>X-rays</p></li>
                <li><p>Exams</p></li>
                <li><p>Sealants</p></li>
                <li><p>Fillings</p></li>
                <li><p>Crowns</p></li>
                <li><p>Night Guards</p></li>
                <li><p>Whitening</p></li>
                <li><p>Invisalign</p></li>
            </ul>
      </div>
  </div>

</div>
<!--Modal 1-->
<script>
// Get the modal
var modal1 = document.getElementById('myModal1');

// Get the button that opens the modal
var btn1 = document.getElementById("myBtn1");

// Get the <span> element that closes the modal
var span1 = document.getElementsByClassName("close1")[0];

// When the user clicks on the button, open the modal 
btn1.onclick = function() {
  modal1.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span1.onclick = function() {
  modal1.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal1) {
    modal1.style.display = "none";
  }
}
</script>

<!--Modal 2-->
<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

    <script type="text/javascript">
        $(function () {

//		$('#login input[name="email"]').focus();
        });
    </script>
</div>