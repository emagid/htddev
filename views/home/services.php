<? $banner= \Model\Banner::getItem(null,['where'=>"page = 'about'"]); 
?>

<div class=" top_clear aboutus_content_wrapper">

    <div class="inner_hero home_hero" style="background-image:url('<?= $banner->image ? UPLOAD_URL.'banners/'.$banner->image : '' ?>')">
        <div class="home_hero_overlay">
            <!-- <h2>why <span>HENRY</span></h2> -->
            <h2>Services</h2>
        </div>
    </div>
    

    
        <div class="blue_statement services_options">
            <div class="wrapper">
                
                <h3 class="smaller_h3 white_font">services in our mobile practices</h3>
                <ul>
                    <li>Cleanings</li>
                    <li>X-rays</li>
                    <li>Exams</li>
                    <li>Sealants</li>
                    <li>Fillings</li>
                    <li>Crowns</li>
                    <li>Night Guards</li>
                    <li>Glo whitening</li>
                    <li>Invisalign</li>
                </ul>
            </div>
            
    </div>
    
            <div class="carolina_statement services_options">
            <div class="wrapper">
                
                <h3 class="smaller_h3 white_font">services at our NEW PROVIDENCE practice</h3>
                <ul>
                    <li>Cleanings</li>
                    <li>X-rays</li>
                    <li>Exams</li>
                    <li>Sealants</li>
                    <li>Fillings</li>
                    <li>Crowns</li>
                    <li>Night Guards</li>
                    <li>Glo whitening</li>
                    <li>Invisalign</li>
                    <li>Periodontal</li>
                    <li>Endodontics</li>
                    <li>Advanced orthodontics</li>
                    <li>Cosmetic dentistry</li>
                </ul>
            </div>
            
    </div>

    
    
</div>

<script>
    $('.gallery_slider').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
        centerMode: true,
  centerPadding: '60px',
        adaptiveHeight: true,
        autoplay: true,
  autoplaySpeed: 3500
    });
	
</script>