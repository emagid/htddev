<div class="content_wrapper">
    <section class="fullsize-video-bg">

<!--
        <div class="home_hero_overlay">
            <h2>the dentist now<br> comes to you.</h2>
            <p>(it's about time)</p>
        </div>
-->

        <div id="video-viewport">
            <video width="1920" height="1280" autoplay muted loop>
                <source src="<?=FRONT_ASSETS?>img/trailer.mp4" type="video/mp4" />
    <!--			<source src="http://www.coverr.co/s3/mp4/Winter-Grass.webm" type="video/webm" />-->
            </video>
        </div>
    </section>


    
	<div class="home_hero" id="mobile_hero "style="background-image:url('/content/frontend/assets/img/banner_inside.jpg');display:none;">
        <div class="home_hero_overlay">
            <h2>the dentist now<br> comes to you.</h2>
            <p>(it's about time)</p>
        </div>
	</div>
    
	<div class="marketing_row home_section_two">
        <div class="graphic">
            <img src="/content/frontend/assets/img/Henry_Crown_Orange.png" alt="HENRY the Dentist crown icon">
            <p class="blue_font"><?php echo $model->banner->main_description ?></p>
            <!-- <p class="blue_font">we believe everyone <br> should have easy access <br> to great dental care</p>
            <br>
            <p class="oj_font">that's why we built HENRY</p> -->
        </div>
	</div>	
    
    <div class="marketing_row home_section_three">
        <div class="left_column">
            <div class="column_wrapper">
            <!-- <h3>how it works for hr</h3> -->
                <h3><?=$model->left_content->main_title;?></h3>
                <div class="column_text">
                    <ul>
<!--
                        <//? if($model->left_content->content != ''){
                            $leftcontents = json_decode($model->left_content->content);
                            $leftimages = array(
                                $model->left_content->featured_image1,
                                $model->left_content->featured_image2,
                                $model->left_content->featured_image3
                            );

                            $i=0;
                            foreach ($leftcontents as $header => $content) { ?>
                                <li>
                                    <img src="<?php echo $leftimages[$i]
                                     ? UPLOAD_URL.'home_contents/'.$leftimages[$i] : '' ?>">
                                    <?=html_entity_decode($content);?>
                                </li>
                            <//? $i++; 
                            }
                        } ?>
-->
                        
                         <li>
                            <img src="/content/frontend/assets/img/headphones.png" alt="HENRY the Dentist headphone icon">
                            <p>Speak with a HENRY representative who will walk you through our scheduling process and how we integrate with your company's PPO plan.</p>
                        </li>
                        <li>
                            <img src="/content/frontend/assets/img/share.png" alt="HENRY the Dentist share icon">
                            <p>We share communication about our services and booking procedures for you to send to your employees.</p>
                        </li>
                        <li>
                            <img src="/content/frontend/assets/img/locate.png" alt="HENRY the Dentist map pin icon">
                            <p>You tell us where to park when on-site, and we handle the rest!</p>
                        </li> 
                    </ul>
                    
                    
                    <a href="/why-henry"><button>Learn More</button></a>
                </div>
            </div>
        </div>
        <div class="right_column">
            <div class="column_wrapper">
                <!-- <h3>how it works for patients</h3> -->
                <h3><?= $model->right_content->main_title; ?></h3>
                <div class="column_text">
                    <ul>
<!--
                        <//? if($model->right_content->content != ''){
                            $rightcontents = json_decode($model->right_content->content);
                            $rightimages = array(
                                $model->right_content->featured_image1,
                                $model->right_content->featured_image2,
                                $model->right_content->featured_image3
                            );

                            $i=0;
                            foreach ($rightcontents as $header => $content) { ?>
                                <li>
                                    <img src="<?php echo $rightimages[$i]
                                     ? UPLOAD_URL.'home_contents/'.$rightimages[$i] : '' ?>">
                                    <p><?=html_entity_decode($content);?></p>
                                </li>
                            <//? $i++; 
                            }
                        } ?>
-->

                         <li>
                            <img src="/content/frontend/assets/img/form.png" alt="HENRY the Dentist form icon">
                            <p>Fill out a new patient registration form, then book your appointment. Appointments are only an hour; just enough time to watch your favorite Netflix or HBO show!</p>
                        </li>
                        <li>
                            <img src="/content/frontend/assets/img/dental-tool.png" alt="HENRY the Dentist dental tool icon">
                            <p>We perform all standard procedures; including cleanings, exams, x-rays, fillings, crowns, whitening, and Invisalign, all done from the comfort of a massage featured dental chair.</p>
                        </li>
                        <li>
                            <img src="/content/frontend/assets/img/tool.png" alt="HENRY the Dentist document icon">
                            <p>After your appointment, we submit your procedures to insurances and handle all the rest. #ThatWasEasy</p>
                        </li> 
                    </ul>
                    
                    <a href="/why-henry"><button>Learn More</button></a>
                </div>
            </div>
        </div>
	</div>	
    
    <div class="marketing_row home_section_four">
        <div class="column_3 left_3" style="background-image:url('/content/frontend/assets/img/banner_left.jpg')">
        </div>
        <div class="column_3 mid_3">
            <div class="column_wrapper">

                <h3><?=$model->mid_content->main_title;?></h3>
                <? if($model->mid_content->content != ''){
                    $contents = json_decode($model->mid_content->content);
                    $i=1;
                    foreach ($contents as $header => $content) { ?>
                        <h5><?=$header?></h5>
                        <p><?=html_entity_decode($content);?></p>
                <? }
                } ?>

                <!-- <h3>our promise</h3>
                <div class="column_text">
                    <h5>consistent quality and standards</h5>
                    <p>We pride ourselves in creating professional relationships with employees. To achieve that, each corporate building will see the same medical staff every visit, so you won't have to worry about meeting someone new each time.</p>
                    
                    <h5>brand experience</h5>
                    <p>We know going to the dentist can be scary, so we've added a lot of features to minimize the fear like Netflix, HBO and noise-canceling BOSE headphones.</p>
                    
                    <h5>ease of insurance navigation</h5>
                    <p>We know insurance policies are confusing, so we take it upon ourselves to map out your coverage before each visit.</p>
                </div> -->
            </div>
        </div>
        <div class="column_3 right_3" style="background-image:url('/content/frontend/assets/img/henry_interior.png')">
        </div>
	</div>
    
    <div class="marketing_row home_section_five">
        <div class="box_border">
            <h3 class="blue_font">&quot;</h3>
            <h2 class="blue_font"><?php echo $model->quotes->quote ?></h2>
        </div> 
	</div>	
    
    <div class="marketing_row home_section_slider">
        <div class="contain">
            <? foreach($model->media_logos as $media){ 
                if($media->url == ''){
                    $url = "#";
                } else {
                    $url = $media->url;
                }?>
                <div>
                    <a href="<?=$url?>" target="_blank">
                        <img src="<?= $media->logo ? UPLOAD_URL.'media_logos/'.$media->logo : '' ?>">
                    </a>
                </div>
            <? } ?> 
            <!-- <div>
                <a href="http://hudsonreporter.com/view/full_story/27496089/article-Dentist-on-wheels--Mobile-dental-office-serves-corporate-employees--needs-?instance=north_bergen_top_story" target="_blank">
                    <img src="/content/frontend/assets/img/hudson_logo.png">
                </a>
            </div><div>
                <a href="http://www.dentistrytoday.com/news/industrynews/item/2512-henry-mobile-office-now-serving-professionals-in-new-jersey" target="_blank">
                    <img src="/content/frontend/assets/img/dt_logo.png">
                </a>
            </div><div>
                <a href="#">
                    <img src="/content/frontend/assets/img/crain_logo.png">
                </a>
            </div><div>
                <a href="http://www.nj.com/hudson/index.ssf/2017/10/delivery_service_promotes_employees_mobile_dentist.html" target="_blank">
                    <img src="/content/frontend/assets/img/nj_logo.png">
                </a>
            </div><div>
                <a href="http://www.njbiz.com/article/20171023/NJBIZ01/171029945/mobile-dental-clinic-launches-in-northern-new-jersey" target="_blank">
                    <img src="/content/frontend/assets/img/njbiz_logo.png">
                </a>
            </div><div>
                <a href="https://hobokengirl.com/shaka-bowl-anniversary-coditums-free-coding-class-hoboken-jersey-city-news-october-22-2017/?mc_cid=aacda9d1fa&mc_eid=190e01f2e3" target="_blank">
                    <img src="/content/frontend/assets/img/hg_logo.png">
                </a>
            </div> -->
        </div>
	</div>

</div>

    <script>
    var min_w = 300;
var vid_w_orig;
var vid_h_orig;

$(function() {

    vid_w_orig = parseInt($('video').attr('width'));
    vid_h_orig = parseInt($('video').attr('height'));

    $(window).resize(function () { fitVideo(); });
    $(window).trigger('resize');

});

function fitVideo() {

    $('#video-viewport').width($('.fullsize-video-bg').width());
    $('#video-viewport').height($('.fullsize-video-bg').height());

    var scale_h = $('.fullsize-video-bg').width() / vid_w_orig;
    var scale_v = $('.fullsize-video-bg').height() / vid_h_orig;
    var scale = scale_h > scale_v ? scale_h : scale_v;

    if (scale * vid_w_orig < min_w) {scale = min_w / vid_w_orig;};

    $('video').width(scale * vid_w_orig);
    $('video').height(scale * vid_h_orig);

    $('#video-viewport').scrollLeft(($('video').width() - $('.fullsize-video-bg').width()) / 2);
    $('#video-viewport').scrollTop(($('video').height() - $('.fullsize-video-bg').height()) / 2);

};
    </script>