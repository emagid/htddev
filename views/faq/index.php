<div class=" top_clear aboutus_content_wrapper">

    <div class="faq">
        <div class="wrapper">
            <h4 class="blue_font hard">faq</h4>
            
            <div class="faq_content">
<!--
                <div class="questions">
                    <ul>
                        <//? foreach ($model->dynamicFAQ as $faq) { ?>
                            <li><?php echo $faq->question?></li>    
                         <//? } ?>
                    </ul>

                </div>
-->
                <div class="answers">
                    <ul>
                        <? foreach ($model->dynamicFAQ as $faq) { ?>
                            <li>
                                <h5>
                                    <?=$faq->question; ?>    
                                </h5>
                                <?= $faq->answer; ?>
                            </li>
                        <? } ?>
                        <!-- <li>
                            <h5>Is there a minimum amount of employees required for HENRY to visit my company?</h5>
                            <p>There is no minimum number of patients required per day. Typically, HENRY visits offices with greater than 200 employees per site.</p>
                        </li>
                                                <li>
                            <h5>What type of dental plans does HENRY take?</h5>
                            <p>HENRY is in-network with all PPO plans. HENRY accepts family members of employees, and employees who are on a spousal plan from a different company. HENRY also accepts cash patients without insurance.</p>
                        </li>
                                                <li>
                            <h5>What if an employee needs follow-up care?</h5>
                            <p>We are usually able to accommodate procedures on the same day or on a consecutive day that we’re onsite. If patients require post-visit care, we have 3 options:</p><br>
<p>
1. We can schedule an employee for a follow-up appointment for the next time we’ll be onsite (typically every 3-4 months)<br>
2. We can schedule a patient when we’re at a site nearby
    <br>
3. If there are enough employees who need an appointment sooner than 3-4 months, we can schedule a separate day to come on site and see them.</p>
                        </li>
                                                <li>
                            <h5>Is all information HIPAA compliant?</h5>
                            <p>Yes, all patient records are managed in a secure HIPAA compliant system</p>
                        </li>
                        
                                                <li>
                            <h5>Will our employees see the same dental team?</h5>
                            <p>Yes, we schedule the same team onsite for all of our visits at each corporate location</p>
                        </li>
                        
                                                <li>
                            <h5>Is there a minimum amount of employees required for HENRY to visit my company?</h5>
                            <p>There is no minimum number of patients required per day. Typically, HENRY visits offices with greater than 200 employees per site.</p>
                        </li>
                                                <li>
                            <h5>How does HENRY provide information to a patient’s general dentist?</h5>
                            <p>Upon request, we will share a patient’s information with their general dentist via a HIPAA compliant, secured email.</p>
                        </li>
                        
                                                <li>
                            <h5>How do patient records get transferred or received in advance of care?</h5>
                            <p>A patient will either ask their previous dentist to send us their medical records in advance of visiting HENRY, or the patient will let us know in advance and we will work with their previous dentist to obtain their records.</p>
                        </li>
                        
                        <li>
                            <h5>How are referrals handled for services outside of HENRY’s scope?</h5>
                            <p>We are able to perform most treatments typically performed by a General Dentistry
 practice, including preventative, restorative and orthodontic services. For Specialty treatments not performed by a General Dentist, such as root canals or extractions, we refer patients to an in-network Specialist (e.g. Periodontist, Endodontist, Oral Surgeon).
<br>
NOTE: HENRY will be opening a brick-and-mortar Specialty Clinic in Q3 2018</p>
                        </li>
                                                
                        <li>
                            <h5>Does is cost our company to have HENRY on-site?</h5>
                            <p>There is no cost to company’s to have HENRY on-site providing services to your employees.</p>
                        </li>
                        
                        <li>
                            <h5>Does it cost incremental money to the employees?</h5>
                            <p>Having HENRY at your office does not cost incremental money for the employee or the company.</p>
                        </li>
                        
                        <li>
                            <h5>Can our kids see HENRY?</h5>
                            <p>HENRY can see spouses and children 12 years and older.</p>
                        </li> -->
                    </ul>
                </div>
            </div>
            
        </div>
    </div>

</div>