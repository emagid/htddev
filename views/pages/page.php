<div class="shippingReturnsPageWrapper">
	<h1 class="color_header">
		<div>
		<a class="left" href="../contact"><span class="left_arrow_white"></span>Browse Diamonds</a>
		<?=$model->page->title?>
		</div>
	</h1>	
	<div class="fixedSidebarUI">
		<? if($model->page->content != ''){
			$contents = json_decode($model->page->content);
			$i=1;
			foreach($contents as $header=>$content){?>
					<div class="subsection" data-expandedSectionID="<?=$i?>">
						<h5 class="title"><?=$header?></h5>
						<span></span>
					</div>
			<?
			$i++;
			}?>
		<?} else {?>
			<div>No content</div>
		<?}?>
	</div>
	<div class="expandedAnswersUI">
 						<div class="expandedBlocks">
 							<? if($model->page->content != ''){
							$contents = json_decode($model->page->content);
							$i=1;
							foreach($contents as $header=>$content){?>
								<div class="expandedQuestionUI" data-expandedSectionLabel="<?=$i?>">
									<h1 class="title"><?=$header?></h1>
									<p class="description"><?=html_entity_decode($content)?></p>
								</div>
							<?
							$i++;
							}?>
							<?} else {?>
								<div>No content</div>
							<?}?>
						</div>	
	</div>
	<script>

		$(document).ready(function(){
			$(".fixedSidebarUI").on("touchstart click",".subsection", function(){
				$(".subsection").removeClass("activeContentSection");
				$(this).addClass("activeContentSection");
				var expandedSectionID=$(this).attr("data-expandedSectionID");
				var $sectionToScrollTo=$(".expandedQuestionUI[data-expandedsectionlabel='"+expandedSectionID+"']");
				$(".expandedQuestionUI").removeClass("expandedSectionActive");
				$sectionToScrollTo.addClass("expandedSectionActive");
				var scrollPos = $sectionToScrollTo.offset().top - 184;
				$('html, body').animate({
				    scrollTop: scrollPos
				 }, 400);					
			});
		});

	</script>
</div>

