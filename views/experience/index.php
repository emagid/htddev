<? $banner = \Model\Banner::getItem(null,['where'=>"page = 'experience' AND banner_type_id = 0"]); 
   $sliderimages = \Model\Banner::getList(['where'=>"page = 'experience' AND banner_type_id=1"]); ?>

<div class="top_clear aboutus_content_wrapper">
	<!-- <div class="inner_hero home_hero" style="background-image:url('/content/frontend/assets/img/banner_left.jpg')">
        <div class="home_hero_overlay">
            <h2>what to expect</h2>
        </div>
	</div> -->

    <div class="inner_hero home_hero" style="background-image:url('<?= $banner->image ? UPLOAD_URL.'banners/'.$banner->image : '' ?>')">
        <div class="home_hero_overlay">
            <!-- <h2>what to expect</h2> -->
            <h2><?=$banner->title;?></h2>
        </div>
    </div>
    
    
    <div class="services_section">
        <h3>services</h3>
        
        <div class="services">
            <div class="dental_service">
                <div class="stamp_year">
                    <div class="icon" style="background-image:url('/content/frontend/assets/img/prevent.png');background-position: center 65%;"></div>
                    <h6>Preventative</h6>
                    <p>Cleanings</p>
                    <p>X-Rays</p>
                    <p>Exams</p>
                    <p>Sealants</p>
                </div>
            </div>
            <div class="dental_service">
                <div class="stamp_year">
                    <div class="icon" style="background-image:url('/content/frontend/assets/img/restore.png')"></div>
                    <h6>Restorative</h6>
                    <p>Fillings</p>
                    <p>Crowns</p>
                    <p>Night Guards</p>
                </div>
            </div>
            <div class="dental_service">
                <div class="stamp_year">
                    <div class="icon" style="background-image:url('/content/frontend/assets/img/cosmetic.png')"></div>
                    <h6>Cosmetic</h6>
                    <p>Whitening</p>
                </div>
            </div>
            <div class="dental_service">
                <div class="stamp_year">
                    <div class="icon" style="background-image:url('/content/frontend/assets/img/ortho.png')"></div>
                    <h6>Orthodontic</h6>
                    <p>Invisalign</p>
                </div>
            </div>
        </div>
    </div>
    
        <div class="gallery_section">
                <div class="leftLines"></div>
    <div class="rightLines"></div>
            <div class="wrapper">
                 <h4>We've invested a lot of thought inside each practice to provide the greatest level of care to our patients and their results.</h4>
                <ul class="feature_list">
                    <li><p>Cerec Scanner</p></li>
                    <span class="bullet">&#8226;</span>
                    <li><p>Apteryx Intraoral Camera</p></li>
                    <span class="bullet">&#8226;</span>
                    <li><p>Nomad Handheld X-Ray</p></li>
                    <span class="bullet">&#8226;</span>
                    <li><p>Midmark Panoramic X-Ray</p></li>
                    <span class="bullet">&#8226;</span>
                    <li><p>Apple TV featuring hbo & netflix</p></li>
                    <span class="bullet">&#8226;</span>
                    <li><p>BOSE noise-cancelling headphones</p></li>
                </ul> 
<!--                <//?=$banner->main_description;?>-->
            </div>
            

    </div>
            <div class="gallery_holder">
                <div class="gallery_slider">
                    <? foreach($sliderimages as $sliderimage){ ?>
                        <div class="gallery_image" style="background-image:url('<?= $sliderimage->image ? UPLOAD_URL.'banners/'.$sliderimage->image : '' ?>')"></div>
                    <? } ?>
                    <!-- <div class="gallery_image" style="background-image:url('/content/frontend/assets/img/banner_left.jpg')">
                    </div>
                    <div class="gallery_image" style="background-image:url('/content/frontend/assets/img/banner_inside.jpg')">
                    </div>
                    <div class="gallery_image" style="background-image:url('/content/frontend/assets/img/banner_left.jpg')">
                    </div>
                    <div class="gallery_image" style="background-image:url('/content/frontend/assets/img/banner_inside.jpg')">
                    </div>
                    <div class="gallery_image" style="background-image:url('/content/frontend/assets/img/banner_left.jpg')">
                    </div>
                    <div class="gallery_image" style="background-image:url('/content/frontend/assets/img/banner_inside.jpg')">
                    </div> -->

            </div>
    
</div>

<script>
    $('.gallery_slider').slick({
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 1,
        centerMode: true,
  centerPadding: '60px',
        adaptiveHeight: true,
        autoplay: true,
  autoplaySpeed: 3500,
        arrows: true,
          responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1
      }
    }
  ]
    });
	
</script>