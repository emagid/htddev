<?php
namespace Model;

class Privacy_Policy extends \Emagid\Core\Model {
	static $tablename = "privacy_policy";
	public static $fields = [
		'content',
	];
}