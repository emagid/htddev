<?php

namespace Model;

class User extends \Emagid\Core\Model
{
    static $tablename = "public.user";

    public static $fields = [
        'wholesale_id',
        'email' => ['required' => true, 'type' => 'email', 'unique' => true],
        'password',
        'hash',
        'first_name',
        'last_name',
        'company',
        'phone',
        'full_address',
        'status', //0->pending, 1->approved, 2->rejected
        'setup', //0->hasn't been setup, 1-has been set up
        'username'
    ];

    static $relationships = [
        [
            'name' => 'payment_profile',
            'class_name' => '\Model\Payment_Profile',
            'local' => 'id',
            'remote' => 'user_id',
            'relationship_type' => 'single'
        ],
        [
            'name' => 'address',
            'class_name' => '\Model\Address',
            'local' => 'id',
            'remote' => 'user_id',
            'relationship_type' => 'single'
        ]
    ];

    /**
     * concatenates first name and last name to create full name string and returns it
     * @return type: string of full name
     */
    public function full_name()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function address()
    {
        return Address::getItem(null, ['where'=>'user_id = '.$this->id]);
    }

    public function payment_profile()
    {
        return Payment_Profile::getItem(null, ['where'=>'user_id = '.$this->id]);
    }

    public function wishlist()
    {
        return Wishlist::getList(['where'=>'user_id = '.$this->id]);
    }

    public function order()
    {
        return Order::getList(['where'=>'user_id = '.$this->id]);
    }

    public function newsletter()
    {
        return Newsletter::getList(['where'=>'user_id = '.$this->id]);
    }

    /**
     * Verify login and create the authentication cookie / session
     */
    public static function login($username, $password)
    {
        $user = \Model\User::getList(['where' => "username = '" . $username . "'"]);
        if (count($user) > 0) {
            $user = $user[0];
            $hash = \Emagid\Core\Membership::hash($password, $user->hash);
            if ($hash['password'] == $user->password) {
                $userRoles = \Model\User_Roles::getList(['where' => 'active = 1 and user_id = ' . $user->id]);
                $rolesIds = [];
                foreach ($userRoles as $role) {
                    $rolesIds[] = $role->role_id;
                }
                $rolesIds = implode(',', $rolesIds);

                $roles = \Model\Role::getList(['where' => 'active = 1 and id in (' . $rolesIds . ')']);
                $rolesNames = [];
                foreach ($roles as $role) {
                    $rolesNames[] = $role->name;
                }

                if(in_array('wholesale',$rolesNames)) {
                    $wholesale = Wholesale::getItem($user->wholesale_id);
                    if($wholesale->status == 1){
                        \Emagid\Core\Membership::setAuthenticationSession($user->id, $rolesNames, $user);
                    } else {
                        $n = new \Notification\ErrorHandler('Your account has not been verified.');
                        $_SESSION["notification"] = serialize($n);
                        return false;
                    }
                } else {
                    \Emagid\Core\Membership::setAuthenticationSession($user->id, $rolesNames, $user);
                }
                return true;
            } else {
                $n = new \Notification\ErrorHandler('Incorrect username or password.');
                $_SESSION["notification"] = serialize($n);

                return false;
            }
        } else {
            $n = new \Notification\ErrorHandler('Email not found.');
            $_SESSION["notification"] = serialize($n);

            return false;
        }
    }

    /**
     *first time login for wholesalers
     */

    public function signup()
    {
        //dd($this);
    }

    public function getPasswordResetLink()
    {
        return SITE_DOMAIN."/reset/password/$this->hash";
    }

    public static function random_pass($length = 8)
    {
        $charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        $key='';
        for($i=0; $i<$length; $i++)
            $key .= $charset[(mt_rand(0,(strlen($charset)-1)))];

        return $key;
    }
}