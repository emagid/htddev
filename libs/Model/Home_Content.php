<?php
namespace Model;

class Home_Content extends \Emagid\Core\Model {
	static $tablename = "home";
	public static $fields = [
		'main_title',
		'section',
		'content',
		'featured_image1',
		'featured_image2',
		'featured_image3',
	];
}