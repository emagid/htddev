<?php
namespace Model;

class Review extends \Emagid\Core\Model {
	static $tablename = "reviews";
	public static $fields = [
		'name',	
		'review_comment',
	];
}