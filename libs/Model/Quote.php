<?php
namespace Model;

class Quote extends \Emagid\Core\Model {
	static $tablename = "quotes";
	public static $fields = [
		'by_name',	
		'quote',
	];
}