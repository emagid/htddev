<?php

namespace Model;

class Banner extends \Emagid\Core\Model {
    static $tablename = "public.banner";

    public static $fields  =  [
    	'title',
    	'image',
        'main_description',
        'banner_type_id',
        'banner_order',
        'page',
    ];    

    public function getBannerType(){
        switch($this->banner_type_id){
            case 0:
                echo 'Main Banner';break;
            case 1:
                echo 'Slider';break;
        }
    }
}
























