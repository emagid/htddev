<?php
namespace Model;

class Faq extends \Emagid\Core\Model {
	static $tablename = "faq";
	public static $fields = [
		'question',	
		'answer',
		'display_order'
	];
}