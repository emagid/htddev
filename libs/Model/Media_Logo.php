<?php
namespace Model;

class Media_Logo extends \Emagid\Core\Model {
	static $tablename = "media";
	public static $fields = [
		'name',	
		'logo',
		'url',
	];
}