<?php

namespace Model;

class Contact extends \Emagid\Core\Model {
    static $tablename = "public.contact";

    public static $fields  =  [
        'first_name',
        'last_name',
        'email',
        'phone',
        'message',
        'company',
    ];
}
























