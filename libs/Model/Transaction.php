<?php
namespace Model;

class Transaction extends \Emagid\Core\Model {
 
    static $tablename = "transaction";

    public static $fields = [
    	'authorize_net_data', //blob
    	'order_id',
    	'ref_trans_id',
    	'ship_first_name',
    	'ship_last_name',
    	'email',
    	'phone',
    	'amount' => ['required' => true, 'name'=>"Payment Amount"],
    	'transaction_date',
    	'ship_address',
    	'ship_address2',
    	'ship_city',
    	'ship_state',
    	'ship_country',
    	'ship_zip',
    	'payment_status',
    	'type',
        'slug',
        'ref_num'
    ];

    static $pay_status = ['Paid','Un-paid','Payment Failed'];

    static $type = ['Invited','Self'];

    public function full_name(){
    	return $this->ship_first_name. " " . $this->ship_last_name;
    }

    public function get_insert_time(){
        global $emagid;
        $db = $emagid->getDb();
        $results = $db->getResults("SELECT insert_time FROM public.transaction WHERE id = $this->id");
        return $results[0]['insert_time'];
    }

    public static function generateSlug()
    {
        $long_token = md5(uniqid(rand(), true));
        $short_token = substr($long_token,0,10);
        return $short_token;
    }

    public static function generateToken()
    {
        return md5(uniqid(rand(), true));
    }
  
}