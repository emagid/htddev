<?php

namespace Emagid;

class Image extends Media {
	
	public $width;
	
	public $height;
	
	public $allowedTypes = ['jpg', 'jpeg', 'png', 'gif'];
	
	/*
	 * If maintain proportions is set, use only a new width or a new height
	 * If newFileName is null it resizes to the same file, if not it creates a copy with the new size
	 */
	public function resize($newFileName = null, $maintainProportions = false, $newWidth = null, $newHeight = null){
		if (isset($this->fileDir) && isset($this->fileName) 
				&& ($newWidth != null || $newHeight != null)
				&& file_exists($this->fileDir.$this->fileName)
				&& in_array($this->fileType, $this->allowedTypes)
				){
			
			list($this->width, $this->height) = (getimagesize($this->fileDir.$this->fileName));
			
			if ($this->fileType == 'jpg'){
				$this->fileType = 'jpeg';
			}
			
			$t = 'imagecreatefrom'.$this->fileType;
			$image = $t($this->fileDir.$this->fileName);
			
			if ($newWidth == null){
				$newWidth = $this->width;
			}
			if ($newHeight == null){
				$newHeight = $this->height;
			}
			
			if ($maintainProportions){
				if ($newHeight != $this->height){
					$newWidth = ($newHeight / $this->height) * $this->width; 
				}
				if ($newWidth != $this->width){
					$newHeight = ($newWidth / $this->width) * $this->height;
				}
			}
			
			$newImage = imagecreatetruecolor($newWidth, $newHeight);
			$newImage = $this->applyTransparency($newImage);
			
			imagecopyresized($newImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $this->width, $this->height);
			
			if ($newFileName == null){
				$newFileName = $this->fileName;
			}
			
			$t = 'image'.$this->fileType;
			$t($newImage, $this->fileDir.$newFileName);
			
			list($this->width, $this->height) = (getimagesize($this->fileDir.$this->fileName));
			
			return true;
		} else {
			return false;
		}
	}
	
	/*
	 * $cropX and $copyY are the coordinates to begin the new file (the crop)
	 * If newFileName is null it resizes to the same file, if not it creates a copy with the new size
	 */
	public function crop($newFileName = null, $newWidth = null, $newHeight = null, $cropX = 0, $cropY = 0){
		if (isset($this->fileDir) && isset($this->fileName)
				&& ($newWidth != null || $newHeight != null)
				&& file_exists($this->fileDir.$this->fileName)
				&& in_array($this->fileType, $this->allowedTypes)
		){
				
			list($this->width, $this->height) = (getimagesize($this->fileDir.$this->fileName));
				
			if ($this->fileType == 'jpg'){
				$this->fileType = 'jpeg';
			}
				
			$t = 'imagecreatefrom'.$this->fileType;
			$image = $t($this->fileDir.$this->fileName);
				
			if ($newWidth == null){
				$newWidth = $this->width;
			}
			if ($newHeight == null){
				$newHeight = $this->height;
			}
				
			$newImage = imagecreatetruecolor($newWidth, $newHeight);
			$newImage = $this->applyTransparency($newImage);
			
			imagecopyresized($newImage, $image, 0, 0, $cropX, $cropY, $newWidth, $newHeight, $newWidth, $newHeight);
			
			if ($newFileName == null){
				$newFileName = $this->fileName;
			}
			
			$t = 'image'.$this->fileType;
			$t($newImage, $this->fileDir.$newFileName);
		
			return true;
		} else {
			return false;
		}
	}
	
	private function applyTransparency($image){
		if($this->fileType == 'gif' or $this->fileType == 'png'){
			imagecolortransparent($image, imagecolorallocatealpha($image, 0, 0, 0, 127));
			imagealphablending($image, false);
			imagesavealpha($image, true);
		}
		return $image;
	}
	
}