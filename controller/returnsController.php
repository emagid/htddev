<?php

class returnsController extends siteController
{

    public function index(Array $params = [])
    {
        //should run under /pages/{slug} format
        $slug = 'returns';
        $this->viewData->returns = \Model\Page::getItem(null,['where'=>"slug = '{$slug}'"]);

        $this->configs['Meta Title'] = "Returns";
        $this->loadView($this->viewData);
    }

}