<?php
use Model\Order;
use Model\Product;
use PayPal\CoreComponentTypes\BasicAmountType;
use PayPal\EBLBaseComponents\DoExpressCheckoutPaymentRequestDetailsType;
use PayPal\EBLBaseComponents\PaymentDetailsItemType;
use PayPal\EBLBaseComponents\PaymentDetailsType;
use PayPal\EBLBaseComponents\SetExpressCheckoutRequestDetailsType;
use PayPal\PayPalAPI\DoExpressCheckoutPaymentReq;
use PayPal\PayPalAPI\DoExpressCheckoutPaymentRequestType;
use PayPal\PayPalAPI\GetExpressCheckoutDetailsReq;
use PayPal\PayPalAPI\GetExpressCheckoutDetailsRequestType;
use PayPal\PayPalAPI\SetExpressCheckoutReq;
use PayPal\PayPalAPI\SetExpressCheckoutRequestType;
use PayPal\Service\PayPalAPIInterfaceServiceService;

/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 3/18/16
 * Time: 2:54 PM
 */

class paypalController extends siteController
{
    /**
     * @var array
     * live:
     * americangrowndiamonds_api1.gmail.com
     * PJ78JWBNFZPJ93GV
     * AFcWxV21C7fd0v3bYYYRCpSSRl31AnOhqi5f4AQIj00XEaqfOOO481f7
     *
     * dev:
     * jb-us-seller_api1.paypal.com
     * WX4WTU3S8MY44S7F
     * AFcWxV21C7fd0v3bYYYRCpSSRl31A7yDhhsPUU2XhtMoZXsWHFxu-RWy
     */
    private $config = array (
        'mode' => 'live' ,
        'acct1.UserName' => 'barucha124_api1.gmail.com',
        'acct1.Password' => '35PXS7EF8ZKEQ5B5',
        'acct1.Signature' => 'ADaZbQNDDpMbSUW0Xkfuw6xDVRDXAlb420uCvZPGbmd8BQfuAFJljx4o'
    );

    private $redirectBase = 'https://www.paypal.com'; // https://www.sandbox.paypal.com

    public function getOrderTotal()
    {
        $shippingCost = 0;
        $tax = 0;
        $total = $subtotal = $this->viewData->cart->total;

        if (isset($_SESSION['coupon']) && ($couponCode = $_SESSION['coupon'])) {
            $coupon = \Model\Coupon::getItem(null, ['where' => "code = '" . $couponCode . "'"]);
            if ($coupon->discount_type == 1) {//$
                $total = $total - $coupon->discount_amount;
            } else if ($coupon->discount_type == 2) {//%
                $total = $total * (1 - $coupon->discount_amount / 100);
            }
        }

//        $shipping_method = \Model\Shipping_Method::getItem($order->shipping_method);
//        $order->shipping_cost = floatval($shipping_method->cost);
        $total = $shippingCost + $subtotal + $tax;
        return $total;
    }


    public function pay(Array $params = [])
    {
        $paypalService = new PayPalAPIInterfaceServiceService($this->config);
        $paymentDetails= new PaymentDetailsType();

        if (count($this->viewData->cart->products) > 0) {
            $itemDetails = new PaymentDetailsItemType();
            $itemDetails->Name = 'Diamond Lab Grown';
            $itemAmount = $this->getOrderTotal();
            $itemDetails->Amount = $itemAmount;
            $itemQuantity = '1';
            $itemDetails->Quantity = $itemQuantity;

            $paymentDetails->PaymentDetailsItem[0] = $itemDetails;

        } else {
            $n = new \Notification\ErrorHandler('Your cart is empty.');
            $_SESSION["notification"] = serialize($n);
            return redirect(SITE_URL . 'checkout');
        }

        $orderTotal = new BasicAmountType();
        $orderTotal->currencyID = 'USD';
        $orderTotal->value = $this->getOrderTotal();

        $paymentDetails->OrderTotal = $orderTotal;
        $paymentDetails->PaymentAction = 'Sale';

        $setECReqDetails = new SetExpressCheckoutRequestDetailsType();
        $setECReqDetails->PaymentDetails[0] = $paymentDetails;
        $setECReqDetails->CancelURL = 'http://http://henrythedentist.com/checkout';
        $setECReqDetails->ReturnURL = 'http://http://henrythedentist.com/paypal/confirm'; //$_SERVER['SERVER_NAME'].'/paypal/confirm';

        $setECReqType = new SetExpressCheckoutRequestType();
        $setECReqType->Version = '104.0';
        $setECReqType->SetExpressCheckoutRequestDetails = $setECReqDetails;

        $setECReq = new SetExpressCheckoutReq();
        $setECReq->SetExpressCheckoutRequest = $setECReqType;

        $setECResponse = $paypalService->SetExpressCheckout($setECReq);
        $token = $setECResponse->Token;
        redirect($this->redirectBase.'/cgi-bin/webscr?cmd=_express-checkout&token='.$token);
    }

    public function confirm()
    {
        if (!$this->viewData->user) {
            $guest = true;
        } else {
            $guest = false;
        }

        $payerId = $_GET['PayerID'];
        $token = $_GET['token'];
        $paypalService = new PayPalAPIInterfaceServiceService($this->config);
        $getExpressCheckoutDetailsRequest = new GetExpressCheckoutDetailsRequestType($token);
        $getExpressCheckoutDetailsRequest->Version = '104.0';
        $getExpressCheckoutReq = new GetExpressCheckoutDetailsReq();
        $getExpressCheckoutReq->GetExpressCheckoutDetailsRequest = $getExpressCheckoutDetailsRequest;

        $getECResponse = $paypalService->GetExpressCheckoutDetails($getExpressCheckoutReq);
        $response = $getECResponse->GetExpressCheckoutDetailsResponseDetails;
        if($getECResponse->Ack != 'Success'){
            $n = new \Notification\ErrorHandler('Paypal checkout failed, please try again.');
            $_SESSION["notification"] = serialize($n);
            redirect(SITE_URL . 'checkout');
        }

        // TODO: save order
        $order = new \Model\Order();
        $order->user_id = $this->viewData->user?$this->viewData->user->id:0;
        $order->status = Order::$status[0];
        $order->insert_time = new DateTime();
        $order->insert_time = date_format($order->insert_time, 'm/d/Y H:i:s');
        $order->coupon_code = isset($_SESSION['coupon'])?$_SESSION['coupon']:null;
        $coupon = null;
        if (!is_null($order->coupon_code)) {
            $coupon = \Model\Coupon::getItem(null, ['where' => "code = '" . $order->coupon_code . "'"]);
            $order->coupon_type = $coupon->discount_type;
            $order->coupon_amount = $coupon->discount_amount;
            if (is_null($coupon)
                || (!is_null($coupon)
                    && (time() < $coupon->start_time || time() >= $coupon->end_time
                        || \Model\Product::getPriceSum(implode(',', $this->viewData->cart->products)) < $coupon->min_amount
                        || \Model\Order::getCount(['where' => "coupon_code = '" . $coupon->code . "'"]) >= $coupon->num_uses_all)
                )
            ) {
                $n = new \Notification\ErrorHandler('Invalid coupon.');
                $_SESSION['coupon'] = null;
                $_SESSION["notification"] = serialize($n);
                redirect(SITE_URL . 'checkout');
            }
        } else {
            $order->coupon_code = null;
        }

        $order->payment_method = 3; // paypal
        $order->ship_country = 'United States';
        $order->bill_country = 'United States';
        $order->ref_num = Product::generateToken();
        $order->ship_first_name = $response->PayerInfo->PayerName->FirstName;
        $order->ship_last_name = $response->PayerInfo->PayerName->LastName;
        $order->ship_address = $response->PayerInfo->Address->Street1;
        $order->ship_address2 = $response->PayerInfo->Address->Street2;
        $order->ship_city = $response->PayerInfo->Address->CityName;
        $order->ship_state = $response->PayerInfo->Address->StateOrProvince;
        $order->ship_country = $response->PayerInfo->Address->CountryName;
        $order->ship_zip = $response->PayerInfo->Address->PostalCode;
        $order->bill_first_name = $order->ship_first_name;
        $order->bill_last_name = $order->ship_last_name;
        $order->bill_address = $order->ship_address;
        $order->bill_address2 = $order->ship_address2;
        $order->bill_city = $order->ship_city;
        $order->bill_state = $order->ship_state;
        $order->bill_country = $order->ship_country;
        $order->bill_zip = $order->ship_zip;
        $order->phone = $response->PayerInfo->Address->Phone?:'';
        $order->email = $response->PayerInfo->Payer;

        $order->shipping_method = 1;

        if ($order->save()) {
            $order->subtotal = 0;

            foreach ($this->viewData->cart->products as $product) {
                //if product is diamond, set quantity to 0
                if($product->product->getMap()->product_type_id == '1'){
                    $product->product->quantity = 0;
                    $product->product->save();
                }

                $order_product = new \Model\Order_Product();
                $order_product->order_id = $order->id;
                $order_product->product_map_id = $product->productMap->id;
                $order_product->quantity = 1;
                $order_product->unit_price = $product->product->price;
                $order_product->save();

                if(isset($product->ringMaterial) && isset($product->size) && isset($product->ring_setting)){
                    $option = new \Model\Order_Product_Option();
                    $option->price = $product->ringMaterial->size_price($product->size) + $product->ringMaterial->price;
                    $option->data = json_encode([
                        'ringMaterial' => $product->ringMaterial,
                        'size' => $product->size,
                        'ringSetting' => $product->ring_setting
                    ]);
                    $option->order_product_id = $order_product->id;
                    $option->save();
                }

                if(isset($product->optionalProduct)){
                    $option = new \Model\Order_Product_Option();
                    $option->price = $product->optionalProduct->getPrice($product->optionalProduct->default_metal, $product->options['stoneId']);
                    $option->data = json_encode([
                        'optionalProduct'=> [
                            'product_map_id' => $product->optionalProduct->getMap()->id,
                        ],
                        'size'=>$product->options['size'],
                        'color'=>$product->optionalProduct->default_color,
                        'metal'=>$product->optionalProduct->default_metal,
                        'shape'=>$product->options['shape'],
                        'stone'=>$product->options['stone'],
                        'stoneId'=>$product->options['stoneId']
                    ]);
                    $option->order_product_id = $order_product->id;
                    $option->save();
                } else if(isset($product->options)){
                    $option = new \Model\Order_Product_Option();
                    $option->price = 0;
                    $option->order_product_id = $order_product->id;
                    $option->data = json_encode($product->options);
                    $option->save();
                }
            }

            $order->total = $order->subtotal = $this->viewData->cart->total;

            if ($coupon) {
                $_SESSION['coupon'] = null;
                if ($coupon->discount_type == 1) {//$
                    $order->total = $order->total - $coupon->discount_amount;
                } else if ($coupon->discount_type == 2) {//%
                    $order->total = $order->total * (1 - $coupon->discount_amount / 100);
                }
            }

            if ($order->ship_state == 'NY') {
                $order->tax_rate = 8.875;
                $order->tax = $order->total * ($order->tax_rate / 100);
            } else {
                $order->tax_rate = null;
                $order->tax = null;
            }

            $shipping_method = \Model\Shipping_Method::getItem($order->shipping_method);
            $order->shipping_cost = floatval($shipping_method->cost);
            $order->total = $order->shipping_cost + $order->subtotal + $order->tax;

            if ($order->payment_method == 2){
                $order->total = $order->total * 0.98;
            }

            $order->save();
            if (!$guest) {
                $payment_profile = \Model\Payment_Profile::getItem(null, ['where'=>['user_id'=>$this->viewData->user->id]]);
                $pp = ['user_id'=>$order->user_id,
                    'country'=>$order->bill_country,
                    'first_name'=>$order->bill_first_name,
                    'last_name'=>$order->bill_last_name,
                    'address'=>$order->bill_address,
                    'address2'=>$order->bill_address2,
                    'city'=>$order->bill_city,
                    'state'=>$order->bill_state,
                    'zip'=>$order->bill_zip,
                    'phone'=>$order->phone,
                    'cc_number'=>$order->cc_number,
                    'cc_expiration_month'=>$order->cc_expiration_month,
                    'cc_expiration_year'=>$order->cc_expiration_year,
                    'cc_ccv'=>$order->cc_ccv];

                $payment = ['user_id'=>$payment_profile->user_id,
                    'country'=>$payment_profile->country,
                    'first_name'=>$payment_profile->first_name,
                    'last_name'=>$payment_profile->last_name,
                    'address'=>$payment_profile->address,
                    'address2'=>$payment_profile->address2,
                    'city'=>$payment_profile->city,
                    'state'=>$payment_profile->state,
                    'zip'=>$payment_profile->zip,
                    'phone'=>$payment_profile->phone,
                    'cc_number'=>$payment_profile->cc_number,
                    'cc_expiration_month'=>$payment_profile->cc_expiration_month,
                    'cc_expiration_year'=>$payment_profile->cc_expiration_year,
                    'cc_ccv'=>$payment_profile->cc_ccv];
                //if user is logged in and payment_profile returns null
                if (isset($payment_profile) && $payment_profile != "" && is_numeric($payment_profile->id)) {
                    //when user changes billing info
                    if(count(array_intersect($payment, $pp)) != count($payment)){
                        foreach($payment as $key=>$value){
                            $payment_profile->{$key} = $pp{$key};
                        }
                        $payment_profile->save();
                    }
                } else {
                    $payment_profile = new \Model\Payment_Profile();
                    foreach($payment as $key=>$value){
                        $payment_profile->{$key} = $pp{$key};
                    }
                    $payment_profile->save();
                }

                $address = \Model\Address::getItem(null, ['where'=>['user_id'=>$this->viewData->user->id]]);
                $sa = ['user_id'=>$order->user_id,
                    'country'=>$order->ship_country,
                    'first_name'=>$order->ship_first_name,
                    'last_name'=>$order->ship_last_name,
                    'address'=>$order->ship_address,
                    'address2'=>$order->ship_address2,
                    'city'=>$order->ship_city,
                    'state'=>$order->ship_state,
                    'zip'=>$order->ship_zip,
                    'phone'=>$order->phone];
                $shipping = ['user_id'=>$address->user_id,
                    'country'=>$address->country,
                    'first_name'=>$address->first_name,
                    'last_name'=>$address->last_name,
                    'address'=>$address->address,
                    'address2'=>$address->address2,
                    'city'=>$address->city,
                    'state'=>$address->state,
                    'zip'=>$address->zip,
                    'phone'=>$address->phone];
                if (isset($address) && $address != "" && is_numeric($address->id)) {
                    //when user changes shipping info
                    if(count(array_intersect($shipping, $sa)) != count($shipping)) {
                        foreach($shipping as $key=>$value){
                            $address->{$key} = $sa{$key};
                        }
                        $address->save();
                    }
                } else {
                    $address = new \Model\Address();
                    foreach($shipping as $key=>$value){
                        $address->{$key} = $sa{$key};
                    }
                    $address->save();
                }
            }
        } else {
            $n = new \Notification\ErrorHandler($order->errors);
            $_SESSION["notification"] = serialize($n);
            redirect(SITE_URL . 'checkout');
        }

        // TODO: commit paypal transaction

//        $paymentDetails->PaymentAction = 'Sale';
//        $paymentDetails->NotifyURL = "http://replaceIpnUrl.com";
        $paymentDetails= new PaymentDetailsType();
        $orderTotal = new BasicAmountType();
        $orderTotal->currencyID = 'USD';
        $orderTotal->value = $order->total;

        $paymentDetails->OrderTotal = $orderTotal;

        $DoECRequestDetails = new DoExpressCheckoutPaymentRequestDetailsType();
        $DoECRequestDetails->PayerID = $payerId;
        $DoECRequestDetails->Token = $token;
        $DoECRequestDetails->PaymentDetails[0] = $paymentDetails;

        $DoECRequest = new DoExpressCheckoutPaymentRequestType();
        $DoECRequest->DoExpressCheckoutPaymentRequestDetails = $DoECRequestDetails;
        $DoECRequest->Version = '104.0';

        $DoECReq = new DoExpressCheckoutPaymentReq();
        $DoECReq->DoExpressCheckoutPaymentRequest = $DoECRequest;

        $DoECResponse = $paypalService->DoExpressCheckoutPayment($DoECReq);

        if($DoECResponse->Ack == 'Success'){
            $order->sendOrderSuccessEmail();

            $n = new \Notification\MessageHandler('Order successfully placed!', $guest);
            $_SESSION["notification"] = serialize($n);

            $this->clearCart();

            $_SESSION['purchase_conversion'] = true;

            redirect(SITE_URL . 'status/' . $order->ref_num);
        } else {
            $n = new \Notification\ErrorHandler("Paypal transaction couldn't be completed, please try again.");
            $_SESSION["notification"] = serialize($n);
            redirect(SITE_URL . 'checkout');
        }
    }

    private function clearCart()
    {
        $this->viewData->cart->products = [];
        $this->viewData->cart->total = Product::getPriceByProducts($this->viewData->cart->products);
        setcookie('cart', serialize([]), time() + 60 * 60 * 24 * 100, "/");
    }

}