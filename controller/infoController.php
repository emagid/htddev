<?php

class infoController extends siteController {
    function __construct(){
        parent::__construct();
    }

//    public function index(Array $params=[]){ //For Offices/practices with multiple locations
//        header("Access-Control-Allow-Origin: *");
//
//        $_offices = \Model\Office::getList(['where'=>'id != 100']);
//        $book_offices = [];
//        foreach($_offices as $office){
//            if(!isset($book_offices[$office->practice()->name])) $book_offices[$office->practice()->name]=[];
//            $book_offices[$office->practice()->name][$office->id] = $office;
//        }
//
//        $office_names = '<option value="">Select a company</option>';
//        $office_ids = '<option value="">Select a location</option>';
//        foreach($book_offices as $name => $offices){
//            $office_names .= "<option value='$name'>$name</option>>";
//            foreach($offices as $id => $office){
//                $office_ids .= "<option value='$id' data-name='$name' style='display: none;' class='office_loc'>{$office->label}</option>";
//            }
//        }
//
//        $this->toJson(['status'=>true,
//            'office_names'=>$office_names,
//            'office_ids'=>$office_ids]);
//    }

    public function index(Array $params=[]){
        header("Access-Control-Allow-Origin: *");

        $states = ['<option value="">Select a state</option>'];

        $_offices = \Model\Office::getList(['where'=>'id != 100']);
        $book_offices = [];
        foreach($_offices as $office){
            if(!isset($states[$office->state])) $states[$office->state] = "<option value='$office->state'>".get_states()[$office->state]."</option>" ;
            if(!isset($book_offices[$office->practice()->name])) $book_offices[$office->practice()->name]=[];
            $book_offices[$office->practice()->name][$office->id] = $office;
        }
        ksort($book_offices);

        $office_names = '<option value="">Select a company</option>';
        $office_ids = '<option value="">Select a location</option>';
        foreach($book_offices as $name => $offices){
            asort($book_offices[$name]);
            $office_names .= "<option value='$name'>$name</option>>";
            foreach($offices as $id => $office){
                $office_ids .= "<option value='$id' data-name='$name' data-state='$office->state' class='office_loc'>$name</option>";
            }
        }

        $this->toJson(['status'=>true,
            'office_names'=>$office_names,
            'office_ids'=>$office_ids,
            'states'=>implode('',$states)]);
    }
}


