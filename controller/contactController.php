<?php

class contactController extends siteController
{

    public function index(Array $params = [])
    {

        $this->configs['Meta Title'] = "Contact | HENRY The Dentist";
        $this->configs['Meta Description'] = "Have some questions that weren’t answered? Contact HENRY the
        Dentist to have them answered.";
        $this->configs['Meta Keywords'] = "contact";
        $this->loadView($this->viewData);
    }

    public function index_post()
    {
        $obj = \Model\Contact::loadFromPost();
        $email = new \Email\MailMaster();
        $recipient = "hello@HENRYthedentist.com";
        if($obj->save()){

            $mergeTags = [
                'CONTENT'=>"Name: ".$obj->first_name." ".$obj->last_name."<br /> Company: ".$obj->company."<br /> Email: ".$obj->email."<br /> Phone: ".$obj->phone." <br /> Message: ".$obj->message
            ];
            $email->setTo(['email' => $recipient, 'name' => ucwords($obj->first_name), 'type' => 'to'])
                ->setMergeTags($mergeTags)
                ->noBCC()  
                ->setSubject('HENRY Contact Form Submission')->setTemplate('henry-contact-form-submission')
                  ;
            $emailresp = $email->send();
            $n = new \Notification\MessageHandler('Cha ching! Don’t worry, your email isn’t in cyberspace. We’ll get back to you ASAP!');
            $_SESSION["notification"] = serialize($n);
            redirect('/');
        } else {
            $n = new \Notification\MessageHandler('Something went wrong.. Please try again.');
            $_SESSION["notification"] = serialize($n);
            redirect('/contact');
        }

    }

}