<?php
use Model\Product;
use Model\Wishlist;

/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 11/11/15
 * Time: 1:13 PM
 */

class wishlistController extends siteController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Array $params = [])
    {
        if($user = $this->viewData->user){
            redirect('/account?tab=wishlist');
        } else {
            $this->loadView($this->viewData);
        }
    }

    public function add_post()
    {
        $item = new stdClass();
        if (isset($_POST['product_id']) && isset($_POST['category'])){
            $product = $this->productMap->getProduct($_POST['product_id'], $_POST['category']);
            if (!is_null($product)) {
                $item->product = $product;
            }
        }

        if(isset($item->product) && $item->product){
            $this->viewData->wishlist->products[Product::generateToken()] = $item;
        }

        if($user = $this->viewData->user){
            $wishlist = new Wishlist();
            $wishlist->user_id = $user->id;
            $wishlist->data = json_encode(['product_id' => $_POST['product_id'], 'product_type_id' => \Model\Product_Type::getTypeId($_POST['category'])]);
            $wishlist->save();
        } else {
            $this->updateCookies();
        }

        echo json_encode(['status' => 'success']);
    }

    public function remove_post()
    {
        if($wishlist = Wishlist::getItem($_POST['wishlistId'])){
            Wishlist::delete($wishlist->id);
        } else {
            foreach ($this->viewData->wishlist->products as $key => $product) {
                if ($key == $_POST['wishlistId']) {
                    unset($this->viewData->wishlist->products[$key]);
                }
            }
            $this->updateCookies();
            redirect('/wishlist');
        }

        echo json_encode(['status' => 'success']);
    }

    private function updateCookies()
    {
        $cookie = [];
        foreach($this->viewData->wishlist->products as $key => $item){
            $data = [];
            $data['product_id'] = $item->product->id;
            $reflect = new ReflectionClass($item->product);
            $data['product_type'] = $reflect->getShortName();
            $cookie[$key] = $data;
        }

        setcookie('wishlist', serialize($cookie), time() + 60 * 60 * 24 * 100, "/");
    }

}