<?php

class shippingController extends siteController
{

    public function index(Array $params = [])
    {
        //should run under /pages/{slug} format
        $slug = 'shipping';
        $this->viewData->shipping = \Model\Page::getItem(null,['where'=>"slug = '{$slug}'"]);

        $this->configs['Meta Title'] = "Shipping";
        $this->loadView($this->viewData);
    }

}