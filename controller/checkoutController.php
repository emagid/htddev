<?php
use Model\Coupon;
use Model\Product;
use Model\Product_Map;
use Model\Ring_Material;
use Model\Shipping_Method;
use Model\Order;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

class checkoutController extends siteController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Array $params = [])
    {
        $this->configs['Meta Title'] = "Checkout | HENRY The Dentist";
        
        $this->viewData->transaction = '';
        if(isset($params['slug']) && $params['slug'] != ''){
            $slug = $params['slug'];
            $transaction = \Model\Transaction::getItem(null,['where'=>"slug='".$slug."'"]);
            if($transaction){
                $this->viewData->transaction = $transaction;
            }
        }

        if (count($this->viewData->cart->products) <= 0) {
            $this->viewData->products = [];
        }
        $total = floatval($this->viewData->cart->total);
        $discountAmount = 0;
        $this->viewData->subtotal = $total;
        if (isset($_SESSION['coupon'])) {
            $coupon = Coupon::getItem(null, ['where' => "code = '" . $_SESSION['coupon'] . "'"]);
            if ((!is_null($coupon)
                && time() >= $coupon->start_time && time() < $coupon->end_time
                && $total >= $coupon->min_amount
                && Order::getCount(['where' => "coupon_code = '" . $coupon->code . "'"]) < $coupon->num_uses_all)
            ) {
                if ($coupon->discount_type == 1) {
                    $discountAmount = $coupon->discount_amount;
                } else if ($coupon->discount_type == 2) {
                    $discountAmount = $total * ($coupon->discount_amount / 100);
                }

                $total = $total - $discountAmount;
            }
        }
        $this->viewData->discountAmount = $discountAmount;
        $this->viewData->total = $total;

        
        $tokenOptions = [];
        if($this->viewData->user) {
            try{
                $btCustomer = Braintree\Customer::find($this->viewData->user->id);
                $tokenOptions["customerId"] = $this->viewData->user->id;
            } catch (Braintree\Exception $be) {

            }
        }
        $this->viewData->token = Braintree\ClientToken::generate($tokenOptions);

        $this->viewData->shipping_methods = [];
        $shipping_methods = Shipping_Method::getList(['orderBy' => 'id asc', 'where' => 'active = 1']);
        foreach ($shipping_methods as $shipping_method) {
            $shipping_method->cart_subtotal_range_min = json_decode($shipping_method->cart_subtotal_range_min);
            $this->viewData->shipping_methods[] = $shipping_method;
        }

        if (isset($_SESSION['checkoutPost']) && isset($_SESSION['checkoutPost']['shipping_method'])) {
            $this->viewData->shipping_method = $_SESSION['checkoutPost']['shipping_method'];

        } else {
            $this->viewData->shipping_method = 0;
        }
        parent::index($params);
    }

    private function updateCookies()
    {
        $this->viewData->cart->total = Product::getPriceByProducts($this->viewData->cart->products);
        $cookie = [];
        foreach($this->viewData->cart->products as $key => $item){
            $data = [];
            $data['product_id'] = $item->product->id;
            $reflect = new ReflectionClass($item->product);
            $data['product_type'] = $reflect->getShortName();

            //Ring Material
            if(isset($item->ringMaterial) && isset($item->size) && isset($item->ring_setting)){
                $data['size'] = $item->size;
                $data['ring_material_id'] = $item->ringMaterial->id;
                $data['ring_setting'] = $item->ring_setting;
            }

            //Extra Options
            if(isset($item->options)){
                $data['options'] = $item->options;
            }

            //Ring + Diamond Combo
            if(isset($item->optionalProduct)){
                $data['size'] = $item->size;
                $data['optional_product_id'] = $item->optionalProduct->id;
                $reflect = new ReflectionClass($item->optionalProduct);
                $data['optional_product_type'] = $reflect->getShortName();
            }
            $cookie[$key] = $data;
        }

        setcookie('cart', serialize($cookie), time() + 60 * 60 * 24 * 100, "/");
    }

    public function cancel_combo()
    {
        $this->destroyCombo();
        redirect('/');
    }

    public function add_post()
    {
        $item = new stdClass();
        if (isset($_POST['product_id']) && isset($_POST['category'])){
            $product = $this->productMap->getProduct($_POST['product_id'], $_POST['category']);
            if (!is_null($product)) {
                if(strtoupper($_POST['category']) == 'PRODUCT' && $product->quantity == 0){
                    echo json_encode(['status'=>'soldOut','message'=>'This product is sold out']);exit;
                }
                $item->product = $product;
            }
        }

        //Only applies to diamond (product_type_id = 1)
        if(isset($_POST['material']) && isset($_POST['size'])){
            $ringMaterial = Ring_Material::getItem(null, ['where' => ['name' => strtolower($_POST['material'])]]);
            $size = $_POST['size'];
            $ring_setting = \Model\Ring_Setting::$shape[$_POST['ring_setting']];

            $item->ringMaterial = $ringMaterial;
            $item->size = $size;
            $item->ring_setting = $ring_setting;
        }

        if(isset($_POST['option_product_id']) && isset($_POST['option_product_category'])){
            $optionalProduct = $this->productMap->getProduct($_POST['option_product_id'], $_POST['option_product_category']);
            $size = $_POST['size'];

            $item->optionalProduct = $optionalProduct;
            $item->size = $size;
        }

        //Mass sweep for all products not diamonds
        if(!isset($item->ringMaterial)) {
            $options = ['size', 'metal', 'color', 'shape', 'stone','stoneId', 'style', 'length', 'back'];
            foreach ($options as $option) {
                if (isset($_POST[$option]) && $_POST[$option] != '') {
                    $item->options[$option] = $_POST[$option];
                }
            }
        }


        if(isset($item->product) && $item->product){
            $this->viewData->cart->products[Product::generateToken()] = $item;
        }

        $this->updateCookies();

        $data = ['cart' => ['quantity' => count($this->viewData->cart->products), 'total' => $this->viewData->cart->total]];
        $this->destroyCombo();
        echo json_encode($data);
    }

    public function add_combo_post()
    {
        $size = 4.0;
        $color = 'White';
        $metal = '14kt gold';
        $shape = 'RB';
        $stone = null;
        $stoneId = isset($_SESSION['stoneId'])?$_SESSION['stoneId']:0;

        // check if diamond has quantity > 0
        if (isset($_POST['product_id']) && isset($_POST['product_type'])){
            $product = $this->productMap->getProduct($_POST['product_id'], $_POST['product_type']);
            if (!is_null($product)) {
                if(strtoupper($_POST['product_type']) == 'PRODUCT' && $product->quantity == 0){
                    echo json_encode(['status'=>'soldOut','message'=>'This product is sold out']);exit;
                }
            }
        }

        if(isset($_POST['product_type'])){
            $map = new \Model\Product_Map();
            if(isset($_POST['product_id'])){
                $map = $map->getProduct($_POST['product_id'], $_POST['product_type']);
                $_SESSION['product'] = serialize($map);
                if(!isset($_SESSION['ring'])){
                    $_SESSION['first'] = 'product';
                }
            }
            else if(isset($_POST['ring_id'])){
                $map = $map->getProduct($_POST['ring_id'], $_POST['product_type']);
                $stoneStr = isset($_POST['stoneId'])?$_POST['stoneId']:0;
                $metalStr = isset($_POST['metal'])?$_POST['metal']:$metal;
                $map->price = $map->getPrice($metalStr,$stoneStr) != null?$map->getPrice($metalStr,$stoneStr):$map->getWebsiteMetalPrice($metalStr);
                $_SESSION['ring'] = serialize($map);
                if(!isset($_SESSION['product'])){
                    $_SESSION['first'] = 'ring';
                }
                $_SESSION['size'] = isset($_POST['size']) ? $_POST['size']: $size;
                $_SESSION['selectMetal'] = isset($_POST['metal']) ? $_POST['metal']: $metal;
                $_SESSION['selectColor'] = isset($_POST['color']) ? $_POST['color']: $color;
                $_SESSION['selectShape'] = isset($_POST['shape']) ? $_POST['shape']: $shape;
                $_SESSION['stone'] = isset($_POST['stone']) ? $_POST['stone']: $stone;
                $_SESSION['stoneId'] = isset($_POST['stoneId']) ? $_POST['stoneId']: $stoneId;

            }
        }
        $this->toJson(['status' => 'success']);

    }

    private function destroyCombo()
    {
        $sesh = ['product','ring','first','size','selectMetal','selectColor','selectShape','stone','stoneId'];
        foreach($sesh as $ses) {
            unset($_SESSION[$ses]);
        }
    }

    public function add_ring_post()
    {
        //   if (isset($_POST['product_id']) && isset($_POST['category'])){
        //       $product = Product_Map::getProduct($_POST['product_id'], $_POST['category']);
        //       $ring = Ring_Material::getItem(null, ['where' => ['name' => strtolower($_POST['material'])]]);
        //       $size = $_POST['size'];
        //       $ring_setting = \Model\Ring_Setting::$shape[$_POST['ring_setting']];
        //       if ($product && $ring && $size) {
        //           $item = new stdClass();
        //           $item->product = $product;
        //           $item->ring = $ring;
        //           $item->size = $size;
        //           $item->ring_setting = $ring_setting;
        //
        //           $this->viewData->cart->products[Product::generateToken()] = $item;
        //       }
        //   }
        //
        //   $this->updateCookies();
        //
        //   $data = ['cart' => ['quantity' => count($this->viewData->cart->products), 'total' => $this->viewData->cart->total]];
        //   echo json_encode($data);
    }

    public function remove_product($params)
    {
        foreach ($this->viewData->cart->products as $key => $product) {
            if ($key == $params['id']) {
                unset($this->viewData->cart->products[$key]);
            }
        }
        $this->updateCookies();
        redirect(SITE_URL . 'cart');
    }

    //    public function delete_post()
    //    {
    //        $productId = $_POST['product_id'];
    //        foreach ($this->viewData->cart->products as $key => $product) {
    //            if ($product->key == $productId) {
    //                unset($this->viewData->cart->products[$key]);
    //            }
    //        }
    //        $this->updateCookies();
    //        $data = ['cart' => ['quantity' => count($this->viewData->cart->products), 'total' => $this->viewData->cart->total]];
    //        echo json_encode($data);
    //    }

    //This function is also executed in the ordersController after a new order is inserted

    public function remove_all()
    {
        $this->viewData->cart->products = [];
        $this->updateCookies();
        redirect(SITE_URL . 'checkout');
    }

    private function clearCart()
    {
        $this->viewData->cart->products = [];
        $this->updateCookies();
    }

    public function updateShipping(Array $params = [])
    {
        if (isset($_GET['id']) && is_numeric($_GET['id']) && !is_null(Shipping_Method::getItem($_GET['id']))) {
            setcookie('shippingMethod', $_GET['id'], time() + 60 * 60 * 24 * 100, "/");
        }
    }

    public function applyCoupon(Array $params = [])
    {
        if (isset($_GET['code'])) {
            $coupon = Coupon::getItem(null, ['where' => " code = '" . $_GET['code'] . "'"]);
            if (is_null($coupon)) {
                $_SESSION['coupon'] = null;
                echo json_encode(['error' => ['code' => 1, 'message' => 'Invalid coupon code.'], 'coupon' => null]);
            } else if (time() >= $coupon->start_time && time() < $coupon->end_time) {
                $cartTotal = (float)str_replace(',', '', $this->viewData->cart->total);
                if ($cartTotal >= $coupon->min_amount) {
                    $already_used = Order::getCount(['where' => "coupon_code = '" . $coupon->code . "'"]);
                    if (is_null($already_used)) {
                        $already_used = 0;
                    }

                    if (is_null($coupon->num_uses_all) || $already_used < $coupon->num_uses_all) {
                        $_SESSION['coupon'] = $coupon->code;
                        echo json_encode(['error' => ['code' => 0, 'message' => ''], 'coupon' => $coupon]);
                    } else {
                        $_SESSION['coupon'] = null;
                        echo json_encode(['error' => ['code' => 4, 'message' => 'Coupon sold out.'], 'coupon' => null]);
                    }
                } else {
                    $_SESSION['coupon'] = null;
                    echo json_encode(['error' => ['code' => 2, 'message' => 'Coupon requires a minimum amount of ' . $coupon->min_amount . '.'], 'coupon' => null]);
                }
            } else {
                $_SESSION['coupon'] = null;
                echo json_encode(['error' => ['code' => 3, 'message' => 'Coupon currently unavailable.'], 'coupon' => null]);
            }
        } else {
            $_SESSION['coupon'] = null;
            echo json_encode(['error' => ['code' => 1, 'message' => 'Invalid coupon code.'], 'coupon' => null]);
        }
    }

//     public function pay_post()
//     {
//         $_SESSION['checkoutPost'] = $_POST;
//         if (!$this->viewData->user) {
//             $guest = true;
//         } else {
//             $guest = false;
//         }
//         foreach ($this->viewData->cart->products as $product) {
//             //if product is diamond, check quantity
//             if ($product->product->getMap()->product_type_id == '1') {
//                 if($product->product->quantity == 0){
//                     $n = new \Notification\ErrorHandler("{$product->product->name} is sold out");
//                     $_SESSION["notification"] = serialize($n);
//                     redirect(SITE_URL . 'checkout');
//                 }
//             }
//         }

//         if (count($this->viewData->cart->products) > 0 && ($this->viewData->user || $guest)) {
//             $order = \Model\Order::loadFromPost();

//             if (!$guest) {
//                 $order->user_id = $this->viewData->user->id;
//             } else {
//                 $order->user_id = 0;
//             }

//             $order->status = Order::$status[0];

//             if (is_null(\Model\Shipping_Method::getItem($order->shipping_method))) {
//                 $n = new \Notification\ErrorHandler('You must choose a valid shipping method.');
//                 $_SESSION["notification"] = serialize($n);
//                 redirect('/checkout');
//             }

//             if(isset($_SESSION['coupon']) && $_SESSION['coupon'] != ''){
//                 $order->coupon_code = $_SESSION['coupon'];
//             }

//             $order->insert_time = new DateTime();
//             $order->insert_time = date_format($order->insert_time, 'm/d/Y H:i:s');
//             $coupon = null;
//             if (!is_null($order->coupon_code)) {
//                 $coupon = \Model\Coupon::getItem(null, ['where' => "code = '" . $order->coupon_code . "'"]);
//                 $order->coupon_type = $coupon->discount_type;
//                 $order->coupon_amount = $coupon->discount_amount;
//                 if (is_null($coupon)
//                     || (!is_null($coupon)
//                         && (time() < $coupon->start_time || time() >= $coupon->end_time
//                             || floatval(\Model\Product::getPriceByProducts($this->viewData->cart->products)) < $coupon->min_amount
//                             || \Model\Order::getCount(['where' => "coupon_code = '" . $coupon->code . "'"]) >= $coupon->num_uses_all)
//                     )
//                 ) {
//                     $n = new \Notification\ErrorHandler('Invalid coupon.');
//                     $_SESSION['coupon'] = null;
//                     $_SESSION["notification"] = serialize($n);
//                     redirect(SITE_URL . 'checkout');
//                 }
//             } else {
//                 $order->coupon_code = null;
//             }
//             $order->payment_method = isset($_POST['is_wired']) ? 2 : 1;
//             $order->ship_country = 'United States';
//             $order->bill_country = 'United States';
//             $order->ref_num = Product::generateToken();
//             if ($_POST['is_same'] == 'on') {
//                 $order->bill_first_name = $order->ship_first_name;
//                 $order->bill_last_name = $order->ship_last_name;
//                 $order->bill_address = $order->ship_address;
//                 $order->bill_address2 = $order->ship_address2;
//                 $order->bill_city = $order->ship_city;
//                 $order->bill_state = $order->ship_state;
//                 $order->bill_country = $order->ship_country;
//                 $order->bill_zip = $order->ship_zip;
//             }

//             if ($order->save()) {
//                 $order->subtotal = 0;

//                 foreach ($this->viewData->cart->products as $product) {
//                     //if product is diamond, set quantity to 0
//                     if($product->product->getMap()->product_type_id == '1'){
//                         $product->product->quantity = $product->product->quantity -1;
//                         $product->product->save();
//                     }

//                     $order_product = new \Model\Order_Product();
//                     $order_product->order_id = $order->id;
//                     $order_product->product_map_id = $product->productMap->id;
//                     $order_product->quantity = 1;
//                     $order_product->unit_price = $product->product->price;
//                     $order_product->save();

//                     if(isset($product->ringMaterial) && isset($product->size) && isset($product->ring_setting)){
//                         $option = new \Model\Order_Product_Option();
//                         $option->price = $product->ringMaterial->size_price($product->size) + $product->ringMaterial->price;
//                         $option->data = json_encode([
//                             'ringMaterial' => $product->ringMaterial,
//                             'size' => $product->size,
//                             'ringSetting' => $product->ring_setting
//                         ]);
//                         $option->order_product_id = $order_product->id;
//                         $option->save();
//                     }

//                     if(isset($product->optionalProduct)){
//                         $option = new \Model\Order_Product_Option();
//                         $option->price = $product->optionalProduct->getPrice($product->optionalProduct->default_metal, $product->options['stoneId']);
//                         $option->data = json_encode([
//                             'optionalProduct'=> [
//                                 'product_map_id' => $product->optionalProduct->getMap()->id,
//                             ],
//                             'size'=>$product->options['size'],
//                             'color'=>$product->optionalProduct->default_color,
//                             'metal'=>$product->optionalProduct->default_metal,
//                             'shape'=>$product->options['shape'],
//                             'stone'=>$product->options['stone'],
//                             'stoneId'=>$product->options['stoneId']
//                         ]);
//                         $option->order_product_id = $order_product->id;
//                         $option->save();
//                     } else if(isset($product->options)){
//                         $option = new \Model\Order_Product_Option();
//                         $option->price = 0;
//                         $option->order_product_id = $order_product->id;
//                         $option->data = json_encode($product->options);
//                         $option->save();
//                     }
//                 }

//                 $order->total = $order->subtotal = $this->viewData->cart->total;
//                 if ($coupon) {
//                     $_SESSION['coupon'] = null;
//                     if ($coupon->discount_type == 1) {//$
//                         $order->total = $order->total - $coupon->discount_amount;
//                     } else if ($coupon->discount_type == 2) {//%
//                         $order->total = $order->total * (1 - $coupon->discount_amount / 100);
//                     }
//                 }

//                 if ($order->ship_state == 'NY') {
//                     $order->tax_rate = 8.875;
//                     $order->tax = $order->total * ($order->tax_rate / 100);
//                 } else {
//                     $order->tax_rate = null;
//                     $order->tax = null;
//                 }

//                 $shipping_method = \Model\Shipping_Method::getItem($order->shipping_method);
//                 $order->shipping_cost = floatval($shipping_method->cost);
//                 $order->total = $order->shipping_cost + $order->total + $order->tax;

//                 if ($order->payment_method == 2){
//                     $order->total = $order->total * 0.98;
//                 }

//                 $order->save();
//                 if (!$guest) {
//                     $payment_profile = \Model\Payment_Profile::getItem(null, ['where'=>['user_id'=>$this->viewData->user->id]]);
//                     $pp = ['user_id'=>$order->user_id,
//                         'country'=>$order->bill_country,
//                         'first_name'=>$order->bill_first_name,
//                         'last_name'=>$order->bill_last_name,
//                         'address'=>$order->bill_address,
//                         'address2'=>$order->bill_address2,
//                         'city'=>$order->bill_city,
//                         'state'=>$order->bill_state,
//                         'zip'=>$order->bill_zip,
//                         'phone'=>$order->phone,
//                         'cc_number'=>$order->cc_number,
//                         'cc_expiration_month'=>$order->cc_expiration_month,
//                         'cc_expiration_year'=>$order->cc_expiration_year,
//                         'cc_ccv'=>$order->cc_ccv];

//                     $payment = ['user_id'=>$payment_profile->user_id,
//                         'country'=>$payment_profile->country,
//                         'first_name'=>$payment_profile->first_name,
//                         'last_name'=>$payment_profile->last_name,
//                         'address'=>$payment_profile->address,
//                         'address2'=>$payment_profile->address2,
//                         'city'=>$payment_profile->city,
//                         'state'=>$payment_profile->state,
//                         'zip'=>$payment_profile->zip,
//                         'phone'=>$payment_profile->phone,
//                         'cc_number'=>$payment_profile->cc_number,
//                         'cc_expiration_month'=>$payment_profile->cc_expiration_month,
//                         'cc_expiration_year'=>$payment_profile->cc_expiration_year,
//                         'cc_ccv'=>$payment_profile->cc_ccv];
//                     //if user is logged in and payment_profile returns null
//                     if (isset($payment_profile) && $payment_profile != "" && is_numeric($payment_profile->id)) {
//                         //when user changes billing info
//                         if(count(array_intersect($payment, $pp)) != count($payment)){
//                             foreach($payment as $key=>$value){
//                                 $payment_profile->{$key} = $pp{$key};
//                             }
//                             $payment_profile->save();
//                         }
//                     } else {
//                         $payment_profile = new \Model\Payment_Profile();
//                         foreach($payment as $key=>$value){
//                             $payment_profile->{$key} = $pp{$key};
//                         }
//                         $payment_profile->save();
//                     }

//                     $address = \Model\Address::getItem(null, ['where'=>['user_id'=>$this->viewData->user->id]]);
//                     $sa = ['user_id'=>$order->user_id,
//                         'country'=>$order->ship_country,
//                         'first_name'=>$order->ship_first_name,
//                         'last_name'=>$order->ship_last_name,
//                         'address'=>$order->ship_address,
//                         'address2'=>$order->ship_address2,
//                         'city'=>$order->ship_city,
//                         'state'=>$order->ship_state,
//                         'zip'=>$order->ship_zip,
//                         'phone'=>$order->phone];
//                     $shipping = ['user_id'=>$address->user_id,
//                         'country'=>$address->country,
//                         'first_name'=>$address->first_name,
//                         'last_name'=>$address->last_name,
//                         'address'=>$address->address,
//                         'address2'=>$address->address2,
//                         'city'=>$address->city,
//                         'state'=>$address->state,
//                         'zip'=>$address->zip,
//                         'phone'=>$address->phone];
//                     if (isset($address) && $address != "" && is_numeric($address->id)) {
//                         //when user changes shipping info
//                         if(count(array_intersect($shipping, $sa)) != count($shipping)) {
//                             foreach($shipping as $key=>$value){
//                                 $address->{$key} = $sa{$key};
//                             }
//                             $address->save();
//                         }
//                     } else {
//                         $address = new \Model\Address();
//                         foreach($shipping as $key=>$value){
//                             $address->{$key} = $sa{$key};
//                         }
//                         $address->save();
//                     }
//                 }
//                 if ($order->payment_method == 1) {
//                     $tran = new Services\PaymentService();
//                     $tran->setCard($order->cc_number)->setExp($order->cc_expiration_month, $order->cc_expiration_year)
//                         ->setAmount($order->total)->setOrderId($order->id)->setCCV($order->cc_ccv)
//                         ->setBillingAddress($order)->setCommand('cc:authonly');
//                     $tran->process();
//                     $localTransaction = new \Model\Transaction();
//                     $localTransaction->order_id = $order->id;
//                     $localTransaction->authorize_net_data = serialize($tran->getResult());
//                     $localTransaction->ref_trans_id = $tran->getRefNum();
//                     $localTransaction->save();

//                     if ($tran->isSuccess()) {
//                         $order->sendOrderSuccessEmail();

//                         $n = new \Notification\MessageHandler('Order successfully placed!', $guest);
//                         $_SESSION["notification"] = serialize($n);

//                         $this->clearCart();

//                         $_SESSION['purchase_conversion'] = true;

//                         redirect(SITE_URL . 'status/' . $order->ref_num);
//                     } else {
//                         $order->status = \Model\Order::$status[2];
//                         $order->save();

//                         $n = new \Notification\ErrorHandler($tran->getError());
//                         $_SESSION["notification"] = serialize($n);
//                         if ($guest) {
//                             redirect(SITE_URL . 'checkout');
//                         } else {
//                             redirect(SITE_URL . 'checkout');
//                         }
//                     }
//                 } else {
// //                      this payment not implement yet
//                     $order->sendOrderSuccessEmail();

//                     $n = new \Notification\MessageHandler('Order successfully placed!', $guest);
//                     $_SESSION["notification"] = serialize($n);

//                     $this->clearCart();

//                     $_SESSION['purchase_conversion'] = true;

//                     redirect(SITE_URL . 'status/' . $order->ref_num);
//                 }
//             } else {
//                 $n = new \Notification\ErrorHandler($order->errors);
//                 $_SESSION["notification"] = serialize($n);
//                 redirect(SITE_URL . 'checkout');
//             }
//         } else {
//             $n = new \Notification\ErrorHandler('Your cart is empty.');
//             $_SESSION["notification"] = serialize($n);
//             redirect(SITE_URL . 'checkout');
//         }
//     }

    // public function index_post(){
        
    //     // define("AUTHORIZENET_LOG_FILE", "phplog");

    //     $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
    //     $merchantAuthentication->setName(AUTHORIZENET_API_LOGIN_ID);
    //     $merchantAuthentication->setTransactionKey(AUTHORIZENET_TRANSACTION_KEY);

    //     $request = new AnetAPI\CreateTransactionRequest();
    //     $request->setMerchantAuthentication($merchantAuthentication);

    //     $transaction = null;
    //     if(isset($_POST['id']) && $_POST['id'] != ''){
    //         $transaction_id = $_POST['id'];
    //         $transaction = \Model\Transaction::getItem($transaction_id);
    //         $transaction->ship_first_name = $_POST['ship_first_name'];
    //         $transaction->ship_last_name = $_POST['ship_last_name'];
    //         $transaction->email = $_POST['email'];
    //         $transaction->phone = $_POST['phone'];
    //         $transaction->ship_address = $_POST['ship_address'];
    //         $transaction->ship_address2 = $_POST['ship_address2'];
    //         $transaction->ship_city = $_POST['ship_city'];
    //         $transaction->ship_state = $_POST['ship_state'];
    //         $transaction->ship_country = $_POST['ship_country'];
    //         $transaction->ship_zip = $_POST['ship_zip'];
    //         $transaction->payment_status = \Model\Transaction::$pay_status[0];
    //     } else {
    //         $transaction = new \Model\Transaction($_POST);
    //         $transaction->payment_status = \Model\Transaction::$pay_status[0];
    //         $transaction->type = \Model\Transaction::$type[1];
    //     }

    //     if($transaction->save()){
    //         // if(!$transaction->slug){
    //         //     $transaction->slug = $transaction->generateSlug();
    //         // }
    //         $transaction->ref_num = $transaction->generateToken();
        
    //         // Create the payment data for a credit card
    //         $creditCard = new AnetAPI\CreditCardType();
    //         $creditCard->setCardNumber($_POST['cc_number']);
    //         $creditCard->setExpirationDate($_POST['cc_expiration_year']."-".$_POST['cc_expiration_month']);
    //         $creditCard->setCardCode($_POST['cc_ccv']);
            
    //         // Add the payment data to a paymentType object
    //         $paymentOne = new AnetAPI\PaymentType();
    //         $paymentOne->setCreditCard($creditCard);
            
    //         // Create order information
    //         $order = new AnetAPI\OrderType();
    //         $order->setInvoiceNumber($transaction->id);
    //         $order->setDescription("Payment at Henrythedentist.com");
            
    //         // Set the customer's Bill To address
    //         $customerAddress = new AnetAPI\CustomerAddressType();
    //         $customerAddress->setFirstName($transaction->ship_first_name);
    //         $customerAddress->setLastName($transaction->ship_last_name);
    //         $customerAddress->setAddress($transaction->ship_address);
    //         $customerAddress->setCity($transaction->ship_city);
    //         $customerAddress->setState($transaction->ship_state);
    //         $customerAddress->setZip($transaction->ship_zip);
    //         $customerAddress->setCountry($transaction->ship_country);

    //         // Set the customer's identifying information
    //         $customerData = new AnetAPI\CustomerDataType();
    //         $customerData->setType("individual");
    //         // $customerData->setId("99999456654");
    //         $customerData->setEmail($transaction->email);
            
    //         // Add values for transaction settings
    //         $duplicateWindowSetting = new AnetAPI\SettingType();
    //         $duplicateWindowSetting->setSettingName("duplicateWindow");
    //         $duplicateWindowSetting->setSettingValue("60");

    //         // Add some merchant defined fields. These fields won't be stored with the transaction,
    //         // but will be echoed back in the response.
    //         // $merchantDefinedField1 = new AnetAPI\UserFieldType();
    //         // $merchantDefinedField1->setName("customerLoyaltyNum");
    //         // $merchantDefinedField1->setValue("1128836273");
    //         // $merchantDefinedField2 = new AnetAPI\UserFieldType();
    //         // $merchantDefinedField2->setName("favoriteColor");
    //         // $merchantDefinedField2->setValue("blue");

    //         // Create a TransactionRequestType object and add the previous objects to it
    //         $transactionRequestType = new AnetAPI\TransactionRequestType();
    //         $transactionRequestType->setTransactionType("authCaptureTransaction");
    //         $transactionRequestType->setAmount(floatval($_POST['amount']));
    //         $transactionRequestType->setOrder($order);
    //         $transactionRequestType->setPayment($paymentOne);
    //         $transactionRequestType->setBillTo($customerAddress);
    //         // $transactionRequestType->setCustomer($customerData);
    //         $transactionRequestType->addToTransactionSettings($duplicateWindowSetting);
    //         // $transactionRequestType->addToUserFields($merchantDefinedField1);
    //         // $transactionRequestType->addToUserFields($merchantDefinedField2);

    //         // Assemble the complete transaction request
    //         $request = new AnetAPI\CreateTransactionRequest();
    //         $request->setMerchantAuthentication($merchantAuthentication);
    //         $request->setRefId($transaction->id);
    //         $request->setTransactionRequest($transactionRequestType);
            
    //         // Create the controller and get the response
    //         $controller = new AnetController\CreateTransactionController($request);
    //         $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
            
    //         if ($response != null) {
    //             // Check to see if the API request was successfully received and acted upon
    //             if ($response->getMessages()->getResultCode() == "Ok") {
    //                 // Since the API request was successful, look for a transaction response
    //                 // and parse it to display the results of authorizing the card
    //                 $tresponse = $response->getTransactionResponse();
                
    //                 // if ($tresponse != null && $tresponse->getMessages() != null) {
    //                 //     echo " Successfully created transaction with Transaction ID: " . $tresponse->getTransId() . "\n";
    //                 //     echo " Transaction Response Code: " . $tresponse->getResponseCode() . "\n";
    //                 //     echo " Message Code: " . $tresponse->getMessages()[0]->getCode() . "\n";
    //                 //     echo " Auth Code: " . $tresponse->getAuthCode() . "\n";
    //                 //     echo " Description: " . $tresponse->getMessages()[0]->getDescription() . "\n";
    //                 // } else {
    //                 //     echo "Transaction Failed \n";
    //                 //     if ($tresponse->getErrors() != null) {
    //                 //         echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
    //                 //         echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
    //                 //     }
    //                 // }
        
    //                 if($transaction->save()){

    //                     $email = new \Email\MailMaster();
    //                     $mergeTags = [
    //                         'CONTENT'=>"
    //                         Order #: ".$transaction->id."<br /> Reference #: ".$transaction->ref_num."<br /> Name:".$transaction->full_name()."<br /> Email: ".$transaction->email."<br /> Phone: ".$transaction->phone."<br /> Amount: ".$transaction->amount." <br /> Address: ".$transaction->ship_address.", ".$transaction->ship_address2."<br />".$transaction->ship_city." ".$transaction->ship_state." ".$transaction->ship_zip
    //                     ];

    //                     $email->setTo(['email' => $transaction->email, 'type' => 'to'])->setTemplate('henry-order-submission')->setMergeTags($mergeTags);

    //                     try{
    //                         $email->send();
    //                         $n = new \Notification\MessageHandler("Transaction successfully completed");
    //                         $_SESSION["notification"] = serialize($n);
    //                         redirect(SITE_URL . 'checkout/confirmation/'. $transaction->ref_num);
    //                     } catch(Mandrill_Error $e){
    //                         $n = new \Notification\ErrorHandler('Email not sent');
    //                         $_SESSION["notification"] = serialize($n);
    //                         redirect(SITE_URL . 'checkout');
    //                     }
    //                 }

    //                 // Or, print errors if the API request wasn't successful
    //             } else {
                    
    //                 $error = '';
                    
    //                 $tresponse = $response->getTransactionResponse();
    //                 if ($tresponse != null && $tresponse->getErrors() != null) {
    //                     // echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
    //                     // echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
    //                     $error = $tresponse->getErrors()[0]->getErrorText();
    //                 } else {
    //                     // echo " Error Code  : " . $response->getMessages()->getMessage()[0]->getCode() . "\n";
    //                     // echo " Error Message : " . $response->getMessages()->getMessage()[0]->getText() . "\n";
    //                     $error = $response->getMessages()->getMessage()[0]->getText();
    //                 } 
    //                 $n = new \Notification\ErrorHandler("Transaction Failed: $error");
    //                 $_SESSION["notification"] = serialize($n);
                
                    
    //             }
    //         } else {
    //             $n = new \Notification\ErrorHandler("Transaction Failed");
    //             $_SESSION["notification"] = serialize($n);
    //         }
    //         if($transaction->slug){
    //             redirect(SITE_URL . "checkout/c/$transaction->slug");
    //         }
    //         redirect(SITE_URL . 'checkout');
            
    //     } else {
    //         $n = new \Notification\ErrorHandler($transaction->errors);
    //         $_SESSION["notification"] = serialize($n);
    //         redirect(SITE_URL . 'checkout');
    //     }
    // }

    public function index_post(){
        $transaction = null;
        if(isset($_POST['id']) && $_POST['id'] != ''){
            $transaction_id = $_POST['id'];
            $transaction = \Model\Transaction::getItem($transaction_id);
            $transaction->ship_first_name = $_POST['ship_first_name'];
            $transaction->ship_last_name = $_POST['ship_last_name'];
            $transaction->email = $_POST['email'];
            $transaction->phone = $_POST['phone'];
            $transaction->ship_address = $_POST['ship_address'];
            $transaction->ship_address2 = $_POST['ship_address2'];
            $transaction->ship_city = $_POST['ship_city'];
            $transaction->ship_state = $_POST['ship_state'];
            $transaction->ship_country = $_POST['ship_country'];
            $transaction->ship_zip = $_POST['ship_zip'];
            $transaction->payment_status = \Model\Transaction::$pay_status[0];
        } else {
            $transaction = \Model\Transaction::loadFromPost();
            $transaction->payment_status = \Model\Transaction::$pay_status[0];
            $transaction->type = \Model\Transaction::$type[1];
        }

        $sale=[];
        $customer = [];
        $customer['phone']      = $transaction->phone;
        $customer['firstName']  = $transaction->ship_first_name;
        $customer['lastName']   = $transaction->ship_last_name;
        if($transaction->patient_id){
            $customer['id'] = $transaction->patient_id;
            $collection = Braintree\Customer::search([
                Braintree\CustomerSearch::id()->is($transaction->patient_id)
            ]);
            if(count($collection) > 0){

            } else {
                Braintree\Customer::create([
                    $customer
                ]);
            }
            $sale['customerId'] = $transaction->patient_id;
        } else {
            $sale['customer'] = $customer;
        }

        // if(!$transaction->slug){
        //     $transaction->slug = $transaction->generateSlug();
        // }
        $transaction->ref_num = $transaction->generateToken();
        
        // Create order information
        $descriptor = [];
        $descriptor['name'] = "HENRY the Dentist";
        $descriptor['url'] = "henrythedentist.com";
        
        // Set the customer's Billing info
        $billing = [];
        $billing['countryName']     = $transaction->ship_country;
        $billing['firstName']       = $transaction->ship_first_name;
        $billing['lastName']        = $transaction->ship_last_name;
        $billing['postalCode']      = $transaction->ship_zip;
        $billing['region']          = $transaction->ship_state;
        $billing['locality']        = $transaction->ship_city;
        $billing['streetAddress']   = $transaction->ship_address;

        $sale['billing'] = $billing;
        $sale['amount'] = $transaction->amount;
        $sale['paymentMethodNonce'] = $_POST['nonce'];
        $sale['options'] = [
            'submitForSettlement'=>true,
            'addBillingAddressToPaymentMethod' =>true,
        ];
        $result = Braintree\Transaction::sale($sale);

        $bTransaction = $result->transaction;

        ini_set('xdebug.show_mem_delta', 1);
        ini_set('xdebug.trace_enable_trigger', -1);
        ini_set('xdebug.auto_trace', -1);
        ini_set('xdebug.collect_assignments', -1);
        ini_set('xdebug.collect_includes', -1);
        ini_set('xdebug.collect_return', -1);
        ini_set('xdebug.trace_output_dir', -1);
        ini_set('xdebug.trace_output_name', -1);
        ini_set('xdebug.collect_params', 4);

        if($transaction->save()){
            if ($result->success) {
                $transaction->ref_trans_id = $bTransaction->id;
                $transaction->payment_status = \Model\Transaction::$pay_status[0];
            
                if($transaction->save()){

                    $email = new \Email\MailMaster();
                    $mergeTags = [
                        'FIRST_NAME'=>$transaction->ship_first_name,
                        'LAST_NAME'=>$transaction->ship_last_name,
                        'AMOUNT'=>"$".number_format($transaction->amount,2),
                        'RECEIVED_DATE'=>date("m/d/y"),
                        'ORDER_NUMBER'=>$transaction->id,
                        'CONTENT'=>"Order #: ".$transaction->id."<br /> Reference #: ".$transaction->ref_num."<br /> Name:".$transaction->full_name()."<br /> Email: ".$transaction->email."<br /> Phone: ".$transaction->phone."<br /> Amount: ".$transaction->amount." <br /> Address: ".$transaction->ship_address.", ".$transaction->ship_address2."<br />".$transaction->ship_city." ".$transaction->ship_state." ".$transaction->ship_zip
                    ];

                    $email->setTo(['email' => $transaction->email, 'type' => 'to'])
                    ->setTemplate('henry-order-submission')
                    ->setMergeTags($mergeTags)
                    ->noBCC()
                    ->setCC([['email'=>"billing@HENRYthedentist.com", 'name'=>"HENRY billing", 'type'=>"bcc"]]);
                    
                    try{
                        $email->send();
                        $n = new \Notification\MessageHandler("Payment Successful!");
                        $_SESSION["notification"] = serialize($n);
                        redirect(SITE_URL . 'checkout/confirmation/'. $transaction->ref_num);
                    } catch(Mandrill_Error $e){
                        $n = new \Notification\ErrorHandler('Payment accepted, email not sent');
                        $_SESSION["notification"] = serialize($n);
                    }
                }
            } else {
                $transaction->payment_status = \Model\Transaction::$pay_status[2];
                $transaction->save();

                $note_errors = "<br />";

                $errors = [['message'=>"Transaction Failed"]];
                if(false && count($result->errors->deepAll())) {
                    foreach($result->errors->deepAll() as $error){
                        $errors[] = ["message"=>"$error->message"];
                        $note_errors .= $error->message."<br/>";
                    }
                } else {
                    $errors[] = ["message"=>"$result->message"];
                    $note_errors .= $result->message."<br/>";
                }
                
                try{
                    $email = new \Email\MailMaster();
                    $email->setTo(['email' => "billing@HENRYthedentist.com", 'type' => 'to'])
                    ->setSubject('Customer Payment Failed')
                    ->setTemplate('henry-blank')
                    ->setMergeTags(['TITLE'=>"Customer payment failed",'CONTENT'=>"Customer ".$transaction->full_name()." payment failed. Failed order #: $transaction->id.<br/> $note_errors"])
                    ->noBCC()
                    ->send();
                }
                catch(Mandrill_Error $e){
                }
                $n = new \Notification\ErrorHandler($errors);
                $_SESSION["notification"] = serialize($n);
            }
        } else {
            $n = new \Notification\ErrorHandler($transaction->errors);
            $_SESSION["notification"] = serialize($n);
        }
        if($transaction->slug){
            redirect(SITE_URL . "checkout/c/$transaction->slug");
        } else {
            redirect(SITE_URL . 'checkout');
        }
    }

    public function confirmation(Array $params = []){
        $this->configs['Meta Title'] = "Confirmation | HENRY The Dentist";
        
        if(isset($params['id']) && $params != ''){
            $ref_num = $params['id'];
            $confirmation = \Model\Transaction::getItem(null,['where'=>"ref_num = '".$ref_num."'"]);
            $this->viewData->order = '';
            if($confirmation){
                global $emagid;
                $timeSql = "SELECT insert_time FROM public.transaction where id = $confirmation->id";
                $db = $emagid->getDb();
                $results = $db->getResults($timeSql);
                $confirmation->insert_time = $results[0]['insert_time'];
                $confirmation->insert_time = new DateTime($confirmation->insert_time);
                $tz = new DateTimeZone('America/New_York');
                $confirmation->insert_time->setTimezone($tz);
                $confirmation->insert_time = $confirmation->insert_time->format('l F j, Y.  h:i a');
                $this->viewData->order = $confirmation;
            }
        }
        parent::index($params);
    }

}




























