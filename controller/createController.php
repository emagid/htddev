<?php

class createController extends siteController
{
    function __construct()
    {
        parent::__construct();
    }

    public function index(Array $params = [])
    {
        //$this->template = false;
        $this->loadView($this->viewData);
    }
}