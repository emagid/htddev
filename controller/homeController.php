<?php

class homeController extends siteController {
        function __construct(){
        parent::__construct();
    }    
    public function index(Array $params = []){

        $this->configs['Meta Title'] = "Home | HENRY The Dentist";
        $this->viewData->banner = \Model\Banner::getItem(null,['where'=>"page = 'home'"]);
       
        $this->viewData->mid_content = \Model\Home_Content::getItem(null,['where'=>"section = 'middle'"]);

        $this->viewData->left_content = \Model\Home_Content::getItem(null,['where'=>"section = 'left'"]);
        // var_dump($this->viewData->left_content);
        // exit;
        $this->viewData->right_content = \Model\Home_Content::getItem(null,['where'=>"section = 'right'"]);

        $this->viewData->media_logos = \Model\Media_Logo::getList(['where'=>"active = 1",'orderBy'=>"id asc"]);
        $this->viewData->quotes = \Model\Quote::getItem(null,['where'=>"active = 1"]);
        $this->viewData->banners = \Model\Banner::getList(['where'=>"active='1'"]);
        $this->viewData->mainBanner = \Model\Banner::getList(['where'=>"active = 1 and featured_id = 0", 'orderBy'=>"banner_order asc"]);
        $this->viewData->jewelry = \Model\Jewelry::getList(['where' => "active = 1 and featured = 1 and discount != 0", 'orderBy' => "RANDOM()", 'limit' => 2]);

        $this->viewData->diamond = \Model\Product::getItem(null, ['where' => "active = 1 and featured = 1 and discount != 0 and quantity > 0", 'orderBy' => "RANDOM()", 'limit' => 1]);
        $this->viewData->ring = \Model\Ring::getItem(null, ['where' => "active = 1 and featured = 1 and discount != 0", 'orderBy' => "RANDOM()", 'limit' => 1]);
        
        $this->loadView($this->viewData);
    }
}