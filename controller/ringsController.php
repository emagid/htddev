<?php
/**
 * Created by PhpStorm.
 * User: Simon
 * Date: 12/8/2015
 * Time: 6:01 PM
 */

class ringsController extends siteController {

    public function index(Array $params = []){
        $this->viewData->ring = $ring = \Model\Ring::getItem(null, ['where' => ['slug'=> $params['slug']]]);
        $this->viewData->recommendWb = $weddingBand = \Model\WeddingBand::getItem(null,['where'=>"slug = '{$ring->slug}'"]);
        $this->viewData->recommendedItems = \Model\Ring::randomizer();
        $this->configs['Meta Title'] = $ring->meta_title;
        $this->configs['Meta Keywords'] = $ring->meta_keywords;
        $this->configs['Meta Description'] = $ring->meta_description;
        $this->loadView($this->viewData);
    }
}