<?php

class experienceController extends siteController
{

    public function index(Array $params = [])
    {

        $this->configs['Meta Title'] = "Experience | HENRY The Dentist";
        $this->configs['Meta Description'] = "See the many services HENRY The Dentist can provide for you. From
        regular cleanings to fillings and whitening, HENRY does it all!";
        $this->configs['Meta Keywords'] = "experience";
        $this->loadView($this->viewData);
    }    
}