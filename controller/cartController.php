<?php
use Model\Coupon;
use Model\Product;
use Model\Product_Map;
use Model\Ring_Material;
use Model\Shipping_Method;
use Model\Order;

class cartController extends siteController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Array $params = [])
    {
        if (count($this->viewData->cart->products) <= 0) {
            $this->viewData->products = [];
        }
        $total = floatval($this->viewData->cart->total);
        $discountAmount = 0;
        if (isset($_SESSION['coupon'])) {
            $coupon = Coupon::getItem(null, ['where' => "code = '" . $_SESSION['coupon'] . "'"]);
            if ((!is_null($coupon)
                && time() >= $coupon->start_time && time() < $coupon->end_time
                && $total >= $coupon->min_amount
                && Order::getCount(['where' => "coupon_code = '" . $coupon->code . "'"]) < $coupon->num_uses_all)
            ) {
                if ($coupon->discount_type == 1) {
                    $discountAmount = $coupon->discount_amount;
                } else if ($coupon->discount_type == 2) {
                    $discountAmount = $total * ($coupon->discount_amount / 100);
                }

                $total = $total - $discountAmount;
            }
        }
        $this->viewData->subtotal = floatval($this->viewData->cart->total);
        $this->viewData->discountAmount = $discountAmount;
        $this->viewData->total = $total;

        $this->viewData->shipping_methods = [];
        $shipping_methods = Shipping_Method::getList(['orderBy' => 'id asc', 'where' => 'active = 1']);
        foreach ($shipping_methods as $shipping_method) {
            $shipping_method->cart_subtotal_range_min = json_decode($shipping_method->cart_subtotal_range_min);
//            if ($total >= $shipping_method->cart_subtotal_range_min) {
//                $shipping_method->cost = 0;
//            }
            $this->viewData->shipping_methods[] = $shipping_method;
        }

        if (isset($_SESSION['checkoutPost']) && isset($_SESSION['checkoutPost']['shipping_method'])) {
            $this->viewData->shipping_method = $_SESSION['checkoutPost']['shipping_method'];

        } else {
            $this->viewData->shipping_method = 0;
        }
        parent::index($params);
    }
    

//    public function applyCoupon(Array $params = [])
//    {
//        if (isset($_GET['code'])) {
//            $coupon = Coupon::getItem(null, ['where' => " code = '" . $_GET['code'] . "'"]);
//            if (is_null($coupon)) {
//                $_SESSION['coupon'] = null;
//                echo json_encode(['error' => ['code' => 1, 'message' => 'Invalid coupon code.'], 'coupon' => null]);
//            } else if (time() >= $coupon->start_time && time() < $coupon->end_time) {
//                $cartTotal = (float)str_replace(',', '', $this->viewData->cart->total);
//                if ($cartTotal >= $coupon->min_amount) {
//                    $already_used = Order::getCount(['where' => "coupon_code = '" . $coupon->code . "'"]);
//                    if (is_null($already_used)) {
//                        $already_used = 0;
//                    }
//
//                    if (is_null($coupon->num_uses_all) || $already_used < $coupon->num_uses_all) {
//                        $_SESSION['coupon'] = $coupon->code;
//                        echo json_encode(['error' => ['code' => 0, 'message' => ''], 'coupon' => $coupon]);
//                    } else {
//                        $_SESSION['coupon'] = null;
//                        echo json_encode(['error' => ['code' => 4, 'message' => 'Coupon sold out.'], 'coupon' => null]);
//                    }
//                } else {
//                    $_SESSION['coupon'] = null;
//                    echo json_encode(['error' => ['code' => 2, 'message' => 'Coupon requires a minimum amount of ' . $coupon->min_amount . '.'], 'coupon' => null]);
//                }
//            } else {
//                $_SESSION['coupon'] = null;
//                echo json_encode(['error' => ['code' => 3, 'message' => 'Coupon currently unavailable.'], 'coupon' => null]);
//            }
//        } else {
//            $_SESSION['coupon'] = null;
//            echo json_encode(['error' => ['code' => 1, 'message' => 'Invalid coupon code.'], 'coupon' => null]);
//        }
//    }


}




























