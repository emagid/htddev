<?php

class hipaaController extends siteController
{

    public function index(Array $params = [])
    {
        
        $this->viewData->hipaa = \Model\Hipaa::getItem(null,['where'=>"active = 1"]);

        $this->configs['Meta Title'] = "Hipaa";
        $this->loadView($this->viewData);
    }

}