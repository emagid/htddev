<?php

class categoryController extends siteController
{
    function __construct()
    {
        parent::__construct();
    }

    public function index(Array $params = [])
    {
        $slug = (isset($params['category_slug'])) ? $params['category_slug'] : '';

        $this->viewData->category = \Model\Category::getItem(null, ['where' => "active='1' and slug = '" . $slug . "'"]);

        $this->viewData->products = \Model\Product_Category::get_category_products($this->viewData->category->id);
        $this->viewData->bottom4Items = array_slice($this->viewData->products, 0, 4);
        $this->viewData->right3Items = array_slice($this->viewData->products, 0, 3);
        $this->viewData->restItems = $this->viewData->products;
        //$this->viewData->products =\Model\Product::getList(['where'=>"active='1' and product_categories = '".$this->viewData->id."'"]);
        $this->loadView($this->viewData);
    }
}