<?php

class productsController extends siteController {


	public function index(Array $params = []){
		$product = \Model\Product::getItem(null, ['where'=>['slug' => $params['slug']]]);
		$this->viewData->product = $product;
		$this->viewData->recommendedItems = \Model\Product::randomizer($product);
		$this->configs['Meta Title'] = $product->meta_title;
		$this->configs['Meta Keywords'] = $product->meta_keywords;
		$this->configs['Meta Description'] = $product->meta_description;
		$this->viewData->rings = \Model\Ring_Material::getList();
		$this->viewData->sizePrice = \Model\Ring_Material::getList();
		$this->loadView($this->viewData);
	}
	
}