<?php

class diamondsController extends siteController
{
    function __construct()
    {
        parent::__construct();
    }

    public function index(Array $params = [])
    {
        //$this->template = false;
        $this->loadView($this->viewData);
    }

    public function search()
    {
        $query = $_POST['query'];
        $searchMeta = $this->getSearchMetadata();

        $fuzzyModel = new \Fuzzy\Fuzzy();
        $data = $fuzzyModel->search($searchMeta, $query, 10);

        $this->toJson($data);
    }

    public function getSearchMetadata()
    {
//        if(isset($_SESSION['searchMetadata']) && $_SESSION['searchMetadata']){
//            return unserialize($_SESSION['searchMetadata']);
//        }

        $metaData = [];

        $metaData = array_merge($metaData, \Model\Product::searchMetaData());
        $metaData = array_merge($metaData, \Model\WeddingBand::searchMetaData());
        $metaData = array_merge($metaData, \Model\Ring::searchMetaData());
        $metaData = array_merge($metaData, \Model\Jewelry::searchMetaData());

//        dd($metaData);
        $_SESSION['searchMetadata'] = serialize($metaData);
        return $metaData;
    }
}