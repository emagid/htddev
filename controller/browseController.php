<?php

class browseController extends siteController
{
    function __construct()
    {
        parent::__construct();
    }

    public function index(Array $params = [])
    {
        $this->viewData->rings = \Model\Ring::getList(['where'=>"active = 1"]);
        $this->loadView($this->viewData);
    }

    public function weddingband(Array $params = []){
        $this->setSEO('weddingbands');
        $this->viewData->wedding_bands = \Model\WeddingBand::getList(['where'=>"active='1'",'orderBy'=>"price DESC"]);
        $this->loadView($this->viewData);
    }

    public function ring(Array $params = []){
        $this->setSEO('engagement-rings');
        if(isset($_SESSION['product'])){
            $product = unserialize($_SESSION['product']);
            $ringsFiltered = \Model\Ring::getList
            (['where'=>"
                min_carat <= $product->length and
                min_carat <= $product->width and
                max_carat >= $product->length and
                max_carat >= $product->width"
            ]);
            $this->viewData->rings = $ringsFiltered;
        } else {
            $this->viewData->rings = \Model\Ring::getList(['where' => "active=1"]);
        }
        $this->loadView($this->viewData);
    }

    public function diamond(Array $params = []){
        $this->setSEO('find-a-diamond');
        if(isset($_SESSION['ring'])){
            $ring = unserialize($_SESSION['ring']);
            $where = "active = 1 and quantity > 0 and length::float > $ring->min_carat and length::float < $ring->max_carat and width::float > $ring->min_carat and width::float < $ring->max_carat";

            $maxPrice = \Model\Product::getItem(null,['where'=>$where,'orderBy'=>"price::float desc", 'limit'=>1]);
            $maxCut = \Model\Product::getItem(null,['where'=>$where,'orderBy'=>"weight::float desc", 'limit'=>1]);
            $products = \Model\Product::getList(['where'=>$where,'orderBy'=>"weight desc"]);

            $this->viewData->products = $products;
            $this->viewData->maxPrice = $maxPrice->price;
            $this->viewData->maxCut = $maxCut->weight;
        } else {
            $sql = 'select cast(price as float) price,id from product where active = 1 ORDER BY price DESC limit 1';
            $this->viewData->maxPrice = \Model\Product::getItem(null, ['sql'=>$sql])->price;
            $sql = 'select cast(weight as float) weight,id from product where active = 1 ORDER BY weight DESC limit 1';
            $this->viewData->maxCut = \Model\Product::getItem(null, ['sql'=>$sql])->weight;
            $orderFilter = [];
            foreach(\Model\Product::getDiamondShapes() as $short=>$diamondShape){
                $orderFilter[] = "shape='".strtoupper($short)."'";
            }
            $orderFilter = implode(',',$orderFilter);
            $this->viewData->products = \Model\Product::getList(['where' => "active=1 AND quantity > 0", 'orderBy'=>"($orderFilter)desc,weight desc"]);
        }
        $this->loadView($this->viewData);
    }

    public function jewelry(){
        //parent slug for category, hardcoded from DB
        $slug = 'jewelry';
        $category = \Model\Category::getItem(null,['where'=>"slug = '{$slug}'"]);
//        $this->viewData->categories = $category->categoryChildren();
        $this->viewData->jewelry = \Model\Jewelry::getList(['where'=>"active = 1", 'orderBy'=>"id asc"]);
        $this->viewData->categories = $category->filteredCategoryChildren(null,'jewelry');
        $this->loadView($this->viewData);
    }

    public function confirm(Array $params = [])
    {
        if(!isset($_SESSION['product']) && isset($_SESSION['ring'])){   //ring is set
            redirect('/browse/diamonds');
        } else if(isset($_SESSION['product']) && !isset($_SESSION['ring'])){
            redirect('/browse/rings');
        }

        $this->viewData->comboProduct = unserialize($_SESSION['product']);
        $this->viewData->comboRing = unserialize($_SESSION['ring']);
        $this->loadView($this->viewData);
    }

    public function add_post(){
        if(isset($_POST['product_type']) && isset($_POST['product_id'])){
            $map = new \Model\Product_Map();
            $map = $map->getProduct($_POST['product_id'], $_POST['product_type']);
            $_SESSION['product'] = serialize($map);
        }
        echo json_encode(['status' => 'success']);
    }

    function setSEO($slug){
        $category = \Model\Category::getItem(null,['where'=>"slug = '{$slug}'"]);
        $this->configs['Meta Title'] = $category->meta_title;
        $this->configs['Meta Keywords'] = $category->meta_keywords;
        $this->configs['Meta Description'] = $category->meta_description;
    }
}