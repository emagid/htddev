<?php

class wholesaleController extends siteController
{

    function __construct()
    {
        parent::__construct();
    }

    public function index(Array $params = [])
    {

        $this->configs['Meta Title'] = "BBy diamond Wholesale";
        $n = new \Notification\ErrorHandler('This account does not exist');
        $_SESSION["notification"] = serialize($n);
        redirect('/home');
    }

    public function index_post(){
        $wholesale = \Model\Wholesale::loadFromPost();
        if($wholesale->save()) {
            $user = \Model\User::loadFromPost();
            $user->password = \Model\User::random_pass();
            $hash = \Emagid\Core\Membership::hash($user->password);
            $user->password = $hash['password'];
            $user->hash = $hash['salt'];
            $user->wholesale_id = $wholesale->id;
            if ($user->save()) {
                $user_role = new \Model\User_Roles();
                $user_role->role_id = 4;
                $user_role->user_id = $user->id;
                $user_role->save();

                $msg = "New user {$user->name} has applied for vendor statuss\n".
                       "Approve or Deny them at ".ADMIN_URL."/users/update/{$user->id}";
//                mail("eitan@emagid.com","New Vendor {$user->name}",$msg);
//                mail("garrett@emagid.com","New Vendor {$user->name}",$msg);
//                $mail = \Emagid\Email::init(["subject"=>"New Vendor {$user->name}","to"=>"eitan@emagid.com","body"=>$msg]);
//                $mail = \Emagid\Email::init(["subject"=>"New Vendor {$user->name}","to"=>"eitan@emagid.com","body"=>$msg]);
//                $mail->send();

                $n = new \Notification\MessageHandler('Welcome to BBY Diamonds! Our representatives will be in touch soon.');
                $_SESSION["notification"] = serialize($n);

//                \Model\User::login($user->email, $_POST['password']);

                $address = \Model\Address::loadFromPost();
                $address->user_id = $user->id;
                $address->save();
                redirect('/home');
            } else {
                $n = new \Notification\ErrorHandler($user->errors);
                $_SESSION["notification"] = serialize($n);
            }
        }

        redirect('/');
    }

}