<?php

class comingsoonController extends siteController
{
    function __construct()
    {
        parent::__construct();
        $this->template = false;
    }

    public function index(Array $params = [])
    {
        //$this->template = false;
        $this->loadView($this->viewData);
    }
}