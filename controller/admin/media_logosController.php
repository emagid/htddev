<?php

class media_logosController extends adminController {
	
	function __construct(){
		parent::__construct("Media_Logo","media_logos");
	}
  	
	function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;    

        parent::index($params);
    }

    function update(Array $params = []){
        
        parent::update($params);
    }

    function update_post(){    

        parent::update_post();
    }
  	
}