<?php

use Emagid\Core\Membership,
Emagid\Html\Form,
Model\Admin,
Model\Banner; 

class dashboardController extends adminController {
	
	function __construct(){
		parent::__construct('Dashboard');
	}

	public function index(Array $params = []){
		$this->_viewData->page_title = 'Dashboard';
		$this->loadView($this->_viewData);
	}

}