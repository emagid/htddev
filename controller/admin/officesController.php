<?php

use Model\User;
use Model\Address;
use Model\Payment_Profile;

class officesController extends adminController {

	function __construct(){
		parent::__construct("Office");


		if ($this->emagid->route['action'] == 'update'){
			$this->_viewData->page_title = 'Manage Practice';
		} else {
			$this->_viewData->page_title = 'Manage Practices';
		}
	}

	function index(Array $params = []){
		$this->_viewData->hasLoadBtn = true;

		parent::index($params);
	}

	function update(Array $params = []){
		curl_setopt($this->_ch, CURLOPT_URL, DENTICON_URL.'v1/api/SetUp/OfficeList/');
		curl_setopt($this->_ch, CURLOPT_HTTPGET, TRUE);
		$result = curl_exec($this->_ch);
		$this->_viewData->ch_result = json_decode($result)->Data;

		$officeID = isset($params['id'])?$params['id']:null;

		foreach($this->_viewData->ch_result as $office){
			if($office->O == $officeID){
				$this->_viewData->_office = $office;
				$this->_viewData->found = 'FOUND';
				break;
			}
		}
		if(isset($this->_viewData->_office)){
			parent::update($params);
		} else {
			redirect(ADMIN_URL.'offices');
		}
	}

	function loadapi(Array $params = []){
		curl_setopt($this->_ch, CURLOPT_URL, DENTICON_URL.'v1/api/SetUp/OfficeList/');
		curl_setopt($this->_ch, CURLOPT_HTTPGET, TRUE);
		$result = curl_exec($this->_ch);
		$this->_viewData->ch_result = json_decode($result)->Data;

		global $emagid;
		$db = $emagid->getDb();
		$offices = [];
		foreach($this->_viewData->ch_result as $_office){

			$offices[] = $_office->O;
			$office = \Model\Office::getItem(intval($_office->O));
			if($office == null){
				$office = new \Model\Office();
				$office->id = intval($_office->O);
				$practice = \Model\Practice::getItem('',['where'=>"original_name = '$_office->OFN'"]);
				if($practice == null){
					$practice = new \Model\Practice();
					$practice->name = $_office->OFN;
					$practice->original_name = $_office->OFN;
					$practice->save();
				}
				$office->practice_id = $practice->id;
			} else {
				$practice = \Model\Practice::getItem($office->practice_id);
				$practice->name = $_office->OFN;
				$practice->save();
			}

			$office->phone = $_office->PH;
			$office->address = $_office->AD;
			$office->address2 = $_office->AD2;
			$office->city = $_office->CT;
			$office->zip = $_office->ZP;
			$office->label = $_office->CT.', '.$_office->ST;
			$office->email = $_office->EM;
			$office->state = $_office->ST;
			//$office->save();
			$sql = "INSERT INTO public.office(id, phone, address, address2, city, zip, label, practice_id, email, state)".
    			"VALUES ({$office->id}, '{$office->phone}', '{$office->address}', '{$office->address2}', '{$office->city}', '{$office->zip}','{$office->label}', {$office->practice_id}, '{$office->email}', '{$office->state}');";
			$db->execute($sql);
		}
		$this->generateSlugs();
		$offices = "(".implode(',',$offices).")";
		$sql = "UPDATE public.office SET active = 0 WHERE id NOT IN $offices";
		$db->execute($sql);
		$sql = "UPDATE public.office SET active = 1 WHERE id IN $offices";
		$db->execute($sql);

		redirect(ADMIN_URL.'offices');
	}

	function update_post() {
		$office = \Model\Office::loadFromPost();
		$office->save();

		$practice = \Model\Practice::getItem($_POST['practice_id']);
		$practice->name = $_POST['practice_name'];
		$practice->save();

		redirect(ADMIN_URL.'offices');
	}

}

































