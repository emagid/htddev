<?php

class home_contentsController extends adminController {
	
	function __construct(){
		parent::__construct("Home_Content");
	}
  	
	function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;    

        parent::index($params);
    }

    function update(Array $params = []){
        
        parent::update($params);
    }

    function update_post(){    

        $arr = [];
        if(isset($_POST['content_title']) && isset($_POST['content_description'])) {
            if(count($_POST['content_title']) == count($_POST['content_description'])){
                asort($_POST['content_order']);
                $order = $_POST['content_order'];
                foreach ($order as $key => $value) {
                    $arr[$_POST['content_title'][$key]] = $_POST['content_description'][$key];
                }
                $_POST['content'] = json_encode($arr);
            } else{
                $n = new \Notification\ErrorHandler("Content header or body missing");
                $_SESSION["notification"] = serialize($n);
                redirect(ADMIN_URL . "home_contents/update/{$_POST['id']}");
            }
        }

    	// $_POST['redirectTo'] = ADMIN_URL.'home';
        parent::update_post();
    }
  	
}