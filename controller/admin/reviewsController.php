<?php

class reviewsController extends adminController {
	
	function __construct(){
		parent::__construct("Review","reviews");
	}
  	
	function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;    

        parent::index($params);
    }

    function update(Array $params = []){
        
        parent::update($params);
    }

    function update_post(){    

        parent::update_post();
    }
  	
}