<?php

class providersController extends adminController {
  
	function __construct(){
		parent::__construct("Provider");
	}

	function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;    

        parent::index($params);
    }
	
	function beforeLoadUpdateView() {
	  	$this->_viewData->services = \Model\Service::getList();

	  	$provider_services = \Model\Provider_Services::getList(['where'=>"provider_id = ".$this->_viewData->provider->id]);
	  	$services = [];

	  	foreach($provider_services as $provider_service){
			$services[] = $provider_service->service_id;	  		
	  	}
	  	$this->_viewData->provider->services = $services;

	  	$this->_viewData->provider->mob = date('m', strtotime($this->_viewData->provider->dob));
	  	$this->_viewData->provider->yob = date('Y', strtotime($this->_viewData->provider->dob));
	  	$this->_viewData->provider->dob = date('d', strtotime($this->_viewData->provider->dob));

	  	$this->_viewData->packages = \Model\Package::getList(['where'=>'provider_id = '.$this->_viewData->provider->id]);
	}
	
	
    function update_post() { 
      	$hash_password = true;
      	$id = filter_input(INPUT_POST,'id',FILTER_VALIDATE_INT);

      	if($id > 0){
	        $provider_exist = \Model\Provider::getItem($id);

	        if($_POST['password'] == ""){
	            $hash_password = false;
	        }
      	}
      
      	$provider = \Model\Provider::loadFromPost();

      	if (isset($provider_exist)){
      		$existFields = ['agreed_manifesto', 'agreed_ethics', 'agreed_terms', 'agreed_privacy', 'key'];
      		foreach ($existFields as $field){
      			$provider->$field = $provider_exist->$field;
      		}
      	}
		
      	if($hash_password) {
	        $hash = \Emagid\Core\Membership::hash($provider->password);
	
	        $provider->password = $hash['password'];
	        $provider->hash = $hash['salt'];
      	} else {
        	$provider->password = $provider_exist->password;
        	$provider->hash = $provider_exist->hash;
      	}

      	if (isset($_POST['dob']) && isset($_POST['mob']) && isset($_POST['yob']) && checkdate($_POST['mob'], $_POST['dob'], $_POST['yob'])){
			$provider->dob = $_POST['yob'].'-'.$_POST['mob'].'-'.$_POST['dob'];
		} else {
			$provider->dob = null;
		}

      	if ($provider->save()){
      		$providerRoles = \Model\Provider_Roles::getList(['where' => 'active = 1 and role_id = 3 and provider_id = '.$provider->id]);
		 	if (count($providerRoles) <= 0){
		 		$providerRole = new \Model\Provider_Roles();
		 		$providerRole->role_id = 3;
		 		$providerRole->provider_id = $provider->id;
		 		$providerRole->save();
		 	}

    		foreach($_FILES as $fileType=>$file){
    			if ($file['error'] == 0){
					$image = new \Emagid\Image();
	    			$image->upload($_FILES[$fileType], UPLOAD_PATH.$this->_content.DS);
	    			$this->afterImageUpload($image);
	    			$provider->$fileType = $image->fileName;
	    			$provider->save();
    			}
    		}

            $this->update_relationships($provider);
            $this->afterObjSave($provider);
            $content = str_replace("\Model\\", "", $this->_model);
            $content = str_replace('_', ' ', $content);
            $n = new \Notification\MessageHandler(ucwords($content).' saved.');
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'providers');
    	} else {
    		$n = new \Notification\ErrorHandler($provider->errors);
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.$this->_content.'/update/'.$provider->id);
    	}
    }

}
