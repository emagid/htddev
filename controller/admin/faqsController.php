<?php

class faqsController extends adminController {
	
	function __construct(){
		parent::__construct("Faq","faqs");
	}
  	
	function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;    

        parent::index($params);
    }

    function update(Array $params = []){
        $pro = new $this->_model(isset($arr['id'])?$arr['id']:null);
        $displays = [1=>1];
        if($pro){
            global $emagid;
            $db = $emagid->getDb();

            $type = $pro->type;
            $sql = "select display_order from faq where display_order is not null order by display_order desc limit 1";
            $res = $db->execute($sql);
            $order = $res ? $res[0]['display_order'] : 0;
            $newDisplays = [];
            for($i = 1; $i <= $order+1; $i++){
                $faq = \Model\Faq::getItem(null,['where'=>"display_order = $i"]);
                if($faq){
                    $newDisplays[$i] = $i;
                } else {
                    $newDisplays[$i] = $i;
                }
            }
        }
        $this->_viewData->displays = isset($newDisplays) && $newDisplays? $newDisplays: $displays;
        parent::update($params);
    }

    function update_post(){    

        parent::update_post();
    }
  	
}