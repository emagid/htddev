<?php

class vendorsController extends adminController {
  
	function __construct(){
		parent::__construct("Vendor");
	}

	function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;

        parent::index($params);
    }
	
	function beforeLoadUpdateView() {
	  	$this->_viewData->services = \Model\Service::getList();

	  	$vendor_services = \Model\Vendor_Services::getList(['where'=>"vendor_id = ".$this->_viewData->vendor->id]);
	  	$services = [];

	  	foreach($vendor_services as $vendor_service){
			$services[] = $vendor_service->service_id;
	  	}
	  	$this->_viewData->vendor->services = $services;

	  	$this->_viewData->vendor->mob = date('m', strtotime($this->_viewData->vendor->dob));
	  	$this->_viewData->vendor->yob = date('Y', strtotime($this->_viewData->vendor->dob));
	  	$this->_viewData->vendor->dob = date('d', strtotime($this->_viewData->vendor->dob));

	  	$this->_viewData->packages = \Model\Package::getList(['where'=>'vendor_id = '.$this->_viewData->vendor->id]);
	}
	
	
    function update_post() { 
      	$hash_password = true;
      	$id = filter_input(INPUT_POST,'id',FILTER_VALIDATE_INT);

      	if($id > 0){
	        $vendor_exist = \Model\Vendor::getItem($id);

	        if($_POST['password'] == ""){
	            $hash_password = false;
	        }
      	}
      
      	$vendor = \Model\Vendor::loadFromPost();

      	if (isset($vendor_exist)){
      		$existFields = ['agreed_manifesto', 'agreed_ethics', 'agreed_terms', 'agreed_privacy', 'key'];
      		foreach ($existFields as $field){
      			$vendor->$field = $vendor_exist->$field;
      		}
      	}
		
      	if($hash_password) {
	        $hash = \Emagid\Core\Membership::hash($vendor->password);
	
	        $vendor->password = $hash['password'];
	        $vendor->hash = $hash['salt'];
      	} else {
        	$vendor->password = $vendor_exist->password;
        	$vendor->hash = $vendor_exist->hash;
      	}

      	if (isset($_POST['dob']) && isset($_POST['mob']) && isset($_POST['yob']) && checkdate($_POST['mob'], $_POST['dob'], $_POST['yob'])){
			$vendor->dob = $_POST['yob'].'-'.$_POST['mob'].'-'.$_POST['dob'];
		} else {
			$vendor->dob = null;
		}

      	if ($vendor->save()){
      		$vendorRoles = \Model\Vendor_Roles::getList(['where' => 'active = 1 and role_id = 3 and vendor_id = '.$vendor->id]);
		 	if (count($vendorRoles) <= 0){
		 		$vendorRole = new \Model\Vendor_Roles();
		 		$vendorRole->role_id = 3;
		 		$vendorRole->vendor_id = $vendor->id;
		 		$vendorRole->save();
		 	}

    		foreach($_FILES as $fileType=>$file){
    			if ($file['error'] == 0){
					$image = new \Emagid\Image();
	    			$image->upload($_FILES[$fileType], UPLOAD_PATH.$this->_content.DS);
	    			$this->afterImageUpload($image);
	    			$vendor->$fileType = $image->fileName;
	    			$vendor->save();
    			}
    		}

            $this->update_relationships($vendor);
            $this->afterObjSave($vendor);
            $content = str_replace("\Model\\", "", $this->_model);
            $content = str_replace('_', ' ', $content);
            $n = new \Notification\MessageHandler(ucwords($content).' saved.');
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'vendors');
    	} else {
    		$n = new \Notification\ErrorHandler($vendor->errors);
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.$this->_content.'/update/'.$vendor->id);
    	}
    }

}
