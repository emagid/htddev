<?php

class contactsController extends adminController {
	
	function __construct(){
		parent::__construct("Contact", "contacts");
	}
	
	function index(Array $params = []){
		$this->_viewData->hasCreateBtn = true;		
		parent::index($params);
	}

	function update(Array $arr = []){
		
		parent::update($arr);
	}

	function update_post(){
		
		parent::update_post();
	}
  
}