<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 11/19/15
 * Time: 5:26 PM
 */

class weddingbandsController extends adminController {

    function __construct(){
        parent::__construct("WeddingBand");
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;
        $params['queryOptions']['page_size'] = 25;
//        $params['queryOptions']['orderBy'] = 'insert_time desc';

        parent::index($params);
    }

    function update(Array $arr = [])
    {
        $wedding = new $this->_model(isset($arr['id'])?$arr['id']:null);
        $this->_viewData->weddingBand = \Model\WeddingBand::getItem($wedding->id, ['where' => ['active' => 1]]);
        $this->_viewData->categories = \Model\Category::getList();
        $this->_viewData->weddingband_category = $this->_viewData->weddingBand->getCategoryIds();

        parent::update($arr);
    }

    public function update_post()
    {
        if(isset($_POST['id']) && $_POST['id'] != ''){
            $weddingband = \Model\WeddingBand::getItem($_POST['id']);
            $_POST['options'] = $weddingband->options;
        }
        if($_POST['metals']){
            $_POST['metals'] = implode(",", $_POST['metals']);
        } else {
            $_POST['metals'] = null;
        }

        $metCol = [];
        foreach(\Model\WeddingBand::getMetals() as $metal){
            $met = str_replace(' ', '_', $metal);
            if(isset($_POST[$met.'_colors'])) {
                foreach (\Model\WeddingBand::getColors() as $color) {
                    in_array($color, $_POST[$met.'_colors']) ? $metCol[$met][$color] = 1 : $metCol[$met][$color] = 0;
                }
            } else {
                foreach(\Model\WeddingBand::getColors() as $color) {
                    $metCol[$met][$color] = 0;
                }
            }
        }
        $_POST['metal_colors'] = json_encode($metCol);
        parent::update_post();
    }

    function upload_images($params)
    {
        header("Content-type:application/json");
        $data = [];
        $data['success'] = false;
        $data['redirect'] = false;
        $id = (isset($params['id']) && is_numeric($params['id']) && $params['id'] > 0) ? $params['id'] : 0;
        if ($wedding = \Model\WeddingBand::getItem($id)) {
            $this->save_upload_images($_FILES['file'], $wedding->id, UPLOAD_PATH . 'products' . DS, []);
            $data['success'] = true;
        } else {
            $data['redirect'] = true;
        }

        //print_r($_FILES);
        echo json_encode($data);
        exit();
    }

    public function save_upload_images($files, $obj_id, $upload_path, $sizes = [])
    {
        $counter = 1;
        $map = new \Model\Product_Map();
        $mapId = $map->getMap($obj_id, "WeddingBand")->id;
        // get highest display order counter if any images exist for specified listing
        $image_high = \Model\Product_Image::getItem(null, ['where' => 'wedding_band_id=' . $obj_id, 'orderBy' => 'display_order', 'sort' => 'DESC']);

        if ($image_high != null) {
            $counter = $image_high->display_order + 1;
        }
        foreach ($files['name'] as $key => $val) {
            $product_image = new \Model\Product_Image();
            $temp_file_name = $files['tmp_name'][$key];
            $file_name = uniqid() . "_" . basename($files['name'][$key]);
            $file_name = str_replace(' ', '_', $file_name);
            $file_name = str_replace('-', '_', $file_name);

            $allow_format = array('jpg', 'png', 'gif', 'JPG');
            $filetype = explode("/", $files['type'][$key]);
            $filetype = strtolower(array_pop($filetype));
            //$filetype = explode(".", $files['name'][$key]); //$file['type'] has value like "image/jpeg"
            //$filetype = array_pop($filetype);
            if ($filetype == 'jpeg' || $filetype == "JPG") {
                $filetype = 'jpg';
            }

            if (in_array($filetype, $allow_format)) {

                $img_path = compress_image($temp_file_name, $upload_path . $file_name, 60, $files['size'][$key]);
                if ($img_path === false) {
                    move_uploaded_file($temp_file_name, $upload_path . $file_name);
                }
                //move_uploaded_file($temp_file_name, $upload_path . $file_name);

                if (count($sizes) > 0) {
                    foreach ($sizes as $key => $val) {
                        if (is_array($val) && count($val) == 2) {
                            resize_image_by_minlen($upload_path,
                                $file_name, min($val), $filetype);

                            $path = images_thumb($file_name, min($val),
                                $val[0], $val[1], file_format($file_name));
                            $image_size_str = implode('_', $val);
                            copy($path, $upload_path . $image_size_str . $file_name);
                        }
                    }
                }

                $product_image->image = $file_name;
                $product_image->product_map_id = $mapId;
                $product_image->display_order = $counter;
                $product_image->save();
            }
            $counter++;
        }
    }

}