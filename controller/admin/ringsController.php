<?php

class RingsController extends adminController{

    public function __construct()
    {
        parent::__construct('Ring');
    }

    public function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;
        $params['queryOptions']['page_size'] = 25;
        parent::index($params);
    }

    public function update(Array $arr = []){
        $pro = new $this->_model(isset($arr['id']) ? $arr['id'] : null);
        $map = new \Model\Product_Map();
        $this->_viewData->map = $map->getProduct($arr['id'], 'ring');
        $this->_viewData->categories = \Model\Category::getList();
        $proCat = new \Model\Product_Category();
        $this->_viewData->product_categories = $proCat->getProductCategories($pro->id, 3);
        parent::update($arr);
    }

    public function update_post()
    {
        $_POST['metals'] = implode(",", $_POST['metals']);
        $_POST['shape'] = implode(",", $_POST['shape']);
        $metCol = [];
        foreach(\Model\Ring::getMetals() as $metal){
            $met = str_replace(' ', '_', $metal);
            if(isset($_POST[$met.'_colors'])) {
                foreach (\Model\Ring::getColors() as $color) {
                    in_array($color, $_POST[$met.'_colors']) ? $metCol[$met][$color] = 1 : $metCol[$met][$color] = 0;
                }
            } else {
                foreach(\Model\Ring::getColors() as $color) {
                    $metCol[$met][$color] = 0;
                }
            }
        }
        $_POST['metal_colors'] = json_encode($metCol);
        $_POST['options'] = \Model\Ring::getItem($_POST['id'])->options;
        parent::update_post();
    }

    function upload_images($params)
    {
        header("Content-type:application/json");
        $data = [];
        $data['success'] = false;
        $data['redirect'] = false;
        $id = (isset($params['id']) && is_numeric($params['id']) && $params['id'] > 0) ? $params['id'] : 0;
        if ((int)$id > 0) {
            $ring = \Model\Ring::getItem($id);
            if ($ring == null) {
                $data['redirect'] = true;
            }
            $this->save_upload_images($_FILES['file'], $ring->id, UPLOAD_PATH . 'products' . DS, []);
            $data['success'] = true;
        }

        //print_r($_FILES);
        echo json_encode($data);
        exit();
    }

    public function save_upload_images($files, $obj_id, $upload_path, $sizes = [])
    {
        //FIND AND DISPLAY ALL EXISTING IMAGES
        $counter = 1;
        $map = new \Model\Product_Map();
        $mapId = $map->getMap($obj_id, "ring")->id;
        // get highest display order counter if any images exist for specified listing
        $image_high = \Model\Product_Image::getItem(null, ['where' => 'product_map_id=' . $mapId, 'orderBy' => 'display_order', 'sort' => 'DESC']);
        if ($image_high != null) {
            $counter = $image_high->display_order + 1;
        }
        //FIND AND DISPLAY ALL EXISTING IMAGES

        //ADD UPLOADED IMAGE
        foreach ($files['name'] as $key => $val) {
            $product_image = new \Model\Product_Image();
            $temp_file_name = $files['tmp_name'][$key];
            $file_name = uniqid() . "_" . basename($files['name'][$key]); //generates unique name
            $file_name = str_replace(' ', '_', $file_name); //replace all spaces with underscore
            $file_name = str_replace('-', '_', $file_name); //replace all dashes with underscore

            $allow_format = array('jpg', 'png', 'gif', 'JPG');
            $filetype = explode("/", $files['type'][$key]);
            $filetype = strtolower(array_pop($filetype)); //remove last element of array
            //$filetype = explode(".", $files['name'][$key]); //$file['type'] has value like "image/jpeg"
            //$filetype = array_pop($filetype);
            if ($filetype == 'jpeg' || $filetype == "JPG") {
                $filetype = 'jpg';
            }

            if (in_array($filetype, $allow_format)) { //if img format is allowed

                $img_path = compress_image($temp_file_name, $upload_path . $file_name, 60, $files['size'][$key]);
                if ($img_path === false) {
                    move_uploaded_file($temp_file_name, $upload_path . $file_name);
                }
                //move_uploaded_file($temp_file_name, $upload_path . $file_name);

                if (count($sizes) > 0) {
                    foreach ($sizes as $key => $val) {
                        if (is_array($val) && count($val) == 2) {
                            resize_image_by_minlen($upload_path,
                                $file_name, min($val), $filetype);

                            $path = images_thumb($file_name, min($val),
                                $val[0], $val[1], file_format($file_name));
                            $image_size_str = implode('_', $val);
                            copy($path, $upload_path . $image_size_str . $file_name);
                        }
                    }
                }

                $product_image->image = $file_name;
                $product_image->product_map_id = $mapId;
                $product_image->display_order = $counter;
                $product_image->save();
            }
            $counter++;
        }
    }
}