<?php

class transactionsController extends adminController {
	
	function __construct(){
		parent::__construct("Transaction","transactions");
	}
  	
	function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;    

        $filter = isset($_GET['status'])?$_GET['status']:0;
        switch ($filter){
            case 1:
                $params['queryOptions']['where'] = "payment_status = 'Paid'";
                break;
            case 2:
                $params['queryOptions']['where'] = "payment_status = 'Un-paid'";
                break;
            case 3:
                $params['queryOptions']['where'] = "payment_status = 'Payment Failed'";
                break;
            default:
                $params['queryOptions']['where'] = "payment_status IN ('Paid','Un-paid')";
                break;
        }

        parent::index($params);
    }

    function update(Array $params = []){
        
        parent::update($params);
    }

    function update_post(){    
        $_POST['type'] = \Model\Transaction::$type[0];
        parent::update_post();
    }

    function afterObjSave($obj){
        $obj->slug = $obj->generateSlug();
        $obj->save();
    }
  	
}