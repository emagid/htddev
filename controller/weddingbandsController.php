<?php
/**
 * Created by PhpStorm.
 * User: Simon
 * Date: 11/25/2015
 * Time: 3:12 PM
 */

class weddingbandsController extends siteController {


    public function index(Array $params = []){
        $wedding_bands = $this->viewData->wedding_bands = \Model\WeddingBand::getItem(null, ['where'=>"slug= '".$params['slug']."'"]);
        $ring = $this->viewData->recommendRing = \Model\Ring::getItem(null,['where'=>"slug = '{$wedding_bands->slug}'"]);
        $this->viewData->recommendedItems = \Model\WeddingBand::randomizer();
        $this->configs['Meta Title'] = $wedding_bands->meta_title;
        $this->configs['Meta Keywords'] = $wedding_bands->meta_keywords;
        $this->configs['Meta Description'] = $wedding_bands->meta_description;
        $weddingBandImage = new \Model\Product_Image();
        $this->viewData->product_image = $weddingBandImage->getProductImage($wedding_bands->id, "weddingBand");
        $this->viewData->image = !empty($this->viewData->product_image) ? UPLOAD_URL."products/".$this->viewData->product_image[0]->image : FRONT_ASSETS."img/ring5.png";
        $this->loadView($this->viewData);
    }

}