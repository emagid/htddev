<?php

class bookController extends siteController
{

    public function index(Array $params = [])
    {
        $this->configs['Meta Title'] = "Book Your Appointment | HENRY The Dentist";
        $this->configs['Meta Description'] = "Book your dental appointment with New Jersey’s state-of-the-art mobile
        dental practice, HENRY the Dentist.";
        $this->configs['Meta Title'] = "book appointment";
        $this->loadView($this->viewData);
    }

}