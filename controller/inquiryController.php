<?php

class inquiryController extends siteController
{

    public function index(Array $params = [])
    {

        $this->configs['Meta Title'] = "Inquiry | Diamond Lab Grown";
        $this->loadView($this->viewData);
    }

    public function index_post()
    {
        $obj = new \Model\Inquiry($_POST);
        if($obj->save()){
            $n = new \Notification\MessageHandler('We will contact you shortly.');
            $_SESSION["notification"] = serialize($n);
        }

        redirect('/');
    }

}