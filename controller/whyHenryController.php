<?php

class whyHenryController extends siteController
{

    public function index(Array $params = [])
    {

        $this->configs['Meta Title'] = "Why HENRY | HENRY The Dentist";
        $this->configs['Meta Description'] = "Why choose HENRY? See the many reasons why HENRY the Dentist is
        the superior dental care choice for your teeth!";
        $this->configs['Meta Keywords'] = "dental care";
        $this->loadView($this->viewData);
    }
}