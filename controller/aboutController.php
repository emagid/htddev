<?php

class aboutController extends siteController
{

    public function index(Array $params = [])
    {
    	$this->viewData->banner = \Model\Banner::getItem(null,['where'=>"page = 'about'"]);
    	$this->viewData->teams = \Model\Team::getList(['where'=>"active=1",'orderBy'=>"id asc"]);
        $this->configs['Meta Title'] = "About Us | HENRY The Dentist";
        $this->configs['Meta Description'] = "Learn about how HENRY the Dentist, a state-of-the-art mobile dental
        practice, began and how they work.";
        $this->configs['Meta Keywords'] = "about us";
        $this->loadView($this->viewData);
    }

}