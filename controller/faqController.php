<?php

class faqController extends siteController
{

    public function index(Array $params = [])
    {
        //should run under /pages/{slug} format
        $slug = 'faq';
        $this->viewData->faq = \Model\Page::getItem(null,['where'=>"slug = '{$slug}'"]);

        $this->viewData->dynamicFAQ = \Model\Faq::getList(["orderBy"=>"display_order asc"]);

        $this->configs['Meta Title'] = "FAQ | HENRY The Dentist";
        $this->configs['Meta Description'] = "See faq that customers like you are asking about HENRY the Dentist, a
        mobile dentist office that services the New Jersey area.";
        $this->configs['Meta Keywords'] = "faq";
        $this->loadView($this->viewData);
    }

}