<?php

class jewelryController extends siteController{

    public function index(Array $params = [])
    {
        $this->viewData->jewelry = $jewelry = \Model\Jewelry::getItem(null,['where'=>"slug = '{$params['slug']}'"]);
        $prodcat = new \Model\Product_Category();
        $category = $prodcat->getProductCategories($jewelry->id,'Jewelry');
        $arr = ['rings'=>false,'earrings'=>false,'necklaces'=>false,'hoops'=>false];
        foreach($category as $item){
            $cat = \Model\Category::getItem($item);
            if(isset($arr[$cat->slug])){
                $arr[$cat->slug] = true;
            }
        }
        $this->viewData->jewelryOptions = $arr;
        $this->viewData->recommendedItems = \Model\Jewelry::randomizer();
        $this->configs['Meta Title'] = $jewelry->meta_title;
        $this->configs['Meta Keywords'] = $jewelry->meta_keywords;
        $this->configs['Meta Description'] = $jewelry->meta_description;
        $this->loadView($this->viewData);
    }
}