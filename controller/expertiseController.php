<?php

class expertiseController extends siteController
{

    public function index(Array $params = [])
    {

        $this->configs['Meta Title'] = "Diamonds Expertise";
        $this->loadView($this->viewData);
    }

    public function the4cs(Array $params = [])
    {

        $this->configs['Meta Title'] = "Beyond the 4Cs | Diamond Lab Grown";
        $this->loadView($this->viewData);
    }
    
    public function fingersizeguide(Array $params = [])
    {

        $this->configs['Meta Title'] = "Beyond the 4Cs | Diamond Lab Grown";
        $this->loadView($this->viewData);
    }

}