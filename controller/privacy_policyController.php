<?php

class privacy_policyController extends siteController
{

    public function index(Array $params = [])
    {
        
        $this->viewData->policy = \Model\Privacy_Policy::getItem(null,['where'=>"active = 1"]);

        $this->configs['Meta Title'] = "Privacy Policy";
        $this->loadView($this->viewData);
    }

}