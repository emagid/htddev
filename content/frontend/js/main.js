$(document).ready(function() {





    $(".show_booking_overlay").click(function(){
        show_booking_overlay();
    });



    $(document).on('click',"body.no_scroll #modal_overlay_booking",function(event){
        hide_booking_overlay();
    });


    $(document).on('click',"body.no_scroll #modal_overlay_booking .centered_wrapper",function(event){
        event.stopImmediatePropagation();
        event.preventDefault();
    });

    $(".hide_booking_overlay, .mobile_hide").click(function(){
        $('.iframe').slideUp();
        hide_booking_overlay();
    });


    function show_booking_overlay(){
        //hide_booking_overlay();
        var $modal_to_open = $("#modal_overlay_booking");
        $("body").addClass("no_scroll");
        $modal_to_open.show().delay(50).queue(function(nextZ){
            $modal_to_open.addClass("active");
            $(".blur_on_overlay").each(function(){
                $(this).addClass("blur_on_overlay_active")
            });
            nextZ();
        });        
    }


    function hide_booking_overlay(){
        var $modal_to_close = $("#modal_overlay_booking");
        $("body").removeClass("no_scroll");
        $(".blur_on_overlay").each(function(){
            $(this).removeClass("blur_on_overlay_active");
        });
        $modal_to_close.removeClass("active").delay(410).queue(function(nextJK){
            $modal_to_close.hide();
            nextJK();
        });
        $('#forget-password-email').hide();
    }
    
    
    
    
    $(".show_register_overlay").click(function(){
        show_register_overlay();
    });



    $(document).on('click',"body.no_scroll #modal_overlay_register",function(event){
        hide_register_overlay();
    });


    $(document).on('click',"body.no_scroll #modal_overlay_register .centered_wrapper",function(event){
        event.stopImmediatePropagation();
        event.preventDefault();
    });

    $(".hide_register_overlay").click(function(){
        hide_register_overlay();
        $('.iframe').slideUp();
    });


    function show_register_overlay(){
        //hide_register_overlay();
        var $modal_to_open = $("#modal_overlay_register");
        $("body").addClass("no_scroll");
        $modal_to_open.show().delay(50).queue(function(nextZ){
            $modal_to_open.addClass("active");
            $(".blur_on_overlay").each(function(){
                $(this).addClass("blur_on_overlay_active")
            });
            nextZ();
        });        
    }


    function hide_register_overlay(){
        var $modal_to_close = $("#modal_overlay_register");
        $("body").removeClass("no_scroll");
        $(".blur_on_overlay").each(function(){
            $(this).removeClass("blur_on_overlay_active");
        });
        $modal_to_close.removeClass("active").delay(410).queue(function(nextJK){
            $modal_to_close.hide();
            nextJK();
        });
        $('#forget-password-email').hide();

        $('.mobile_frame_background, .mobile_hide').fadeOut();
        $('#book2').slideUp();
        $('#primary_page_controller_wrapper').fadeIn();
    }

});


